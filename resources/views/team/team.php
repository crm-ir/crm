<?php 
  $path_init = $_SERVER['DOCUMENT_ROOT']."/crm/resources/init.php";
include_once($path_init);

 ?>
 <link href="<?php echo ROOT_URI; ?>/resources/lib/select2/select2.min.css" rel="stylesheet" />
  <script src="<?php echo ROOT_URI; ?>/resources/lib/time_ago/tinyAgo.min.js"></script>
  <link href="<?php echo ROOT_URI; ?>/resources/lib/timepicker/mdtimepicker.css" rel="stylesheet" type="text/css" />

   <link href="<?php echo ROOT_URI; ?>/assets/css/team.css" rel="stylesheet" type="text/css">
  <script src="<?php echo ROOT_URI; ?>/resources/lib/select2/select2.min.js"></script>
 <script type="text/javascript">
var user_role1 = sessionStorage.getItem("role");
 
if(user_role1==1){
   
  window.location.href =  location.protocol + '//' + location.host + location.pathname+"?panel=10";
}
</script>
<!-- right side main body starts -->
<div class="col-md-10 col-lg-10 col-sm-10 col-xs-10 col-lg-offset-2 col-md-offset-2 col-sm-offset-2 col-xs-offset-2 no-lr-pad back-color-grey">
   <!-- for upper part -->
    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 padding-top-15 border-bottom padding-left-40 back-color-white">
        <p class="font-size-24 float-inline"><b>My Team</b></p>
        <div class="margin-top-6">
          <a data-toggle="modal" id="add_member_top_btn" data-target="#add_member_modal" class="a-deco color-brand margin-left-16"><img class="margin-right-8" src="<?php echo ROOT_URI; ?>/assets/img/assets/create-new-task.svg">Add New Member</a>
        </div>
    </div>
   <!-- upper part ends -->
   <!-- lower part/main part starts here -->
   <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 margin-top-10 padding-left-40 padding-right-40 ">
        <!-- items tiles starts here  -->
        <div id="append_users"></div>
        <!-- items tiles starts here  -->

   </div>
   <!-- lower part/main part ends here -->
   <!-- modals -->


  <!-- Modal -->
  <div id="add_member_modal" class="modal fade" role="dialog">
    <div class="modal-dialog margin-top-100">

      <!-- Modal content-->
      <div class="modal-content col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
        <div class="modal-header col-md-12 col-lg-12 col-sm-12 col-xs-12 ">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <p class="font-size-20 margin-left-10"><b>Add New Member</b></p>
        </div>
        <div class="modal-body col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
          <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                <!-- <div class="form-group col-md-12 col-lg-12 col-sm-12 col-xs-12" >
                  <span class="font-size-12 color-grey-1" for="username">Username:</span>
                  <input type="text" name="username" class="form-control" id="username">
                </div> -->
                <div class="form-group col-md-6 col-lg-6 col-sm-6 col-xs-6">
                  <span class="font-size-12 color-grey-1"  for="firstName">First Name</span>
                  <input type="text" name="firstName" class="form-control margin-top-5" placeholder="Enter first name of member" id="firstName">
                </div>
                <div class="form-group col-md-6 col-lg-6 col-sm-6 col-xs-6">
                  <span class="font-size-12 color-grey-1"  for="lastName">Last Name</span>
                  <input type="text" name="lastName" class="form-control margin-top-5" placeholder="Enter last name of member" id="lastName">
                </div>
               
                <div class="form-group col-md-6 col-lg-6 col-sm-6 col-xs-6">
                  <span class="font-size-12 color-grey-1"  for="phone">Phone</span>
                  <input type="text" name="phone" class="form-control margin-top-5" id="phone" placeholder="Enter phone number of member">
                </div>
                <div class="form-group col-md-6 col-lg-6 col-sm-6 col-xs-6">
                  <span class="font-size-12 color-grey-1"  for="email">Email</span>
                  <input type="text" name="email" class="form-control margin-top-5" placeholder="Enter email of member" id="email">
                </div>
                <div class="form-group col-md-6 col-lg-6 col-sm-6 col-xs-6">
                  <span class="font-size-12 color-grey-1"  for="password_new">Password</span>
                  <input type="text" name="password_new" class="form-control margin-top-5" id="password_new" placeholder="Enter password">
                </div>
                <div class="form-group col-md-6 col-lg-6 col-sm-6 col-xs-6">
                  <span class="font-size-12 color-grey-1"  for="ordergini_id_val">ENTERO DIRECT ID</span>
                  <select class=" form-control tag-for-cust  js-data-select-ajax width-100" name="tags_for_create[]" id="ordergini_id_val">
                  </select>
                  <!-- <input type="text" name="ordergini_id_val" class="form-control margin-top-5" id="ordergini_id_val" placeholder="Enter ENTERO DIRECT ID"> -->
                </div>
                
                <div class="form-group col-md-12 col-lg-12 col-sm-12 col-xs-12">
                  <input id="hidn_id" type="hidden" value=''>
                   <span class="font-size-12 color-grey-1 "  for="email">Assign Role</span><br>
                 
                   <!--  <label id="user_distributor" class="radio-inline margin-top-5">
                      <input  name="user_role_chk_r" type="radio"   value="2">Distributor Partner
                    </label> -->
                    <label id="user_teamlead" class="radio-inline margin-top-5 padding-left-40">
                      <input  name="user_role_chk_r" type="radio"  value="3">Team Lead
                    </label>
                    <label id="user_agent" class="radio-inline margin-top-5 padding-left-40">
                      <input  name="user_role_chk_r" type="radio" value="4" checked>Agent
                    </label>
                </div>
                <div class="form-group col-md-12 col-lg-12 col-sm-12 col-xs-12 select-ajax-assign" id="dist_form">
                  <p class="font-size-12 color-grey-1 margin-bottom--15"  for="customer_sel">Assign To</p><br>
                    <select class=" form-control tag-for-cust  js-data-select-ajax-assign width-100" name="tags_for_assign[]" id="dist_display">
                    </select>
                </div>
                <div class="form-group col-md-12 col-lg-12 col-sm-12 col-xs-12 select-ajax-assign" id="team_lead_form">
                  <p class="font-size-12 color-grey-1 margin-bottom--15"  for="customer_sel">Assign To</p><br>
                    <select class=" form-control tag-for-cust  js-data-select-ajax-assign width-100" name="tags_for_assign[]" id="agent_display">
                    </select>
                </div>
                <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12" id="user_mem_msg">
                 
                </div>
          </div>
        </div>
        <div class="modal-footer col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
          <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 padding-left-30 padding-right-30">
              <button type="button" class="btn btn-default pull-left add-mem-btn1" data-dismiss="modal">Cancel</button>
              <button type="button" class="btn btn-default pull-right brand-btn add-mem-btn"  id="add_mem_ajax">Add</button>
          </div>
        </div>
      </div>

    </div>
  </div>

    <!-- Modal -->
        <div id="create_task_modal" class="modal fade" role="dialog">
          <div class="modal-dialog margin-top-70">

            <!-- Modal content-->
            <div class="modal-content col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
              <div class="modal-header col-md-12 col-lg-12 col-sm-12 col-xs-12 ">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <p class="font-size-20 margin-left-10"><b>Create New Task</b></p>
              </div>
              <div class="modal-body col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
                <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                      <!-- <div class="form-group col-md-12 col-lg-12 col-sm-12 col-xs-12" >
                        <span class="font-size-12 color-grey-1" for="username">Username:</span>
                        <input type="text" name="username" class="form-control" id="username">
                      </div> -->
                      <div class="form-group col-md-12 col-lg-12 col-sm-12 col-xs-12">
                        <p class="font-size-12 color-grey-1 margin-bottom--15"  for="customer_sel">Customer</p><br>
                          <select class=" form-control tag-for-cust  js-data-select-ajax width-100" name="tags_for_create[]" id="customer_display">
                          </select>
                      </div>
                      <div class="form-group col-md-12 col-lg-12 col-sm-12 col-xs-12">
                        <span class="font-size-12 color-grey-1"  for="lastName">Name</span>
                        <input type="text" name="task_name" class="form-control margin-top-5" placeholder="Enter task name" id="task_name">
                      </div>
                                           
                      <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
                        <!-- <input type="time" name="task_date" class="form-control margin-top-25" placeholder="Select date" id="task_date">        -->
                      <input name="task_date" class="form-control margin-top-25 back-color-white" placeholder="Select time" type="text" id="timepicker"/>
                      </div>
                </div>
              </div>
              <div class="modal-footer col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
                <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 padding-left-30 padding-right-30">
                    <button type="button" class="btn btn-default pull-left add-mem-btn1" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-default pull-right brand-btn add-mem-btn" data-user_id_task_modal="" id="create_task_ajax">Create</button>
                </div>
              </div>
            </div>

          </div>
        </div>

        <!-- create a new case modal  -->
            <div id="create_case_modal" class="modal fade" role="dialog">
              <div class="modal-dialog margin-top-70">

                <!-- Modal content-->
                <div class="modal-content col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
                  <div class="modal-header col-md-12 col-lg-12 col-sm-12 col-xs-12 ">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <p class="font-size-20 margin-left-10"><b>Create New Case</b></p>
                  </div>
                  <div class="modal-body col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                          <!-- <div class="form-group col-md-12 col-lg-12 col-sm-12 col-xs-12" >
                            <span class="font-size-12 color-grey-1" for="username">Username:</span>
                            <input type="text" name="username" class="form-control" id="username">
                          </div> -->
                          <div class="form-group col-md-12 col-lg-12 col-sm-12 col-xs-12">
                            <p class="font-size-12 color-grey-1 margin-bottom--15"  for="customer_sel">Customer</p><br>
                              <select class=" form-control tag-for-cust  js-data-select-ajax width-100" id="assign-agent-drop" name="tags_for_create[]">
                              </select> 
                          </div>
                          <div class="form-group col-md-12 col-lg-12 col-sm-12 col-xs-12">
                            <span class="font-size-12 color-grey-1"  for="case">Case</span>
                            <input type="text" name="case" class="form-control margin-top-5" placeholder="Enter case detail" id="case_name">
                          </div>
                         
                    </div>
                  </div>
                  <div class="modal-footer col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 padding-left-30 padding-right-30">
                        <button type="button" class="btn btn-default pull-left add-mem-btn1" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-default pull-right brand-btn add-mem-btn" data-user_id_case_modal=""  id="create_case_ajax">Create</button>
                    </div>
                  </div>
                </div>

              </div>
           </div>

          <!-- create a new case modal ends here  -->

          <div id="edit_role_modal" class="modal fade" role="dialog">
            <div class="modal-dialog margin-top-70">

              <!-- Modal content-->
              <div class="modal-content col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
                <div class="modal-header col-md-12 col-lg-12 col-sm-12 col-xs-12 ">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <p class="font-size-20 margin-left-10"><b>Change User Role</b></p>
                </div>
                <div class="modal-body col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
                  <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                        
                        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                          <p class="font-size-12 color-grey">Promote Agent to Team Lead</p>   
                        </div>
                       
                  </div>
                </div>
                <div class="modal-footer col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
                  <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 padding-left-30 padding-right-30">
                      <button type="button" class="btn btn-default pull-left add-mem-btn1" data-dismiss="modal">Cancel</button>
                      <button type="button" class="btn btn-default pull-right brand-btn add-mem-btn width-88 confirm_edit_role" id="confirm_edit_role" data-user_id_edit_role="" data-user_role_edit_role="" >Confirm</button>
                  </div>
                </div>
              </div>

            </div>
          </div>


      <!-- modals ends here -->

       <!-- Modal -->
       <div id="reassign-modal" class="modal fade" role="dialog">
         <div class="modal-dialog margin-top-70">
          <!-- Modal content-->
          <div class="modal-content col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
            <div class="modal-header col-md-12 col-lg-12 col-sm-12 col-xs-12 ">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <p class="font-size-20 margin-left-10"><b>Re-assign</b></p>
            </div>
            <div class="modal-body col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
              <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                       
                    <div class="form-group col-md-12 col-lg-12 col-sm-12 col-xs-12 select-ajax-assign">
                      <p class="font-size-12 color-grey-1 margin-bottom--15"  for="customer_sel">Assign To</p><br>
                        <select class=" form-control tag-for-cust  js-data-select-ajax-assign width-100" id="display_team_lead" name="tags_for_assign[]">
                        </select>
                    </div>
                                     
              </div>
            </div>
            <div class="modal-footer col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
              <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 padding-left-30 padding-right-30">
                  <button type="button" class="btn btn-default pull-left add-mem-btn1" data-dismiss="modal">Cancel</button>
                  <button type="button" class="btn btn-default pull-right brand-btn add-mem-btn width-88 confirm_assign_button" data-agent_user_id="" >Confirm</button>
              </div>
            </div>
          </div>

        </div>
      </div>
      <!-- modal1 ends here -->
             <!-- Modal -->
       <div id="pass-ch-modal" class="modal fade" role="dialog">
         <div class="modal-dialog margin-top-70">
          <!-- Modal content-->
          <div class="modal-content col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
            <div class="modal-header col-md-12 col-lg-12 col-sm-12 col-xs-12 ">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <p class="font-size-20 margin-left-10"><b>Change Password</b></p>
            </div>
            <div class="modal-body col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
              <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                       
                    <div class="form-group col-md-12 col-lg-12 col-sm-12 col-xs-12 select-ajax-assign">
                      <p class="font-size-12 color-grey-1 margin-bottom--15">New Password</p><br>
                      <input class="form-control" type="text" id="pass_new_ch" placeholder="enter new password">
                    </div>
                                     
              </div>
            </div>
            <div class="modal-footer col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
              <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 padding-left-30 padding-right-30">
                  <button type="button" class="btn btn-default pull-left add-mem-btn1" data-dismiss="modal">Cancel</button>
                  <button type="button" class="btn btn-default pull-right brand-btn add-mem-btn width-88 confirm_assign_button_pass" data-agent_user_id="" >Confirm</button>
              </div>
            </div>
          </div>

        </div>
      </div>
      <!-- modal1 ends here -->

      <div id="change_ed_id_modal" class="modal fade" role="dialog">
            <div class="modal-dialog margin-top-70">

              <!-- Modal content-->
              <div class="modal-content col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
                <div class="modal-header col-md-12 col-lg-12 col-sm-12 col-xs-12 ">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <p class="font-size-20 margin-left-10"><b>Edit ENTERO DIRECT ID</b></p>
                </div>
                <div class="modal-body col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
                  <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                        
                        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                          <span class="font-size-12 color-grey-1"  for="entero_direct_id">ENTERO DIRECT ID</span>
                          <input type="text" name="entero_direct_id" class="form-control margin-top-5" placeholder="ENTERO DIRECT ID" id="entero_direct_id">
                        </div>
                       
                  </div>
                </div>
                <div class="modal-footer col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
                  <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 padding-left-30 padding-right-30">
                      <button type="button" class="btn btn-default pull-left add-mem-btn1" data-dismiss="modal">Cancel</button>
                      <button type="button" class="btn btn-default pull-right brand-btn add-mem-btn width-88 confirm_edit_chagnge_ed_id" id="confirm_edit_chagnge_ed_id" data-user_id_change_ed="" >Confirm</button>
                  </div>
                </div>
              </div>

            </div>
          </div>



</div>
 <!-- right side main body ends -->
      
 <script src="<?php echo ROOT_URI; ?>/resources/lib/timepicker/mdtimepicker.js"></script>
<script type="text/javascript">

  $(document).ready(function() {
//  var settings = {
//   "async": true,
//   "crossDomain": true,
//   "url": "http://13.127.131.56:8080/api/CRMServices/GetChemistTop20OrderCompanies",
//   "method": "POST",
//   "headers": {
    
//     "Access-Control-Allow-Origin": "*",
//     "Access-Control-Allow-Methods": "GET, POST, PATCH, PUT, DELETE, OPTIONS",
//     "Access-Control-Allow-Headers": "Origin, Content-Type, X-Auth-Token",
//     "authorization": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOjQwLCJyb2xlIjoic3VwZXJhZG1pbiIsImlhdCI6MTU0MzU2NjU3MCwiZXhwIjoxNTQzNjAyNTcwfQ.ZY1Zmdcz0v8xHw6-3oQhiPsdFEGhzP__VqNQKuA0nGw",
//     "cache-control": "no-cache",
//     "postman-token": "797620e5-6f73-01cc-d376-470903c485dd",
//     "content-type": "application/x-www-form-urlencoded"
//   },
//   "data": {
//     "ChemistID": "7573"
//   }
// }
var settings = {
  "async": true,
  "crossDomain": true,
  "url": "http://13.127.131.56:8080/auth/local",
  "method": "POST",
  "headers": {
    "content-type": "application/x-www-form-urlencoded",
    "cache-control": "no-cache",
    "postman-token": "2dd53295-59bb-b3e2-ed33-3be842ccb9a2"
  },
  "data": {
    "email": "Entero_CRM",
    "password": "password"
  }
}

$.ajax(settings).done(function (response) {
  console.log(response);
 
});
$.ajax({
  url: "http://13.127.131.56:8080/api/CRMServices/GetChemistTop20OrderCompanies",
  dataType: 'jsonp',
  method:'POST',
  data: {
    ChemistID: '7573'
  },
  success: function(data, status) {
    return console.log("The returned data", data);
  },
  beforeSend: function(xhr, settings) { xhr.setRequestHeader('Authorization','Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOjQwLCJyb2xlIjoic3VwZXJhZG1pbiIsImlhdCI6MTU0MzU2NjU3MCwiZXhwIjoxNTQzNjAyNTcwfQ.ZY1Zmdcz0v8xHw6-3oQhiPsdFEGhzP__VqNQKuA0nGw'); } 
});

        var session_email=sessionStorage.getItem("email");
        
        // if(sessionStorage.getItem("email")==null){
        //    location.reload();
        // }
        if(sessionStorage.getItem("role")==4){
                 
                $("#add_member_top_btn").hide();
        }
        $("#add_member_modal").on('shown.bs.modal', function (e) {
          $("#hidn_id").val(sessionStorage.getItem("user_id"));
             if(sessionStorage.getItem("role")==3){
                $("#user_teamlead").hide();
                $("#team_lead_form").hide();
                $("#dist_form").hide();
                // $("#user_distributor").hide();
             }
             if(sessionStorage.getItem("role")==2){
                   $("#dist_form").hide();
                // $("#user_distributor").hide();
             }
             if(sessionStorage.getItem("role")==1){
              
               $("#team_lead_form").hide();

                   // $("#dist_form").hide();
                // $("#user_distributor").hide();
             }
        });

        $('#dist_display').change(function() {
          $("#team_lead_form").show();
          $("#hidn_id").val($('#dist_display').val());
        });
        $('input[type=radio][name=user_role_chk_r]').change(function() {
            if (this.value == '2') {
              $("#agent_display").attr("disabled","disabled");
                // alert("2");
            }
            else if (this.value == '3') {
               $("#agent_display").attr("disabled","disabled");
                // alert("3");
            }
            else if (this.value == '4') {
               $("#agent_display").removeAttr("disabled");
                // alert("4");
            }
        });
         

        $('#agent_display').select2({
           
           ajax: {
                url: "<?php echo ROOT_URI; ?>/resources/services/get_team_lead.php",
                dataType: 'json',
                delay: 350,
                data: function (params) {
                    return {
                        q: params.term, // search term
                          page: params.page || 1,
                          user_id:$('#hidn_id').val()
                    };
                },
                processResults: function (data, params) {
                     params.page = params.page || 1;
                    return {
                        results: data.results,
                        pagination: {
                          more: (params.page * 5) < data.total_count
                        }

                    };
                },
                cache: true
            },
            
            tags: true,
            placeholder: "Select team member"
    });
        $('#dist_display').select2({
           ajax: {
                url: "<?php echo ROOT_URI; ?>/resources/services/get_dist_d.php",
                dataType: 'json',
                delay: 350,
                data: function (params) {
                    return {
                        q: params.term, // search term
                          page: params.page || 1,
                           
                    };
                },
                processResults: function (data, params) {
                     params.page = params.page || 1;
                    return {
                        results: data.results,
                        pagination: {
                          more: (params.page * 5) < data.total_count
                        }

                    };
                },
                cache: true
            },
            
            tags: true,
            placeholder: "Select Distributor"
    });
      $(document).on('click','#add_mem_ajax',function(){
          var fname        =$("#firstName").val();
          var lname        =$("#lastName").val();
          var phone        =$("#phone").val();
          var email        =$("#email").val();
          var ordergini_id =$("#ordergini_id_val").val();
          var role_new     =$("input[type=radio][name=user_role_chk_r]:checked").val();
          var password_new =$("#password_new").val();
          var mapping_user_id = '';
          if(sessionStorage.getItem('role')==3){
              mapping_user_id = sessionStorage.getItem('user_id');
          }

          if(sessionStorage.getItem('role')==2){
            if(role_new==3){
              mapping_user_id = sessionStorage.getItem('user_id');
            }
            if(role_new==4){
              mapping_user_id =  $("#agent_display").val();
            }

              
          } 
          if(sessionStorage.getItem('role')==1){
            if(role_new==3){
              mapping_user_id =$("#dist_display").val();
            }
            if(role_new==4){
              mapping_user_id =  $("#agent_display").val();
            }

              
          }   
                  

         // alert(mapping_user_id);return false;
          // return false;
          if(fname=='' || fname==' '){
            toast_it("First name is mandatory");
              return false;
          }
          if(mapping_user_id=='' || mapping_user_id==null){
            toast_it("Please assign one team leader/Distributor");
              return false;
          }
          
          if(phone=='' || phone==' '){
             toast_it("phone number is mandatory");
              return false;
          }
          if(password_new=='' || password_new==' '){
             toast_it("Password is mandatory");
              return false;
          }
          function isEmail(email1) {
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            return regex.test(email1);
          }
          if(email=='' || email==' '){
             
          }
          else{
              if(!isEmail(email)){
                toast_it("Enter a valid email");
                return false;
              } 
          }
          

           
          $.ajax({
                  url: '<?php echo ROOT_URI; ?>/resources/services/add_user_crm.php',
                  method: 'POST',
                  data: "{\n\"firstName\":\""+fname+"\",\n\"lastName\":\""+lname+"\",\n\"email\":\""+email+"\",\n\"password_new\":\""+password_new+"\",\n\"phone\":\""+phone+"\",\n\"role\":\""+role_new+"\",\n\"mapping_user_id\":\""+mapping_user_id+"\",\n\"ordergini_id\":\""+ordergini_id+"\", \n\"action\":\"start_user_add\"\n}",
                  dataType: 'json', 
                }).success(function(resp) {
                       // console.lo
                      if(resp.response_code==400){
                        toast_it("Something went wrong, Try again");
                      }
                      else{
                        if(resp.response_status==1){
                          // $('#add_member_modal').modal('hide');
                          toast_it("User added successfully");

                          $("#firstName").val("");
                          $("#lastName").val("");
                          $("#email").val("");
                          $("#user_mem_msg").html('<p class="success-col">User '+phone+' added successfully with id:  <b>'+resp.user_id+'</b></p>');
                        }
                        else if(resp.response_status==4){
                          toast_it("Failure,User added successfully in keycloak but failed to add in crm database,contact admin.");

                          $("#firstName").val("");
                          $("#lastName").val("");
                          $("#email").val("");
                          $("#user_mem_msg").html('<p class="success-col">User '+phone+' added successfully with id:  <b>'+resp.user_id+'</b> in keycloak but failed to add in crm</p>');
                        }
                        else if(resp.response_status==2){
                            toast_it("User already exists with same phone/email");
                             $("#user_mem_msg").html('<p class="error-col">Phone number '+phone+' already exists with id:  <b>'+resp.user_id+'</b></p>');
                        }
                        else if(resp.response_status==5){
                            toast_it("User already exists in keycloak and  added in crm");
                             $("#user_mem_msg").html('<p class="error-col">Phone number '+phone+' already exists in keycloak and  added in crm with id:  <b>'+resp.user_id+'</b></p>');
                        }
                        else if(resp.response_status==6){
                            toast_it("User already exists in keycloak and failed to add in crm");
                             $("#user_mem_msg").html('<p class="error-col">Phone number '+phone+' already in keycloak and failed to add in crm with id:  <b>'+resp.user_id+'</b></p>');
                        }
                        else if(resp.response_status==3){
                            toast_it("please enter 10 digit phone number");
                        } 
                        
                                        
                      }
            });//ajax 


      });
// alert(user_s_role);

            var user_role_team = sessionStorage.getItem("role");
            var user_id_team   = sessionStorage.getItem("user_id");
            var users_list = ''; 
            var ele = "";
            $.ajax({
                     url  : '<?php echo ROOT_URI; ?>/resources/services/team_api.php',
                     type : 'POST',
                     data : "{\n\"user_id_team\":\""+user_id_team+"\",\n\"user_role_team\":\""+user_role_team+"\",\n\"action\":\"display_users_team\"\n}",
                     processData: false,  // tell jQuery not to process the data
                     contentType: false,  // tell jQuery not to set contentType
                     success : function(resp)
                      {
                        if(resp.response_code == 200){
                          var role_user;
                          users_list = resp.data;
                          users_list.forEach(function(entry){

                            if(entry.role== 3)
                            { 
                              role_user = "Team Lead"
                              clas='label-default';
                            }
                            else if(entry.role== 2)
                            { 
                              role_user = "Distributor";
                              clas="label-default";
                            }
                            else if(entry.role == 4)
                            {
                              role_user = "Agent";
                              clas='label-default';
                            }

                         ele = ele + '<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad main-item-for-tiles box-shadow1 margin-top-20 back-color-white">'+
                              '<div class="col-md-5 col-lg-5 col-sm-5 col-xs-5 no-lr-pad">'+
                                '<div class="col-md-3 col-lg-3 col-sm-3 col-xs-3 no-lr-pad">';
                                if(entry.profile_picture == '' || entry.profile_picture == null){
                        ele = ele +      '<img src="<?php echo S3_BUCKET; ?>icons/dummy.png" class="img-responsive float-inline user-image-big-tile margin-left-20">';
                                        }else{
                        ele = ele +      '<img src="<?php echo S3_BUCKET; ?>'+entry.profile_picture+'" class="img-responsive float-inline user-image-big-tile margin-left-20">';
                                        }
                        ele = ele + '</div>'+
                                '<div class="col-md-9 col-lg-9 col-sm-9 col-xs-9 no-lr-pad">'+
                                  '<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 text-left">'+
                                      '<p class="font-size-16   margin-top-24 margin-right-8  ellipsis" data-toggle="tooltip" title="'+entry.name+'"><b>'+entry.name+'</b><span class="label '+clas+' margin-left-10">'+role_user+'</span></p><p class=" margin-top-25 float-inline">'+
                                        '<b> <span class="color-grey-1 font-size-14 "></span></b></p>'+ 
                                  '</div>'+
                                  '<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 text-left margin-top--30">'+
                                      '<img class="margin-right-8 float-inline" src="<?php echo ROOT_URI; ?>/assets/img/assets/email.svg">'+
                                      '<p class="float-inline margin-top--2 ellipsis" data-toggle="tooltip" title="'+entry.email+'">'+entry.email+'</p>'+
                                  '</div>'+
                                  '<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">'+
                                      '<img class="margin-right-8 float-inline" src="<?php echo ROOT_URI; ?>/assets/img/assets/phone.svg">'+
                                      '<p class="float-inline margin-top--2">'+entry.calling_no+'</p>'+
                                  '</div>'+
                                  '<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">'+
                                      '<p class="float-inline margin-top--2 font-size-12">user_id: '+entry.user_id+'</p>'+
                                  '</div>'+
                                '</div>'+
                                   
                              '</div>'+
                              '<div class="col-md-1 col-lg-1 col-sm-1 col-xs-1 no-lr-pad">'+
                                '<p class="font-size-12 color-grey-1 margin-top-25 text-center">Assigned Customers</p>'+
                                '<p class="font-size-24 margin-top-0 text-center"><b>'+entry.customer[0].customer+'</b></p>'+
                              '</div>'+
                              '<div class="col-md-2 col-lg-2 col-sm-2 col-xs-2 no-lr-pad">'+
                                '<p class="font-size-12 color-grey-1 margin-top-25 text-center">Today’s task</p>'+
                                '<p class="font-size-24 margin-top-25 text-center"><b>'+entry.tasks[0].task+'</span></b></p>'+
                              '</div>'+
                              '<div class="col-md-1 col-lg-1 col-sm-1 col-xs-1 no-lr-pad">'+
                                '<p class="font-size-12 color-grey-1 margin-top-25 text-center">Assigned Cases</p>'+
                                '<p class="font-size-24 margin-top-25 text-center"><b>'+entry.cases[0].cases+'</b></p>'+
                              '</div>'+
                            '<div class="col-md-3 col-lg-3 col-sm-3 col-xs-3 no-lr-pad ">'+
                              '<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6   margin-top-25">'+
                                  '<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6 no-lr-pad ">';
                                    if(entry.status == 1){
                                             ele = ele +   '<p class="color-green margin-top-25 padding-left-10  text-right"><b data-delete_user_status="'+entry.user_id+'">Active</b></p>';

                                          }else{
                                             ele = ele +   '<p class="color-orange margin-top-25 padding-left-10  text-right"><b data-delete_user_status="'+entry.user_id+'">Inactive</b></p>';

                                          }

                     
                  ele = ele + '</div>'+
                                                                  
                              '</div>'+
                              '<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6 padding-right-30  margin-top-40 text-right">'+
                                  '<div class="dropdown">'+
                                  '<button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">Action'+
                                    '<img class="margin-left-10" src="<?php echo ROOT_URI; ?>/assets/img/assets/dropdown-arrow.svg">'+
                                  //<!-- <span class="caret"></span> -->
                                  '</button>'+
                                  '<ul class="dropdown-menu pull-right">';

                                  if(entry.status == 0){
                ele = ele + '<li ><a class="drop-sub-chl activate_user_team" data-user_id="'+entry.user_id+'">Activate User</a></li>';
                                  }
                                  else{
                ele = ele +        '<li ><a class="drop-sub-chl delete_user_team" href="#" data-user_id = "'+entry.user_id+'">Deactivate User</a></li><li ><a class="drop-sub-chl chng_password_btn" href="#" data-user_id = "'+entry.user_id+'">Change Password</a></li>';
                ele = ele +       '<li ><a class="drop-sub-chl assign_task_team" href="#" data-user_id_task="'+entry.user_id+'" data-user_role_task="'+entry.role+'" >Assign Task</a></li>'+
                                    '<li ><a class="drop-sub-chl assign_case_team" href="#" data-user_id_case="'+entry.user_id+'" data-user_role_case="'+entry.role+'">Assign Case</a></li>';
                                    // '<li ><a class="drop-sub-chl" href="#">View Reports</a></li>';
                                  }
                                  if(user_role_team == 2){

                ele = ele + '<li ><a class="drop-sub-chl change_ed_id"  href="#" data-ed_id="'+entry.og_id+'"  data-user_id="'+entry.user_id+'">Edit ENTERO DIRECT ID</a></li>';


                                   }else{

                                   }
                                                 

                 // ele = ele +         '<li ><a class="drop-sub-chl" href="#">Edit Role</a></li>';
                                                             

                  // ele = ele +       '<li ><a class="drop-sub-chl assign_task_team" href="#" data-user_id_task="'+entry.user_id+'" data-user_role_task="'+entry.role+'" >Assign Task</a></li>'+
                  //                   '<li ><a class="drop-sub-chl assign_case_team" href="#" data-user_id_case="'+entry.user_id+'" data-user_role_case="'+entry.role+'">Assign Case</a></li>'+
                  //                   '<li ><a class="drop-sub-chl" href="#">View Reports</a></li>'+
                ele = ele +         '</ul>'+
                                '</div>'+
                                '</div>'+
                                '</div>'+
                               
                            '</div>';

                             if(entry.role == 4){}
                             else{

                ele = ele + '<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 padding-right-0 text-right">'+
                          '<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12   text-center">'+
                            '<a href="#demo_'+entry.user_id+'" class="collapse_user_team " data-toggle="collapse" data-user_id_collapse="'+entry.user_id+'" data-user_role_collapse="'+entry.role+'">View Team</a>'+
                            '</div>'+
                            '<div id="demo_'+entry.user_id+'" class="collapse back-color-grey col-md-11 col-md-offset-1 padding-right-0"></div>'+
                          //'</div>'+
                          '</div>'+
                          '</div>';
                           }

                        $('#append_users').html(ele);
                          $('[data-toggle="tooltip"]').tooltip();
                          });//foreach ends here

                          
                        }
                        else if(resp.response_code == 400){
                          $('#append_users').html("No one assigned yet!");

                        }
                      } // close success.
                    }); // close ajax

                  $(document).on('click','.assign_task_team',function(){

                    var user_id_task   = $(this).attr('data-user_id_task');
                    var user_role_task = $(this).attr('data-user_role_task');
                    $('#create_task_ajax').attr('data-user_id_task_modal',user_id_task);
                    //toast_it(user_id_task);
                    $('#create_task_modal').modal('show');

                  });

                  $(document).on('click','.assign_case_team',function(){

                    var user_id_case   = $(this).attr('data-user_id_case');
                    var user_role_case = $(this).attr('data-user_role_case');
                    $('#create_case_ajax').attr('data-user_id_case_modal',user_id_case);

                    $('#create_case_modal').modal('show');   

                  });

                  $('#create_case_modal').on('hidden.bs.modal', function (e) { $(this) .find("input,textarea,select") .val('') .end() .find("input[type=checkbox], input[type=radio]") .prop("checked", "") .end();
                   })
                  $(".modal").on("hidden.bs.modal", function(){
                    $(this).removeData();
                  });

                  $('#create_task_modal').on('hidden.bs.modal', function (e) { $(this) .find("input,textarea,select") .val('') .end() .find("input[type=checkbox], input[type=radio]") .prop("checked", "") .end();
                   })
                  $(".modal").on("hidden.bs.modal", function(){
                    $(this).removeData();
                  });


                  $(document).on('click','.collapse_user_team',function(){


                    var user_id         = $(this).attr('data-user_id_collapse');
                    var user_role       = $(this).attr('data-user_role_collapse');
                    var id_append_users = $(this).parent().siblings().attr('id');


                    //toast_it(id_append_users);
                    get_users_team(user_id,user_role,id_append_users);

                  });

                  function get_users_team(user_id_team,user_role_team,id_append_users){
                    // alert(id_append_users);
                    var users_list = ''; 
                    var loggedin_user_role = sessionStorage.getItem("role");

                    var ele = "";
                    $.ajax({
                             url  : '<?php echo ROOT_URI; ?>/resources/services/team_api.php',
                             type : 'POST',
                             data : "{\n\"user_id_team\":\""+user_id_team+"\",\n\"user_role_team\":\""+user_role_team+"\",\n\"action\":\"display_users_team\"\n}",
                             processData: false,  // tell jQuery not to process the data
                             contentType: false,  // tell jQuery not to set contentType
                             success : function(resp)
                              {
                                if(resp.response_code == 200){
                                 // console.log(resp);
                                  var role_user;
                                  users_list = resp.data;
                                  users_list.forEach(function(entry){

                                    if(entry.role== 3)
                                    { 
                                      role_user = "Team Lead"
                                      clas='label-default';
                                    }
                                    else if(entry.role== 2)
                                    { 
                                      role_user = "Distributor";
                                      clas="label-";
                                    }
                                    else if(entry.role == 4)
                                    {
                                      role_user = "Agent";
                                      clas='label-default';
                                    }

                        ele = ele + '<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 margin-bottom-10 no-lr-pad main-item-for-tiles box-shadow1 margin-top-10 back-color-white">'+
                                      '<div class="col-md-4 col-lg-4 col-sm-4 col-xs-4 no-lr-pad">'+
                                        '<div class="col-md-3 col-lg-3 col-sm-3 col-xs-3 no-lr-pad">';
                                        if(entry.profile_picture == '' || entry.profile_picture == null){
                        ele = ele +      '<img src="<?php echo S3_BUCKET; ?>icons/dummy.png" class="img-responsive float-inline user-image-big-tile-1 margin-left-20">';
                                        }else{
                        ele = ele +      '<img src="<?php echo S3_BUCKET; ?>'+entry.profile_picture+'" class="img-responsive float-inline user-image-big-tile-1 margin-left-20">';
                                        }

                        

                        ele = ele +     '</div>'+
                                        '<div class="col-md-9 col-lg-9 col-sm-9 col-xs-9 no-lr-pad">'+
                                          '<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 text-left">'+
                                              '<p class="font-size-16 margin-top-24 margin-right-8 ellipsis" data-toggle="tooltip" title="'+entry.name+'"><b>'+entry.name+'</b><span class="label '+clas+' margin-left-10">'+role_user+'</span></p><p class=" margin-top-25 float-inline">'+
                                                '<b> <span class="color-grey-1 font-size-14 "></span></b></p>'+ 
                                          '</div>'+
                                          '<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 text-left margin-top--30">'+
                                              '<img class="margin-right-8 float-inline" src="<?php echo ROOT_URI; ?>/assets/img/assets/email.svg">'+
                                              '<p class="margin-top--2 ellipsis" data-toggle="tooltip" title="'+entry.email+'">'+entry.email+'</p>'+
                                          '</div>'+
                                          '<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">'+
                                              '<img class="margin-right-8 float-inline" src="<?php echo ROOT_URI; ?>/assets/img/assets/phone.svg">'+
                                              '<p class="float-inline margin-top--2">'+entry.calling_no+'</p>'+
                                          '</div>'+
                                           '<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">'+
                                      '<p class="float-inline margin-top--2 font-size-12">user_id: '+entry.user_id+'</p>'+
                                  '</div>'+
                                        '</div>'+
                                           
                                      '</div>'+
                                     '<div class="col-md-1 col-lg-1 col-sm-1 col-xs-1 no-lr-pad">'+
                                        '<p class="font-size-12 color-grey-1 margin-top-25 text-center">Assigned Customers</p>'+
                                        '<p class="font-size-24 margin-top-0 text-center"><b>'+entry.customer[0].customer+'</b></p>'+
                                      '</div>'+
                                      '<div class="col-md-2 col-lg-2 col-sm-2 col-xs-2 no-lr-pad">'+
                                        '<p class="font-size-12 color-grey-1 margin-top-25 text-center">Today’s task</p>'+
                                        '<p class="font-size-24 margin-top-25 text-center"><b>'+entry.tasks[0].task+'</span></b></p>'+
                                      '</div>'+
                                      '<div class="col-md-2 col-lg-2 col-sm-2 col-xs-2 no-lr-pad">'+
                                        '<p class="font-size-12 color-grey-1 margin-top-25 text-center">Assigned Cases</p>'+
                                        '<p class="font-size-24 margin-top-25 text-center"><b>'+entry.cases[0].cases+'</b></p>'+
                                      '</div>'+
                                    '<div class="col-md-3 col-lg-3 col-sm-3 col-xs-3 no-lr-pad ">'+
                                      '<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6   margin-top-25">'+
                                          '<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6 no-lr-pad ">';

                                          if(entry.status == 1){
                                             ele = ele +   '<p class="color-green margin-top-25 padding-left-10  text-right"><b data-delete_user_status="'+entry.user_id+'">Active</b></p>';

                                          }else{
                                             ele = ele +   '<p class="color-orange margin-top-25 padding-left-10  text-right"><b data-delete_user_status="'+entry.user_id+'">Inactive</b></p>';

                                          }

                     
                          ele = ele + '</div>'+
                                                                          
                                      '</div>'+
                                      '<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6 padding-right-30  margin-top-40 text-right">'+
                                          '<div class="dropdown">'+
                                          '<button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">Action'+
                                            '<img class="margin-left-10" src="<?php echo ROOT_URI; ?>/assets/img/assets/dropdown-arrow.svg">'+
                                          //<!-- <span class="caret"></span> -->
                                          '</button>'+
                                          '<ul class="dropdown-menu pull-right"><li ><a class="drop-sub-chl chng_password_btn" href="#" data-user_id = "'+entry.user_id+'">Change Password</a></li>';

                                            if(entry.status == 0){
                          ele = ele + '<li ><a class="drop-sub-chl activate_user_team" data-user_id="'+entry.user_id+'">Activate User</a></li>';

                                            }
                                            else{
                          ele = ele +         '<li ><a class="drop-sub-chl delete_user_team" href="#" data-user_id = "'+entry.user_id+'">Deactivate User</a></li>';
                                            }
                                            if(entry.role == 4 && sessionStorage.getItem("role") == 2){ 

                          ele = ele +         '<li ><a class="drop-sub-chl edit_role_team" href="#" data-user_id_edit_role="'+entry.user_id+'" data-user_role_edit_role="'+entry.role+'">Edit Role</a></li>';
                          ele = ele + '<li ><a class="drop-sub-chl edit_team_leader" href="#" data-user_id = "'+entry.user_id+'">Edit Team Leader</a></li>';

                                             }else{

                                             }


                          ele = ele +       '<li ><a class="drop-sub-chl assign_task_team" href="#" data-user_id_task="'+entry.user_id+'" data-user_role_task="'+entry.role+'" >Assign Task</a></li>'+
                                            '<li ><a class="drop-sub-chl assign_case_team" href="#" data-user_id_case="'+entry.user_id+'" data-user_role_case="'+entry.role+'">Assign Case</a></li>';
                                            if(loggedin_user_role == 2){

                          ele = ele + '<li ><a class="drop-sub-chl change_ed_id"  href="#" data-ed_id="'+entry.og_id+'"  data-user_id="'+entry.user_id+'">Edit ENTERO DIRECT ID</a></li>';

                                              }else{

                                              }
                                            // '<li ><a class="drop-sub-chl" href="#">View Reports</a></li>'+
                          ele = ele +     '</ul>'+
                                        '</div>'+
                                      '</div>'+
                                      '</div>'+
                                    '</div>';



                                     if(entry.role == 4){}
                                     else{

                      ele = ele + '<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 padding-right-0   text-right">'+
                                  '<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12  text-center">'+
                                    '<a href="#demo_'+entry.user_id+'" class="collapse_user_team" data-toggle="collapse" data-user_id_collapse="'+entry.user_id+'" data-user_role_collapse="'+entry.role+'">View Team</a>'+
                                   
                                    
                                  '</div>'+
                                  '<div id="demo_'+entry.user_id+'" class="collapse col-md-11 col-lg-11 col-sm-11 col-xs-11 col-md-offset-1 padding-right-0 back-color-grey "></div>'+
                                  '</div>'+
                                  
                                  
                                   
                                  '</div>';
                                    }  // else close

                                    $('#'+id_append_users).html(ele);
                                    $('[data-toggle="tooltip"]').tooltip();
                                  });  // close foreach

                                  
                                } // close sucess 
                                else if(resp.response_code == 400){

                                    $('#'+id_append_users).html('<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 padding-right-30 text-right"><p class="font-size-12 color-grey-1 margin-top-25 text-center">No one assigned yet!</p></div>');

                                }
                              } // close success.
                            }); // close ajax

                  } // close function 

                  $(document).on('click', '#create_task_ajax', function () {
                          var user_id   =$('#customer_display').val();
                          var time      =$('#timepicker').val();
                          var task_name =$('#task_name').val();
                          var emp_id    = $(this).attr('data-user_id_task_modal');
                          
                          var hours   = Number(time.match(/^(\d+)/)[1]);
                          var minutes = Number(time.match(/:(\d+)/)[1]);
                          var AMPM    = time.match(/\s(.*)$/)[1];
                          if(AMPM == "PM" && hours<12) hours = hours+12;
                          if(AMPM == "AM" && hours==12) hours = hours-12;
                          var sHours   = hours.toString();
                          var sMinutes = minutes.toString();
                          if(hours<10) sHours = "0" + sHours;
                          if(minutes<10) sMinutes = "0" + sMinutes;
                          var time=sHours + ":" + sMinutes+":"+"00";
                          
                          $.ajax({
                                dataType :'json',
                                         method : 'POST',
                                         async:false,
                                    url : "<?php echo ROOT_URI; ?>/resources/services/task_main.php",
                                    data: "{\n\"action\":\"add_task\",\n\"taskname\":\""+task_name+"\",\n\"emp_id\":\""+emp_id+"\",\n\"cust_id\":\""+user_id+"\",\n\"current_cust_id\":\""+sessionStorage.getItem('user_id')+"\",\n\"task_when\":\""+time+"\"}",}).success(function(resp) {
                                      toast_it(resp.response_message);
                                        }); 
                                    $('#create_task_modal').modal('toggle');
                                    setInterval(function(){ location.reload();}, 1000);

                  });

                  $('#timepicker').mdtimepicker();
                  $('#timepicker1').mdtimepicker();
                  $('#timepicker2').mdtimepicker();


                  $(document).on('click', '#create_case_ajax', function () {
                          var user_id   = $('#assign-agent-drop').val();
                          var case_name = $('#case_name').val();
                          var emp_id    = $(this).attr('data-user_id_case_modal');

                          $.ajax({
                                dataType :'json',
                                         method : 'POST',
                                         async:false,
                                    url : "<?php echo ROOT_URI; ?>/resources/services/case_main.php",
                                    data: "{\n\"action\":\"add_case\",\n\"casename\":\""+case_name+"\",\n\"empid\":\""+emp_id+"\",\n\"user\":\""+user_id+"\",\n\"current_empid\":\""+sessionStorage.getItem('user_id')+"\"}",}).success(function(resp) {
                                      toast_it(resp.response_message);
                                        }); 
                                    $('#create_case_modal').modal('toggle');
                                    setInterval(function(){ location.reload();}, 1000);

                  });

                  $('#assign-agent-drop').select2({
                      ajax: {
                            url: "<?php echo ROOT_URI; ?>/resources/services/tag_api.php",
                            dataType: 'json',
                            delay: 350,
                            data: function (params) {
                                return {
                                    q: params.term, // search term
                                      page: params.page || 1,
                                      token: sessionStorage.getItem("token"),
                                      dist_id:sessionStorage.getItem('dist_id')

                                };
                            },
                            processResults: function (data, params) {
                                 params.page = params.page || 1;
                                return {
                                    results: data.results,
                                    pagination: {
                                      more: (params.page * 10) < data.total_count
                                    }

                                };
                            },
                            cache: true
                        },
                        
                        tags: true,
                        placeholder: "Select Customer"
                  });

                  $('#customer_display').select2({
                         ajax: {
                              url: "<?php echo ROOT_URI; ?>/resources/services/tag_api.php",
                              dataType: 'json',
                              delay: 350,
                              data: function (params) {
                                  return {
                                      q: params.term, // search term
                                        page: params.page || 1,
                                        token: sessionStorage.getItem("token"),
                                        dist_id:sessionStorage.getItem('dist_id')
                                  };
                              },
                              processResults: function (data, params) {
                                   params.page = params.page || 1;
                                  return {
                                      results: data.results,
                                      pagination: {
                                        more: (params.page * 5) < data.total_count
                                      }

                                  };
                              },
                              cache: true
                          },
                          
                          tags: true,
                          placeholder: "Select Customer"
                  });

                   $('#ordergini_id_val').select2({
                         ajax: {
                              url: "<?php echo ROOT_URI; ?>/resources/services/salesman_api.php",
                              dataType: 'json',
                              delay: 350,
                              data: function (params) {
                                  return {
                                      q: params.term, // search term
                                        page: params.page || 1,
                                        token: sessionStorage.getItem("token"),
                                        dist_id:sessionStorage.getItem('dist_id')
                                  };
                              },
                              processResults: function (data, params) {
                                   params.page = params.page || 1;
                                  return {
                                      results: data.results,
                                      pagination: {
                                        more: (params.page * 5) < data.total_count
                                      }

                                  };
                              },
                              cache: true
                          },
                          
                          tags: true,
                          placeholder: "Select Customer"
                  });
                  
                  $(document).on('click','.chng_password_btn',function(){
                    var user_id_pass = $(this).attr('data-user_id');
                    $('.confirm_assign_button_pass').attr('data-agent_user_id',user_id_pass);
                    $('#pass-ch-modal').modal('show');  
                  });

                   $(document).on('click','.confirm_assign_button_pass',function(){
                      var pass=$("#pass_new_ch").val();
                       
                    var user_id_pass_ch = $(this).attr('data-agent_user_id');
                     
                      if(pass=='' || pass==' '){
                         toast_it("Please enter password");
                          return false;
                      }
                       
                      $.ajax({
                              url: '<?php echo ROOT_URI; ?>/resources/services/reset_password_id.php',
                              method: 'POST',
                              data: "{\n\"user_id\":\""+user_id_pass_ch+"\",\n\"password\":\""+pass+"\", \n\"action\":\"reset_password_id\"\n}",
                              dataType: 'json', 
                            }).success(function(resp) {
                                   // console.lo
                                  if(resp.response_code==400){
                                    toast_it("Something went wrong, Try again");
                                  }
                                  else{
                                    if(resp.response_status==1){
                                      $('#pass-ch-modal').modal('hide');
                                      
                                       toast_it("Password changed successfully");
                                      
                                    }
                                    else{

                                       toast_it("Something went wrong");

                                    }                        
                                                    
                                  }
                        });//ajax 

                });
                  $(document).on('click','.edit_team_leader',function(){
                    var user_id_agent = $(this).attr('data-user_id');
                    $('.confirm_assign_button').attr('data-agent_user_id',user_id_agent);
                    $('#reassign-modal').modal('show');  
                  });

                  $(document).on('click','.confirm_assign_button',function(){

                    var user_id_team_lead = $('#display_team_lead').val();
                    var agent_user_id = $(this).attr('data-agent_user_id');
                    var user_id_agents = [];
                    user_id_agents[0] = agent_user_id;
                    console.log(user_id_agents);


                    $.ajax({
                       url : "<?php echo ROOT_URI; ?>/resources/services/team_api.php",
                       type : 'POST',
                       data : "{\n\"user_id_agents\":"+JSON.stringify(user_id_agents)+",\n\"user_id_team_lead\":\""+user_id_team_lead+"\",\n\"action\":\"reassign_multiple_agent\"\n}",

                       processData: false,  // tell jQuery not to process the data
                       contentType: false,  // tell jQuery not to set contentType 
                       dataType: 'JSON',
                             success: function(response) {

                              if(response.response_code == 200){
                                toast_it("Team Lead Changed");
                                  setInterval(function(){ location.reload();}, 1200);
                              }
                              
                             }
                             // error: function() {  
                             //   toast_it("There was an error.");
                             // }
                       }); //ajax close
                    // toast_it(agent_user_id);



                  });

                  $(document).on('click','.change_ed_id',function(){

                    $('#change_ed_id_modal').modal('show');
                    var user_id = $(this).attr('data-user_id');
                    var ed_id = $(this).attr('data-ed_id');
                    $('#entero_direct_id').val(ed_id);
                    $('#confirm_edit_chagnge_ed_id').attr('data-user_id_change_ed',user_id);

                  });

                  $(document).on('click','#confirm_edit_chagnge_ed_id',function(){

                      var user_id = $(this).attr('data-user_id_change_ed');

                      var ed_id = $('#entero_direct_id').val();

                      // toast_it(ed_id);

                      $.ajax({
                       url : "<?php echo ROOT_URI; ?>/resources/services/team_api.php",
                       type : 'POST',
                       data : "{\n\"user_id\":\""+user_id+"\",\n\"ed_id\":\""+ed_id+"\",\n\"action\":\"edit_ed_id\"\n}",

                       processData: false,  // tell jQuery not to process the data
                       contentType: false,  // tell jQuery not to set contentType 
                       dataType: 'JSON',
                             success: function(response) {

                              if(response.response_code == 200){
                                toast_it("ENTERO DIRECT ID Changed");
                                  setInterval(function(){ location.reload();}, 1200);
                              }
                              
                             }
                             // error: function() {  
                             //   toast_it("There was an error.");
                             // }
                       }); //ajax close
                    // toast_it(agent_user_id);




                  });


                  $('#display_team_lead').select2({
           
                         ajax: {
                              url: "<?php echo ROOT_URI; ?>/resources/services/get_team_lead.php",
                              dataType: 'json',
                              delay: 350,
                              data: function (params) {
                                  return {
                                      q: params.term, // search term
                                        page: params.page || 1,
                                        user_id: sessionStorage.getItem('user_id')
                                  };
                              },
                              processResults: function (data, params) {
                                   params.page = params.page || 1;
                                  return {
                                      results: data.results,
                                      pagination: {
                                        more: (params.page * 5) < data.total_count
                                      }

                                  };
                              },
                              cache: true
                          },
                          
                          tags: true,
                          placeholder: "Select team member"
                  });


                  $(document).on('click','.delete_user_team',function(){

                    var user_id = $(this).attr('data-user_id');
      //              toast_it(user_id);

                    $.ajax({
                       url : "<?php echo ROOT_URI; ?>/resources/services/delete_user.php",
                       type : 'POST',
                       data : "{\n\t\"user_id\":\""+user_id+"\"\n}",

                       processData: false,  // tell jQuery not to process the data
                       contentType: false,  // tell jQuery not to set contentType 
                       dataType: 'JSON',
                             success: function(response) {

                              if(response.response_code == 200){
                                toast_it("User deleted successfully");
                                  setInterval(function(){ location.reload();}, 1200);
                              }
                              
                             }
                             // error: function() {  
                             //   toast_it("There was an error.");
                             // }
                       }); //ajax close 
                  });  // close on click delete user

                  $(document).on('click','.activate_user_team',function(){

                    var user_id = $(this).attr('data-user_id');
      //              toast_it(user_id);

                    $.ajax({
                       url : "<?php echo ROOT_URI; ?>/resources/services/activate_user.php",
                       type : 'POST',
                       data : "{\n\t\"user_id\":\""+user_id+"\"\n}",

                       processData: false,  // tell jQuery not to process the data
                       contentType: false,  // tell jQuery not to set contentType 
                       dataType: 'JSON',
                             success: function(response) {

                              if(response.response_code == 200){
                                toast_it("User Activated successfully");
                                  setInterval(function(){ location.reload();}, 1200);
                              }
                              
                             }
                             // error: function() {  
                             //   toast_it("There was an error.");
                             // }
                       }); //ajax close 
                  });  // close on click delete user

                  $(document).on('click','.edit_role_team',function(){

                    $('#edit_role_modal').modal('show');
                    
                   var user_id   = $(this).attr('data-user_id_edit_role');
                   var user_role = $(this).attr('data-user_role_edit_role');

                   $('#confirm_edit_role').attr('data-user_id_edit_role',user_id);
                   $('#confirm_edit_role').attr('data-user_role_edit_role',user_role); 

                  }); // close on click edit role.

                  $(document).on('click','.confirm_edit_role',function(){

                   var user_id   = $(this).attr('data-user_id_edit_role');
                   var user_role = $(this).attr('data-user_role_edit_role');
                   //toast_it(user_id);
                   //return false;
                    $.ajax({
                       url : "<?php echo ROOT_URI; ?>/resources/services/team_api.php",
                       type : 'POST',
                       data : "{\n\"user_id\":\""+user_id+"\",\n\"user_role\":\""+user_role+"\",\n\"user_id_dist\":\""+sessionStorage.getItem('user_id')+"\",\n\"action\":\"change_role_team\"\n}",

                       processData: false,  // tell jQuery not to process the data
                       contentType: false,  // tell jQuery not to set contentType 
                       dataType: 'JSON',
                             success: function(response) {

                              if(response.response_code == 200){
                                toast_it("User Role Changed");
                                  setInterval(function(){ location.reload();}, 1200);
                              }
                              
                             }
                             // error: function() {  
                             //   toast_it("There was an error.");
                             // }
                       }); //ajax close



                  });  // close on click confirm edit role.





  });
 
</script>
 

 