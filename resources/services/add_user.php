<?php
#################################################################################################
#################################################################################################
###################################### Do not change anything in this file ######################
#################################################################################################
#################################################################################################
header('Access-Control-Allow-Origin: *');
header('Content-type: application/json');

$data = json_decode(file_get_contents('php://input'));
$return_id='';
if(strlen($data->phone)==10){

    // print_r($data->firstName);die;
    $curl = curl_init();
    $result=0;
    curl_setopt_array($curl, array(
      CURLOPT_URL => "http://users.enteroteam.com/auth/realms/master/protocol/openid-connect/token",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => "grant_type=client_credentials",
      //do not change authorization field until unless client secret is changed
      CURLOPT_HTTPHEADER => array(
        "authorization: Basic YWRtaW4tY2xpOjY2ZTYzMWMzLTJmNDctNDVjNS04MDU0LWQxM2JjY2Y2ZDZiNg==",
        "cache-control: no-cache",
        "content-type: application/x-www-form-urlencoded",
        "postman-token: a4ba15c5-1e19-15b0-17e6-4de5e26c0a2d"
      ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
     $result=0;
    } else {
      $temp_acc = json_decode($response);
      // print_r($temp_acc->access_token);

      // success condition

        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "http://users.enteroteam.com/auth/admin/realms/master/users",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => " {\r\n     \"id\": \" ".$data->phone." \",\r\n     \"username\": \"".$data->phone."\",\r\n     \"enabled\": true,\r\n     \"emailVerified\": false,\r\n     \"firstName\": \"".$data->firstName."\",\r\n     \"email\": \"".$data->email."\",\r\n     \"lastName\": \"".$data->lastName."\",\r\n     \"credentials\": [\r\n         {\r\n             \"type\": \"password\",\r\n             \"value\": \"".$data->password_new."\",\r\n             \"temporary\": false\r\n         }\r\n     ],\r\n     \"realmRoles\": [\"admin\"]\r\n }",
          CURLOPT_HTTPHEADER => array(
            "authorization: Bearer ".$temp_acc->access_token,
            "cache-control: no-cache",
            "content-type: application/json",
            "postman-token: a063a0b2-fae0-5d60-9601-c4e9d0ec4fba"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
      
        curl_close($curl);
        // print_r($response);die;

          $temp_acc1 = json_decode($response);
         
           // print_r($temp_acc1);die;
           
        if ($err) {
            
         
         $result=0;
        } else {

            if(@$temp_acc1->errorMessage){
               $result=2;
                
            }else{
               $result=1;
            }
              ###########################
                  $curl = curl_init();

                  curl_setopt_array($curl, array(
                    CURLOPT_URL => "http://users.enteroteam.com/auth/admin/realms/master/users?username=".$data->phone,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 30,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "GET",
                    CURLOPT_POSTFIELDS => "grant_type=client_credentials",
                    CURLOPT_HTTPHEADER => array(
                      "authorization: Bearer ".$temp_acc->access_token,
                      "cache-control: no-cache",
                      "content-type: application/x-www-form-urlencoded",
                      "postman-token: f84d1fa7-4fdf-522b-4c3c-5ae1af5bb5cb"
                    ),
                  ));

                  $response_exist_curl = curl_exec($curl);
                  $err = curl_error($curl);

                  curl_close($curl);

                  if ($err) {
                    
                  } else {
                    $exist_curl_code = json_decode($response_exist_curl);
                    if($exist_curl_code){
                      $return_id=$exist_curl_code[0]->id;
                    }
                    else{
                      ###########################
                        ########check user email for already exists########
                        $curl = curl_init();

                        curl_setopt_array($curl, array(
                          CURLOPT_URL => "http://users.enteroteam.com/auth/admin/realms/master/users?email=".$data->email,
                          CURLOPT_RETURNTRANSFER => true,
                          CURLOPT_ENCODING => "",
                          CURLOPT_MAXREDIRS => 10,
                          CURLOPT_TIMEOUT => 30,
                          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                          CURLOPT_CUSTOMREQUEST => "GET",
                          CURLOPT_POSTFIELDS => "grant_type=client_credentials",
                          CURLOPT_HTTPHEADER => array(
                            "authorization: Bearer ".$temp_acc->access_token,
                            "cache-control: no-cache",
                            "content-type: application/x-www-form-urlencoded",
                            "postman-token: f84d1fa7-4fdf-522b-4c3c-5ae1af5bb5cb"
                          ),
                        ));

                        $response_exist_curl = curl_exec($curl);
                        $err = curl_error($curl);

                        curl_close($curl);

                        if ($err) {
                          
                        } else {
                          $exist_curl_code = json_decode($response_exist_curl);
                          if($exist_curl_code){
                            $return_id=$exist_curl_code[0]->id;
                          }
                        }
                      ###########################
                    }
                    
                  }
                  ############################
         
           // echo '<a href="../main/" class="btn btn-info pull-left margin-top-30 btn-mine">Home</a>';

           // echo "user added successfully<Br>";
                

        }
    }
}
else{
  $result=3;
}
if($result==1){
  $response1['response_message']= "success";
  $response1['response_code'] = 200;
  $response1['user_id'] = $return_id;
  $response1['response_status'] = 1;
}
elseif ($result==2) {
   $response1['response_message']= "User already exists";
  $response1['response_code'] = 200;
  $response1['user_id'] = $return_id;

  $response1['response_status'] = 2;
}
elseif ($result==3) {
   $response1['response_message']= "Enter 10 digit phone number";
  $response1['response_code'] = 200;
  $response1['response_status'] = 3;
}
else{
  $response1['response_message']= "Failure";
  $response1['response_code'] = 400;
  $response1['response_status'] = 0;
}
echo json_encode($response1)
?>