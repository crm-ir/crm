<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>CRM App | Enteroteam.com</title>
  <link href='https://fonts.googleapis.com/css?family=Open Sans:300' rel='stylesheet'>

    <link rel="stylesheet" type="text/css" href="styles.css"/>
<link href="../../lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <script src="../../lib/jquery/jquery-2.2.4.min.js"></script>
    
    <script src="http://users.enteroteam.com/auth/js/keycloak.js"></script>
    <script src="app.js"></script>

</head>

<body>
    <div class="section">
        <div class="row" style="margin:0px;">
            <div class="col-md-3" style="border-right:1px solid #ccc;margin-top:50px;">
                <h2 class="text-center" style="margin-top:40px;">Welcome <span id="name"></span></h2>
                <h5 style="margin:30px;">Username: <span id="username"></span></h5>
                <h5 style="margin:30px;">Email: <span id="email"></span></h5>
                 
            </div>
            <div class="col-md-9">
                <div class="col-md-12" >
                    <div id="not-authenticated" class="">
                        <!-- <button class="btn btn-info" name="loginBtn" onclick="keycloak.login()">Login</button> -->
                    </div>

                    <div id="authenticated" class="">
                        <button class="btn btn-info pull-right margin-top-30 btn-mine" name="logoutBtn" onclick="keycloak.logout()">Logout</button>
                        <a href="add_step1.php" class="btn btn-info pull-right margin-top-30 btn-mine" name="add">Add Members</a>
                        <!-- <button name="accountBtn" onclick="keycloak.accountManagement()">Account</button> -->
                    </div>

                    <!-- <div class="content">
                        <button name="publicBtn" onclick="request('public')">Invoke Public</button>
                        <button name="securedBtn" onclick="request('secured')">Invoke Secured</button>
                        <button name="adminBtn" onclick="request('admin')">Invoke Admin</button>

                        <div class="message" id="message"></div>
                    </div> -->
                </div>
                <div class="col-md-12" style="margin-top:150px">
                    <!-- <br><Br><br><br><Br> -->
                    <h1 class="text-center">Welcome to Enteroteam.com</h1>
                </div>
                <div class="col-md-12">
                    <br><Br><br><br>
                    <div class="row">
                        <div class="col-md-5">
                        
                            <h1 class="text-center pull-right">
                                
                                  <a href="../app1" class="card a-deco box-shadow">
                                    <div class="card-body text-center ">
                                      <p class="card-text">APP 1</p>
                                    </div>
                                  </a>
                                
                            </h1>
                        </div>
                        <div class="col-md-1">
                        </div>
                        <div class="col-md-1">
                        </div>
                        <div class="col-md-5">
                        
                            <h1 class="text-center">
                                
                                  <a href="../app2" class="card a-deco box-shadow">
                                    <div class="card-body text-center ">
                                      <p class="card-text">APP 2</p>
                                    </div>
                                  </a>
                                
                            </h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</body>
 <script src="../../lib/bootstrap/js/bootstrap.min.js"></script>
    
</html>