<!DOCTYPE html>
<html>
<head>
  <title>Publish Subscribe Tutorial</title>
</head>

<body>
<input id="publish-button" type="submit" value="Click here to Publish"/>
</body>

<script src="https://cdn.pubnub.com/sdk/javascript/pubnub.4.21.7.min.js"></script>
<script>
 
  const pubnub = new PubNub({
    publishKey: "pub-c-add7802c-5963-49f2-b79e-81999f1d87db",
    subscribeKey: "sub-c-9d06025e-db85-11e9-b2a7-e243f66d3f10",
    uuid: uuid
  });

  pubnub.subscribe({
    channels: ['hello_world'] 
  });

  pubnub.addListener({
    message: function(event) {
      let pElement = document.createElement('p');
      pElement.appendChild(document.createTextNode(event.message.content));
      document.body.appendChild(pElement);
    }
  });
 

</script>
</html>