 <?php 
  $path_init = $_SERVER['DOCUMENT_ROOT']."/crm/resources/init.php";
include_once($path_init);

 ?>

   <link href="<?php echo ROOT_URI; ?>/resources/lib/select2/select2.min.css" rel="stylesheet" />
   <link href="<?php echo ROOT_URI; ?>/assets/css/cust_css.css" rel="stylesheet" type="text/css">
  <script src="<?php echo ROOT_URI; ?>/resources/lib/select2/select2.min.js"></script>
   <script src="<?php echo ROOT_URI; ?>/resources/lib/time_ago/tinyAgo.min.js"></script>
  <link href="<?php echo ROOT_URI; ?>/resources/lib/timepicker/mdtimepicker.css" rel="stylesheet" type="text/css">
   <script src="<?php echo ROOT_URI; ?>/assets/js/rating.js" type="text/javascript"></script> 

<div class="col-md-10 col-lg-10 col-sm-10 col-xs-10 col-lg-offset-2 col-md-offset-2 col-sm-offset-2 col-xs-offset-2 no-lr-pad back-color-grey">
    <!-- main section starts here contains upper part and lower part of task page, task detail section is outside this section -->
    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad " id="main-section-task">
   <!-- for upper part -->

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="card-box mt-15 mb-15">
                    <div class="container-fluid">

                        <div class="row">
                            <div class="col-xs-12">
                                <div class="block-label mt-15 topdiv">
                                    <span>Customer Sign Up</span>
                                </div>
                            </div>
                        </div>

                        <div class="row mt-15">
                            <div class="col-md-6">
                                <div class="container-fluid">
                                    <div class="row special">
                                        <div class="col-xs-4">
                                            <span class="text-uppercase">billing entity</span>
                                        </div>
                                        <div class="col-xs-8">
                                            <input type="text" class="form-control input-sm" id="billing_entity_input" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="container-fluid">
                                    <div class="row special">
                                        <div class="col-xs-4">
                                            <span class="text-uppercase">Contact Person</span>
                                        </div>
                                        <div class="col-xs-8">
                                            <input type="text" class="form-control input-sm" id="contact_person_input" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row mt-15">
                            <div class="col-md-6">
                                <div class="container-fluid">
                                    <div class="row special">
                                        <div class="col-xs-4">
                                            <span class="text-uppercase">Landline No.</span>
                                        </div>
                                        <div class="col-xs-8">
                                            <input type="text" class="form-control input-sm" id="landline_no_input" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="container-fluid">
                                    <div class="row special">
                                        <div class="col-xs-4">
                                            <span class="text-uppercase">Company Email</span>
                                        </div>
                                        <div class="col-xs-8">
                                            <input type="text" class="form-control input-sm" id="company_email_input" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="row mt-15">
                            <div class="col-md-6">
                                <div class="container-fluid">
                                    <div class="row special">
                                        <div class="col-xs-4">
                                            <span class="text-uppercase">Mobile No.</span>
                                        </div>
                                        <div class="col-xs-8">
                                            <input type="text" class="form-control input-sm" id="mobile_no_input" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="container-fluid">
                                    <div class="row special">
                                        <div class="col-xs-4">
                                            <span class="text-uppercase">Search Pincode</span>
                                        </div>
                                        <div class="col-xs-8">
                                             <select class=" form-control tag-for-cust  js-data-select-ajax-assign width-100" id="search_pincode_input" name="tags_for_assign[]">
                                             </select>
                                             <input type="hidden" name="" class="state_id_input" value="">
                                             <input type="hidden" name="" class="city_id_input" value="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row mt-15">
                            <div class="col-md-12">
                                <div class="container-fluid">
                                    <div class="row special ">
                                        <div class="col-xs-4 col-md-2">
                                            <span class="text-uppercase">Address</span>
                                        </div>
                                        <div class="col-xs-8 col-md-10">
                                            <textarea class="form-control input-sm input_big" id="address_input"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>



                        <div class="row mt-15">
                            <div class="col-md-6">
                                <div class="container-fluid">
                                    <div class="row special">
                                        <div class="col-xs-4">
                                            <span class="text-uppercase">city</span>
                                        </div>
                                        <div class="col-xs-8">
                                            <input type="text" class="form-control input-sm" id="city_input" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="container-fluid">
                                    <div class="row special">
                                        <div class="col-xs-4">
                                            <span class="text-uppercase">State</span>
                                        </div>
                                        <div class="col-xs-8">
                                            <input type="text" class="form-control input-sm" id="state_input" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>




                        <div class="row mt-15">
                            <div class="col-md-6">
                                <div class="container-fluid">
                                    <div class="row special">
                                        <div class="col-xs-4">
                                            <span class="text-uppercase">Drug Licence</span>
                                        </div>
                                        <div class="col-xs-8">
                                            <input type="text" class="form-control input-sm" id="drug_licence_input" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="container-fluid">
                                    <div class="row special">
                                        <div class="col-xs-4">
                                            <span class="text-uppercase">GST Number</span>
                                        </div>
                                        <div class="col-xs-8">
                                            <input type="text" class="form-control input-sm" id="gst_number_input" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>




                        <div class="row mt-15">
                            <div class="col-md-12">
                                <div class="line"></div>
                                <span class="span">Requested By</span>
                            </div>
                        </div>


                        <div class="row mt-15">
                            <div class="col-md-6">
                                <div class="container-fluid">
                                    <div class="row special">
                                        <div class="col-xs-4">
                                            <span class="text-uppercase">Name</span>
                                        </div>
                                        <div class="col-xs-8">
                                            <input type="text" class="form-control input-sm" id="requested_by_name_input" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="container-fluid">
                                    <div class="row special">
                                        <div class="col-xs-4">
                                            <span class="text-uppercase">division</span>
                                        </div>
                                        <div class="col-xs-8">
                                            <input type="text" class="form-control input-sm" id="division_input" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>



                        <div class="row mt-15">
                            <div class="col-md-6">
                                <div class="container-fluid">
                                    <div class="row special">
                                        <div class="col-xs-4">
                                            <span class="text-uppercase">designation</span>
                                        </div>
                                        <div class="col-xs-8">
                                            <input type="text" class="form-control input-sm" id="designation_input" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="container-fluid">
                                    <div class="row special">
                                        <div class="col-xs-4">
                                            <span class="text-uppercase">Phone No. </span>
                                        </div>
                                        <div class="col-xs-8">
                                            <input type="text" class="form-control input-sm" id="phone_number_input" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>



                        <div class="row mt-15">
                            <div class="col-md-6">
                                <div class="container-fluid">
                                    <div class="row special">
                                        <div class="col-xs-4">
                                            <span class="text-uppercase">Email Id</span>
                                        </div>
                                        <div class="col-xs-8">
                                            <input type="text" class="form-control input-sm" id="email_input" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="container-fluid">
                                    <div class="row special">

                                        <div class="col-xs-4">
                                            <label class="form-check-label text-uppercase"><input type="radio"
                                                    name="checkbox" class="role_input" value="Chemist">Chemist</label>
                                        </div>

                                        <div class="col-xs-4">
                                            <label class="form-check-label text-uppercase"><input type="radio"
                                                    name="checkbox" class="role_input" value="Doctor">Doctor</label>
                                        </div>

                                        <div class="col-xs-4">
                                            <label class="form-check-label text-uppercase"><input type="radio"
                                                    name="checkbox" class="role_input" value="Hospital">Hospital</label>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>



                        <div class="row mt-15">
                            <div class="col-md-12">
                                <div class="action text-center">
                                    <button class="btn btn-primary signup" id="signup_button">Sign Up </button>
                                    <!-- <button type="submit" class="btn   btn-default exit">EXIT </button> -->
                                </div>
                            </div>

                        </div>


                    </div>

                </div>
            </div>
        </div>




    </div>



    <div class="cfp-hotkeys-container fade ng-scope" ng-class="{in: helpVisible}" style="display: none;">
        <div class="cfp-hotkeys">
            <!-- ngIf: !header -->
            <h4 class="cfp-hotkeys-title ng-binding ng-scope" ng-if="!header">Keyboard Shortcuts:</h4>
            <!-- end ngIf: !header -->
            <!-- ngIf: header -->
            <table>
                <tbody>
                    <!-- ngRepeat: hotkey in hotkeys | filter:{ description: '!$$undefined$$' } -->
                    <tr ng-repeat="hotkey in hotkeys | filter:{ description: '!$$undefined$$' }" class="ng-scope">
                        <td class="cfp-hotkeys-keys">
                            <!-- ngRepeat: key in hotkey.format() track by $index --><span
                                ng-repeat="key in hotkey.format() track by $index"
                                class="cfp-hotkeys-key ng-binding ng-scope">?</span>
                            <!-- end ngRepeat: key in hotkey.format() track by $index -->
                        </td>
                        <td class="cfp-hotkeys-text ng-binding">Show / hide this help menu</td>
                    </tr><!-- end ngRepeat: hotkey in hotkeys | filter:{ description: '!$$undefined$$' } -->
                </tbody>
            </table><!-- ngIf: footer -->
            <div class="cfp-hotkeys-close" ng-click="toggleCheatSheet()">×</div>
        </div>
    </div>

</div>
</div>

<script type="text/javascript">
    
    $(document).ready(function(){

        // $('#search_pincode_input').on('keyup',function(){

            // console.log('dds');
                    $('#search_pincode_input').select2({
           
                         ajax: {
                              url: "<?php echo ROOT_URI; ?>/resources/services/search_pincode.php",
                              dataType: 'json',
                              delay: 350,
                              data: function (params) {
                                  return {
                                      q: params.term, // search term
                                  };
                              },
                              processResults: function (data, params) {
                                   // params.page = params.page || 1;
                                 return {
                                    results: $.map(data.results, function (item) {
                                        return {
                                            text: item.text,
                                            id: item.id,
                                            state_id: item.state_id,
                                            city_id: item.city_id,
                                            city_name: item.city_name,
                                            state_name: item.state_name
                                        }
                                    })
                                };
                              },
                              cache: true
                          },
                          
                          tags: true,
                          placeholder: "Search Pincode"
                  });
            // $.get("http://uat.enterodirect.com/api/PincodeMasters/getPinCodeList/4", function(data, status){
            //     console.log("Data: " + data + "\nStatus: " + status);
            // });

        // }); // close function on change search pincode

        $('#search_pincode_input').on('select2:select', function(evt){
            state_id = evt.params.data.state_id;
            city_id = evt.params.data.city_id;
            city_name = evt.params.data.city_name;
            state_name = evt.params.data.state_name;

            $('#city_input').val(city_name);
            $('#state_input').val(state_name);

            $('.city_id_input').val(city_id);
            $('.state_id_input').val(state_id);
        });

        $(document).on('click','#signup_button',function(){

            var billing_entity = $('#billing_entity_input').val();

            if(billing_entity == ''){
                toast_it("Empty Billing Entity"); return false;
            }

            var contact_person = $('#contact_person_input').val();

            if(contact_person == ''){
                toast_it("Empty Contact Person"); return false;
            }

            var landline_no = $('#landline_no_input').val();

            if(landline_no == ''){
                toast_it("Empty Landline No"); return false;
            }

            var company_email = $('#company_email_input').val();

            if(company_email == ''){
                toast_it("Empty company email"); return false;
            }

            var mobile_no = $('#mobile_no_input').val();

            if(mobile_no == ''){
                toast_it("Empty Mobile No"); return false;
            }

            var search_pincode = $('#search_pincode_input').val();

            if(!search_pincode){
                toast_it("Empty Pincode"); return false;
            }

            var address = $('#address_input').val();

            if(address == ''){
                toast_it("Empty Address"); return false;
            }

            var city = $('#city_input').val();


            if(city == ''){
                toast_it("Empty City"); return false;
            }

            var city_id = $('.city_id_input').val();

            var state = $('#state_input').val();

            if(state == ''){
                toast_it("Empty State"); return false;
            }

            var state_id = $('.state_id_input').val();

            var drug_licence = $('#drug_licence_input').val();

            if(drug_licence == ''){
                toast_it("Empty Drug Licence"); return false;
            }

            var gst_number = $('#gst_number_input').val();

            if(gst_number == ''){
                toast_it("Empty GST Number"); return false;
            }

            var requested_by_name = $('#requested_by_name_input').val();

            if(requested_by_name == ''){
                toast_it("Empty Name"); return false;
            }

            var division = $('#division_input').val();

            if(division == ''){
                toast_it("Empty division"); return false;
            }

            var designation = $('#designation_input').val();

            if(designation == ''){
                toast_it("Empty designation"); return false;
            }

            var phone_number = $('#phone_number_input').val();

            if(phone_number == ''){
                toast_it("Empty Phone Number"); return false;
            }

            var email = $('#email_input').val();

            if(email == ''){
                toast_it("Empty Email"); return false;
            }

            var role = $('.role_input:checked').val();

            if(!role){
                toast_it("Empty role"); return false;
            }

            $.ajax({
                 url : "<?php echo ROOT_URI; ?>/resources/services/customer_api.php",
                 type : 'POST',
                 data : "{\n\"token\":\""+sessionStorage.getItem('token')+"\",\n\"billing_entity\":\""+billing_entity+"\",\n\"contact_person\":\""+contact_person+"\",\n\"landline_no\":\""+landline_no+"\",\n\"company_email\":\""+company_email+"\",\n\"mobile_no\":\""+mobile_no+"\",\n\"search_pincode\":\""+search_pincode+"\",\n\"address\":\""+address+"\",\n\"city\":\""+city+"\",\n\"city_id\":\""+city_id+"\",\n\"state\":\""+state+"\",\n\"state_id\":\""+state_id+"\",\n\"drug_licence\":\""+drug_licence+"\",\n\"gst_number\":\""+gst_number+"\",\n\"requested_by_name\":\""+requested_by_name+"\",\n\"division\":\""+division+"\",\n\"designation\":\""+designation+"\",\n\"phone_number\":\""+phone_number+"\",\n\"email\":\""+email+"\",\n\"role\":\""+role+"\",\n\"action\":\"cust_signup\"\n}",

                 processData: false,  // tell jQuery not to process the data
                 contentType: false,  // tell jQuery not to set contentType 
                 dataType: 'JSON',
                       success: function(response) {

                        if(response.response_code == 200){

                            toast_it(response.data);
                        setInterval(function(){ location.reload();}, 1000);
                          
                        }  //  close if response code 200
                        else if(response.response_code == 400){
                            toast_it(response.response_message);
                        }
                        
                       }  //  close success 
                      
              }); //ajax close


        }); // close function signup button

    }); // clsoe function document ready

</script>