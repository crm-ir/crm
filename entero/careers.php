<?php 
// print_r($_POST);die;
$msg_q_c ='';
if(isset($_POST['frm_submit'])){
	// print_r($_POST); 
	// print_r($_FILES);die;
	// mail code starts from here
	if(!class_exists('PHPMailer',false))
		{
			require_once __DIR__ . '/PHPMailer-master/class.phpmailer.php';
		}

		$mail = new PHPMailer;

		$mail->ClearAddresses();
		$mail->isSMTP();                                      			// Set mailer to use SMTP
		$mail->Host = 'ssl://smtp.gmail.com';	    		  			// Specify main and backup server
		$mail->Port = 465;
		$mail->SMTPAuth = true;                               			// Enable SMTP authentication
		$mail->Username = 'entero.tech@gmail.com';                        // SMTP username
		$mail->Password = 'Entero123';                     // SMTP password
	                           	        // Enable encryption, 'ssl' also accepted
		$mail->From = 'entero.tech@gmail.com';
		$mail->FromName = 'Enterohealthcare.com';
		$mail->addAddress('info@enterohealthcare.com');               							// Name is optional

		$mail->WordWrap = 50;                              				// Set word wrap to 50 characters

		$mail->isHTML(true);                                  			// Set email format to HTML
		
		// $file_to_attach = 'logo.png';

		$mail->AddAttachment( @$_FILES['file']['tmp_name'] , @$_FILES['file']['name'] );
		
		$mail->Subject = 'Career application from '.@$_POST['name'];
		$body='Hi, We have received an application from '.@$_POST['name'].'<br><br> Email: '.@$_POST['mail'].'<br> Phone: '.@$_POST['phone'].'<br> Message: '.@$_POST['msg'].'<br>';
		$mail->Body= $body;
		$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

		if(!$mail->send()) {
		    $msg_q_c = 'Something went wrong, please try again';
		}
		else{
			$msg_q_c = 'Thank-you for reaching out to us. We will get back to you shortly';
		}
			 
	// mail ends here
}

 ?>
<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width,initial-scale=1.0">
	<title>Entero Healthcare: Careers & jobs at Entero Healthcare</title>
	<meta name="robots" content="noindex, nofollow" />
	<link href="https://fonts.googleapis.com/css?family=Istok+Web" rel="stylesheet">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
 <link href="css/animation.css" rel="stylesheet" />
<link rel="shortcut icon" type="image/png" href="img/favicon.png"/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="owlcarousel/owl.carousel.css">
<link rel="stylesheet" href="owlcarousel/owl.theme.default.css">
<link rel="shortcut icon" type="image/png" href="img/favicon.png"/>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<link rel="stylesheet" href="css/style.css" />
</head>

<body>
<div class="main_hdr">
<header>
<div class="head">
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<div class="logo">
					<a href="home.html"><img src="img/logo.png" /></a>
				</div>
			</div>
			<div class="col-md-9">
				<nav class="navbar navbar-expand-lg">
				<button type="button" class="navbar-toggler" >
      				<span class="fa fa-bars"></span>
    			</button>
    			<div class="collapse navbar-collapse" id="mn">
				<ul class="navbar nav">
					<li class="nav-item"><a class="nav-link" href="who-we-are.html">who we are</a></li>
					<li class="nav-item"><a class="nav-link" href="what-we-do.html">what we do</a></li>
					<li class="nav-item"><a class="nav-link" href="why-choose-us.html">why choose us</a></li>
					<!-- <li class="nav-item"><a class="nav-link" href="#">our products</a></li> -->
					<li class="nav-item"><a class="nav-link active"  href="careers.php">careers <i class="fa fa-angle-down myang"></i></a></li>
					<!-- <li class="nav-item"><a class="nav-link" href="news.html">news</a></li> -->
					<li class="nav-item"><a class="nav-link" href="contact-us.php">contact us</a></li>
				</ul>
			</div>
			</nav>
			</div>
		</div>
	</div>
</div>
</header>
</div>
<section>
	<div class="creer">
		<div class="row m-0">
			<div class="col-md-4 p-0 cr">
				<div class="c_image">
					<img src="img/career.png" />
				</div>
			</div>
			<div class="col-md-8">
				<div class="career_sec pb-5">
				<div class="row">

					<div class="col-md-12">
						<h2 class="c_hdng pt-5 pb-2 mb-3">CAREERS</h2>
						<p>Our people are our most valuable assets. Our culturally diverse workforce is one of our biggest strengths and the rich experience they bring, across varied skill-sets and backgrounds, is invaluable. We are proud that entire workforce is bound together by our common values.<br /><br />

						Entero Healthcare nurtures new talent to be leaders of future. As you work with inspiring people on different projects, you learn various skills and valuable insight into the industry. We believe in instilling confidence and skill-sets to ensure that our people reflect the utmost standard of excellence.<br /><br />

						We are committed to hiring exceptionally talented people and nurture them professionally. Our multi-dimensional work environment offers high growth opportunities through challenging roles with clear responsibilities and the opportunity to work on a variety of assignments. At Entero Healthcare, our employees are provided with opportunities to enhance their technical and soft skills through continuous training and development programs.<br /><br />

						As an employee of Entero Healthcare, you will work with an inspiring, approachable and visionary leadership. Our open, enabling and trust-based culture will offer you an exciting environment to work and grow. Entero provides opportunities for growth and development to employees by cultivating their knowledge, skills and competencies which ultimately translates to improve employee motivation and job satisfaction.<br /><br />

						To apply or get in touch with us, please fill the form below:</p>
					</div>
				</div>
				<div class="row">
				<div class="col-md-12">
				<div class="frm1">
					<form action="" method="post" enctype="multipart/form-data">
						<?php
							 
							if(!empty($msg_q_c)){
						 ?>
						   <div class="alert alert-success">
								 
							    <?php echo $msg_q_c; ?>
							  </div>
						 <?php } ?>
						<input required type="text" name="name" id="name" placeholder="Name" class="form-control mb-3" />
						<input required type="text" name="mail" id="mail" placeholder="Email ID" class="form-control" />
						<input type="text" name="phone" id="phone" placeholder="Phone No." class="form-control my-4" />
						<textarea class="form-control my-3" name="msg" id="msg" cols="11" placeholder="Covering Letter (Max 100 Characters)"></textarea>						

    					<div class="custom-file mb-3">
					      <input type="file" accept=".pdf,.doc, .png, .jpg" class="custom-file-input" id="file" name="file">
					      <label class="custom-file-label" for="file">Add Attachment (.pdf .doc .png .jpg format)</label>
					    	<!-- <div class="box-prev" style="width: 30px;height:30px;margin-left: -15px;margin-top:-3px;display: none;float: left;"></div> -->
					    </div>
					    <!-- <div class="box-prev mb-3"></div> -->

						<button  type="submit" name="frm_submit"  class="btn mybtn1 waves-effect waves-light my-3"><span style="color:red; font-weight: bold;">Apply</span> <i class="fa fa-angle-right fa-2x"></i></button>
					</form>
				</div>
				</div>
				</div>
			</div>
			</div>
		</div>
	</div>
</section>




<footer>
	<div class="ftr">
		<div class="container">
			<div class="row">
				<div class="col-md-12 p-2">
					<p class="mt-2 float-right">
<span class="mr-3">CONNECT WITH US</span>
<a href="https://www.linkedin.com/company/entero-healthcare/about/" target="new"><i class="fa fa-linkedin"></i></a>
</p>
				</div>
			</div>
		</div>
	</div>
	<div class="copyright p-1">
		<p class="text-center mt-2">Copyright 2019 @ Entero Healthcare</p>
	</div>
</footer>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" ></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" ></script>
<script type="text/javascript" src="js/mdb.min.js"></script>
<script src="js/jquery.countTo.js"></script>

<script src="js/custom.js"></script>
<script>
            $('#file').on('change',function(e){
                //get the file name
                 var fileName = e.target.files[0].name;
                //replace the "Choose a file" label
                $(this).next('.custom-file-label').html(fileName);

         //        var item=$(this).siblings(".box-prev");
			      // item.show();
			      // // $(this).siblings(".box-upload-cover").children(".upload-icon").hide();
			      //   var files = !!this.files ? this.files : [];
			      //   if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support
			 
			      //   if (/^image/.test( files[0].type)){ // only image file
			      //       var reader = new FileReader(); // instance of the FileReader
			      //       reader.readAsDataURL(files[0]); // read the local file
			 
			      //       reader.onloadend = function(){ // set image data as background of div
			      //           item.css("background-image", "url("+this.result+")");
			      //           item.css("background-size", "cover");
			      //           item.css("background-repeat", "no-repeat");
			      //           item.css("background-position", "center center");

			      //           // item.parents("label").siblings(".preview-cl").show();
			      //       }
			      //   }

            })
        </script>
</body>
</html>