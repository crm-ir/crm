<?php
$path_init = $_SERVER['DOCUMENT_ROOT']."/crm/resources/init.php";
include_once($path_init); 
header('Content-type: application/json');
$data = json_decode(file_get_contents('php://input')); 
$remark_obj=new remark();
$user_obj=new user();
$action=$data->action;
$response=array();
switch($action)
{
	case 'add_remarks':
					$user_id=$data->user_id;//user_id of user
					$entity_id=$data->entity_id;
					$entity_type=$data->entity_type; 
					$remark=$data->remark;

					$result=$remark_obj->add_remark($entity_id,$entity_type,$remark,$user_id);
					if($result==0)
					{
						$response['response_code']=400;
						$response['response_message']="Something Went Wrong";
					}
					else
					{
						$response['response_code']=200;
						$response['response_message']="Remark Inserted Sucessfuly";
					}
	break;
	case 'display_remarks':
					$page_no=$data->page_no;//page no 
					$limit=5;
					$entity_id=$data->entity_id;
					$entity_type=$data->entity_type;
					$result_remark=$remark_obj->display_remark($entity_id,$entity_type,($page_no-1)*$limit,$limit);
					if(empty($result_remark))
					{
						$response['response_code']=400;
						$response['response_message']="Something Went Wrong";
					}
					else
					{
					$i=0;
					foreach ($result_remark as $key => $row) 
						{
							$user_details=$user_obj->get_user_information($row->user_id);
							$tempArray = array(
						                            "remark"=>$row->remark,
						                             "created_date"=>$row->created_date,
						                             "user_id"=>$row->user_id,
						                             "user_name"=>$user_details->name,
						                             "user_profile_pic"=>$user_details->profile_picture,
						                        );
							$result_array[$i++]=$tempArray;
						}
						$response['response_code']=200;
						$response['response_message']="Succes";
						$response['data']=$result_array;
						}
						break;
						case 'display_remarks_all':
					$entity_id=$data->entity_id;
					$entity_type=$data->entity_type;
					$result_remark=$remark_obj->display_remark_all($entity_id,$entity_type);
					if(empty($result_remark))
					{
						$response['response_code']=400;
						$response['response_message']="Something Went Wrong";
					}
					else
					{
					$i=0;
					foreach ($result_remark as $key => $row) 
						{
					$user_details=$user_obj->get_user_information($row->user_id);
							$tempArray = array(
						                            "remark"=>$row->remark,
						                             "created_date"=>$row->created_date,
						                             "user_id"=>$row->user_id,
						                             "user_name"=>$user_details->name,
						                             "user_profile_pic"=>$user_details->profile_picture,
						                             "role"=>$user_details->role,

						                        );
							$result_array[$i++]=$tempArray;
						}
						$response['response_code']=200;
						$response['response_message']="Succes";
						$response['data']=$result_array;
						}
						break;
						default:
						$response['response_message']= "Wrong Action ";
								$response['response_code'] = 400;
									# code...
	break;

}
echo(json_encode($response));

?>