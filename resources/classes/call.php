<?php
class call
{
	private $_db, $dt,$_user_obj;

	public function __construct($user=null)
	{
		$this->_db = DB::getInstance();
		date_default_timezone_set('Asia/Calcutta');
        $this->dt = date('Y-m-d H:i:s');
	}
	public function caller_no($from,$exo){

		$sql = "SELECT from_number from call_details WHERE to_number=? order by id desc";
		$result_from = $this->_db->query($sql,array($from))->results();
      $temp = '';
      $number_string = '';

      $number_string  = @$result_from[0]->from_number;

         $sql2 = "SELECT user_id FROM distributor_mapping WHERE dist_phone = '$exo' LIMIT 1";
         $distributor_id = $this->_db->query($sql2)->results();

         // print_r();die;

         $distributor_id = @$distributor_id[0]->user_id;

         $team_lead = $this->get_team_leads($distributor_id);
         $agent = $this->get_agents($distributor_id);

         $all_users = array_merge($agent,$team_lead);

         if(empty($all_users)){

            $sql4 = "SELECT calling_no FROM users WHERE user_id = '$distributor_id'";
            $temp1 = $this->_db->query($sql4)->results();

            $temp = $temp1[0]->calling_no;
            // return $temp;

            $number_string = $number_string;
            
         }else{
            
            function cmp($a, $b) {
                return strcmp($a->priority, $b->priority);
            }

            usort($all_users, "cmp");

 
            foreach ($all_users as $key => $value) {
            
               $number_string = $number_string.",".$value->calling_no;

            }

         }

         $number_string=ltrim($number_string,',');
         // $number_string=rtrim($number_string,',');

		return $number_string;

	}

   public function get_team_leads($user_id){

    $sql = "SELECT us.calling_no,us.priority from users as us join user_distributor_mapping as udm on us.user_id = udm.user_id_l2 WHERE user_id_distributor = '$user_id' AND us.status =1 and us.priority!=0 ORDER BY us.priority asc ";
    $result = $this->_db->query($sql)->results();

    return $result;
  }

  public function get_agents($user_id){

    $sql = "SELECT us.calling_no,us.priority from l1_l2_mapping as map JOIN user_distributor_mapping as udm on udm.user_id_l2 = map.user_id_l2 JOIN users as us on map.user_id_l1 = us.user_id WHERE udm.user_id_distributor = '$user_id' AND us.status = 1 and us.priority!=0 ORDER BY us.priority asc";
    $result = $this->_db->query($sql)->results();
    // print_r($result); die();
    
    return $result;
  }

   public function last_contracted($task_id){
      $sql="SELECT cal.created_date,cad.to_number from calls as cal join call_details as cad on cal.call_id = cad.call_id where cal.customer_id=? ORDER BY cal.created_date DESC LIMIT 1";
      $result_last=$this->_db->query($sql,array($task_id))->results();

      // print_r($result_last); print_r($task_id); die();
      if(empty($result_last))
      {
         return 0;
      }
      else
      {
            foreach($result_last as $last=>$row2){
               return $row2;
            }

      }
   }
   public function last_contracted_custm($customer_id){
      $sql="SELECT cal.created_date,cad.to_number from calls as cal join call_details as cad on cal.call_id = cad.call_id where cal.customer_id=? ORDER BY cal.created_date DESC LIMIT 1";
      $result_last=$this->_db->query($sql,array($customer_id))->results();

      // print_r($result_last); die();
      if(empty($result_last))
      {
         return 0;
      }
      else
      {
            foreach($result_last as $last=>$row2){
               return $row2;
            }

      }
   }

public function insert_call_details($call_id,$start_time,$end_time,$duration=NULL,$to_number,$from_number,$price=NULL,$recording_url=NULL,$call_url=NULL,$status)
{
   $sql="INSERT INTO call_details(call_id,start_time,end_time,duration,to_number,from_number,price,recording_url,call_url,status) VALUES(?,?,?,?,?,?,?,?,?,?)";
   $result_call=$this->_db->query($sql,array($call_id,$start_time,$end_time,$duration,$to_number,$from_number,$price,$recording_url,$call_url,$status));
    // print_r($result_call);die;
   if($result_call->_error)
   {
      return 0;
   }
   else
   {
      return 1;
   }
}
public function add_detail_call($calSid,$tid=NULL,$uid,$cust_id)
{
   $sql="INSERT INTO calls(call_id,task_id,user_id,customer_id) VALUES(?,?,?,?)";
   $result_call=$this->_db->query($sql,array($calSid,$tid,$uid,$cust_id));
    // print_r($result_call);die;
   if($result_call->_error)
   {
      return 0;
   }
   else
   {
      return 1;
   }
}
public function insert_call_details1($post){
$sql="INSERT INTO post(post) VALUES(?)";
   $result_call=$this->_db->query($sql,array($post));
   // print_r($result_call);die;
   if($result_call->_error)
   {
      return 0;
   }
   else
   {
      return 1;
   }
}
public function select_otp($phone){
   $sql_dist="SELECT * from otp_pass  where phone=?";
    $result_dist= $this->_db->query($sql_dist,array($phone))->results();
      if($result_dist){
      return $result_dist[0]->otp;
      }
      else{
         return 0;
      }
}

public function insert_otp($otp,$phone){
   $sql_dist="SELECT * from otp_pass  where phone=?";
    $result_dist= $this->_db->query($sql_dist,array($phone))->results();
    if($result_dist){
         $query ="UPDATE otp_pass SET otp = ? WHERE phone = '$phone'";
         $result1 = $this->_db->query($query,array($otp));  
         if($result1->_error){
            return 0;
         }
         else{
            return 1;
         }
    }
    else{
         $sql="INSERT INTO otp_pass(phone,otp) VALUES(?,?)";
         $result_call=$this->_db->query($sql,array($phone,$otp));
         // print_r($result_call);die;
         if($result_call->_error){
            return 0;
         }
         else
         {
            return 1;
         }

    }
   
}
public function call_history($userid,$page_no,$limit)
{
   $sql="SELECT a.start_time,a.end_time,a.duration from call_details as a INNER JOIN calls as b ON a.call_id=b.call_id WHERE b.customer_id=? ORDER BY DATE(a.start_time) Desc LIMIT $page_no,$limit";
   //echo $sql;
   $result = $this->_db->query($sql,array($userid))->results();
   return $result;
}
public function check_call($sid)
{
   $sql="SELECT EXISTS (SELECT * FROM call_details WHERE call_id =? LIMIT 1) as ch";
   //echo $sql;
   $result = $this->_db->query($sql,array($sid))->results();
   return $result;
}
public function get_token_id_of_user_for_app($calling_no){
    $sql1 = "SELECT token FROM users WHERE calling_no='$calling_no'";
        $result = $this->_db->query($sql1)->results();
        if(empty($result)){
          return 0;
        }
        else{

          $res_arr=array();
          foreach ($result as $key => $value) {
             $res_arr[]=$value->token;

          }
          return $res_arr;
      }

 }
public function call_log_insert($callsid,$from_number,$to_number,$exo_phone_number,$call_status,$call_direction,$call_duration,$recording_url){

   $sql1 = "SELECT id FROM call_logs WHERE callsid = '$callsid'";
   $result = $this->_db->query($sql1)->results();

   $sql_dist = "SELECT user_id FROM distributor_mapping WHERE dist_phone = '$exo_phone_number'";
   $result_distributor = $this->_db->query($sql_dist)->results();

   $dist_id = @$result_distributor[0]->user_id;



   if($call_direction == "incoming"){

      if($to_number){
         $calling_no = substr($to_number,-10);
      }else{
         $calling_no = 0;
      }

   }else{

      $calling_no = substr($from_number,-10);
   }

   if($calling_no){

      $sql_agent = "SELECT user_id FROM users WHERE calling_no = '$calling_no'";
      $result_agent = $this->_db->query($sql_agent)->results();

      $agent_id = $result_agent[0]->user_id;
   }else{
      $agent_id = '';
   }

  

   if(empty($result)){

      $sql = "INSERT INTO call_logs(callsid,from_number,to_number,exo_phone_number,dist_id,user_id,call_status,call_direction,call_duration,recording_url) VALUES('$callsid','$from_number','$to_number','$exo_phone_number','$dist_id','$agent_id','$call_status','$call_direction','$call_duration','$recording_url')";

      $result_call=$this->_db->query($sql);

         if($result_call->_error){
            return 0;
         }
         else
         {
            return 1;
         }

   }else{

      $sql2 = "UPDATE call_logs SET from_number = '$from_number', to_number = '$to_number', exo_phone_number = '$exo_phone_number', dist_id = '$dist_id', user_id = '$agent_id', call_status = '$call_status', call_direction = '$call_direction', call_duration = '$call_duration', recording_url = '$recording_url' WHERE callsid = '$callsid'";

      $result_call2=$this->_db->query($sql2);

         if($result_call2->_error){
            return 0;
         }
         else
         {
            return 1;
         }

   }

   
}

public function insert_customer_number($from_number,$user_id){

   $sql = "INSERT INTO customer_number_save(customer_number,user_id) VALUES('$from_number','$user_id')";
   $result_call=$this->_db->query($sql);

   if($result_call->_error){
      return 0;
   }
   else
   {
      return 1;
   }

}  // close function 

public function get_user_id_by_number($result_call){

   $result_call = substr($result_call, -10);

   $sql = "SELECT user_id FROM users WHERE calling_no = '$result_call' LIMIT 1";
   $result = $this->_db->query($sql)->results();

   if(empty($result)){
      return 0;
   }else{
      return $result[0]->user_id;     
   }

} // close function 

public function call_logs_display($user_id,$call_status,$dist_id){

   $cond1 = '';
   $cond2 = '';

   if($call_status == "misscall"){
      $cond1 = "AND call_status = 'no-answer' AND call_direction = 'incoming'";
      $cond2 = 'cl.dist_id ="'.$dist_id.'"';

   }else{
      $cond1 = '';
      $cond2 = "cl.user_id IN(".$user_id.")";
   }

   $sql = "SELECT cl.*, us.name FROM call_logs as cl left join users as us on cl.user_id = us.user_id  WHERE $cond2 AND cl.status = 1 $cond1 ORDER BY cl.created_date desc";
   $result = $this->_db->query($sql)->results();

   // print_r($result);

   if(empty($result)){
      return 0;
   }else{
      return $result;     
   }

}  // close function call logs display

public function mark_calledback($callsid){

   $sql = "UPDATE call_logs SET call_status = 'called-back', status = 0 WHERE callsid = '$callsid'";
   $result_call=$this->_db->query($sql);

   if($result_call->_error){
      return 0;
   }
   else
   {
      return 1;
   }

} // close function mark_calledback

public function mark_misscall($dist_id){

   $sql = "UPDATE call_logs SET misscall_status = 0 WHERE dist_id = '$dist_id'";
   $result_call=$this->_db->query($sql);

   if($result_call->_error){
      return 0;
   }
   else
   {
      return 1;
   }

} // close function mark_calledback
public function add_call_planner($main_input){
   foreach ($main_input as $key => $value) {
      $customer_id = $value[1];
      $user_id = $value[0];
      // print_r($customer_id);
      $sql  = "SELECT * from user_customer_mapping WHERE customer_id = ?";
      $result = $this->_db->query($sql,array($customer_id))->results();
      

      if(empty($result)){
         $sql2 = "INSERT INTO user_customer_mapping(user_id,customer_id,mo,tu,we,th,fr,sa,su,time_from,time_to) VALUES(?,?,?,?,?,?,?,?,?,?,?)";
         $result2 = $this->_db->query($sql2,array($user_id,$customer_id,$value[2],$value[3],$value[4],$value[5],$value[6],$value[7],$value[8],$value[9],$value[10]));
           //print_r($result2);
           if($result2->_error){
               return 0;
           }else{
               
           }
      } // outer if close
      else{

         $sql3 = "UPDATE user_customer_mapping SET user_id = ? , mo = ?,tu = ?,we = ?,th = ?,fr = ?,sa = ?,su = ?,time_from = ?,time_to =? WHERE customer_id = ?";
         $result3 = $this->_db->query($sql3,array($user_id,$value[2],$value[3],$value[4],$value[5],$value[6],$value[7],$value[8],$value[9],$value[10],$customer_id));
           // print_r($result3);
           if($result3->_error){
               return 0;
           }else{
               
           }

      }
   }
   return 1;  
}

}

