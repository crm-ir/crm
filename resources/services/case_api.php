<?php
$path_init = $_SERVER['DOCUMENT_ROOT']."/crm/resources/init.php";
include_once($path_init); 
header('Content-type: application/json');
$data = json_decode(file_get_contents('php://input'));
$obj_case=new case_class();//creates object of task_class
$obj_call=new call();
$obj_user=new user();
$obj_remark=new remark();
$flag=$data->flag;
$user_id=$data->user_id;//user_id of user
$page_no=$data->page_no;//page no 
$role=$data->role;//role of user
$token=$data->token;
$limit=5;

$response=array();
$user_details_current=$obj_user->get_user_information($user_id);
$result=1;
switch ($flag) {
	case 'my_cases':
	$result=$obj_case->case_details_mycases($user_id,($page_no-1)*$limit,$limit);
	break;
	case 'other_cases':
	$result=$obj_case->case_details_othercases($user_id,$role,($page_no-1)*$limit,$limit);
	break;
	case 'closed_cases':
	$result=$obj_case->case_details_closedcases($user_id,$role,($page_no-1)*$limit,$limit);
	break;
	case 'cancelled_cases':
	$result=$obj_case->case_details_cancelledcases($user_id,$role,($page_no-1)*$limit,$limit);
	break;
	default:
	$response['response_code']=400;
	$response['response_message']="Wrong Flag";

}
$res=array();
if(empty($result))
{
	$response['response_code']=200;
	$response['response_message']="No Cases To Show";
}
else if($result!=1)
{

	$customer_ids='';
	foreach($result as $keys=>$pair)
	{
		$customer_ids.=','.$pair->customer_id;
	}
	$customer_ids=ltrim($customer_ids,',');
	$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => ORDERGINI_API."/api/CRMServices/GetAllChemistDetails",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_POSTFIELDS => "{\"ChemistDArry\":[".$customer_ids."]}",
  CURLOPT_HTTPHEADER => array(
    "Authorization: Bearer ".$token,
    "Content-Type: application/json",
    "Postman-Token: ee2b2d47-87ad-48e8-a522-8c96fb6c31a1",
    "cache-control: no-cache"
  ),
));

$response_cus_id= curl_exec($curl);

$err = curl_error($curl);

curl_close($curl);

if (!$err) {
$response_cus_id=json_decode($response_cus_id);
$i=0;
foreach ($result as $key => $row) 
	{
		$remark=$obj_remark->last_remark($row->case_id,2);
		if($remark)
		{
			$last_remark=$remark->remark;
			$user_details_remark=$obj_user->get_user_information($remark->user_id);
			$remark_user_profile_pic=$user_details_remark->profile_picture;
			$remark_user_name=$user_details_remark->name;
			$remark_created_date=$remark->created_date;
		}
		else{
			$last_remark=null;
			$remark_user_profile_pic=null;
			$remark_user_name=null;
			$remark_created_date=null;
		}
		$user_details_assigned=$obj_user->get_user_information($row->assigned_user_id);
		$user_details_created=$obj_user->get_user_information($row->creator_user_id);

		$tempArray = array(
	                             "case_name"=>$row->case_name,
	                             "case_id"=>$row->case_id,
	                             "case_created_date"=>$row->created_date,
	                             "assigned_user_id"=>$row->assigned_user_id,
	                             "assigned_user_name"=>$user_details_assigned->name,
	                             "assigned_user_profile_pic"=>$user_details_assigned->profile_picture,
	                             "user_name_current"=>$user_details_current->name,
	                             "user_id_current"=>$user_id,
	                             "user_current_profile_pic"=>$user_details_current->profile_picture,
	                             "creator_user_id"=>$row->creator_user_id,
	                             "creator_user_name"=>$user_details_created->name,
	                             "creator_profile_pic"=>$user_details_created->profile_picture,
	                             "customer_id"=>$response_cus_id->result[$key]->ESCID,
	                             "customer_name"=>$response_cus_id->result[$key]->S_Name,
	                             "customer_address"=>"NOIDA SECTOR-7,INDIA",
	                             "last_remark"=>$last_remark,
	                             "remark_user_name"=>$remark_user_name,
	                             "remark_user_profile_pic"=>$remark_user_profile_pic,
	                             "remark_created_date"=>$remark_created_date,

	                        );
		$result_array[$i++]=$tempArray;
	}

	$response['response_code']=200;
	$response['response_message']="Success";
	$response['data']=$result_array;
}
else
{
	$response['response_code']=400;
	$response['response_message']="Somethng Went Wrong";
}
}
echo(json_encode($response));