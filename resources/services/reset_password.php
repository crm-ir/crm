<?php
$path_init = $_SERVER['DOCUMENT_ROOT']."/crm/resources/init.php";
include_once($path_init); 
 header('Content-type: application/json');
$data = json_decode(file_get_contents('php://input'));
 
$result='';
 

    // keycloak code to get token
    $curl = curl_init();
    $result=0;
    curl_setopt_array($curl, array(
      CURLOPT_URL => "http://users.enteroteam.com/auth/realms/master/protocol/openid-connect/token",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => "grant_type=client_credentials",
      //do not change authorization field until unless client secret is changed
      CURLOPT_HTTPHEADER => array(
        "authorization: Basic YWRtaW4tY2xpOjY2ZTYzMWMzLTJmNDctNDVjNS04MDU0LWQxM2JjY2Y2ZDZiNg==",
        "cache-control: no-cache",
        "content-type: application/x-www-form-urlencoded",
        "postman-token: a4ba15c5-1e19-15b0-17e6-4de5e26c0a2d"
      ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
     $result=0;
    } else {
         $temp_acc = json_decode($response);
         ###########################
            $curl = curl_init();

            curl_setopt_array($curl, array(
              CURLOPT_URL => "http://users.enteroteam.com/auth/admin/realms/master/users?username=".$data->phone,
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 30,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "GET",
              CURLOPT_POSTFIELDS => "grant_type=client_credentials",
              CURLOPT_HTTPHEADER => array(
                "authorization: Bearer ".$temp_acc->access_token,
                "cache-control: no-cache",
                "content-type: application/x-www-form-urlencoded",
                "postman-token: f84d1fa7-4fdf-522b-4c3c-5ae1af5bb5cb"
              ),
            ));

            $response_exist_curl = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            if ($err) {
                $result=0;
            } else {
              $exist_curl_code = json_decode($response_exist_curl);
              // print_r($exist_curl_code);die;
             if(!empty($exist_curl_code)){
              $return_id=$exist_curl_code[0]->id;
              
                  #################################
                  ######code to reset password#####
                  $curl = curl_init();

                curl_setopt_array($curl, array(
                  CURLOPT_URL => "http://users.enteroteam.com/auth/admin/realms/master/users/".$return_id."/reset-password",
                  CURLOPT_RETURNTRANSFER => true,
                  CURLOPT_ENCODING => "",
                  CURLOPT_MAXREDIRS => 10,
                  CURLOPT_TIMEOUT => 30,
                  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                  CURLOPT_CUSTOMREQUEST => "PUT",
                  CURLOPT_POSTFIELDS => '{ "type": "password", "temporary": false, "value": "'.$data->password.'"   }',
                  CURLOPT_HTTPHEADER => array(
                    "authorization: Bearer ".$temp_acc->access_token,
                    "cache-control: no-cache",
                    "content-type: application/json",
                    "postman-token: 06a055e3-051c-817b-f6ee-1ec9986da5c7"
                  ),
                ));

                $response = curl_exec($curl);
                $err = curl_error($curl);
                // print_r($vr_t);
                //print_r($response);die;
                curl_close($curl);

                if ($err) {
                 $result=0;
                } else {
                  $result=1;
                }
                  #################################
              }
              else{
                 $result=2;
              }

            }
            ############################
    }

 
if($result==1){
  $response1['response_message']= "success";
  $response1['response_code'] = 200;
   
  $response1['response_status'] = 1;
}
elseif ($result==2) {
   $response1['response_message']= "No user found";
  $response1['response_code'] = 200;
  $response1['response_status'] = 2;
}
else{
  $response1['response_message']= "Failure";
  $response1['response_code'] = 400;
  $response1['response_status'] = 0;
}
echo json_encode($response1)
?>