<?php
class customer{
	private $_db, $dt,$_user_obj;

	public function __construct($user=null)
	{
		$this->_db = DB::getInstance();
		$this->_user_obj = user::getInstance_of_user();
		date_default_timezone_set('Asia/Calcutta');
        $this->dt = date('Y-m-d H:i:s');
	}

	public function customer_page_details($agent_id,$role,$page_no,$limit,$customer_id){
			$cond="";

			if($customer_id){

				$sql1="SELECT * from user_customer_mapping WHERE status=1 and customer_id=$customer_id";
				$result1 = $this->_db->query($sql1)->results();
			}else{

				$user_str='';
				// print_r($agent_id);
				$user_id_dorm=$this->_user_obj->get_userid($agent_id,$role);
				// print_r($user_id_dorm);die;
				foreach ($user_id_dorm as $key => $value) {
					$user_str.=",'".$value."'";
				}
				$user_str=ltrim($user_str,',');
			
				// $user_str=implode(",",$user_id);
				//echo $user_str;
				$cond1="AND user_id IN(".$user_str.")";
			
				 
				$cond3=" LIMIT $page_no,$limit";
				// print_r($cond3);die;
				$sql1="SELECT customer_id,user_id from user_customer_mapping WHERE status=1 ".$cond1.$cond3;
				$result1 = $this->_db->query($sql1)->results();

			}
		
			// $user_str='';
			// // print_r($agent_id);
			// $user_id_dorm=$this->_user_obj->get_userid($agent_id,$role);
			// // print_r($user_id_dorm);die;
			// foreach ($user_id_dorm as $key => $value) {
			// 	$user_str.=",'".$value."'";
			// }
			// $user_str=ltrim($user_str,',');
		
			// // $user_str=implode(",",$user_id);
			// //echo $user_str;
			// $cond1="AND user_id IN(".$user_str.")";
		
			 
			// $cond3=" LIMIT $page_no,$limit";
			// // print_r($cond3);die;
			// $sql1="SELECT customer_id,user_id from user_customer_mapping WHERE status=1 ".$cond1.$cond3;
			// $result1 = $this->_db->query($sql1)->results();
			// print_r($result1);die;
			return $result1;

	}
	public function caller_page_details($agent_id,$role,$page_no,$limit,$customer_id,$filter_assigned,$user_ids_filter){
			$cond="";
			$cond_for_filter_assign="";
		
			$user_str='';
			// print_r($agent_id);
			if($filter_assigned==1){
				$cond_for_filter_assign=" AND (mo=1 OR tu=1 OR we=1 OR th=1 OR fr=1 OR sa=1 OR su=1)";
			}
			elseif ($filter_assigned==2) {
				$cond_for_filter_assign=" AND mo=0 AND tu=0 and we=0 and th=0 and fr=0 and sa=0 and su=0";
			}
			else{

			}
			if($customer_id==0){

					$user_str='';
					if($user_ids_filter=="null"){
						$user_id_dorm=$this->_user_obj->get_userid($agent_id,$role);
						// print_r($user_id_dorm);die;
						foreach ($user_id_dorm as $key => $value) {
							$user_str.=",'".$value."'";
						}
						$user_str=ltrim($user_str,',');
					}
					else{
						$user_str=$user_ids_filter;
					}
					
				
					 // $user_str=implode(",",$user_id);
					// echo $user_str;
					$cond1="AND user_id IN(".$user_str.")";
				
					 
					$cond3=" LIMIT $page_no,$limit";
					// print_r($cond3);die;
					$sql1="SELECT * from user_customer_mapping WHERE status=1 ".$cond1.$cond_for_filter_assign.$cond3;
					$result1 = $this->_db->query($sql1)->results();
					// print_r($result1);die;
			}
			else{
				$sql1="SELECT * from user_customer_mapping WHERE status=1 and customer_id=$customer_id".$cond_for_filter_assign;
				$result1 = $this->_db->query($sql1)->results();
			}
			// print_r($result1);die;
			return $result1;

	}
	public function update_planner($customer_id,$update_column,$updated_value){
		if($update_column=='time_to' || $update_column=='time_from'){
			$updated_value=trim($updated_value,"'"); 
			$updated_value  = date("H:i", strtotime($updated_value));
		}
	
		$sql3 = "UPDATE user_customer_mapping SET $update_column = ? WHERE customer_id = ?";
		$result3 = $this->_db->query($sql3,array($updated_value,$customer_id));
		// print_r($result3);die;
		 
		if($result3->_error){
            return 0;
        }else{
            return 1;
        }
	}

	public function customer_assign($user_id,$customer_id,$assign_condition){

		 if($assign_condition == 1){

		$sql 	= "SELECT * from user_customer_mapping WHERE customer_id = ?";
		$result = $this->_db->query($sql,array($customer_id))->results();
		

		if(empty($result)){
			$sql2 = "INSERT INTO user_customer_mapping(user_id,customer_id) VALUES(?,?)";
			$result2 = $this->_db->query($sql2,array($user_id,$customer_id));
	        //print_r($result);
	        if($result2->_error){
	            return 0;
	        }else{
	            return 1;
	        }
		} // outer if close
		else{

			$sql3 = "UPDATE user_customer_mapping SET user_id = ? WHERE customer_id = ?";
			$result3 = $this->_db->query($sql3,array($user_id,$customer_id));
	        //print_r($result);
	        if($result3->_error){
	            return 0;
	        }else{
	            return 1;
	        }

		}

		} // close outer if condition
		else if($assign_condition == 2){
 			
 			//return $customer_id->customers;
 			$val;

			foreach ($customer_id->customers as $key => $value) {
				# code...
			$sql 	= "SELECT * from user_customer_mapping WHERE customer_id = ?";
			$result = $this->_db->query($sql,array($value))->results();
			

			if(empty($result)){
				$sql2 = "INSERT INTO user_customer_mapping(user_id,customer_id) VALUES(?,?)";
				$result2 = $this->_db->query($sql2,array($user_id,$value));
		        //print_r($result);
		        // if($result2->_error){
		        //     return 0;
		        // }else{
		        //     return 1;
		        // }
			} // outer if close
			else{

				$sql3 = "UPDATE user_customer_mapping SET user_id = ? WHERE customer_id = ?";
				$result3 = $this->_db->query($sql3,array($user_id,$value));
		        //print_r($result);
		        // if($result3->_error){
		        //     return 0;
		        // }else{
		        //     return 1;
		        // }

			}

 			}

 			return 1;

		}

	}  // close function customer assign

	public function rate_customer($user_id,$customer_id,$rating){

		$sql = "SELECT * FROM customer_rating WHERE user_id = ? AND customer_id = ?";
		$result = $this->_db->query($sql,array($user_id,$customer_id))->results();

		if(empty($result)){
			$sql2 = "INSERT INTO customer_rating(user_id,customer_id,rating) VALUES(?,?,?)";
			$result2 = $this->_db->query($sql2,array($user_id,$customer_id,$rating));
			if($result2->_error){
	            return 0;
	        }else{
	            return 1;
	        }

		}
		else{

			$sql3 = "UPDATE customer_rating SET rating = ? WHERE user_id = ? AND customer_id = ?";
			$result3 = $this->_db->query($sql3,array($rating,$user_id,$customer_id));
			if($result3->_error){
	            return 0;
	        }else{
	            return 1;
	        }
		}

	} // close function rate customer

	public function get_rating_customer($customer_id){

		$sql = "SELECT AVG(rating) as rating FROM customer_rating WHERE customer_id = ?";
		$result = $this->_db->query($sql,array($customer_id))->results();
		if(empty($result)){
			return 0;
		}else{
			//print_r($result[0]->rating);
			return $result[0]->rating;
		}
	}  // close function get rating

	public function add_promotion_csv(){

		$sql = "";

	}  // close function add promotion csv

	public function get_customer_number($user_id){

		$sql = "SELECT customer_number FROM customer_number_save WHERE TIMESTAMPDIFF(SECOND,created_date,NOW()) <30 AND user_id = '$user_id'";
		$result = $this->_db->query($sql)->results();
		if(empty($result)){
			return 0;
		}else{
			return $result[0]->customer_number;
		}
	}
	public function get_multiple_customer(){
		$sql = "SELECT customer_name,customer_value FROM multiple_customer WHERE status=1";
		$result = $this->_db->query($sql)->results();
		if(empty($result)){
			return 0;
		}else{
			return $result;
		}
	}


}