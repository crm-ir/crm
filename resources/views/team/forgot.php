<?php 
$page_name="reset_password";    

$path = $_SERVER['DOCUMENT_ROOT']."/crm/resources/views/header.php";
include_once($path); 

 ?>

 <link href="<?php echo ROOT_URI; ?>/assets/css/team.css" rel="stylesheet" type="text/css">

    <section class="padding-top-20">
  
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 ">
           <h1 class="text-center margin-top-70">Enteroteam.com</h1>
        </div>
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 margin-top-50">
        	<div class="col-md-4 col-lg-4 col-sm-4 col-xs-0 ">
            </div>
        	<div class="col-md-4 col-lg-4 col-sm-4 col-xs-12 ">
                <h3 class="text-center margin-top-40">Reset Password</h3>
                <form  id="start_pass_sec">
                    <div class="form-group">
                        <label for="phone">Phone:</label>
                        <input type="phone" class="form-control" id="phone" placeholder="Enter phone number">
                    </div>
                    <div class="form-group display-none" id="password_sec">
                        <label for="password">OTP:</label>
                        <input type="password" class="form-control " id="password" placeholder="Enter OTP">
                    </div>
                    <div class="form-group">
                        
                        <a  id="gen_otp" class="btn btn-default brand-btn  add-mem-btn margin-top-20 pull-right">Submit</a>
                         <a  id="sub_sec" class="btn btn-default brand-btn display-none width-btn-100 add-mem-btn margin-top-20 pull-right">Submit OTP</a>
                    </div>
                </form>
                <form  id="set_pass_sec" class="display-none">
                    
                    <div class="form-group " id="password_sec">
                        <label for="password">New Password:</label>
                        <input type="password" class="form-control " id="password1"  >
                    </div>
                    <div class="form-group " id="password_sec">
                        <label for="password">Confirm Password:</label>
                        <input type="password" class="form-control " id="password2"  >
                    </div>
                    <div class="form-group">
                        
                        <a  id="set_pass" class="btn btn-default brand-btn  add-mem-btn margin-top-20 pull-right">Submit</a>
                          
                    </div>
                </form>
        		<a href="<?php echo DOMAIN_BASE;?>/resources/views/team/sidebar.php?panel=6" class="btn btn-default brand-btn add-mem-btn pull-left margin-top-20">Login</a>
            
        	</div>
        </div>
    </section >
    <script type="text/javascript">
    $(document).ready(function() {
        $(document).on('click','#gen_otp',function(){
              var phone=$("#phone").val();
              
                $("#phone").prop('disabled', true);
               $("#password_sec").show();
               $("#sub_sec").show();
               $("#gen_otp").hide();

              if(phone=='' || phone==' '){
                 toast_it("phone number is mandatory");
                  return false;
              }
               

               
              $.ajax({
                      url: '<?php echo ROOT_URI; ?>/resources/services/generate_otp.php',
                      method: 'POST',
                      data: "{\n\"phone\":\""+phone+"\", \n\"action\":\"generate_otp\"\n}",
                      dataType: 'json', 
                    }).success(function(resp) {
                           // console.lo
                          if(resp.response_code==400){
                            toast_it("Something went wrong, Try again");
                          }
                          else{
                            if(resp.response_status==1){
                              // $('#add_member_modal').modal('hide');
                              
                              toast_it("OTP sent successfully");
                            }
                            else if(resp.response_status==2){
                                toast_it("please enter 10 digit phone number");
                            } else{}
                            

                              
                                            
                          }
                });//ajax 


        });

        $(document).on('click','#sub_sec',function(){
              var otp=$("#password").val();
              var phone=$("#phone").val();
              

              if(otp=='' || otp==' '){
                 toast_it("OTP is mandatory");
                  return false;
              }
               

               
              $.ajax({
                      url: '<?php echo ROOT_URI; ?>/resources/services/check_otp.php',
                      method: 'POST',
                      data: "{\n\"phone\":\""+phone+"\",\n\"otp\":\""+otp+"\", \n\"action\":\"check_otp\"\n}",
                      dataType: 'json', 
                    }).success(function(resp) {
                           // console.lo
                          if(resp.response_code==400){
                            toast_it("Something went wrong, Try again");
                          }
                          else{
                            if(resp.response_status==1){
                              // $('#add_member_modal').modal('hide');
                              
                              // toast_it("OTP sent successfully");
                              $("#set_pass_sec").show();
                              $("#start_pass_sec").hide();

                            }
                            else if(resp.response_status==2){
                                toast_it("Wrong OTP, Please enter valid OTP");
                            } else{}
                            

                              
                                            
                          }
                });//ajax 


        });
        $(document).on('click','#set_pass',function(){
              var pass=$("#password1").val();
              var pass1=$("#password2").val();
              var phone=$("#phone").val();
            
              if(pass1=='' || pass1==' '){
                 toast_it("Please enter password");
                  return false;
              }

              if(pass=='' || pass==' '){
                 toast_it("Please enter password");
                  return false;
              }
              if(pass1!=pass){
                 toast_it("Password not matched");
                  return false;
              }
               

               
              $.ajax({
                      url: '<?php echo ROOT_URI; ?>/resources/services/reset_password.php',
                      method: 'POST',
                      data: "{\n\"phone\":\""+phone+"\",\n\"password\":\""+pass+"\", \n\"action\":\"reset_password\"\n}",
                      dataType: 'json', 
                    }).success(function(resp) {
                           // console.lo
                          if(resp.response_code==400){
                            toast_it("Something went wrong, Try again");
                          }
                          else{
                            if(resp.response_status==1){
                              // $('#add_member_modal').modal('hide');
                              
                               toast_it("Password changed successfully");
                              
                            }
                            else if(resp.response_status==2){
                                toast_it("User does not exist with this Phone number");
                            } else{}
                            

                              
                                            
                          }
                });//ajax 


        });
            
            

    });
     
    </script>
    </body>
</html>