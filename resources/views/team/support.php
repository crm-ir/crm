	 <?php 
  $path_init = $_SERVER['DOCUMENT_ROOT']."/crm/resources/init.php";
include_once($path_init);

 ?>

<link href="<?php echo ROOT_URI; ?>/resources/lib/select2/select2.min.css" rel="stylesheet" />
<link href="<?php echo ROOT_URI; ?>/assets/css/team.css" rel="stylesheet" type="text/css">
<script src="<?php echo ROOT_URI; ?>/resources/lib/select2/select2.min.js"></script>
<link href="<?php echo ROOT_URI; ?>/resources/lib/timepicker/mdtimepicker.css" rel="stylesheet" type="text/css">

<!-- right side main body starts -->
<div class="col-md-10 col-lg-10 col-sm-10 col-xs-10 col-lg-offset-2 col-md-offset-2 col-sm-offset-2 col-xs-offset-2 no-lr-pad back-color-grey">
	<!-- main section starts here contains upper part and lower part of cases page, cases detail section is outside this section -->
	<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad " id="main-section-cases">
		 <!-- for upper part -->
	
	    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 back-color-white padding-top-13 border-bottom padding-left-40">
	        <p class="font-size-24 float-inline"><b>Help & Support</b></p>
	    </div>
	   <!-- upper part ends -->

	     <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 margin-top-10 padding-left-40 padding-right-40" id="main-section-support">
	     
	     	<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 padding-top-30 ">
	     		<div class="col-lg-12 col-md-12 margin-bottom-20 support-tile back-color-white ">
		     		<div class="col-lg-11 margin-top-15">
		     		<p class="font-size-16 text-bold">Question number 01 goes in here?</p>
		     		<p class="font-size-14 margin-top-5 padding-bottom-10 collapse" id="answer">	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pretium pretium tempor. Ut eget imperdiet neque. In volutpat ante semper diam molestie, et aliquam erat laoreet.</p>				
		     		</div>
		     		<div class="col-lg-1 margin-top-15"><i class="fa fa-chevron-down rotate" data-toggle="collapse" data-target="#answer"></i></div>
		     	</div>
	     		<?php 
	     		for($i=0 ; $i < 10; $i++) {

	     		?>
	     		<div class="col-lg-12 col-md-12 margin-bottom-10 support-tile support-tile-inactive ">
		     		<div class="col-lg-11 margin-top-15">
		     		<p class="font-size-16 text-bold">Question number 01 goes in here?</p>
		     		<p class="font-size-14 padding-bottom-10 collapse" id="answer">	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pretium pretium tempor. Ut eget imperdiet neque. In volutpat ante semper diam molestie, et aliquam erat laoreet.</p>				
		     		</div>
		     		<div class="col-lg-1 margin-top-15"><i class="fa fa-chevron-down rotate" data-toggle="collapse" data-target="#answer"></i></div>
		     	</div>
		     	<?php } ?>
	     	</div>
	     	<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 no-lr-pad" >
	     		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 back-color-white no-lr-pad padding-bottom-10">
	     			<div class="col-lg-12 col-md-12 border-bottom">
		     			<p class="text-bold font-size-20 padding-top-10">Write to us</p>
		     		</div>
		     		<div class="col-lg-12 col-md-12 no-lr-pad ">
		     			<textarea id="query_text" class="cust-textarea lr-margin-15 margin-top-10 " rows="7" ></textarea><hr>	
		     			 <button type="button" id="contact_us" class="btn btn-default brand-btn btn-send-query margin-bottom-20 lr-margin-15">Send us your query</button>
		     		</div>	     			
	     		</div>
	     		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	     			<p class="text-center color-grey-1 padding-top-20">Or Contact Cutomer Care</p>
	     			<p class="font-size-24 text-bold text-center">1800 00 0000</p>
	     		</div>
	     		

	     	</div>
	     </div>

	</div>
</div>

<script src="<?php echo ROOT_URI; ?>/resources/lib/timepicker/mdtimepicker.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
 	 $(".rotate").click(function(){
    		$(this).toggleClass("down"); 
    		$(this).css('color',blue);
	 });

 	$(document).on('click','#contact_us',function(){

 	var query_org = $('#query_text').val();
 	var query= JSON.stringify(query_org);

 	 $.ajax({
	   url : "<?php echo ROOT_URI; ?>/resources/services/team_api.php",
	   type : 'POST',
	   data : "{\n\"query\":"+query+",\n\"user_id\":\""+sessionStorage.getItem('user_id')+"\",\n\"action\":\"help_and_support\"\n}",

	   processData: false,  // tell jQuery not to process the data
	   contentType: false,  // tell jQuery not to set contentType 
	   dataType: 'JSON',
	         success: function(response) {

	          if(response.response_code == 200){
	            toast_it("Query Submitted");
	              setInterval(function(){ location.reload();}, 1200);
	          }
	          
	         }
	         // error: function() {  
	         //   toast_it("There was an error.");
	         // }
	   }); //ajax close
 	});


  });
 
</script>
 

 