 <?php 
  $path_init = $_SERVER['DOCUMENT_ROOT']."/crm/resources/init.php";
include_once($path_init);

 ?>

  <link href="<?php echo ROOT_URI; ?>/resources/lib/select2/select2.min.css" rel="stylesheet" />
  <link href="<?php echo ROOT_URI; ?>/assets/css/team.css" rel="stylesheet" type="text/css">
  <script src="<?php echo ROOT_URI; ?>/resources/lib/select2/select2.min.js"></script>
  <link href="<?php echo ROOT_URI; ?>/resources/lib/timepicker/mdtimepicker.css" rel="stylesheet" type="text/css">
 

  

<!-- right side main body starts -->
<div class="col-md-10 col-lg-10 col-sm-10 col-xs-10 col-lg-offset-2 col-md-offset-2 col-sm-offset-2 col-xs-offset-2 no-lr-pad back-color-grey">
  <!-- main section starts here contains upper part and lower part of task page, task detail section is outside this section -->
      <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad " id="main-section-task">
   
           <!-- for upper part -->
  
            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 back-color-white padding-top-13 border-bottom padding-left-40">
                <p class="font-size-24 float-inline"><b>Call Planner</b></p>
                <p class="font-size-14 float-inline margin-left-30"><button class="btn btn-default brand-btn width-auto" id="design_btn_up_task">Upload bulk call/task planner</button></p> <input type="file" class="flow-control display-none" style="display: none !important;" accept=".csv" name="upload_task" id="upload_task">
                <p  id="download_sample" class="font-size-14 float-inline margin-left-30 margin-top-5"><a href="user_task_mapping.csv" target="_blank">Sample sheet for bulk call/task planner upload</a></p>
                
            </div>
           <!-- upper part ends -->
      </div>
      <!-- main part starts -->
      <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 margin-top-10 padding-left-40 padding-right-40 min-height-nrml">
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
            <div class="form-group col-md-4 col-lg-4 col-sm-4 col-xs-4 margin-top-10" id="assign_div">
                    
                      <select class="form-control tag-for-cust js-data-select-ajax-assign  width-100" name="tags_for_assign[]" id="customer_display">
                      </select>
            </div>
            <div class="col-md-3 col-lg-3 col-sm-3 col-xs-3   margin-top-10"  >
                <div class="dropdown float-inline margin-left-25 margin-top-2" >
                  <button class="btn btn-default brand-btn-white add-mem-btn1 btn-drop-dwn  width-auto dropdown-toggle" type="button" data-toggle="dropdown" id="drop-d-action-task"><img class="" src="<?php echo ROOT_URI; ?>/assets/img/assets/filter.svg"> <span class="padding-left-5">Filter By Agent Name </span>
                    
                  <span class="caret"></span></button>
                  <ul class="dropdown-menu pull-right" id="append_users_filter" >
                
                      
                  </ul>
                </div>

             </div>
            <div class="col-md-5 col-lg-5 col-sm-5 col-xs-5">
                <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6 margin-top-10">
                      <div class="filt-chk checkbox margin-top-8">
                        <label><input type="checkbox" class="filter_assign" value="1"><span class="margin-left-10"><b>Call Planner Assigned</b></span></label>
                        <input type="hidden" id='filter_reference' data-filter_refer="0" data-page_refer="1">
                      </div>
                </div>
                <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6 margin-top-10">
                      <div class="filt-chk checkbox margin-top-8">
                        <label><input type="checkbox" class="filter_assign" value="2"><span class="margin-left-10"><b>Call Planner Unassigned</b></span></label>
                      </div>
                </div>
            </div>
        </div>
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 back-color-white">
          <!-- for headlines -->
            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
                <div class="col-md-3 col-lg-3 col-sm-3 col-xs-3 ">
                      <h6 class="color-grey"><strong>CUSTOMER</strong></h6>
                </div> 
                <div class="col-md-2 col-lg-2 col-sm-2 col-xs-2 ">
                      <h6 class="color-grey"><strong>ASSIGNED TO</strong></h6>
                </div> 
                <div class="col-md-2 col-lg-2 col-sm-2 col-xs-2 ">
                      <h6 class="color-grey"><strong>PREFERED TIME</strong></h6>
                </div> 
                <div class="col-md-5 col-lg-5 col-sm-5 col-xs-5 ">
                      <h6 class="color-grey"><strong>DAYS</strong></h6>
                </div>    
            </div>
            <!-- for main data -->
            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad border-top " id="append_customer">
               
            </div>
            <a class="comment_loader_message margin-top-10 margin-bottom-10 col-sm-12 col-xs-12 col-md-12 col-lg-12 a-deco-grey text-center font-size-12"><u>Load More</u></a>

        </div>
      </div>

      <!-- main part ends -->

       <!-- Modal -->
         <div id="reassign-modal" class="modal fade" role="dialog">
           <div class="modal-dialog margin-top-70">
            <!-- Modal content-->
            <div class="modal-content col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
              <div class="modal-header col-md-12 col-lg-12 col-sm-12 col-xs-12 ">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <p class="font-size-20 margin-left-10"><b>Re-assign</b></p>
              </div>
              <div class="modal-body col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
                <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                         
                      <div class="form-group col-md-12 col-lg-12 col-sm-12 col-xs-12 select-ajax-assign">
                        <p class="font-size-12 color-grey-1 margin-bottom--15"  for="customer_sel">Assign To</p><br>
                          <select class=" form-control tag-for-cust  js-data-select-ajax-assign width-100 agent_display" id="user_id_assign" name="tags_for_assign[]">
                          </select>
                      </div>
                                       
                </div>
              </div>
              <div class="modal-footer col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
                <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 padding-left-30 padding-right-30">
                    <button type="button" class="btn btn-default pull-left add-mem-btn1" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-default pull-right brand-btn add-mem-btn width-88 confirm_assign_button" data-customer_id="" data-flag="" >Confirm</button>
                </div>
              </div>
            </div>

          </div>
        </div>
        <!-- modal1 ends here -->

</div>
        
    <script src="<?php echo ROOT_URI; ?>/resources/lib/timepicker/mdtimepicker.js"></script>
  
    <script type="text/javascript">
    $(document).ready(function() {
       var filter_assigned = 0;
       var customer_id_er = null;
       var user_ids_filter = null;
       var user_role_task = sessionStorage.getItem("role");
           if(user_role_task != 2){
              $('#design_btn_up_task').hide();
              $('#download_sample').hide();
               

        }
      $(document).on('click', '#design_btn_up_task', function () {
        $("#upload_task").click();
      });
      $(document).on('change', '#upload_task', function () {
           var fd = new FormData();
          fd.append('file', this.files[0]); // since this is your file input
          fd.append('action', "upload_task_bulk"); // since this is your file input

          $.ajax({
              url: "<?php echo ROOT_URI; ?>/resources/services/bulk_task.php",
              type: "post",
              dataType: 'json',
              processData: false, // important
              contentType: false, // important
              data: fd,
              success: function(resp) {
                  // alert(resp);
                  if(resp.response_code==200){
                      toast_it("Customers/tasks assigned successfully");
                  }
                  else{
                      toast_it("error occured,contact admin");

                  }
              },
              error: function() {
                  // alert("An error occured, please try again.");         
              }
          });
      });

      $(document).on('click', '.cir-passive', function () {

          $(this).toggleClass("cir-chk");
          $(this).children("input").prop("checked", ! $(this).children("input").prop("checked"));
          if($(this).children("input").is(":checked")==true){
              update_planner($(this).attr("data-customer_id"),$(this).attr("data-update_column_type"),1);

          }
          else{
              update_planner($(this).attr("data-customer_id"),$(this).attr("data-update_column_type"),0);

          }
      
      });
      
      $(document).on('change', '.timepicker', function () {
           
          update_planner($(this).attr("data-customer_id"),$(this).attr("data-update_column_type"),$(this).val());

      });
      //checkbox script for assigned and not assigned
      $(document).on('change', '.filter_assign', function () {
   
          var this_ob=$(this).is(":checked");
         
          $('.filter_assign').prop('checked', false);
          if(this_ob==true){
              $('#filter_reference').attr('data-filter_refer',$(this).val());
             display_customer(1,customer_id_er,$(this).val(),user_ids_filter);
            $('#filter_reference').attr('data-page_refer',1); 
            $(this).prop('checked', true);
          }else{
              $('#filter_reference').attr('data-filter_refer',0);
             display_customer(1,customer_id_er,0,user_ids_filter);
            $('#filter_reference').attr('data-page_refer',1);
            $(this).prop('checked', false);
          }
          
         
      }); 

      // script for the filter agent
      $(document).on('change', '.filter_agents', function () {

            user_ids_filter='';
          

          $('.filter_agents').filter(':checked').each(function() {
                  
                user_ids_filter = user_ids_filter+'\''+$(this).val()+'\',';
                   
          });
          user_ids_filter = user_ids_filter.substring(0,(user_ids_filter.length-1));
          display_customer(1,customer_id_er,filter_assigned,user_ids_filter);
         
      }); 

         
        $('#customer_display').select2({
              ajax: {
                   url: "<?php echo ROOT_URI; ?>/resources/services/tag_api.php",
                   dataType: 'json',
                   delay: 350,
                   data: function (params) {
                       return {
                           q: params.term, // search term
                             page: params.page || 1,
                             token: sessionStorage.getItem("token"),
                             dist_id:sessionStorage.getItem('dist_id')
                       };
                   },
                   processResults: function (data, params) {
                        params.page = params.page || 1;
                       return {
                           results: data.results,
                           pagination: {
                             more: (params.page * 10) < data.total_count
                           }

                       };
                   },
                   cache: true
               },
               
               tags: true,
               placeholder: "Select Customer"
       });

        // toast_it();

           //get value of select2
        $('#customer_display').on('change', function() {
            var data = $("#customer_display option:selected").val();
             display_customer(1,data,filter_assigned,user_ids_filter);
        });  

        display_customer(1,customer_id_er,filter_assigned,user_ids_filter);
        var page_count = $('#filter_reference').attr('data-page_refer');
               
        $(document).on('click', '.comment_loader_message', function () {
             var page_count = $('#filter_reference').attr('data-page_refer');
            display_customer(++page_count,customer_id_er,$('#filter_reference').attr('data-filter_refer'));
            $('#filter_reference').attr('data-page_refer',page_count);
        });
        function display_customer(page_no,customer_id=null,filter_assigned=0,user_ids_filter=null){
          if(customer_id!=null){
            customer_id_to_send=customer_id;
          }
          else{
             customer_id_to_send=0;
          }
              $.ajax({
                       url: "<?php echo ROOT_URI; ?>/resources/services/customer_api.php",
                       dataType :'json',
                       method : 'POST',
                       async:false,
                       data: '{\n\"action\":\"display_call_planner\",\n\"token\":\"'+sessionStorage.getItem("token")+'\",\n\"user_id\":\"'+sessionStorage.getItem('user_id')+'\",\n\"role\":'+sessionStorage.getItem('role')+',\n\"customer_id\":'+customer_id_to_send+',\n\"filter_assigned\":'+filter_assigned+',\n\"user_ids_filter\":"'+user_ids_filter+'",\n\"page_no\":'+page_no+'}',
                       }).success(function(resp){
                        // console.log(resp);
                        if(resp.response_code==200){
                            $('.comment_loader_message').show();

                                  if(!(resp.data).length){
                                    toast_it("No more data to show");
                                  }

                                  var htmlText ="";

                                  for(i=0;i<resp.data.length;i++)
                                  {
                                     
                                      htmlText+='<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad border-bottom padding-top-10 padding-bottom-10">'+
                                          '<div class="col-md-3 col-lg-3 col-sm-3 col-xs-3 no-lr-pad">'+
                                               '<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 text-left no-lr-pad margin-top-10">'+
                                                        '<p class="font-size-16 margin-right-8  ellipsis" data-original-title="'+resp.data[i].customer_name+'" data-toggle="tooltip"><b>'+resp.data[i].customer_name+'</b>'+
                                                    '</div>'+
                                                    '<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 text-left no-lr-pad">'+
                                                        '<p    class="  color-grey-1 custm_p_add ellipsis"   data-original-title="'+resp.data[i].customer_address+'" data-toggle="tooltip"><i class="fa fa-map-marker" aria-hidden="true"></i> '+resp.data[i].customer_address+'</p>'+
                                  
                                                    '</div>'+
                                          '</div> '+
                                          '<div class="col-md-2 col-lg-2 col-sm-2 col-xs-2 margin-top-15">';

                                              if(resp.data[i].assigned_user_id){
                                  htmlText += '<div class="assign-my-task-div">'+
                                                '<img src="<?php echo S3_BUCKET; ?>'+resp.data[i].assigned_user_profile_pic+'" class="float-inline user_assign-img">'+
                                                '<p class="font-size-12   margin-top-6 padding-left-5  ellipsis"><b> '+resp.data[i].assigned_user_name+'</b></p>'+
                                              '</div>';

                                              }else{

                                  htmlText += '<div class="assign-my-task-div a-deco resassign_modal_cust" data-customer_id="'+resp.data[i].customer_id+'">'+
                                                '<p class="font-size-12 margin-top-6 padding-left-5  ellipsis text-danger"><b> Assign Customer</b></p>'+
                                              '</div>';
                                              }

                                              
                                  htmlText +=  '</div> '+
                                          '<div class="col-md-2 col-lg-2 col-sm-2 col-xs-2  ">'+
                                              '<input name="time_from" class="form-control back-color-white timepicker" value="'+((resp.data[i].time_from != null) ? resp.data[i].time_from : '')+'" style="height:30px;" placeholder="Select from time" type="text" '+((resp.data[i].time_from == 0) ? ' cir-chk ' : '')+'" data-customer_id="'+resp.data[i].customer_id+'" data-update_column_type="time_from"/> '+
                                              '<input name="time_to" value="'+((resp.data[i].time_from != null) ? resp.data[i].time_to : '')+'"  class="margin-top-10 form-control back-color-white timepicker" style="height:30px;" placeholder="Select to time" type="text" '+((resp.data[i].time_to == 0) ? ' cir-chk ' : '')+'" data-customer_id="'+resp.data[i].customer_id+'" data-update_column_type="time_to"/> '+
                                          '</div> '+
                                          '<div class="col-md-5 col-lg-5 col-sm-5 col-xs-5 no-lr-pad margin-top-15" >'+
                                                    '<div class="cir-passive float-inline margin-left-10 text-center cursor-pointer '+((resp.data[i].mo == 0) ? ' cir-chk ' : '')+'" data-customer_id="'+resp.data[i].customer_id+'" data-update_column_type="mo">'+
                                                        '<input   class="form-control display-none" type="checkbox" '+((resp.data[i].mo != 0) ? ' checked ' : '')+'  >'+
                                                         'Mo'+
                                                   '</div>'+
                                                   '<div class="cir-passive float-inline margin-left-10 text-center cursor-pointer '+((resp.data[i].tu == 0) ? ' cir-chk ' : '')+'" data-customer_id="'+resp.data[i].customer_id+'" data-update_column_type="tu">'+
                                                       '<input name="choices" class="form-control display-none" type="checkbox" value="2"  '+((resp.data[i].tu != 0) ? ' checked ' : '')+'>'+
                                                        'Tu'+
                                                     '</div>'+
                                                   '<div class="cir-passive float-inline margin-left-10 text-center cursor-pointer '+((resp.data[i].we == 0) ? ' cir-chk ' : '')+'" data-customer_id="'+resp.data[i].customer_id+'" data-update_column_type="we">'+
                                                       '<input name="choices" class="form-control display-none" type="checkbox" value="3"  '+((resp.data[i].we != 0) ? ' checked ' : '')+'>'+
                                                        'We'+
                                                     '</div>'+
                                                   '<div class="cir-passive float-inline margin-left-10 text-center cursor-pointer '+((resp.data[i].th == 0) ? ' cir-chk ' : '')+'" data-customer_id="'+resp.data[i].customer_id+'" data-update_column_type="th">'+
                                                       '<input name="choices" class="form-control display-none" type="checkbox" value="4"  '+((resp.data[i].th != 0) ? ' checked ' : '')+'>'+
                                                        'Th'+
                                                     '</div>'+
                                                    '<div class="cir-passive float-inline margin-left-10 text-center cursor-pointer '+((resp.data[i].fr == 0) ? ' cir-chk ' : '')+'" data-customer_id="'+resp.data[i].customer_id+'" data-update_column_type="fr">'+
                                                       '<input name="choices" class="form-control display-none" type="checkbox" value="0"  '+((resp.data[i].fr != 0) ? ' checked ' : '')+'>'+
                                                       ' Fr'+
                                                    '</div>'+
                                                    '<div class="cir-passive float-inline margin-left-10 text-center cursor-pointer '+((resp.data[i].sa == 0) ? ' cir-chk ' : '')+'" data-customer_id="'+resp.data[i].customer_id+'" data-update_column_type="sa">'+
                                                       '<input name="choices" class="form-control display-none" type="checkbox" value="0"  '+((resp.data[i].sa != 0) ? ' checked ' : '')+'>'+
                                                        'Sa'+
                                                    '</div>'+
                                                    '<div class="cir-passive float-inline margin-left-10 text-center cursor-pointer '+((resp.data[i].su == 0) ? ' cir-chk ' : '')+'" data-customer_id="'+resp.data[i].customer_id+'" data-update_column_type="su">'+
                                                       '<input name="choices" class="form-control display-none" type="checkbox" value="0"  '+((resp.data[i].su != 0) ? ' checked ' : '')+'>'+
                                                       ' Su'+
                                                    '</div>'+
                                                   
                                          '</div>  '+  
                                      '</div>';


                                }
                                //alert(htmlText);
                                if(page_no==1){
                                  $("#append_customer").html(""); 
                                }
                                 $("#append_customer").append(htmlText); 
                                $('.timepicker').mdtimepicker();
                                 $('[data-toggle="tooltip"]').tooltip();
                         }
                          else {
                              $("#append_customer").html("<div class='col-md-12 text-center color-grey margin-top-50 padding-bottom-20 '>Data not found</div>"); 
                              // toast_it(resp.response_message);
                              $('.comment_loader_message').hide();
                          }

              });
          // main ajax ends

        } 

        $(document).on('click','.resassign_modal_cust',function(){

          //toast_it("NEW");
          var customer_id = $(this).attr('data-customer_id');
          // toast_it(customer_id);
          $('.confirm_assign_button').attr('data-customer_id',customer_id);
          $('#reassign-modal').modal('show');
          // $('.confirm_assign_button').attr('data-flag',1);

        });


        $(document).on('click','.confirm_assign_button',function(){


          // if($(this).attr('data-flag')==1){
               
               var customer_id = $(this).attr('data-customer_id');
               var user_id = $('#user_id_assign').val();
               
               assign_customer(customer_id,user_id,1);

         
          // toast_it(user_id);

        }); // close on click assign 


        function assign_customer(customer_id,user_id,assign_condition){

          $.ajax({
             url : "<?php echo ROOT_URI; ?>/resources/services/customer_api.php",
             type : 'POST',
             data : "{\n\"user_id\":\""+user_id+"\",\n\"assign_condition\":\""+assign_condition+"\",\n\"customer_id\":"+customer_id+",\n\"action\":\"assign_customer\"\n}",

             processData: false,  // tell jQuery not to process the data
             contentType: false,  // tell jQuery not to set contentType 
             dataType: 'JSON',
                   success: function(response) {

                    if(response.response_code == 200){
                      toast_it("User Assigned");
                      setInterval(function(){ location.reload();}, 1200);
                    }
                    
                   }
                   // error: function() {  
                   //   toast_it("There was an error.");
                   // }
             }); //ajax close

        }  // close function 

        $('.agent_display').select2({
           
              ajax: {
                    url: "<?php echo ROOT_URI; ?>/resources/services/display_all_customer.php",
                    dataType: 'json',
                    delay: 350,
                    data: function (params) {
                        return {
                            q: params.term, // search term
                              page: params.page || 1,
                              user_id:sessionStorage.getItem('user_id'),
                              role:sessionStorage.getItem('role')
                        };
                    },
                    processResults: function (data, params) {
                         params.page = params.page || 1;
                        return {
                            results: data.results,
                            pagination: {
                              more: (params.page * 5) < data.total_count
                            }

                        };
                    },
                    cache: true
                },
                
                tags: true,
                placeholder: "Select team member"
        });

        //############## ajax for filter users data
        $.ajax({
                 url: "<?php echo ROOT_URI; ?>/resources/services/team_api.php",
                 dataType :'json',
                 method : 'POST',
                 async:false,
                 data: '{\n\"action\":\"display_users_for_call_planner\",\n\"user_id\":\"'+sessionStorage.getItem('user_id')+'\",\n\"role\":'+sessionStorage.getItem('role')+'}',
                 }).success(function(resp){
                  console.log(resp);
                  if(resp.response_code==200){
                        $('.comment_loader_message').show();

                        var htmlText ="";

                        for(i=0;i<resp.data.length;i++){
                             
                            htmlText+='<li>'+
                                        '<a class="drop-sub-chl " >'+
                                          '<div class="filt-chk checkbox margin-top-bot-2">'+
                                            '<label>'+
                                                '<input type="checkbox" class="filter_agents" value="'+resp.data[i].user_id+'">'+
                                                    '<p class="margin-left-10 float-inline">'+
                                                        '<span class="float-inline">'+
                                                           '<img class="img-responsive img-circle" width="24" height="24" src="<?php echo S3_BUCKET; ?>'+resp.data[i].profile_picture+'" >'+
                                                        '</span>'+
                                                      '</p><p class=" ellipsis">'+
                                                        '<span class=" padding-left-5"> '+resp.data[i].name+'</span>'+
                                                    '</p>'+
                                                '</label>'+
                                          '</div>'+
                                        '</a>'+
                                      '</li>';


                        }
                       
                        $("#append_users_filter").append(htmlText); 
                         
                        $('[data-toggle="tooltip"]').tooltip();
                   }
                    else{
                    
                   }

              });
          // ajax ends for filter data

          function update_planner(customer_id,update_column,updated_value){
            $.ajax({
                 url: "<?php echo ROOT_URI; ?>/resources/services/customer_api.php",
                 dataType :'json',
                 method : 'POST',
                 async:false,
                 data: '{\n\"action\":\"update_planner\",\n\"customer_id\":\"'+customer_id+'\",\n\"updated_value\":\"'+updated_value+'\",\n\"update_column\":"'+update_column+'"}',
                 }).success(function(resp){
                  console.log(resp);
                  if(resp.response_code==200){
                       
                      toast_it("Updated successfully");
                   }
                    else{
                      toast_it("Something went wrong, please try again");
                   }

              });
          }

    });
     
    </script>
    </body>
</html>