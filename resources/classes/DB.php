<?php

	class DB 																	
	{
		public $_id;
		private static $_instance = null;
		public $_pdo, $_query, $_results, $_count=0, $error=false;

		private function __construct()							//Instantiates PDO connection, called from getInstance() method		 	
		{
			try
			{
				ini_set('max_execution_time', 3000000);
				$this->_pdo = new PDO('mysql:host='.Config::get('mysql/host').';dbname='.Config::get('mysql/db'),Config::get('mysql/user'),Config::get('mysql/pass'),array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8mb4'"));

			}
			catch(PDOException $e)
			{
				die($e->getMessage());
			}
		}

		public static function getInstance()					//Used to instantiate an object
			{													//Checks if instance already created else creates a new Instance																	
			if(!isset(self::$_instance))
			{
				self::$_instance=new DB();
			}
			return self::$_instance;
		}

		public function query($sql,$params=array())				//Automatically runs query, arguments-($sql - query string, $params - array of parameters to bind)
		{														//Usage - DB::getInstance->query("Select * from Users where id=?",array('1'));
			$this->_error=false;
			if($this->_query=$this->_pdo->prepare($sql))
			{
				$x=1; 
				if(count($params))
				{
					foreach($params as $param)
					{
						$this->_query->bindValue($x,$param);
						$x++;
					}
				
				}
			

				if($this->_query->execute()){
					$this->_results=$this->_query->fetchAll(PDO::FETCH_OBJ);
					$this->_count=$this->_query->rowCount();
				} else {
					$this->_error =true;
					$this->_errorInfo =$this->_query->errorInfo();
				}
			}

			return $this;
		}

		public function results()								//returns results as an object
		{
			return $this->_results;  
		}

		public function first()									//returns the first row of the results
		{
			$var = $this->results();
			$var1 = !empty($var[0]) ? $var[0] : "";
			return $var1;

		}

		public function error()									//Returns true if error
		{
			return $this->_error;
		}
		
		public function last()
		{
			return $this->_id=$this->_pdo->lastInsertId();
		}


	}
?>
