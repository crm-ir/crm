<?php 
  $path_init = $_SERVER['DOCUMENT_ROOT']."/crm/resources/init.php";
include_once($path_init);

 ?>
<link href="<?php echo ROOT_URI; ?>/assets/css/team.css" rel="stylesheet" type="text/css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.min.js"></script>
<!-- right side main body starts -->
<script type="text/javascript">
var user_role1 = sessionStorage.getItem("role");
 
if(user_role1==1){
   
  window.location.href =  location.protocol + '//' + location.host + location.pathname+"?panel=10";
}
</script>
<div class="col-md-10 col-lg-10 col-sm-10 col-xs-10 col-lg-offset-2 col-md-offset-2 col-sm-offset-2 col-xs-offset-2 no-lr-pad back-color-grey">
   <!-- for upper part -->
    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 padding-top-15 border-bottom padding-left-40 back-color-white  ">
        <p class="font-size-24 float-inline"><b>Dashboard</b></p>
    </div>
    <!-- Dashboard tabs -->
     <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 back-color-white padding-top-20 border-bottom padding-left-40">
        <div class="col-lg-offset-3">
            <a class="a-deco color-grey-1 float-inline margin-left-25 display-task-active"  id="performance">My Performance Dashboard</a>
            <a class="a-deco color-grey-1 float-inline margin-left-25" id="team-performance">My Team Performance Dashboard</a>
        </div>
     </div>
     <!-- Dashboard tabs ends here  -->
   <!-- upper part ends -->

  <!-- performance dashboard starts here  -->
  <div class="col-lg-12 col-md-12 tab-pane " id="performance-dashboard">
     <!-- Dashboard headings  -->
    <div class="col-md-12 col-lg-12">
      <p class="text-bold font-size-24 margin-left-40 margin-top-30 margin-bottom-10 pull-left">My Performance Overview</p>
      <div class=" font-size-14 margin-right-40 margin-top-30 margin-bottom-10 pull-right">
        <ul class="nav nav-pills">
          <!-- <li class="active" id="individual_tab_today"><a class="a-deco" id="today_individual_tab" data-time_span="">Today</a></li> -->
          <!-- <li class="" id="individual_tab_week"><a class="a-deco" id="week_individual_tab" href="#">Week</a></li> -->
          <!-- <li class="active"><a href="#">Month</a></li> -->
        </ul>
      </div>
    </div>
    <!-- Dashboard headings ends here  -->
   <!-- Dashboard 1st row starts here  -->
     <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 margin-top-10 padding-left-40 padding-right-40 " id="dashboard">
          <!-- Small Tiles order info  -->
          <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 ">
              <div class="back-color-white dashboard-small-tile box-shadow1  margin-5"> 
                    <p class="color-grey-1 text-center text-uppercase font-size-12 padding-top-20">Customers Ordered</p>
                    <p class="text-bold text-center font-size-36" id="customers_ordered_ind"></p>
                    <!-- <p class="text-success text-center text-bold font-size-14" id="difference_order_ind"></p> -->
              </div> 
          </div>
          <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 ">
              <div class="back-color-white dashboard-small-tile box-shadow1  margin-5"> 
                    <p class="color-grey-1 text-center text-uppercase font-size-12 padding-top-20">Total Ordered Items</p>
                    <p class="text-bold text-center font-size-36" id="total_items_ind" ></p>
                    <!-- <p class="text-danger text-center text-bold font-size-14" id="difference_items_ind">  -->
                     <!--  <a data-toggle="modal" data-target="#ordered-items_modal" class="a-deco">
                      <img src="<?php //echo ROOT_URI; ?>/assets/img/assets/orders-modal.svg" width="15" >
                    </a>  -->
                    </p>
              </div> 
          </div>
          <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 ">
              <div class="back-color-white dashboard-small-tile box-shadow1  margin-5"> 
                    <p class="color-grey-1 text-center text-uppercase font-size-12 padding-top-20">Average Order Value</p>
                    <p class="text-bold text-center font-size-36" id="average_order_value_ind"></p>
                    <!-- <p class="text-success text-center text-bold font-size-14" id="difference_avg_order_value_ind"> </p> -->
              </div> 
          </div>
          <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 ">
              <div class="back-color-white dashboard-small-tile box-shadow1  margin-5"> 
                    <p class="color-grey-1 text-center text-uppercase font-size-12 padding-top-20">Total Order Value</p>
                    <p class="text-bold text-center font-size-36" id="total_order_value_ind"></p>
                    <!-- <p class="text-success text-center text-bold font-size-14" id="difference_total_order_value_ind"></p> -->
              </div> 
          </div>
          <!-- Small Tiles order info ends here  -->    
          <div class="overlay1" id="overlay_ind">
                    <i class="fa fa-spinner fa-pulse fa-3x fa-fw color-white load-pos-c"></i>
                    <span class="sr-only">Loading...</span>
          </div>    
      </div>
     <!-- Dashboard 1st row ends here  -->
   <!-- Dashboard 2nd row starts here -->
     <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 margin-top-10 pad-left-right-55">
            <!-- Call summary starts here  -->
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  padding-top-15 padding-left-15 back-color-white box-shadow1 ">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 "> 
                    <p class="text-bold text-uppercase">All Call Summary</p>
                </div>               
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 pull-right"> 
                    <!-- <p class="font-size-14"> <a href="#" class="a-deco pull-right">View Full Report <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a></p> -->
                </div>  
                  <div class="col-lg-9 col-md-9 col-sm-12 col-xs-6 padding-bottom-20">
                    <canvas id="callsummary" width="400" height="170"></canvas>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-12 col-xs-6 ">
                    <div class="border-around-1 dashboard-small-tile box-shadow1  margin-top-30 pad-10">
                      <p class="color-grey-1 text-center text-uppercase font-size-12 padding-top-20" id="time_span_status_indi">Number of calls Today</p>
                      <p class="text-bold text-center font-size-36" id="call_today_1">30</p>
                      <p class="text-success text-center text-bold font-size-14" id="call_difference"></p>
                    </div>            
                  </div>     
            </div>
            <!-- call summary ends here  -->
      </div>
   <!-- Dashboard 2nd row ends here  -->
     <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 margin-top-10 pad-left-right-55 mar gin-bottom-20 ">
          <!-- Perform and Task summary starts here  -->
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-left-15 back-color-white box-shadow1 ">
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 border-right">
                <div class="padding-top-15 margin-bottom-20">
                  <p class="text-bold text-uppercase"> Your Performance</p>
                  <canvas id="performance_chart" width="400" height="400"></canvas>
                </div>
              </div>     
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="padding-top-15 padding-bottom-15">  <p class="text-bold text-uppercase pull-left">Today's Task Summary</p>
                <!-- <p class=""> <a href="#" class="a-deco pull-right">See All Tasks <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a></p> -->
                <br>
                <br>
                    <p class="text-center">All Tasks: <span id="all_tasks_indi"></span></p>
                    <p class="text-center">Pending:   <span id="pending_indi"></span></p>
                    <p class="text-center">Completed: <span id="completed_indi"></span></p>
                    <p class="text-center">Follow Up: <span id="follow_up_indi"></span></p>

                <!-- <canvas id="task-summary_chart" width="400" height="200"></canvas> -->
              </div>
              </div>     
          </div>
          <!-- Performance and Task summary ends here  -->
      </div>
     <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 margin-top-10 pad-left-right-55 margin-bottom-20">
          <!-- Last 5 Task Summary -->
          <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 no-lr-pad back-color-white box-shadow1 ">
              <div class="" style="">
                <div class="col-lg-12 border-bottom">
                  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="padding-top-15 margin-bottom-20 ">
                      <p class="text-bold text-uppercase"> Last 5 Task Summary</p>
                    </div>
                  </div>     
                  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="padding-top-15 margin-bottom-20">
                      <!-- <p class=""> <a href="#" class="a-deco pull-right">View Today's Report <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a></p> -->
                    </div>
                  </div>  
                </div>
                
                <div class="col-lg-12 col-md-12 ">
                  <div class="table-responsive"><table class="table table-hover">
                      <tbody id="append_last_5_task_indv">
                            <!-- <tr>
                              <td class="font-size-14">Health Industry Pvt Ltd
                                 <br> <span class="font-size-12 color-grey">Called @ 2 P.M, 2nd</span>
                              </td>
                              <td>50 Items</td>
                              <td><span class="order-status not-ordered a-deco">Not Ordered</span></td>
                              <td><i class="fa fa-inr" aria-hidden="true"></i> 8000</td>
                            </tr><tr>
                              <td class="font-size-14">Health Industry Pvt Ltd
                                 <br> <span class="font-size-12 color-grey">Called @ 2 P.M, 2nd</span>
                              </td>
                              <td>50 Items</td>
                              <td><span class="order-status ordered a-deco">Ordered</span></td>
                              <td><i class="fa fa-inr" aria-hidden="true"></i> 9000</td>
                            </tr><tr>
                              <td class="font-size-14">Health Industry Pvt Ltd
                                 <br> <span class="font-size-12 color-grey">Called @ 2 P.M, 2nd</span>
                              </td>
                              <td>50 Items</td>
                              <td><span class="order-status call-not-picked a-deco">Call Not Picked</span></td>
                              <td><i class="fa fa-inr" aria-hidden="true"></i> 900</td>
                            </tr><tr>
                              <td class="font-size-14">Health Industry Pvt Ltd
                                 <br> <span class="font-size-12 color-grey">Called @ 2 P.M, 2nd</span>
                              </td><td>50 Items</td>
                              <td><span class="order-status ordered a-deco">Ordered</span></td>
                              <td><i class="fa fa-inr" aria-hidden="true"></i> 9000</td>
                            </tr> -->
                      </tbody>
                    </table>
                  </div>
                </div>      
              </div>
          </div>  
          <!-- Last 5 Task Summart ends here  -->
          <!-- Top 10 brands summary -->
         <!--  <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 padding-right-0" >
                 <div class="col-lg-12 back-color-white box-shadow1  no-lr-pad" style="">
                    <div class="col-lg-12 border-bottom">
                      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 ">
                        <div class="padding-top-15 margin-bottom-20 ">
                          <p class="text-bold text-uppercase"> TOP 10 BRANDS</p>
                        </div>
                      </div>     
                      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="padding-top-15 margin-bottom-20 pull-right">
                          <p class="font-size-14"> <a href="#" class="a-deco pull-right">View Brand's Report <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a></p>
                        </div>
                      </div>
                    </div>
                     
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 margin-top-20" id="append_brand_list_ind">
                      
                    </div>    
                </div>
          </div>   -->
          <!-- Top 10 brands summary ends here -->
     </div> 
  </div>
   
  <!-- performance dashboard ends here  -->


  <!-- team performance dashboard  -->

    <div class="col-lg-12 col-md-12 " id="team-performance-dashboard"> 
      <div class="col-lg-8 col-md-8">
          <!-- team performace Dashboard headings  -->
         <div class="col-md-12 col-lg-12 no-lr-pad">
          <p class="text-bold font-size-24 margin-top-30 margin-bottom-10 pull-left">Showing Data for all users</p>
          <div class=" font-size-14 margin-top-30 margin-bottom-10 pull-right">
            <ul class="nav nav-pills">
              <!-- <li class="active" id="tab_switch_today"><a class="a-deco" id="todays_team_tab" data-time_span="1">Today</a></li> -->
              <!-- <li id="tab_switch_week"><a class="a-deco" id="weeks_team_tab">Week</a></li> -->
              <!-- <li ><a href="#">Month</a></li> -->
            </ul>
          </div>
          </div>
          <!-- team performance Dashboard headings ends here  -->
          <!-- Dashboard 1st row starts here  -->
         <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad margin-top-10" id="">
            <div class="row">
              <!-- Small Tiles order info  -->
              <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 ">
                  <div class="back-color-white dashboard-small-tile box-shadow1  margin-5"> 
                        <p class="color-grey-1 text-center text-uppercase font-size-12 padding-top-20">Customers Ordered</p>
                        <p class="text-bold text-center font-size-36" id="customers_ordered_team"></p>
                        <!-- <p class="text-success text-center text-bold font-size-14" id="difference_order_team"> </p> -->
                  </div> 
              </div>
              <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 ">
                  <div class="back-color-white dashboard-small-tile box-shadow1  margin-5"> 
                        <p class="color-grey-1 text-center text-uppercase font-size-12 padding-top-20">Total Ordered Items</p>
                        <p class="text-bold text-center font-size-36" id="total_items_team"></p>
                        <!-- <p class="text-danger text-center text-bold font-size-14" id="difference_items_team">  -->
                          <!-- <a data-toggle="modal" data-target="#ordered-items_modal" class="a-deco">
                          <img src="<?php// echo ROOT_URI; ?>/assets/img/assets/orders-modal.svg" width="15" >
                        </a>  -->
                        </p>
                  </div> 
              </div>
              <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 ">
                  <div class="back-color-white dashboard-small-tile box-shadow1  margin-5"> 
                        <p class="color-grey-1 text-center text-uppercase font-size-12 padding-top-20">Average Order Value</p>
                        <p class="text-bold text-center font-size-36" id="average_order_value_team"></p>
                        <!-- <p class="text-success text-center text-bold font-size-14" id="difference_avg_order_value_team"> </p> -->
                  </div> 
              </div>
              <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 margin-top-20">
                  <div class="back-color-white dashboard-small-tile box-shadow1  margin-5"> 
                        <p class="color-grey-1 text-center text-uppercase font-size-12 padding-top-20">Total Order Value</p>
                        <p class="text-bold text-center font-size-36" id="total_order_value_team"></p>
                        <!-- <p class="text-success text-center text-bold font-size-14" id="difference_total_order_value_team"> </p> -->
                  </div> 
              </div>

              <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 margin-top-20">
                  <div class="back-color-white dashboard-small-tile box-shadow1  margin-5"> 
                        <p class="color-grey-1 text-center text-uppercase font-size-12 padding-top-20">Cases Resolved</p>
                        <p class="text-bold text-center font-size-36" id="append_cases_resolved"></p>
                        <p class="text-danger text-center text-bold font-size-14"><span id="append_cases_unresolved">0</span> cases still pending to resolve </p>
                  </div> 
              </div>
              <!-- Small Tiles order info ends here  -->
            </div>
               
                <div class="overlay1" id="overlay_team">
                    <i class="fa fa-spinner fa-pulse fa-3x fa-fw color-white load-pos-c"></i>
                    <span class="sr-only">Loading...</span>
                </div>
                          
          </div>
         <!-- Dashboard 1st row ends here  -->
       <!-- Dashboard 2nd row starts here -->
         <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad margin-top-20 margin-bottom-20 ">
                <!-- Call summary starts here  -->
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  no-lr-pad padding-top-15 back-color-white box-shadow1 ">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"> 
                        <p class="text-bold text-uppercase">All Call Summary</p>
                    </div>               
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 pull-right"> 
                        <!-- <p class="font-size-14"> <a href="#" class="a-deco pull-right">View Full Report <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a></p> -->
                    </div>  
                      <div class="col-lg-8 col-md-8 col-sm-12 col-xs-6">
                        <canvas id="callsummary_1" width="400" height="200"><canvas>
                      </div>
                      <div class="col-lg-4 col-md-4 col-sm-12 col-xs-6 ">
                        <div class="border-around-1 dashboard-small-tile box-shadow1  margin-top-10 margin-bottom-10">
                          <p class="color-grey-1 text-center text-uppercase font-size-12 padding-top-20" id="time_span_status_team">Number of calls Today</p>
                          <p class="text-bold text-center font-size-36" id="call_today_tab2"></p>
                          <p class="text-success text-center text-bold font-size-14" id="call_difference2"></p>
                        </div>            
                      </div>     
                </div>
                <!-- call summary ends here  -->
          </div>
       <!-- Dashboard 2nd row ends here  -->
         <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad margin-bottom-20 ">
              <!-- Performance summary starts here  -->
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 margin-bottom-20 back-color-white box-shadow1 ">
                    <div class="padding-top-15 margin-bottom-20">
                      <p class="text-bold text-uppercase"> Performance</p>
                      <canvas id="performance_chart_1" width="400" height="400"></canvas>
                    </div>
                  </div>     
                  <div class="col-lg-12 col-md-6 col-sm-12 col-xs-12 back-color-white box-shadow1 ">
                    <div class="padding-top-15 padding-bottom-15">  <p class="text-bold text-uppercase pull-left">Today's Task</p>
                    <!-- <p class=""> <a href="#" class="a-deco pull-right">See All Tasks <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a></p><br> -->
                    <br>
                    <br>
                    <p class="text-center">All Tasks: <span id="all_tasks_team"></span></p>
                    <p class="text-center">Pending:   <span id="pending_team"></span></p>
                    <p class="text-center">Completed: <span id="completed_team"></span></p>
                    <p class="text-center">Follow Up: <span id="follow_up_team"></span></p>

                    <!-- <canvas id="task-summary_chart_1" width="400" height="200"></canvas> -->
                  </div>
                  </div>     
              <!-- Performance summary ends here  -->
          </div>
              <!-- Last 5 Task Summary -->
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-lr-pad margin-top-20 back-color-white box-shadow1 ">
                  <div class="" style="">
                    <div class="col-lg-12 border-bottom">
                      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="padding-top-15 margin-bottom-20 ">
                          <p class="text-bold text-uppercase"> Last 5 Task Summary</p>
                        </div>
                      </div>     
                      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="padding-top-15 margin-bottom-20">
                          <!-- <p class=""> <a href="#" class="a-deco pull-right">View Task's Report <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a></p> -->
                        </div>
                      </div>  
                    </div>
                    
                    <div class="col-lg-12 col-md-12 ">
                      <div class="table-responsive">
                        <table class="table table-hover">
                          <tbody id="append_last_5_task_team">
                           
                          </tbody>
                        </table>
                      </div>
                    </div>      
                  </div>
              </div>  
              <!-- Last 5 Task Summart ends here  -->
              <!-- Top 10 brands summary -->
             <!--  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-lr-pad margin-top-20 padding-bottom-20" >
                     <div class="col-lg-12 back-color-white box-shadow1  no-lr-pad" style="">
                        <div class="col-lg-12 border-bottom">
                          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 ">
                            <div class="padding-top-15 margin-bottom-20 ">
                              <p class="text-bold text-uppercase"> TOP 10 BRANDS</p>
                            </div>
                          </div>     
                          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <div class="padding-top-15 margin-bottom-20 pull-right"> -->
                              <!-- <p class="font-size-14"> <a href="#" class="a-deco pull-right">View Brand's Report <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a></p>
                            </div>
                          </div>
                        </div>
                         
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 margin-top-20" id="append_brand_list_team">
                         
                        </div>    
               </div>
              </div>   -->
              <!-- Top 10 brands summary ends here -->

      
      </div>

      <div class="col-lg-4 col-md-4">
        <div class="col-lg-12 col-md-12">
          <p class="font-size-16 margin-top-30 text-bold">Filter Data by Users</p>
          <p class="font-size-14 color-grey">Data will be visible only for selected users</p>
          <div class="filt-chk checkbox margin-top-bot-2">
              <!-- <label><input type="checkbox" value="" class="view_all_button"><span class="margin-left-10 font-size-14">View for all</span></label> -->
          </div>
           <div class="margin-top-20" id="users_list">
              <!-- <div class="filt-chk checkbox a-deco margin-bottom-20">
                <label class="a-deco"><input type="checkbox" value=""><img src="<?php //echo ROOT_URI; ?>/assets/img/1.jpg" width="20" class="img-circle margin-left-10" ><span class="margin-left-10 font-size-14">G. Mehta</span></label>
              </div>  -->
           </div> 
            
          

          </div>
          <div class="col-lg-12 col-md-12 ">
            <button class="btn btn-default width-100 margin-top-10 brand-btn-grey" id="apply_filter_dashboard"> Apply</button>         
            <button class="btn btn-default width-100 margin-top-10 brand-btn-grey reset_button" > Reset</button>
          </div>
  
        </div>
      </div>
          
    </div>

  <!-- team performance dashboard ends here  -->



   


   <!-- lower part/main part ends here -->
   

   <!-- Modal -->
  <div id="ordered-items_modal" class="modal fade" role="dialog">
    <div class="modal-dialog margin-top-100">

      <!-- Modal content-->
      <div class="modal-content col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
        <div class="modal-header col-md-12 col-lg-12 col-sm-12 col-xs-12 ">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <p class="font-size-20 margin-left-10"><b>TODAY'S ORDERED ITEMS</b></p>
        </div>
        <div class="modal-body col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
            <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12  ">
                  <div class="table-responsive">
                    <table class="table " id="ordered-items">
                      <tbody>
                        <?php for($i=0;$i<10;$i++) { ?>
                        <tr>
                          <td class="text-bold text-uppercase font-size-14">ALFALFA TONIC - GENERAL</td>
                          <td class="text-uppercase font-size-12">ALFALFA TONIC</td>
                          <td class="text-uppercase font-size-12">500 ML</td>
                        </tr>
                      <?php } ?>
                      </tbody>
                    </table>
                  </div>
            </div>
        </div>
        <div class="modal-footer col-md-12 col-lg-12 col-sm-12 col-xs-12">
          <button type="button" class="btn btn-default pull-right brand-btn add-mem-btn1">close</button>
        </div>
      </div>

    </div>
  </div>
  <!-- modals ends here -->
</div>
 <!-- right side main body ends -->
      
 
<script type="text/javascript">

  $( document ).ready(function() {
    $( "#performance" ).on( "click", function() {
      $( "#performance-dashboard" ).fadeIn( "slow");
        $( "#team-performance-dashboard" ).hide();
      $("#team-performance").removeClass('display-task-active');
      $("#performance").addClass('display-task-active');
    });    
    $( "#team-performance" ).on( "click", function() {
      $( "#team-performance-dashboard" ).fadeIn( "slow");
      $( "#performance-dashboard" ).hide();
      $("#performance").removeClass('display-task-active');
      $("#team-performance").addClass('display-task-active');
    });

    
      task_summary_individual(1);
      call_summary(1);
      dashboard_overview_indi(1);
      top_10_brands_ind(1);
      $('#overlay_ind').hide();

      var user_role = sessionStorage.getItem('role');
      if(user_role  == 4){
        $('#team-performance').hide();
        // toast_it(user_role);
      }

      $(document).on('click','#week_individual_tab',function(){

          $('#individual_tab_today').removeClass("active");
          $('#individual_tab_week').addClass("active");
          $('#today_individual_tab').attr('data-time_span',2);

          task_summary_individual(2);
          call_summary(2);
          // dashboard_overview_indi(2);
          top_10_brands_ind(2);

      });

      $(document).on('click','#today_individual_tab',function(){

          $('#individual_tab_week').removeClass("active");
          $('#individual_tab_today').addClass("active");
          $('#today_individual_tab').attr('data-time_span',1);
          
          task_summary_individual(1);
          call_summary(1);
          dashboard_overview_indi(1);
          top_10_brands_ind(1);

      });


     function task_summary_individual(time_span){


        $.ajax({
           url : "<?php echo ROOT_URI; ?>/resources/services/dashboard_api.php",
           type : 'POST',
           data : "{\n\"user_id\":\"'"+sessionStorage.getItem('user_id')+"'\",\n\"time_span\":\""+time_span+"\",\n\"action\":\"todays_task_summary\"\n}",

           processData: false,  // tell jQuery not to process the data
           contentType: false,  // tell jQuery not to set contentType 
           dataType: 'JSON',
                 success: function(response) {

                  if(response.response_code == 200){

                        $('#all_tasks_indi').text(response.data.all_tasks[0].count);
                        $('#pending_indi').text(response.data.pending[0].count);
                        $('#completed_indi').text(response.data.completed[0].count);
                        $('#follow_up_indi').text(response.data.followup[0].count);

                    //toast_it('Yeah');
                  }
                  
                 }
                 // error: function() {  
                 //   toast_it("There was an error.");
                 // }
           }); //ajax close

     }

      function top_10_brands_ind(time_span){

        $.ajax({
           url : "<?php echo ROOT_URI; ?>/resources/services/dashboard_api.php",
           type : 'POST',
           data : "{\n\"user_id\":\"'"+sessionStorage.getItem('user_id')+"'\",\n\"token\":\""+sessionStorage.getItem("token")+"\",\n\"time_span\":\""+time_span+"\",\n\"action\":\"top_10_brands\"\n}",

           processData: false,  // tell jQuery not to process the data
           contentType: false,  // tell jQuery not to set contentType 
           dataType: 'JSON',
                 success: function(response) {

                  if(response.response_code == 200){
                    var ele = "";
                    var brand_list = response.data;
                    brand_list.forEach(function(entry){

                      if(JSON.stringify(entry) === '{}'){

                      }
                      else{
                      ele = ele + '<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 no-lr-pad">'+
                        '<p class="ellipsis">'+entry.customer_name+'</p>'+
                      '</div>'+
                      '<div class="col-lg-8 col-md-8 col-sm-11 col-xs-11 ">'+
                         '<div class="progress">'+
                            '<div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow='+entry.percent_value+' aria-valuemin="0" aria-valuemax="100" style="width:'+entry.percent_value+'%">'+
                            '</div>'+
                          '</div>'+  
                      '</div>'+
                      '<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 no-lr-pad">'+
                        '<p class="margin-right-10">'+entry.percent_value+'%</p>'+
                      '</div>';
                     }

                    $('#append_brand_list_ind').html(ele);

                    });    
                    //toast_it('Yeah');
                  }else{
                    $('#append_brand_list_ind').html('<p class="text-center margin-bottom-20">No Brand Data Available</p>');
                  }

                  
                 }  // close success
                 
        }); //ajax close

      }

      function dashboard_overview_indi(time_span){
      
        $.ajax({
           url : "<?php echo ROOT_URI; ?>/resources/services/dashboard_api.php",
           type : 'POST',
           data : "{\n\"user_id\":\"'"+sessionStorage.getItem('user_id')+"'\",\n\"token\":\""+sessionStorage.getItem("token")+"\",\n\"time_span\":\""+time_span+"\",\n\"action\":\"performance_overview\"\n}",

           processData: false,  // tell jQuery not to process the data
           contentType: false,  // tell jQuery not to set contentType 
           dataType: 'JSON',
                 success: function(response) {

                  if(response.response_code == 200){

                    if(response.data.average_order_value == null){
                      $('#average_order_value_ind').html("NA");
                    }
                    else{
                      $('#average_order_value_ind').html('INR '+Math.round(response.data.average_order_value));
                    }
                    
                    if(response.data.customers_ordered == null){
                      $('#customers_ordered_ind').html("NA");

                    }else{
                      $('#customers_ordered_ind').html(response.data.customers_ordered);
                    } 
                    if(response.data.difference_avg_order_value == null){
                      $('#difference_avg_order_value_ind').html("NA");

                    }else{
                      $('#difference_avg_order_value_ind').html("Difference from yesterday: INR "+response.data.difference_avg_order_value);
                    } 
                    if(response.data.difference_items == null){
                      $('#difference_items_ind').html("NA");

                    }else{
                      $('#difference_items_ind').html("Difference from yesterday: "+response.data.difference_items);
                    } 
                    if(response.data.difference_order == null){
                      $('#difference_order_ind').html("NA");

                    }else{
                      $('#difference_order_ind').html("Difference from yesterday: "+response.data.difference_order);
                    } 
                    if(response.data.difference_total_order_value == null){
                      $('#difference_total_order_value_ind').html("NA");

                    }else{
                      $('#difference_total_order_value_ind').html("Difference from yesterday: INR "+response.data.difference_total_order_value);
                    } 
                    if(response.data.total_items == null){
                      $('#total_items_ind').html("NA");

                    }else{
                      $('#total_items_ind').html(response.data.total_items);
                    } 
                    if(response.data.total_order_value == null){
                      $('#total_order_value_ind').html("NA");
                    }else{
                      $('#total_order_value_ind').html('INR '+Math.round(response.data.total_order_value));
                    }

                      
                    //toast_it('Yeah');
                  }else{

                      $('#average_order_value_ind').html("NA");
                      $('#total_order_value_ind').html("NA");
                      $('#total_items_ind').html("NA");
                      $('#difference_total_order_value_ind').html("NA");
                      $('#difference_order_ind').html("NA");
                      $('#difference_items_ind').html("NA");
                      $('#difference_avg_order_value_ind').html("NA");
                      $('#customers_ordered_ind').html("NA");

                  }
                  
                 }
                 // error: function() {  
                 //   toast_it("There was an error.");
                 // }
           }); //ajax close

     }  // close function performance overview

     function call_summary(time_span){

      var call_list = '';
         $.ajax({
         url : "<?php echo ROOT_URI; ?>/resources/services/dashboard_api.php",
         type : 'POST',
         data : "{\n\"user_id\":\"'"+sessionStorage.getItem('user_id')+"'\",\n\"action\":\"get_call_summary\"\n}",

         processData: false,  // tell jQuery not to process the data
         contentType: false,  // tell jQuery not to set contentType 
         dataType: 'JSON',
               success: function(response) {
                  var sum = 0;
                if(response.response_code == 200){
                  var call_count = [];
                  var call_date = [];

                  call_list = response.data;
                  call_list.forEach(function(entry){
                    console.log(entry.calls);
                    sum = sum + parseInt(entry.calls);
                    //call_count[i] = entry.calls;
                    call_count.push(entry.calls);
                    call_date.push(entry.DayDate);
                    //i++;
                    //console.log(call_count);
                  });
                  //console.log(call_count);
                  //console.log(call_date);

                  call_date[5] = 'Yesterday';

                  if(time_span == 1){

                    $('#time_span_status_indi').text("Number of calls Today");

                    var diff = parseInt(call_count[5]) - parseInt(call_count[6]);
                    //toast_it(diff);
                    if(diff == 0){
                      $('#call_difference').text("Same as Yesterday");
                    }else if(diff >0){
                      $('#call_difference').text(diff+" Less than yesterday");
                    }else if(diff <0){
                      $('#call_difference').text(Math.abs(diff)+" More than yesterday");
                    }
                    $('#call_today_1').text(call_count[6]);

                  }
                  else if(time_span == 2){
                    $('#time_span_status_indi').text("Total Number of calls this Week");
                    $('#call_today_1').text(sum);
                    $('#call_difference').text("");
                  }

                  call_date[6] = 'Today';


                  var ctx = document.getElementById("callsummary");
                  ctx.height = 120;
                  var myChart = new Chart(ctx, {
                      type: 'bar',
                      data: {
                          labels: call_date,
                          datasets: [{
                              label: '# of Calls',
                              data: call_count,
                              backgroundColor: [
                                  'rgba(255, 99, 132, 0.2)',
                                  'rgba(54, 162, 235, 0.2)',
                                  'rgba(255, 206, 86, 0.2)',
                                  'rgba(75, 192, 192, 0.2)',
                                  'rgba(153, 102, 255, 0.2)',
                                  'rgba(255, 159, 64, 0.2)'
                              ],
                              borderColor: [
                                  'rgba(255,99,132,1)',
                                  'rgba(54, 162, 235, 1)',
                                  'rgba(255, 206, 86, 1)',
                                  'rgba(75, 192, 192, 1)',
                                  'rgba(153, 102, 255, 1)',
                                  'rgba(255, 159, 64, 1)'
                              ],
                              borderWidth: 1
                          }]
                      },
                      options: {
                          scales: {
                              yAxes: [{
                                  ticks: {
                                      beginAtZero:true
                                  }
                              }]
                          }
                      }
                  });

                  //toast_it('Yeah2');
                }
                
               }
               // error: function() {  
               //   toast_it("There was an error.");
               // }
         }); //ajax close

     }  // close function call summary

     var task_list = '';
     $.ajax({
     url : "<?php echo ROOT_URI; ?>/resources/services/dashboard_api.php",
     type : 'POST',
     data : "{\n\"user_id\":\"'"+sessionStorage.getItem('user_id')+"'\",\n\"token\":\""+sessionStorage.getItem("token")+"\",\n\"action\":\"average_task\"\n}",

     processData: false,  // tell jQuery not to process the data
     contentType: false,  // tell jQuery not to set contentType 
     dataType: 'JSON',
           success: function(response) {

            if(response.response_code == 200){

              var task_count = [];
              var cust_count = [];
              var task_date  = [];

              task_list = response.data;
              task_list.forEach(function(entry){
                //console.log(entry.tasks);
                //sum = sum + parseInt(entry.tasks);
                //task_count[i] = entry.tasks;
                task_count.push(entry.tasks);
                cust_count.push(entry.customers_order);
                task_date.push(entry.DayDate);
                //i++;
                //console.log(task_count);
              });
              //console.log(task_count);
              //console.log(task_date);

              task_date[5] = 'Yesterday';

              task_date[6] = 'Today';




              //toast_it('Yeah3');
                  var ctx1 = document.getElementById("performance_chart");
                  ctx1.height = 200;
                  var chart = new Chart(ctx1, {
                    // The type of chart we want to create
                    type: 'line',

                    // The data for our dataset
                    data: {
                        labels: task_date,
                    datasets: [{
                        label: "Completed Tasks",
                        data: task_count,
                        backgroundColor: [
                          'rgba(105, 0, 132, .2)',
                        ],
                        borderColor: [
                          'rgba(200, 99, 132, .7)',
                        ],
                        borderWidth: 2
                      },
                        {
                        label: "Orders Taken",
                        data: cust_count,
                        backgroundColor: [
                          'rgba(0, 137, 132, .2)',
                        ],
                        borderColor: [
                          'rgba(0, 10, 130, .7)',
                        ],
                        borderWidth: 2
                      }

                        ]
                    },

                    // Configuration options go here
                    options: {}
                  }); // Team performance charts 

            }
            
           }
           // error: function() {  
           //   toast_it("There was an error.");
           // }
     }); //ajax close

     $.ajax({
     url : "<?php echo ROOT_URI; ?>/resources/services/dashboard_api.php",
     type : 'POST',
     data : "{\n\"user_id\":\"'"+sessionStorage.getItem('user_id')+"'\",\n\"token\":\""+sessionStorage.getItem("token")+"\",\n\"action\":\"last_5_task_summary\"\n}",

     processData: false,  // tell jQuery not to process the data
     contentType: false,  // tell jQuery not to set contentType 
     dataType: 'JSON',
           success: function(response) {

            if(response.response_code == 200){
              //toast_it('Yeah3');
              $('#append_last_5_task_indv').append("");

              var ele = '';
                var task_list_call = response.data;
                task_list_call.forEach(function(entry){

                  var ele = ele+ '<tr>'+
                              '<td class="font-size-14">'+entry.customer_name+' <br> <span class="font-size-12 color-grey">Called @'+entry.call_time+'</span>'+
                              '</td>'+
                              // '<td>'+entry.items+' Items</td>'+
                              // '<td><span class="order-status ordered a-deco">'+entry.order_status+'</span></td>'+
                              // '<td><i class="fa fa-inr" aria-hidden="true"></i>'+entry.total+'</td>'+
                            '</tr>';
                  $('#append_last_5_task_indv').append(ele);
                });
            }else{
                  $('#append_last_5_task_indv').append('<p class="text-center margin-top-20">No Task Data Available</p>');
            }
            
           }
           // error: function() {  
           //   toast_it("There was an error.");
           // }
     }); //ajax close



  //    window.onload = function() {
  //     var ctx2 = document.getElementById('task-summary_chart_1').getContext('2d');
  //     ctx2.height = 100;
  //     window.myDoughnut = new Chart(ctx2, config);

  //     var task_ctx = document.getElementById('task-summary_chart').getContext('2d');
  //     task_ctx.height = 100;
  //     window.myDoughnut_1 = new Chart(task_ctx, config);
  // };

  // var randomScalingFactor = function() {
  //     return Math.round(Math.random() * 100);
  //   };
  // var config = {
  //     type: 'doughnut',
  //     data: {
  //       datasets: [{
  //         data: [
  //           randomScalingFactor(),
  //           randomScalingFactor(),
  //           randomScalingFactor(),
  //           randomScalingFactor(),
  //           randomScalingFactor(),
  //         ],
  //         backgroundColor: [
  //           'rgb(255,0,0)',
  //           'rgb(0,255,0)',
  //           'rgb(0,0,255)',
  //           'rgb(255,255,0)',
  //           'rgb(0,255,255)',
  //         ],
  //         label: 'Dataset 1'
  //       }],
  //       labels: [
  //         'Red',
  //         'Orange',
  //         'Yellow',
  //         'Green',
  //         'Blue'
  //       ]
  //     },
  //     options: {
  //       responsive: true,
  //       legend: {
  //         position: 'top',
  //       },
  //       title: {
  //         display: true,
  //         text: ''
  //       },
  //       animation: {
  //         animateScale: true,
  //         animateRotate: true
  //       }
  //     }
  //   };

//   var options = {
//   exportEnabled: true,
//   animationEnabled: true,
//   title:{
//     text: "Accounting"
//   },
//   legend:{
//     horizontalAlign: "right",
//     verticalAlign: "center"
//   },
//   data: [{
//     type: "pie",
//     showInLegend: true,
//     toolTipContent: "<b>{name}</b>: ${y} (#percent%)",
//     indexLabel: "{name}",
//     legendText: "{name} (#percent%)",
//     indexLabelPlacement: "inside",
//     dataPoints: [
//       { y: 6566.4, name: "Housing" },
//       { y: 2599.2, name: "Food" },
//       { y: 1231.2, name: "Fun" },
//       { y: 1368, name: "Clothes" },
//       { y: 684, name: "Others"},
//       { y: 1231.2, name: "Utilities" }
//     ]
//   }]
// };
// // $("#task-summary_chart").CanvasJSChart(options);



 // var ctx1 = document.getElementById("task-summary_chart");
 //                  ctx1.height = 200;
 //                  var chart = new Chart(ctx1, {
 //                    // The type of chart we want to create
 //                    //type: 'line',

 //                    // The data for our dataset
 //                     data: [{
 //                      type: "pie",
 //                      showInLegend: true,
 //                      toolTipContent: "<b>{name}</b>: ${y} (#percent%)",
 //                      indexLabel: "{name}",
 //                      legendText: "{name} (#percent%)",
 //                      indexLabelPlacement: "inside",
 //                      dataPoints: [
 //                        { y: 6566.4, name: "Housing" },
 //                        { y: 2599.2, name: "Food" },
 //                        { y: 1231.2, name: "Fun" },
 //                        { y: 1368, name: "Clothes" },
 //                        { y: 684, name: "Others"},
 //                        { y: 1231.2, name: "Utilities" }
 //                      ]
 //                    }]

 //                    // Configuration options go here
 //                    // options: {}
 //                  }); // Team performance charts 




    $(document).on('click','#team-performance',function(){


      var user_id  = sessionStorage.getItem("user_id");
      var all_users_list = [];
      // all_users_list.push("'"+user_id+"'");

      $.ajax({
       url : "<?php echo ROOT_URI; ?>/resources/services/dashboard_api.php",
       type : 'POST',
       data : "{\n\"user_role\":\""+sessionStorage.getItem('role')+"\",\n\"user_id\":\"'"+sessionStorage.getItem('user_id')+"'\",\n\"action\":\"get_users\"\n}",

       processData: false,  // tell jQuery not to process the data
       contentType: false,  // tell jQuery not to set contentType 
       dataType: 'JSON',
             success: function(response) {

              if(response.response_code == 200){

                var ele = '';

                var user_list = response.data;
                user_list.forEach(function(entry){

                  all_users_list.push("'"+entry.user_id+"'");

               ele = ele + '<div class="filt-chk checkbox a-deco margin-bottom-20" >'+
                '<label class="a-deco"><input class="dashboard_filter" nmme="filter_name" type="checkbox"  data-user_id_refer="'+entry.user_id+'" data-filter-type="1" value="'+entry.user_id+'">'+
                '<img src="<?php echo S3_BUCKET; ?>'+entry.profile_picture+'" width="20" class="img-circle margin-left-10" ><span class="margin-left-10 font-size-14">'+entry.name+'</span></label>'+
                '</div>';

                $('#users_list').html(ele);
                });
                //console.log(all_users_list);
              }

             
              
             }
             // error: function() {  
             //   toast_it("There was an error.");
             // }
       }); //ajax close

      //console.log(all_users_list);
      setTimeout( function(){ 
      //console.log(all_users_list);
      average_task_team(all_users_list);
      get_call_summary_team(all_users_list,$('#todays_team_tab').attr('data-time_span'));
      last_5_task_summary(all_users_list);
      case_resolved_team(all_users_list,$('#todays_team_tab').attr('data-time_span'));
      task_summary_team(all_users_list,$('#todays_team_tab').attr('data-time_span'));
      // case_resolved_team(all_users_list,$('#todays_team_tab').attr('data-time_span'));
      dashboard_overview_team(all_users_list,$('#todays_team_tab').attr('data-time_span'));
      top_10_brands_team(all_users_list,$('#todays_team_tab').attr('data-time_span'));
      $('#overlay_team').hide();


      // var jObject_all_users={};
      //           all_users = {};
      var selectedFilters ;
      var results = [];
      var jObject_selectedFilters={};
      var filterCheckboxes = [];


      $(document).on('click','#apply_filter_dashboard',function(){

         let a = [];
          $(".dashboard_filter:checked").each(function() {
              a.push("'"+this.value+"'");
          });

          $('#append_last_5_task_team').html('');

          //console.log(a);
          case_resolved_team(a,$('#todays_team_tab').attr('data-time_span'));
          last_5_task_summary(a);
          get_call_summary_team(a,$('#todays_team_tab').attr('data-time_span'));
          average_task_team(a);
          task_summary_team(a,$('#todays_team_tab').attr('data-time_span'));
          case_resolved_team(a,$('#todays_team_tab').attr('data-time_span'));
          dashboard_overview_team(a,$('#todays_team_tab').attr('data-time_span'));
          top_10_brands_team(a,$('#todays_team_tab').attr('data-time_span'));


      });

      $(document).on('click','.view_all_button',function(){

        $(".dashboard_filter").prop('checked', true);
        $('#append_last_5_task_team').html('');


          average_task_team(all_users_list);
          get_call_summary_team(all_users_list,$('#todays_team_tab').attr('data-time_span'));
          last_5_task_summary(all_users_list);
          case_resolved_team(all_users_list,$('#todays_team_tab').attr('data-time_span'));
          task_summary_team(all_users_list,$('#todays_team_tab').attr('data-time_span'));
          case_resolved_team(all_users_list,$('#todays_team_tab').attr('data-time_span'));
          dashboard_overview_team(all_users_list,$('#todays_team_tab').attr('data-time_span'));
          top_10_brands_team(all_users_list,$('#todays_team_tab').attr('data-time_span'));

      });

      $(document).on('click','.reset_button',function(){

        $(".dashboard_filter").prop('checked', false);
        $(".view_all_button").prop('checked', false);



          average_task_team(all_users_list);
          get_call_summary_team(all_users_list,$('#todays_team_tab').attr('data-time_span'));
          last_5_task_summary(all_users_list);
          case_resolved_team(all_users_list,$('#todays_team_tab').attr('data-time_span'));
          task_summary_team(all_users_list,$('#todays_team_tab').attr('data-time_span'));
          case_resolved_team(all_users_list,$('#todays_team_tab').attr('data-time_span'));
          dashboard_overview_team(all_users_list,$('#todays_team_tab').attr('data-time_span'));
          top_10_brands_team(all_users_list,$('#todays_team_tab').attr('data-time_span'));

      });

      // $(document).on('click','input[type="checkbox"]',function(){

      //   var user_id = $(this).attr('data-user_id_refer');
      //   var filterCheckboxes = $('input[type="checkbox"]');
      //   console.log(filterCheckboxes);

      //   filterCheckboxes.filter(':checked').each(function() {

      //    // if($(this).attr("data-filter-type")==1){
      //                 selectedFilters.push("'"+user_id+"'");
      //            // }
      //     //selectedFilters.push(user_id);
      //   });
        
        // case_resolved_team(selectedFilters);
        // last_5_task_summary(selectedFilters);
        // get_call_summary_team(selectedFilters);
        // average_task_team(selectedFilters);

        // console.log(selectedFilters);

        //toast_it(user_id);

      //});  // timeout close


      $(document).on('click','#weeks_team_tab',function(){

          $('input:checkbox').attr('checked',false);
          
          $('#tab_switch_today').removeClass("active");
          $('#tab_switch_week').addClass("active");
          $('#todays_team_tab').attr('data-time_span',2);

          task_summary_team(all_users_list,2);
          case_resolved_team(all_users_list,2);
          get_call_summary_team(all_users_list,2);
          // dashboard_overview_team(all_users_list,2);
          top_10_brands_team(all_users_list,2);

      });

      $(document).on('click','#todays_team_tab',function(){

          $('input:checkbox').attr('checked',false);

          $('#tab_switch_week').removeClass("active");
          $('#tab_switch_today').addClass("active");
          $('#todays_team_tab').attr('data-time_span',1);
          
          task_summary_team(all_users_list,1);
          case_resolved_team(all_users_list,1);
          get_call_summary_team(all_users_list,1);
          dashboard_overview_team(all_users_list,1);
          top_10_brands_team(all_users_list,1);

      });

      function dashboard_overview_team(all_users_list,time_span){
          // if(time_span==undefined){
            time_span=1;
          // }
        $.ajax({
           url : "<?php echo ROOT_URI; ?>/resources/services/dashboard_api.php",
           type : 'POST',
           data : "{\n\"user_id\":\""+all_users_list+"\",\n\"token\":\""+sessionStorage.getItem("token")+"\",\n\"time_span\":\""+time_span+"\",\n\"action\":\"performance_overview\"\n}",

           processData: false,  // tell jQuery not to process the data
           contentType: false,  // tell jQuery not to set contentType 
           dataType: 'JSON',
                 success: function(response) {

                  if(response.response_code == 200){

                    if(response.data.average_order_value == null){
                      $('#average_order_value_team').html("NA");
                    }
                    else{
                      $('#average_order_value_team').html('INR '+Math.round(response.data.average_order_value));
                    }
                    
                    if(response.data.customers_ordered == null){
                      $('#customers_ordered_team').html("NA");

                    }else{
                      $('#customers_ordered_team').html(response.data.customers_ordered);
                    } 
                    if(response.data.difference_avg_order_value == null){
                      $('#difference_avg_order_value_team').html("NA");

                    }else{
                      $('#difference_avg_order_value_team').html("Difference from yesterday: INR "+response.data.difference_avg_order_value);
                    } 
                    if(response.data.difference_items == null){
                      $('#difference_items_team').html("NA");

                    }else{
                      $('#difference_items_team').html("Difference from yesterday: "+response.data.difference_items);
                    } 
                    if(response.data.difference_order == null){
                      $('#difference_order_team').html("NA");

                    }else{
                      $('#difference_order_team').html("Difference from yesterday: "+response.data.difference_order);
                    } 
                    if(response.data.difference_total_order_value == null){
                      $('#difference_total_order_value_team').html("NA");

                    }else{
                      $('#difference_total_order_value_team').html("Difference from yesterday: INR "+response.data.difference_total_order_value);
                    } 
                    if(response.data.total_items == null){
                      $('#total_items_team').html("NA");

                    }else{
                      $('#total_items_team').html(response.data.total_items);
                    } 
                    if(response.data.total_order_value == null){
                      $('#total_order_value_team').html("NA");
                    }else{
                      $('#total_order_value_team').html('INR '+Math.round(response.data.total_order_value));
                    }

                      
                    //toast_it('Yeah');
                  }
                  else{

                      $('#average_order_value_team').html("NA");
                      $('#total_order_value_team').html("NA");
                      $('#total_items_team').html("NA");
                      $('#difference_total_order_value_team').html("NA");
                      $('#difference_order_team').html("NA");
                      $('#difference_items_team').html("NA");
                      $('#difference_avg_order_value_team').html("NA");
                      $('#customers_ordered_team').html("NA");

                  }
                  
                 }
                 // error: function() {  
                 //   toast_it("There was an error.");
                 // }
           }); //ajax close

     }  // close function performance overview


      function task_summary_team(all_users_list,time_span){


           $.ajax({
             url : "<?php echo ROOT_URI; ?>/resources/services/dashboard_api.php",
             type : 'POST',
             data : "{\n\"user_id\":\""+all_users_list+"\",\n\"time_span\":\""+time_span+"\",\n\"action\":\"todays_task_summary\"\n}",

             processData: false,  // tell jQuery not to process the data
             contentType: false,  // tell jQuery not to set contentType 
             dataType: 'JSON',
                   success: function(response) {

                    if(response.response_code == 200){

                        $('#all_tasks_team').text(response.data.all_tasks[0].count);
                        $('#pending_team').text(response.data.pending[0].count);
                        $('#completed_team').text(response.data.completed[0].count);
                        $('#follow_up_team').text(response.data.followup[0].count);



                      //toast_it('Yeah');
                    }
                    
                   }
                   // error: function() {  
                   //   toast_it("There was an error.");
                   // }
             }); //ajax close

      }

    


     function average_task_team(all_users_list){

     var task_list = '';
     $.ajax({
     url : "<?php echo ROOT_URI; ?>/resources/services/dashboard_api.php",
     type : 'POST',
     data : "{\n\"user_id\":\""+all_users_list+"\",\n\"token\":\""+sessionStorage.getItem("token")+"\",\n\"action\":\"average_task\"\n}",

     processData: false,  // tell jQuery not to process the data
     contentType: false,  // tell jQuery not to set contentType 
     dataType: 'JSON',
           success: function(response) {
            // console.log(all_users_list);
            if(response.response_code == 200){

              var cust_count = [];
              var task_count = [];
              var task_date = [];

              task_list = response.data;
              task_list.forEach(function(entry){
                //console.log(entry.tasks);
                //sum = sum + parseInt(entry.tasks);
                //task_count[i] = entry.tasks;
                task_count.push(entry.tasks);
                cust_count.push(entry.customers_order);
                task_date.push(entry.DayDate);
                //i++;
                //console.log(task_count);
              });
              //console.log(task_count);
              //console.log(task_date);

              task_date[5] = 'Yesterday';

              task_date[6] = 'Today';




              //toast_it('Yeah3');
                  var ctx11 = document.getElementById("performance_chart_1");
                  ctx11.height = 150;
                  var chart = new Chart(ctx11, {
                    // The type of chart we want to create
                    type: 'line',

                    // The data for our dataset
                    data: {
                        labels: task_date,
                    datasets: [{
                        label: "Average Task Completion Per Day",
                        data: task_count,
                        backgroundColor: [
                          'rgba(105, 0, 132, .2)',
                        ],
                        borderColor: [
                          'rgba(200, 99, 132, .7)',
                        ],
                        borderWidth: 2
                      },
                        {
                        label: "Average Customer Ordering Per Day",
                        data: cust_count,
                        backgroundColor: [
                          'rgba(0, 137, 132, .2)',
                        ],
                        borderColor: [
                          'rgba(0, 10, 130, .7)',
                        ],
                        borderWidth: 2
                      }

                        ]
                    },

                    // Configuration options go here
                    options: {}
                  }); // Team performance charts 

            }
            
           }
           // error: function() {  
           //   toast_it("There was an error.");
           // }
     }); //ajax close


     } // close function


     function get_call_summary_team(all_users_list,time_span){

     var call_list = '';
     $.ajax({
     url : "<?php echo ROOT_URI; ?>/resources/services/dashboard_api.php",
     type : 'POST',
     data : "{\n\"user_id\":\""+all_users_list+"\",\n\"action\":\"get_call_summary\"\n}",

     processData: false,  // tell jQuery not to process the data
     contentType: false,  // tell jQuery not to set contentType 
     dataType: 'JSON',
           success: function(response) {
              var sum = 0;
            if(response.response_code == 200){
              var call_count = [];
              var call_date = [];

              call_list = response.data;
              call_list.forEach(function(entry){
                  console.log(entry.calls);
                sum = sum + parseInt(entry.calls);
                //call_count[i] = entry.calls;
                call_count.push(entry.calls);
                call_date.push(entry.DayDate);
                //i++;
                //console.log(call_count);
              });
              console.log(sum);
              //console.log(call_date);

              call_date[5] = 'Yesterday';
              
              if(time_span == 1){

              $('#time_span_status_team').text("Number of calls Today");
              var diff = parseInt(call_count[5]) - parseInt(call_count[6]);
              //toast_it(diff);
              if(diff == 0){
                $('#call_difference2').text("Same as Yesterday");
              }else if(diff >0){
                $('#call_difference2').text(diff+" Less than yesterday");
              }else if(diff <0){
                $('#call_difference2').text(Math.abs(diff)+" More than yesterday");
              }

              $('#call_today_tab2').text(call_count[6]);

            }
            else{
                $('#time_span_status_team').text("Number of calls this Week");
                $('#call_today_tab2').text(sum);
                $('#call_difference2').text(" ");

            }


              call_date[6] = 'Today';


              var ctx12 = document.getElementById("callsummary_1");
              ctx12.height = 190;
              var myChart = new Chart(ctx12, {
                  type: 'bar',
                  data: {
                      labels: call_date,
                      datasets: [{
                          label: '# of Calls',
                          data: call_count,
                          backgroundColor: [
                              'rgba(255, 99, 132, 0.2)',
                              'rgba(54, 162, 235, 0.2)',
                              'rgba(255, 206, 86, 0.2)',
                              'rgba(75, 192, 192, 0.2)',
                              'rgba(153, 102, 255, 0.2)',
                              'rgba(255, 159, 64, 0.2)'
                          ],
                          borderColor: [
                              'rgba(255,99,132,1)',
                              'rgba(54, 162, 235, 1)',
                              'rgba(255, 206, 86, 1)',
                              'rgba(75, 192, 192, 1)',
                              'rgba(153, 102, 255, 1)',
                              'rgba(255, 159, 64, 1)'
                          ],
                          borderWidth: 1
                      }]
                  },
                  options: {
                      scales: {
                          yAxes: [{
                              ticks: {
                                  beginAtZero:true
                              }
                          }]
                      }
                  }
              });

              //toast_it('Yeah2');
            }
            
           }
           // error: function() {  
           //   toast_it("There was an error.");
           // }
     }); //ajax close

     }  // function close


      

     function last_5_task_summary(all_users_list){


      $.ajax({
     url : "<?php echo ROOT_URI; ?>/resources/services/dashboard_api.php",
     type : 'POST',
     data : "{\n\"user_id\":\""+all_users_list+"\",\n\"token\":\""+sessionStorage.getItem("token")+"\",\n\"action\":\"last_5_task_summary\"\n}",

     processData: false,  // tell jQuery not to process the data
     contentType: false,  // tell jQuery not to set contentType 
     dataType: 'JSON',
           success: function(response) {

            if(response.response_code == 200){
               $('#append_last_5_task_team').html('');
              //toast_it('Yeah3');
              var ele = '';
                var task_list_call = response.data;
                task_list_call.forEach(function(entry){

                  var ele = ele+ '<tr>'+
                              '<td class="font-size-14">'+entry.customer_name+' <br> <span class="font-size-12 color-grey">Called @'+entry.call_time+'</span>'+
                              '</td>'+
                              // '<td>'+entry.items+' Items</td>'+
                              '<td><span class="order-status ordered a-deco">'+entry.order_status+'</span></td>'+
                              // '<td><i class="fa fa-inr" aria-hidden="true"></i>'+entry.total+'</td>'+
                            '</tr>';
                  $('#append_last_5_task_team').append(ele);                  
                });

            }
            else{
                  $('#append_last_5_task_team').append('<p class="text-center margin-top-20">No Task Data Available</p>');
            }
            
           }
           // error: function() {  
           //   toast_it("There was an error.");
           // }
     }); //ajax close

     } // function close

     function top_10_brands_team(all_users_list,time_span){


      $.ajax({
     url : "<?php echo ROOT_URI; ?>/resources/services/dashboard_api.php",
     type : 'POST',
     data : "{\n\"user_id\":\""+all_users_list+"\",\n\"time_span\":\""+time_span+"\",\n\"token\":\""+sessionStorage.getItem("token")+"\",\n\"action\":\"top_10_brands\"\n}",

     processData: false,  // tell jQuery not to process the data
     contentType: false,  // tell jQuery not to set contentType 
     dataType: 'JSON',
           success: function(response) {

            if(response.response_code == 200){
                    var ele = "";
                    var brand_list = response.data;
                    brand_list.forEach(function(entry){

                      if(JSON.stringify(entry) === '{}'){

                      }
                      else{
                      ele = ele + '<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 no-lr-pad">'+
                        '<p class="ellipsis">'+entry.customer_name+'</p>'+
                      '</div>'+
                      '<div class="col-lg-8 col-md-8 col-sm-11 col-xs-11 ">'+
                         '<div class="progress">'+
                            '<div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="'+entry.percent_value+'" aria-valuemin="0" aria-valuemax="100" style="width:'+entry.percent_value+'%">'+
                            '</div>'+
                          '</div>'+  
                      '</div>'+
                      '<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 no-lr-pad">'+
                        '<p class="margin-right-10">'+entry.percent_value+'%</p>'+
                      '</div>';
                     }

                    $('#append_brand_list_team').html(ele);

                    });    
                    //toast_it('Yeah');
                  }else{
                    $('#append_brand_list_team').html('<p class="text-center">No Brand Data Available</p>');
                  }

            
           }  // close success
           
     }); //ajax close

     } // function close


     

   function case_resolved_team(all_users_list,time_span){

    $.ajax({
     url : "<?php echo ROOT_URI; ?>/resources/services/dashboard_api.php",
     type : 'POST',
     data : "{\n\"user_id\":\""+all_users_list+"\",\n\"time_span\":\""+time_span+"\",\n\"action\":\"cases_resolved_team\"\n}",

     processData: false,  // tell jQuery not to process the data
     contentType: false,  // tell jQuery not to set contentType 
     dataType: 'JSON',
           success: function(response) {

            if(response.response_code == 200){
              //toast_it('Yeah3');
              $('#append_cases_resolved').text(response.data.resolved[0].count);
              $('#append_cases_unresolved').text(response.data.unresolved[0].count);
            }
            
           }
           // error: function() {  
           //   toast_it("There was an error.");
           // }
     }); //ajax close


  }  // function close


        }  , 1500 ); // timeout close
        

    });


  });
        

  </script>
 

 
 <script src="<?php echo ROOT_URI; ?>/assets/js/generate_charts.js"></script>