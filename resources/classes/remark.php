<?php
class remark
{
	private $_db, $dt,$_user_obj;

	public function __construct($user=null)
	{
		$this->_db = DB::getInstance();
		$this->_user_obj = user::getInstance_of_user();
		date_default_timezone_set('Asia/Calcutta');
        $this->dt = date('Y-m-d H:i:s');
	}
	 public function add_remark($entity_id,$entity_type,$remark,$uid)//Used to add a new remark
  {
   
      $sql ="INSERT INTO remarks(entity_id,entity_type,remark,user_id) VALUES(?,?,?,?)";
      $result = $this->_db->query($sql,array($entity_id,$entity_type,$remark,$uid));
   	  if($result->error())
   		{
   			            return 0;
        }
        else
        {
	 		return 1;
	  	}
	}
	public function display_remark($entity_id,$entity_type,$page_no,$limit)
{
   $sql="SELECT remark,created_date,user_id from remarks WHERE entity_id=? AND entity_type=?  ORDER BY created_date DESC LIMIT $page_no,$limit";
   //echo $sql;
   $result = $this->_db->query($sql,array($entity_id,$entity_type))->results();
   return $result;
}
  public function display_remark_all($entity_id,$entity_type)
{
   $sql="SELECT remark,created_date,user_id from remarks WHERE entity_id=? AND entity_type=? ORDER BY created_date DESC";
   //echo $sql;
   $result = $this->_db->query($sql,array($entity_id,$entity_type))->results();
   return $result;
}
 
  public function last_remark($entity_id,$entity_type)
{
   $sql="SELECT remark,created_date,user_id from remarks WHERE entity_id=? AND entity_type=? ORDER BY created_date DESC LIMIT 0,1";
   //echo $sql;
   $result = $this->_db->query($sql,array($entity_id,$entity_type))->results();
    if(empty($result))
      {
         return 0;
      }
      else
      {
      foreach($result as $last_user=>$row_user)
      {
           return $row_user;
      }

   }
  }
}