 <?php 
  $path_init = $_SERVER['DOCUMENT_ROOT']."/crm/resources/init.php";
include_once($path_init);

 ?>

  <link href="<?php echo ROOT_URI; ?>/assets/css/temp.css" rel="stylesheet" type="text/css">
  <link href="<?php echo ROOT_URI; ?>/assets/css/team.css" rel="stylesheet" type="text/css">

<div class="col-md-10 col-lg-10 col-sm-10 col-xs-10 col-lg-offset-2 col-md-offset-2 col-sm-offset-2 col-xs-offset-2 no-lr-pad back-color-grey">
    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad " id="main-section-task">
   
           <!-- for upper part -->
  
            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 back-color-white padding-top-13 border-bottom padding-left-40">
                <p class="font-size-24 float-inline"><b>Calling Priority</b></p>
                <div class="dropdown pull-right margin-right-30">
                 
                </div>
            </div>
           <!-- upper part ends -->
    </div>
    <div class="col-md-11 col-lg-11 col-sm-11 col-xs-11 no-lr-pad ">
          <table class="table table-bordered">
            <thead>
              <tr>
                <th>Serial</th>
                <th>Name</th>
                <th>Email</th>
                <th>Phone</th>
                <th>Status</th>
                <th>Priority</th>
              </tr>
            </thead>
            <tbody id="append_call_logs">

  
            </tbody>
          </table>
    </div>
        

</div>  
    <script type="text/javascript">
    $(document).ready(function() {
         
          // call_logs_display(call_status);
          

        $(document).on('change','.chk_prior_btn',function(){

            var val_prior = $(this).val();
            var user_id_set = $(this).attr('data-user_id');
            

            // alert("hello");
            $.ajax({
                  dataType :'json',
                  method : 'POST',
                  async:false,
                  url : "<?php echo ROOT_URI; ?>/resources/services/team_api.php",
                  data: "{\n\"action\":\"set_priority\",\n\"user_id\":\""+user_id_set+"\",\n\"new_value\":\""+val_prior+"\"}",}).success(function(resp){
                          
                          if(resp.response_code==200){
                            setInterval(function(){ location.reload();}, 1000);

                             toast_it("updated"); 
                          }
                          else if(resp.response_code==400){
                             toast_it("Couldn't update, please try again"); 
                          }
                          
                      });   
          });  // close function on click  

          
              $.ajax({
                 url : "<?php echo ROOT_URI; ?>/resources/services/team_api.php",
                 type : 'POST',
                 data : "{\n\"dist_id\":\""+sessionStorage.getItem('dist_id')+"\",\n\"action\":\"get_user_priority\"\n}",

                 processData: false,  // tell jQuery not to process the data
                 contentType: false,  // tell jQuery not to set contentType 
                 dataType: 'JSON',
                       success: function(response) {

                        if(response.response_code == 200){
 
                          var html_var = '';
                          var i =1;
                          var total_count = response.data.length;
                          response.data.forEach(function(entry){
                            var activ_st = 'Active';
                            if(entry.status==0){
                                activ_st = 'Inactive';
                            }
                            html_var = html_var + '<tr>'+
                                '<td>'+i+'</td>'+
                                '<td>'+entry.name+'</td>'+
                                '<td>'+entry.email+'</td>'+
                                '<td>'+entry.phone+'</td>'+
                                '<td>'+activ_st+'</td>'+
                                '<td><select data-user_id="'+entry.user_id+'" class="form-control chk_prior_btn" id="sel1">';
                                  var selected = '';
                                   
                                 
                                  for (var j = 0; j < total_count+1; j++) { 
                                      if(j==entry.priority){
                                        selected = 'selected';
                                      }
                                      html_var = html_var +   '<option '+selected+' value="'+j+'">'+j+'</option>';
                                       selected ='';
                                  }
                            html_var = html_var + '</select>'+ 
                                   '</td>'+
                                '</tr>';
                                i++;
                            }  //  close if response code 200
                          )};
                            $("#append_call_logs").html(html_var);

                       }  //  close success 
                      
              }); //ajax close


          
   

    });  // close document on ready
     
    </script>
    </body>
</html>