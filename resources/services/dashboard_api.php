<?php
$path_init = $_SERVER['DOCUMENT_ROOT']."/crm/resources/init.php";
include_once($path_init); 
header('Content-type: application/json');
$data = json_decode(file_get_contents('php://input')); 
$obj_task =new task_class();
$obj_call =new call();
$obj_user =new user();
$obj_dashboard =new Dashboard();

$action =$data->action;
	
	switch ($action) {

		case 'get_call_summary':

			$user_id = $data->user_id;

			$result = $obj_dashboard->get_call_summary($user_id);

			if($result){
			$response['response_code']=200;
			$response['response_message']="Success";
			$response['data']=$result;
			}
			else{
			$response['response_code']=400;
			$response['response_message']="Failure";	
			}

			echo json_encode($response);

		break;

		case 'todays_task_summary':

			$user_id   = $data->user_id;
			$time_span = $data->time_span;


		    $result = (object)array();

			$result->all_tasks = $obj_dashboard->todays_task_summary($user_id,3,$time_span);  // 3 for all task
			$result->pending   = $obj_dashboard->todays_task_summary($user_id,1,$time_span);  // 1 for pending
			$result->completed = $obj_dashboard->todays_task_summary($user_id,0,$time_span);  // 0 for completed 
			$result->followup  = $obj_dashboard->todays_task_summary($user_id,2,$time_span);  // 2 for follow up

			if($result){
			$response['response_code']=200;
			$response['response_message']="Success";
			$response['data']=$result;
			}
			else{
			$response['response_code']=400;
			$response['response_message']="Failure";	
			}

			echo json_encode($response);

		break;

		case 'last_5_task_summary':

			$user_id = $data->user_id;
			$token = @$data->token;

			$result = $obj_dashboard->last_5_task_summary($user_id);
			

			// $user_id_curl =  str_replace("'", "", $user_id);

			// $og_id  = $obj_dashboard->get_og_id($user_id);

			// print_r($og_id); die();

			if(!$result){

				$response['response_code']=400;
				$response['response_message']="Failure";

			}else{

			$customer_ids='';
			foreach($result as $keys1=>$pair1)
			{
				$customer_ids.=','.$pair1->customer_id;
			}
			$customer_ids=ltrim($customer_ids,',');

			//print_r($customer_ids); die();

			$task_ids='';
			foreach($result as $keys=>$pair)
			{
				$task_ids.=','.$pair->task_id;
			}
			$task_ids=ltrim($task_ids,',');

			// print_r($task_ids); die();
			      $curl_cust = curl_init();

				  curl_setopt_array($curl_cust, array(
				  CURLOPT_URL => ORDERGINI_API."/api/CRMServices/GetAllChemistDetails",
				  CURLOPT_RETURNTRANSFER => true,
				  CURLOPT_ENCODING => "",
				  CURLOPT_MAXREDIRS => 10,
				  CURLOPT_TIMEOUT => 30,
				  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				  CURLOPT_CUSTOMREQUEST => "POST",
				  CURLOPT_POSTFIELDS => "{\"ChemistDArry\":[".$customer_ids."]}",
				  CURLOPT_HTTPHEADER => array(
				    "Authorization: Bearer ".$token,
				    "Content-Type: application/json",
				    "Postman-Token: ee2b2d47-87ad-48e8-a522-8c96fb6c31a1",
				    "cache-control: no-cache"
				  ),
				 ));

				$response_cus_name= curl_exec($curl_cust);
				$cust_name_curl = json_decode($response_cus_name);



				// print_r($cust_name_curl); die();


				$err_cust = curl_error($curl_cust);

				curl_close($curl_cust);


			  $curl = curl_init();

			  curl_setopt_array($curl, array(
			  CURLOPT_URL => ORDERGINI_API."/api/CRMServices/DashboardOrderStatus",
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "POST",
			  CURLOPT_POSTFIELDS => " {\"TransactionArry\":[".$task_ids."]}",
			  CURLOPT_HTTPHEADER => array(
			    "Authorization: Bearer ".$token,
			    "Content-Type: application/json",
			    "Postman-Token: ee2b2d47-87ad-48e8-a522-8c96fb6c31a1",
			    "cache-control: no-cache"
			  ),
			));

			$response_cus_id= curl_exec($curl);
			$dashbord_details = json_decode($response_cus_id);

			//print_r($dashbord_details); die();

			$err = curl_error($curl);

			curl_close($curl);

			//$task_details = array();
			if(@$cust_name_curl){

			foreach ($result as $key1 => $value1) {

				foreach (@$cust_name_curl->result as $key2 => $value) {
					if(@$cust_name_curl->result[$key2]->ESCID != $value1->customer_id){
					// print_r($result[$key1]->customer_id);
						$result[$key1]->customer_name = "NA";
					}else{
						$result[$key1]->customer_name = @$cust_name_curl->result[$key2]->S_Name;
						// print_r($result[$key1]->customer_id);
					}
				}  // inner foreach	
			}  // outer foreach
			}  // if cust name curl

			if(@$dashbord_details){
			foreach (@$dashbord_details->DashBandOrderStatus as $key => $value) {

			// $result[$key]->task_id = "10";
			// $result[$key]->call_time = $result->call_time;
			$result[$key]->items= "XYZ";
			$result[$key]->total= "XYZ";
			$result[$key]->order_status	= $value->_Status;

			//print_r($user_details); die();
			}  // close for each
			}  // close inner if

			} // close outer else
			
			if($result){
			$response['response_code']=200;
			$response['response_message']="Success";
			$response['data']=$result;
			}
			else{
			$response['response_code']=400;
			$response['response_message']="Failure";	
			}


			echo json_encode($response);
			
		break;

		case 'average_task':

			$user_id = $data->user_id;
			$token   = @$data->token;
			$user_id_curl =  str_replace("'", "", $user_id);

			$og_id  = $obj_dashboard->get_og_id($user_id);

			$result  = $obj_dashboard->average_task($user_id);

			$curl = curl_init();

			curl_setopt_array($curl, array(
			  CURLOPT_URL => ORDERGINI_API."/api/CRMServices/Dashboard7DayCustomerCount",
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "POST",
			  CURLOPT_POSTFIELDS => " {\"UserID\":[".$og_id."]}",
			  CURLOPT_HTTPHEADER => array(
			    "Authorization: Bearer ".$token,
			    "Content-Type: application/json",
			    "Postman-Token: ee2b2d47-87ad-48e8-a522-8c96fb6c31a1",
			    "cache-control: no-cache"
			  ),
			));

			$response_cus_id= curl_exec($curl);
			$dashbord_details = json_decode($response_cus_id);

			//print_r($dashbord_details); die();

			$err = curl_error($curl);

			curl_close($curl);

		    if(@$dashbord_details){
			foreach (@$dashbord_details->DashCustomerCount as $key => $value1) {

				//$result[$key]->DayDate = $value->DayDate;
				$result[$key]->customers_order = @$value1->OrderCount;
			//print_r($user_details); die();
			}
			}

			if($result){
			foreach ($result as $key => $value) {

					$result[$key]->DayDate = $value->DayDate;
					$result[$key]->tasks = $value->tasks;
			}
			}

			if($result){
			$response['response_code']=200;
			$response['response_message']="Success";
			$response['data']=$result;
			}
			else{
			$response['response_code']=400;
			$response['response_message']="Failure";	
			}

			echo json_encode($response);

		break;


		case 'get_users':

			$user_id = $data->user_id;
			$user_role = $data->user_role;
			$all_users = '';

			if($user_role == 1){

				$dis = $obj_dashboard->get_distributors();
				// $team_lead = $obj_dashboard->get_team_leads($user_id);
				// $agent = $obj_dashboard->get_agents($user_id);

				$all_users = $dis;    //array_merge($dis,$team_lead,$agent);

			}elseif($user_role == 2){

				$team_lead = $obj_dashboard->get_team_leads($user_id);
				$agent = $obj_dashboard->get_agents($user_id,2);

				$all_users = array_merge($team_lead,$agent);
				// print_r($all_users);

			}elseif($user_role == 3){

				$all_users = $obj_dashboard->get_agents($user_id,3);
				// print_r($all_users);

			}

			if($all_users){
			$response['response_code']=200;
			$response['response_message']="Success";
			$response['data']=$all_users;
			}
			else{
			$response['response_code']=400;
			$response['response_message']="Failure";	
			$response['data']=$all_users;

			}

			echo json_encode($response);

		break;


		case 'cases_resolved_team':

			$user_id    = $data->user_id;
			$time_span	= $data->time_span;

            $result = (object)array();

			$result->resolved = $obj_dashboard->cases_resolved_team($user_id,$time_span);
			$result->unresolved = $obj_dashboard->cases_unresolved_team($user_id,$time_span);

			if($result){
			$response['response_code']=200;
			$response['response_message']="Success";
			$response['data']=$result;
			}
			else{
			$response['response_code']=400;
			$response['response_message']="Failure";	
			}

			echo json_encode($response);
			
		break;

		case 'performance_overview':

			$user_id = $data->user_id;
			$token = @$data->token;
			$time_span = @$data->time_span;



			$user_id_curl =  str_replace("'", "", $user_id);

			$og_id  = $obj_dashboard->get_og_id($user_id);


			// $result = (object)array();


			 // print_r($og_id);  

			if($time_span == 1){

			$curl = curl_init();

			curl_setopt_array($curl, array(
			  CURLOPT_URL => ORDERGINI_API."/api/CRMServices/DashboardTodayOrder",
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "POST",
			  CURLOPT_POSTFIELDS => "{\"Dash_1\":[{\"UserID\":[\"".$og_id."\"]}]}",
			  CURLOPT_HTTPHEADER => array(
			    "Authorization: Bearer ".$token,
			    "Content-Type: application/json",
			    "Postman-Token: ee2b2d47-87ad-48e8-a522-8c96fb6c31a1",
			    "cache-control: no-cache"
			  ),
			));

			$response_cus_id= curl_exec($curl);
			$dashbord_details = json_decode($response_cus_id)->result;
			   // print_r($dashbord_details); die();

			  // print_r($result); die();


			// if($dashbord_details->status == "failure"){
			// 	$response['response_code']=400;
			// 	$response['response_message']="Failure";	

			// 	echo json_encode($response); die();
			// }

 
			$err = curl_error($curl);

			curl_close($curl);

			}
			elseif($time_span == 2){

			  $curl = curl_init();

			  curl_setopt_array($curl, array(
			  CURLOPT_URL => ORDERGINI_API."/api/CRMServices/DashboardWeeklyOrder",
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "POST",
			  CURLOPT_POSTFIELDS => "{\"Dash_2\":[{\"UserID\":[\"".$og_id."\"]}]}",
			  CURLOPT_HTTPHEADER => array(
			    "Authorization: Bearer ".$token,
			    "Content-Type: application/json",
			    "Postman-Token: ee2b2d47-87ad-48e8-a522-8c96fb6c31a1",
			    "cache-control: no-cache"
			  ),
			));

			$response_cus_id= curl_exec($curl);

			$dashbord_details = json_decode($response_cus_id);

			// if($dashbord_details->status == "failure"){
			// 	$response['response_code']=400;
			// 	$response['response_message']="Failure";	

			// 	echo json_encode($response); die();
			// }
			// print_r($dashbord_details); die();

			$err = curl_error($curl);

			curl_close($curl);

			}

			$result = (object)array();

			
			$result->customers_ordered = @$dashbord_details[0]->TodayCustomerOrderCount;
			$result->difference_order  = @$dashbord_details[0]->CustomerOrderCount;

			$result->total_items	   = @$dashbord_details[0]->TotalOrderItems;
			$result->difference_items  = @$dashbord_details[0]->CustomerOrderItems;

			$result->average_order_value          = @$dashbord_details[0]->TodayAvgOrderAmount;
			$result->difference_avg_order_value   = @$dashbord_details[0]->CustomerAvgOrderAmount;

			$result->total_order_value            = @$dashbord_details[0]->TodayOrderAmount;
			$result->difference_total_order_value = @$dashbord_details[0]->CustomerOrderValue;


			// $result->customers_ordered = "11";
			// $result->difference_order  = "11";

			// $result->total_items	   = "11";
			// $result->difference_items  = "11";

			// $result->average_order_value          = "11";
			// $result->difference_avg_order_value   = "11";

			// $result->total_order_value            = "11";
			// $result->difference_total_order_value = "11";



			if($result){
			$response['response_code']=200;
			$response['response_message']="Success";
			$response['data']=$result;
			}
			else{
			$response['response_code']=400;
			$response['response_message']="Failure";	
			}

			echo json_encode($response);

		break;

		case 'top_10_brands':

			$user_id = $data->user_id;
			$token = @$data->token;
			$time_span = @$data->time_span;

			$count = 0;

			$user_id_curl =  str_replace("'", "", $user_id);

			$og_id  = $obj_dashboard->get_og_id($user_id);


			$curl = curl_init();

			  curl_setopt_array($curl, array(
			  CURLOPT_URL => ORDERGINI_API."/api/CRMServices/DashboardTotalBrandsOrder",
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "POST",
			  CURLOPT_POSTFIELDS => "{\"UserID\":[\"".$og_id."\"]}",
			  CURLOPT_HTTPHEADER => array(
			    "Authorization: Bearer ".$token,
			    "Content-Type: application/json",
			    "Postman-Token: ee2b2d47-87ad-48e8-a522-8c96fb6c31a1",
			    "cache-control: no-cache"
			  ),
			));

			$response_cus_id= curl_exec($curl);

		    //print_r($response_cus_id); die();

			$dashbord_details = json_decode($response_cus_id);

			  // print_r($dashbord_details); die();

			// if($dashbord_details->status == "failure"){
			// 	$response['response_code']=400;
			// 	$response['response_message']="Failure";	

			// 	echo json_encode($response); die();
			// }

			$err = curl_error($curl);

			curl_close($curl);

			$today = array();
			$week = array();
			$result = array();
			$i = 0;

			if($time_span == 1){

			if($dashbord_details->message == "OrderTotalBrandsToday Record Not found" || $dashbord_details->status == "failure"){
				$response['response_code']=400;
				$response['response_message']="Failure";	

				echo json_encode($response); die();
			}
			

		 	foreach (@$dashbord_details->OrderTotalBrandsToday as $key => $value) {

				$today[$key] = new \stdClass();
		 		if(empty($value->TodayTopBrand)){

		 		}else{
		 			$count  += $value->Count;
			 		$today[$key]->customer_name = $value->TodayTopBrand;
			 		$today[$key]->percent_value = $value->Count;
		 		}
		 		if ($key == 10) break;
			}

			foreach ($today as $key => $row) {	    	
			    // $percent = ($row->Count/$count)*100;
		 		$result[$i] = new \stdClass();
			    if(empty($row->customer_name)){

		 		}else{
			        $percent = ($row->percent_value/$count)*100;
			    	$result[$i]->customer_name = $row->customer_name;
			    	$result[$i]->percent_value  = round($percent,2);
			    	$i++;
		 		}
		 		if ($key == 10) break;

			}

			if($result){
			$response['response_code']=200;
			$response['response_message']="Success";
			$response['data']=$result;
			}
			else{
			$response['response_code']=400;
			$response['response_message']="Failure";	
			}

			echo json_encode($response); die();

			}

			if($time_span == 2){

			if($dashbord_details->message == "OrderTotalBrandsToday Record Not found" || $dashbord_details->status == "failure"){
				$response['response_code']=400;
				$response['response_message']="Failure";	

				echo json_encode($response); die();
			}

		 	foreach (@$dashbord_details->OrderTotalBrandsWeekly as $key => $value) {



		 		$week[$key] = new \stdClass();
		 		if(empty($value->WeeklyTopBrand)){

		 		}else{
		 			$count  += $value->Count;
			 		$week[$key]->customer_name = $value->WeeklyTopBrand;
			 		$week[$key]->percent_value = $value->Count;
		 		}

		 		if ($key == 10) break;
			}

			// print_r($count); die();

			foreach ($week as $key => $row) {	    	
			    // $percent = ($row->Count/$count)*100;
		 		$result[$i] = new \stdClass();
			    if(empty($row->customer_name)){

		 		}else{

			        $percent = ($row->percent_value/$count)*100;
			    	$result[$i]->customer_name = $row->customer_name;
			    	$result[$i]->percent_value  = round($percent,2);
			    	$i++;
		 		}

		 		if ($key == 10) break;

			}

			if($result){
			$response['response_code']=200;
			$response['response_message']="Success";
			$response['data']=$result;
			}
			else{
			$response['response_code']=400;
			$response['response_message']="Failure";	
			}

			echo json_encode($response);

			}

			
		break;

	}

?>