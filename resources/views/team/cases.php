 <?php 
  $path_init = $_SERVER['DOCUMENT_ROOT']."/crm/resources/init.php";
	include_once($path_init);

 ?>

<link href="<?php echo ROOT_URI; ?>/resources/lib/select2/select2.min.css" rel="stylesheet" />
<link href="<?php echo ROOT_URI; ?>/assets/css/team.css" rel="stylesheet" type="text/css">
<script src="<?php echo ROOT_URI; ?>/resources/lib/select2/select2.min.js"></script>
 <script src="<?php echo ROOT_URI; ?>/resources/lib/time_ago/tinyAgo.min.js"></script>
<link href="<?php echo ROOT_URI; ?>/resources/lib/timepicker/mdtimepicker.css" rel="stylesheet" type="text/css">
<!-- right side main body starts -->
<div class="col-md-10 col-lg-10 col-sm-10 col-xs-10 col-lg-offset-2 col-md-offset-2 col-sm-offset-2 col-xs-offset-2 no-lr-pad back-color-grey">
	<!-- main section starts here contains upper part and lower part of cases page, cases detail section is outside this section -->
	<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad " id="main-section-cases">
		 <!-- for upper part -->
	
	    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 back-color-white padding-top-13 border-bottom padding-left-40">
	        <p class="font-size-24 float-inline"><b>Cases</b></p>
	        <div class="margin-top-6">
	          <a data-toggle="modal" data-target="#create_case_modal" class="a-deco color-brand margin-left-16"><img class="margin-right-8" src="<?php echo ROOT_URI; ?>/assets/img/assets/create-new-task.svg">Create New Case</a>
	        </div>
	        <div class="pull-right margin-top--20">
	        	<a class="a-deco color-grey-1 float-inline margin-left-25 display-task-active tab_case" data-filter_selected="" id="all_case_tab">All</a>
	        	<a class="a-deco color-grey-1 float-inline margin-left-25 tab_case" data-filter_selected="" id="open_case_tab">Open</a>
	        	<a class="a-deco color-grey-1 float-inline margin-left-25 tab_case" data-filter_selected="" id="cancelled_tab">Cancel</a>
	        	<a class="a-deco color-grey-1 float-inline margin-left-25 tab_case" data-filter_selected="" id="closed_case_tab">Close</a>
	        	<div class="dropdown float-inline margin-left-25 margin-top--10">
	                <button class="btn btn-default brand-btn-white add-mem-btn1 btn-drop-dwn width-107  dropdown-toggle" type="button" data-toggle="dropdown" id="drop-d-action-task"><img class="" src="<?php echo ROOT_URI; ?>/assets/img/assets/filter.svg"> <span class="padding-left-5">Filters</span>
	                  
	                <!-- <span class="caret"></span> --></button>
	                <ul class="dropdown-menu pull-right">
	                  <li>
	                  	<a class="drop-sub-chl " >
		                  	<div class="filt-chk checkbox margin-top-bot-2">
							  <label><input class="filter_condition" data-sort_selected="1" type="checkbox" data-condition_value="5" value=""><span class="margin-left-10">Case Associated With Me</span></label>
							</div>
	                  	</a>
	                  </li>
	                   <li>
	                  	<a class="drop-sub-chl " >
		                  	<div class="filt-chk checkbox margin-top-bot-2">
							  <label><input class="filter_condition" data-sort_selected="1" type="checkbox" data-condition_value="6" value=""><span class="margin-left-10">Case Not Associated With Me</span></label>
							</div>
	                  	</a>
	                  </li>
	                </ul>
	              </div>
	        </div>
	    </div>
	   <!-- upper part ends -->

	     <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 margin-top-10 padding-left-40 padding-right-40" id="main_case">
	        <!-- Case 1 tile -->
	        
	         <!-- Case 1 tile ends here  -->

	         <!-- Case 2 tile  -->
	         
	         <!-- Case 2 tile ends here  -->

	       
	     </div>
	     <a class="comment_loader_message margin-top-10 margin-bottom-10 col-sm-12 col-xs-12 col-md-12 col-lg-12 a-deco-grey text-center font-size-12"><u>Load More</u></a>
	</div>
	    <!-- items tiles starts here  -->
	     <!-- Modal starts here  -->
	     		<!-- transfer modal  -->
		     	  <div id="transfer-modal" class="modal fade" role="dialog">
				    <div class="modal-dialog margin-top-70">

				      <!-- Modal content-->
				      <div class="modal-content col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
				        <div class="modal-header col-md-12 col-lg-12 col-sm-12 col-xs-12 ">
				          <button type="button" class="close" data-dismiss="modal">&times;</button>
				          <p class="font-size-20 margin-left-10"><b>Transfer Case</b></p>
				        </div>
				        <div class="modal-body col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
				          <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
				                 
				                
				               	<div class="form-group col-md-12 col-lg-12 col-sm-12 col-xs-12 select-ajax-assign">
				                  <p class="font-size-12 color-grey-1 margin-bottom--15"  for="customer_sel">Assign To</p><br>
				                  <!-- Option for assigned to -->
									<select class=" form-control tag-for-cust  js-data-select-ajax-assign width-100" id="assign_customer_modal" name="tags_for_assign[]">
				            
				                    </select>
				                    <!-- option for assigned to ends here -->

				                </div>
				                
				          </div>
				        </div>
				        <div class="modal-footer col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
				          <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 padding-left-30 padding-right-30">
				              <button type="button" class="btn btn-default pull-left add-mem-btn1" data-dismiss="modal">Cancel</button>
				              <button type="button" class="btn btn-default pull-right brand-btn add-mem-btn width-88" id="confirm_reassign" >Confirm</button>
				          </div>
				        </div>
				      </div>

				    </div>
				  </div>
				 <!-- transfer modal ends here  -->
				 <!-- Case Cancelled modal  -->
		     	  <div id="case_cancelled-modal" class="modal fade" role="dialog">
				    <div class="modal-dialog margin-top-70">

				      <!-- Modal content-->
				      <div class="modal-content col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-padmodal-content col-md-6 col-lg-6 col-sm-12 col-xs-12 col-lg-offset-3 col-md-offset-3 no-lr-pad">
				        <div class="modal-header col-md-12 col-lg-12 col-sm-12 col-xs-12 ">
				          <button type="button" class="close" data-dismiss="modal">&times;</button>
				          <p class="font-size-20 margin-left-10"><b>Cancel This Case</b></p>
				        </div>
				        <div class="modal-body col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
				          <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
				                 
				                
				               	<div class="form-group col-md-12 col-lg-12 col-sm-12 col-xs-12 select-ajax-assign">
				                  <p class="font-size-14 color-grey-1 margin-bottom--15" >Are you sure you want to cancel this case ?</p><br>

				                </div>
				                
				          </div>
				        </div>
				        <div class="modal-footer col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
				          <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 padding-left-30 padding-right-30">
				              <button type="button" class="btn btn-default pull-left add-mem-btn1" data-dismiss="modal">Cancel</button>
				              <button type="button" class="btn btn-default pull-right brand-btn add-mem-btn width-88" id="confirm_cancel">Confirm</button>
				          </div>
				        </div>
				      </div>

				    </div>
				  </div>
				 <!-- Case Cancelled modal ends here  -->

	    		<!-- Case Closed Modal  -->
	    			 <div id="case_closed-modal" class="modal fade" role="dialog">
					    <div class="modal-dialog margin-top-70">

					      <!-- Modal content-->
					      <div class="modal-content col-md-6 col-lg-6 col-sm-12 col-xs-12 col-lg-offset-3 col-md-offset-3 no-lr-pad">
					        <div class="modal-header col-md-12 col-lg-12 col-sm-12 col-xs-12 ">
					          <button type="button" class="close" data-dismiss="modal">&times;</button>
					          <p class="font-size-20 margin-left-10"><b>Close This Case</b></p>
					        </div>
					        <div class="modal-body col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
					          <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
					                 
					                
					               	<div class="form-group col-md-12 col-lg-12 col-sm-12 col-xs-12 select-ajax-assign">
					                  <p class="font-size-12 color-grey-1 text-center margin-bottom--15"  for="customer_sel">
					                  	Are you sure want to close this case?
					                  </p><br>
					                </div>
					                
					          </div>
					        </div>
					        <div class="modal-footer col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
					          <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 padding-left-30 padding-right-30">
					              <button type="button" class="btn btn-default pull-left add-mem-btn1" data-dismiss="modal">Cancel</button>
					              <button type="button" class="btn btn-default pull-right brand-btn add-mem-btn width-88" id="confirm_close">Confirm</button>
					          </div>
					        </div>
					      </div>

					    </div>
					  </div>	
	    		<!-- Case Closed Modal ends here  -->
	    		<!-- create a new case modal  -->
	    			<div id="create_case_modal" class="modal fade" role="dialog">
					    <div class="modal-dialog margin-top-70">

					      <!-- Modal content-->
					      <div class="modal-content col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
					        <div class="modal-header col-md-12 col-lg-12 col-sm-12 col-xs-12 ">
					          <button type="button" class="close" data-dismiss="modal">&times;</button>
					          <p class="font-size-20 margin-left-10"><b>Create New Case</b></p>
					        </div>
					        <div class="modal-body col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
					          <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
					                <!-- <div class="form-group col-md-12 col-lg-12 col-sm-12 col-xs-12" >
					                  <span class="font-size-12 color-grey-1" for="username">Username:</span>
					                  <input type="text" name="username" class="form-control" id="username">
					                </div> -->
					                <div class="form-group col-md-12 col-lg-12 col-sm-12 col-xs-12">
					                  <p class="font-size-12 color-grey-1 margin-bottom--15"  for="customer_sel">Customer</p><br>
										<select class=" form-control tag-for-cust  js-data-select-ajax width-100" id="assign-agent-drop" name="tags_for_create[]">
					                    </select>	
					                </div>
					                <div class="form-group col-md-12 col-lg-12 col-sm-12 col-xs-12">
					                  <span class="font-size-12 color-grey-1"  for="case">Case</span>
					                  <input type="text" name="case" class="form-control margin-top-5" placeholder="Enter case detail" id="case_name">
					                </div>
					                <div class="form-group col-md-12 col-lg-12 col-sm-12 col-xs-12">
					                   	<div class="checkbox">
										  <label class="color-grey"><input type="checkbox" class="assign-chk" id="check_emp" value="me">Assign to Myself</label>
										</div> 
					                </div>
					               	<div class="form-group col-md-12 col-lg-12 col-sm-12 col-xs-12 select-ajax-assign" id="assign_div">
					                  <p class="font-size-12 color-grey-1 margin-bottom--15"  for="customer_sel">Assign To</p><br>
										<select class=" form-control tag-for-cust  js-data-select-ajax-assign width-100" id="agent_display" name="tags_for_assign[]">
					                    </select>
					                </div>
					          </div>
					        </div>
					        <div class="modal-footer col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
					          <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 padding-left-30 padding-right-30">
					              <button type="button" class="btn btn-default pull-left add-mem-btn1" data-dismiss="modal">Cancel</button>
					              <button type="button" class="btn btn-default pull-right brand-btn add-mem-btn"  id="create_case_ajax">Create</button>
					          </div>
					        </div>
					      </div>

					    </div>
					 </div>

	    		<!-- create a new case modal ends here  -->

	     <!-- modal ends here  -->
	   </div>
</div>
	<!-- main section ends here -->
  
 <!-- right side main body ends -->
      
 <script src="<?php echo ROOT_URI; ?>/resources/lib/timepicker/mdtimepicker.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
  		 var numbering_for_case_serial=0;
  	var html_load='';
  	 $(document).on('click','.load_more',function() {
      var id_task=$(this).attr("data-id-case");

      var entity_id=2;
      var flag=0;

       $.ajax({
              dataType :'json',
                       method : 'POST',
                       async:false,
                  url : "<?php echo ROOT_URI; ?>/resources/services/remark_api.php",
                  data: "{\n\"action\":\"display_remarks_all\",\n\"entity_id\":\""+id_task+"\",\n\"entity_type\":\""+entity_id+"\"}",}).success(function(resp) {
                    
                    if(resp.response_code==200)
                    {  html_load='';
                    	for(i=1;i<resp.data.length;i++){
                    		user_role="";
                    		 if(resp.data[i].role==1){
                    		 	user_role="Admin";
                    		 }
                    		 else if(resp.data[i].role==2){
                    		 	user_role="Distributor";
                    		 }
                    		 else if(resp.data[i].role==3){
                    		 	user_role="Team Lead";
                    		 }
                    		 else if(resp.data[i].role==4){
                    		 	user_role="Agent";
                    		 }
                    		 else{

                    		 }
                        var d_case = new Date(resp.data[i].created_date);
                  var p_case=(ago(d_case.getTime()));
                    	html_load+='<div class="col-md-11 col-lg-11 col-sm-11 col-xs-11 col-md-offset-1 col-lg-offset-1 padding-top-10 " >'+
		        		'<div class="col-lg-1 col-md-1">'+
		        			'<img src="<?php echo S3_BUCKET;?>'+resp.data[i].user_profile_pic+'" class="img-circle  margin-left-30" width="40">'+
		        		'</div>'+
		        		'<div class="col-md-11 col-lg-11 col-sm-11 col-xs-11">'+
		        			'<p class="margin-bottom-0 font-size-14"><b>'+resp.data[i].user_name+'</b><span class="margin-left-20 font-size-12 color-grey-1">'+p_case+'</span></p>'+
		        			'<p class="font-size-12 color-grey ">'+user_role+'</p>'+
		        		'</div>'+
	        		'</div>'+
	        		'<div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 col-md-offset-2 ">'+
		        		'<p class="padding-left-5">'+resp.data[i].remark+'</p>'+
		        	'</div>';
                    }
                    html_load+='<div class="col-lg-11 col-md-11 col-sm-10 col-xs-10 col-md-offset-1 ">'+
	        		
	        	'</div>'+
	        	'<!-- collapsed comments starts here  -->'+
	        	'<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 collapse in" id="add-comment">'+
	        		'<div class="col-md-11 col-lg-11 col-sm-11 col-xs-11 col-md-offset-1 col-lg-offset-1 padding-top-10 " >'+
		        		'<div class="col-lg-1 col-md-1  col-sm-11 col-xs-11">'+
		        			'<img src="<?php echo S3_BUCKET;?>'+sessionStorage.getItem("profile_pic")+'" class="img-circle  margin-left-30" width="40">'+
		        		'</div>'+
		        		'<div class="col-md-11 col-lg-11 col-sm-11 col-xs-11">'+
		        			'<textarea class="width-100 comments_text"></textarea>'+
		        		'</div>'+
		        		'<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 col-lg-offset-1 col-md-offset-1">'+
		        			'<button class="btn btn-default brand-btn comments_save" data-case_id_save="'+id_task+'">Submit</button>'+
		        			'<button class="btn btn-default brand-btn-white">Ignore</button>'+
		        		'</div>'+
	        			'<div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 col-md-offset-2 ">'+
		        		
		        		'</div>'+
	        		'</div>'+ 		
	        	'</div>';
                    $('#comments'+id_task).html(html_load);
                }
                });
              });

  	 var condition = '';
  	 var filter = '';
  	 var page_count = 1;
         
          $(document).on('click', '.comment_loader_message', function () {
                var conditon=$('.filter_condition').attr('data-sort_selected');
                var filter=$('.tab_case').attr('data-filter_selected');
                  display_case(conditon,filter,++page_count);
                  
                
          });
  	$(document).on('click','#cancelled_tab',function(){

  		$('.tab_case').removeClass('display-task-active');
  		$(this).addClass('display-task-active');
   		$('#main_case').html('');
  		$('.filter_condition').attr('data-sort_selected',3);
  		page_count=1;
  		numbering_for_case_serial=0;
  		display_case(3,$(this).attr('data-filter_selected'),1);	

  	});

  	$(document).on('click','#open_case_tab',function(){

  		$('.tab_case').removeClass('display-task-active');
  		$(this).addClass('display-task-active');
  		$('#main_case').html('');
  		$('.filter_condition').attr('data-sort_selected',2);
  		page_count=1;
  		numbering_for_case_serial=0;
  		display_case(2,$(this).attr('data-filter_selected'),1);

  	});

  	$(document).on('click','#closed_case_tab',function(){

  		$('.tab_case').removeClass('display-task-active');
  		$(this).addClass('display-task-active');
  		$('#main_case').html('');
  		$('.filter_condition').attr('data-sort_selected',4);
  		page_count=1;
  		numbering_for_case_serial=0;
  		display_case(4,$(this).attr('data-filter_selected'),1);

  	});

  	$(document).on('click','#all_case_tab',function(){
		
		$('.tab_case').removeClass('display-task-active');
  		$(this).addClass('display-task-active');
  		$('#main_case').html('');
  		$('.filter_condition').attr('data-sort_selected',1);
  		page_count=1;
  		numbering_for_case_serial=0;
  		display_case(1,$(this).attr('data-filter_selected'),1);
  	});

  	$('input.filter_condition').on('change', function() {
    
    	$('input.filter_condition').not(this).prop('checked', false);  
	
	});

  	$(document).on('click','input[type="checkbox"]',function(){

  		if($(this).attr('data-condition_value') == 6 && $(this).is(':checked')){  // not associated with me
  			//toast_it('not');
  			$('.tab_case').attr('data-filter_selected',6);
  			$('#main_case').html('');
  			page_count=1;
  			numbering_for_case_serial=0;
  			display_case($(this).attr('data-sort_selected'),6,1);
  		}

  		if($(this).attr('data-condition_value') == 5 && $(this).is(':checked')){  // associated with me 
  			//toast_it('with');
  			$('#main_case').html('');
  			$(this).attr('data-sort_selected');
  			$('.tab_case').attr('data-filter_selected',5);
  			page_count=1;
  			numbering_for_case_serial=0;
  			display_case($(this).attr('data-sort_selected'),5,1)

  		}

  		if($(this).is(':not(:checked)')){
  			//toast_it('none');
   		    $('#main_case').html('');
   		    $('.tab_case').removeClass('display-task-active');
   		    $('#all_case_tab').addClass('display-task-active');
   		    $('.tab_case').attr('data-filter_selected',''); 	
   		    page_count=1;	
   		    numbering_for_case_serial=0;	
  			display_case($('.filter_condition').attr('data-sort_selected'),'',1);
  		}


  	});

  	 display_case(1,'',1);
  

  	 function display_case(condition,filter,page_no){

  	 		$.ajax({

  		
                       url: "<?php echo ROOT_URI; ?>/resources/services/case_main.php",
                       dataType :'json',
                       method : 'POST',
                       async:false,
                       data: '{\n\"action\":\"display_case\",\n\"condition\":\"'+condition+'\",\n\"filter\":\"'+filter+'\",\n\"token\":\"'+sessionStorage.getItem("token")+'\",\n\"user_id\":\"'+sessionStorage.getItem("user_id")+'\",\n\"role\":\"'+sessionStorage.getItem("role")+'\",\n\"page_no\":'+page_no+'\n}',
                       }).success(function(resp){
                        console.log(resp);
                        if(resp.response_code==200)
                        {
                        	
                  			$('.comment_loader_message').show();
                        	var htmlText="";
                        	for(i=0;i<resp.data.length;i++)
                                  {
                                  	
                                  	// console.log(numbering_for_case_serial);
                                  	numbering_for_case_serial=numbering_for_case_serial+1;
                                  	 // alert(numbering_for_case_serial);
                                  	var clas='';
				                    if(resp.data[i].status_label=="EXPIRED")
				                    {
				                      clas='label-default';
				                    }
				                    else if(resp.data[i].status_label=="NEW")
				                    {
				                      clas="label-success";
				                    }
				                    else
				                    {
				                      clas='label-danger';
				                    }

                                  	var d_cr= new Date(resp.data[i].case_created_date);
                                  	var d_as= new Date(resp.data[i].case_assigned_date);
                                  	var d_cl= new Date(resp.data[i].case_close_date);
                                  	var d_can= new Date(resp.data[i].case_cancel_date);
                                  	var p=(ago(d_cr.getTime()));
                                  	var q=(ago(d_as.getTime()));
                                  	var r=(ago(d_cl.getTime()));
                                  	var s=(ago(d_can.getTime()));
                                  	if(resp.data[i].last_remark==null)
                                  	{

                                  		htmlText+='  <div class="back-color-white col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad main-item-for-tiles-cases box-shadow1 margin-top-16">'+
	        	'<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 margin-top-10">'+
	        		'<div class="col-lg-1 col-md-1 col-sm-4 col-xs-4 margin-left-10 margin-top-25">'+
	            		'<span class="case-num-div '+clas+' text-bold">#'+numbering_for_case_serial+'</span>'+
	          		 '</div>'+
		            '<div class="col-lg-8 col-md-8 col-sm-4 col-xs-4 no-lr-pad">'+
		            	'<p class="font-size-16 margin-top-16 margin-bottom-0 pad-0 text-bold ellipsis">'+resp.data[i].customer_name+' <span class="label '+clas+' margin-left-10">'+resp.data[i].status_label+'</span></p>'+
		            	 '<p class=" color-grey-1 pad-0 ellipsis" data-toggle="tooltip" title="'+resp.data[i].customer_address+'"><i class="fa fa-map-marker" aria-hidden="true"></i> '+resp.data[i].customer_address+'</p>'+
		            '</div>'+
	            	'<div class="dropdown col-lg-2 col-md-2 col-sm-12 col-xs-12 pull-right">';
	            		if(resp.data[i].status == 0 && resp.data[i].case_state ==1){}
	        			else{
	        htmlText = htmlText + '<button class="btn btn-default brand-btn add-mem-btn1 btn-drop-dwn dropdown-toggle width-btn-120 margin-top-20 call-btn-task-tiles pull-right" type="button" data-toggle="dropdown" id="drop-d-action-task">Resolution</button>'+	            		
						'<ul class="dropdown-menu  dropdown-menu-right">';
	        			}
						if(resp.data[i].case_state == 0){
			htmlText = htmlText + '<li><a class="drop-sub-chl re-assign-pop" data-target="#transfer-modal" data-toggle="modal" data-id-task="'+resp.data[i].case_id+'">Transfer </a></li>';
						}

						if(resp.data[i].case_state == 1 && resp.data[i].status == 0){

						}

						if(resp.data[i].case_state == 1 && resp.data[i].status == 1){
			htmlText = htmlText + '<li><a class="drop-sub-chl re-assign-pop" data-target="#transfer-modal" data-toggle="modal" data-id-task="'+resp.data[i].case_id+'">Transfer </a></li>'+
							'<li><a class="drop-sub-chl cancel-pop" data-target="#case_cancelled-modal" data-toggle="modal" data-id-task="'+resp.data[i].case_id+'">Cancel</a></li>'+
							'<li><a class="drop-sub-chl close-pop" data-target="#case_closed-modal" data-toggle="modal" data-id-task="'+resp.data[i].case_id+'">Close</a></li>';
						}
	

							// '<li><a class="drop-sub-chl re-assign-pop" data-target="#transfer-modal" data-toggle="modal" data-id-task="'+resp.data[i].case_id+'">Transfer </a></li>'+
							// '<li><a class="drop-sub-chl cancel-pop" data-target="#case_cancelled-modal" data-toggle="modal" data-id-task="'+resp.data[i].case_id+'">Cancel</a></li>'+
							// '<li><a class="drop-sub-chl close-pop" data-target="#case_closed-modal" data-toggle="modal" data-id-task="'+resp.data[i].case_id+'">Close</a></li>'+
			htmlText = htmlText + '</ul>'+
	            	'</div>'+
	        	'</div>'+
	        	'<div class="col-md-11 col-lg-11 col-sm-11 col-xs-11 col-md-offset-1 col-lg-offset-1  ">'+	        		
	        		'<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad wrap">'+
		            	'<p class="margin-left-10 margin-top-10 font-size-12 text-bold color-grey">Case</p>'+
		            	'<p class=" margin-left-10 margin-top-10 font-size-14">'+resp.data[i].case_name+'</p>'+
		            '</div>'+
	        	'</div>'+
	        	'<div class="col-lg-11 col-md-11 col-sm-10 col-xs-10 col-md-offset-1 ">'+
	        		'<a class="margin-left-40 padding-left-5 text-bold a-deco" data-toggle="collapse" data-target="#add-comment'+resp.data[i].case_id+'" aria-controls="add-comment" id="add-comment-btn"><i class="fa fa-plus padding-right-5"></i>Add Comment</a>'+
	        	'</div>'+
	        	'<!-- collapsed comments starts here  -->'+
	        	'<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 collapse in" id="add-comment'+resp.data[i].case_id+'">'+
	        		'<div class="col-md-11 col-lg-11 col-sm-11 col-xs-11 col-md-offset-1 col-lg-offset-1 padding-top-10 " >'+
		        		'<div class="col-lg-1 col-md-1  col-sm-11 col-xs-11">'+
		        			'<img src="<?php echo S3_BUCKET;?>'+resp.data[i].user_current_profile_pic+'" class="img-circle  margin-left-30" width="40">'+
		        		'</div>'+
		        		'<div class="col-md-11 col-lg-11 col-sm-11 col-xs-11">'+
		        			'<textarea class="width-100 comments_text" ></textarea>'+
		        		'</div>'+
		        		'<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 col-lg-offset-1 col-md-offset-1">'+
		        			'<button class="btn btn-default brand-btn comments_save" data-case_id_save="'+resp.data[i].case_id+'">Submit</button>'+
		        			'<button class="btn btn-default brand-btn-white">Ignore</button>'+
		        		'</div>'+
	        			'<div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 col-md-offset-2 ">'+
		        		
		        		'</div>'+
	        		'</div>'+ 		
	        	'</div>'+
	        	'<!-- collapsed comments ends here  -->'+
	        	'<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 margin-top-30 border-bottom"></div>'+
	            '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 margin-top-30 ">'+
	            	'<div class="wrapp">'+
	            		'<!-- <hr class="cust-hr"> -->'+
					  '<div class="links">'+
					    '<div class="assign-my-task-div-case track-case-assigned-active">'+
					      '<img src="<?php echo S3_BUCKET;?>'+resp.data[i].creator_profile_pic+'" class="float-inline user_assign-img"><p class="font-size-12 float-inline margin-top-6 margin-left-10 "><b>Created by '+resp.data[i].creator_user_name+'</b></p>'+
					      '<br><p class="case-timestamp font-size-12 text-muted">'+p+'</p>'+
					    '</div>'+
					    '<div class="assign-my-task-div-case ">'+
					      '<img src="<?php echo S3_BUCKET;?>'+resp.data[i].assigned_user_profile_pic+'" class="float-inline user_assign-img"><p class="font-size-12 float-inline margin-top-6 margin-left-10 "><b>Assigned to '+resp.data[i].assigned_user_name+'</b></p>'+
					      '<br><p class="case-timestamp font-size-12 text-muted">'+q+'</p>'+
					    '</div>';

					    if(resp.data[i].case_state == 0 ){
		htmlText = htmlText +  '<div class="assign-my-task-div-case track-case-assigned-inactive">'+
					     '<img src="<?php echo S3_BUCKET;?>'+resp.data[i].cancelling_user_profile_pic+'" class="float-inline user_assign-img"><p class="font-size-12 float-inline margin-top-6 margin-left-10 "><b>Cancelled by '+resp.data[i].cancelling_user_name+'</b></p>'+
					      '<br><p class="case-timestamp font-size-12 text-muted">'+s+'</p>'+
					    '</div>';

					    }

					    if(resp.data[i].status == 0 && resp.data[i].case_state == 1){
		htmlText = htmlText +  '<div class="assign-my-task-div-case track-case-assigned-inactive">'+
					     '<img src="<?php echo S3_BUCKET;?>'+resp.data[i].closing_user_profile_pic+'" class="float-inline user_assign-img"><p class="font-size-12 float-inline margin-top-6 margin-left-10 "><b>Closed by '+resp.data[i].closing_user_name+'</b></p>'+
					      '<br><p class="case-timestamp font-size-12 text-muted">'+r+'</p>'+
					    '</div>';
					    }
					   
		htmlText = htmlText + '</div>'+
					'</div>'+
	            '</div>'+
	         '</div>';
                                  	}
                                  	else
                                  	{
                                  		user_role="";
                    		 if(resp.data[i].remark_user_role==1){
                    		 	user_role="Admin";
                    		 }
                    		 else if(resp.data[i].remark_user_role==2){
                    		 	user_role="Distributor";
                    		 }
                    		 else if(resp.data[i].remark_user_role==3){
                    		 	user_role="Team Lead";
                    		 }
                    		 else if(resp.data[i].remark_user_role==4){
                    		 	user_role="Agent";
                    		 }
                    		 else{

                    		 }
                                  		var rem_cr= new Date(resp.data[i].remark_created_date);
                                  	var remar=(ago(rem_cr.getTime()));
                                  		htmlText+='<div class="back-color-white col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad main-item-for-tiles-cases box-shadow1 margin-top-16">'+
	        	'<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 margin-top-10">'+
	        		'<div class="col-lg-1 col-md-1 col-sm-4 col-xs-4 margin-left-10 margin-top-25">'+
	            		'<span class="case-num-div '+clas+' text-bold">#'+numbering_for_case_serial+'</span>'+
	          		 '</div>'+
		            '<div class="col-lg-8 col-md-8 col-sm-4 col-xs-4 no-lr-pad">'+
		            	'<p class="font-size-16 margin-top-16 margin-bottom-0 pad-0 text-bold">'+resp.data[i].customer_name+' <span class="label '+clas+' margin-left-10">'+resp.data[i].status_label+'</span></p>'+
		            	 '<p class=" color-grey-1 pad-0 ellipsis" data-toggle="tooltip" title="'+resp.data[i].customer_address+'"><i class="fa fa-map-marker" aria-hidden="true"></i> '+resp.data[i].customer_address+'</p>'+
		           '</div>'+
	            	'<div class="dropdown col-lg-2 col-md-2 col-sm-12 col-xs-12 pull-right">';
	        			if(resp.data[i].status == 0 && resp.data[i].case_state == 1){}
	        			else{
	        htmlText = htmlText + '<button class="btn btn-default brand-btn add-mem-btn1 btn-drop-dwn dropdown-toggle width-btn-120 margin-top-20 call-btn-task-tiles pull-right" type="button" data-toggle="dropdown" id="drop-d-action-task">Resolution</button>'+	            		
						'<ul class="dropdown-menu  dropdown-menu-right">';
	        			}
	            		

						if(resp.data[i].case_state == 0){
			htmlText = htmlText + '<li><a class="drop-sub-chl re-assign-pop" data-target="#transfer-modal" data-toggle="modal" data-id-task="'+resp.data[i].case_id+'">Transfer </a></li>';
						}

						if(resp.data[i].case_state == 1 && resp.data[i].status == 0){

						}

						if(resp.data[i].case_state == 1 && resp.data[i].status == 1){
			htmlText = htmlText + '<li><a class="drop-sub-chl re-assign-pop" data-target="#transfer-modal" data-toggle="modal" data-id-task="'+resp.data[i].case_id+'">Transfer </a></li>'+
							'<li><a class="drop-sub-chl cancel-pop" data-target="#case_cancelled-modal" data-toggle="modal" data-id-task="'+resp.data[i].case_id+'">Cancel</a></li>'+
							'<li><a class="drop-sub-chl close-pop" data-target="#case_closed-modal" data-toggle="modal" data-id-task="'+resp.data[i].case_id+'">Close</a></li>';
						}
	

							// '<li><a class="drop-sub-chl re-assign-pop" data-target="#transfer-modal" data-toggle="modal" data-id-task="'+resp.data[i].case_id+'">Transfer </a></li>'+
							// '<li><a class="drop-sub-chl cancel-pop" data-target="#case_cancelled-modal" data-toggle="modal" data-id-task="'+resp.data[i].case_id+'">Cancel</a></li>'+
							// '<li><a class="drop-sub-chl close-pop" data-target="#case_closed-modal" data-toggle="modal" data-id-task="'+resp.data[i].case_id+'">Close</a></li>'+
			htmlText = htmlText + '</ul>'+
	            	'</div>'+
	        	'</div>'+
	        	'<div class="col-md-11 col-lg-11 col-sm-11 col-xs-11 col-md-offset-1 col-lg-offset-1  ">'+	        		
	        		'<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad wrap">'+
		            	'<p class="margin-left-10 margin-top-10 font-size-12 text-bold color-grey">Case</p>'+
		            	'<p class="padding-bottom-20 margin-left-10 margin-top-10 font-size-14">'+resp.data[i].case_name+'</p>'+
		            '</div>'+
	        	'</div>'+
	        	'<div class="col-md-11 col-lg-11 col-sm-11 col-xs-11 col-md-offset-1 col-lg-offset-1 ">'+
	        		'<hr class="margin-left-30 margin-top-0">'+
	        		'<div class="col-lg-1 col-md-1">'+
	        			'<img src="<?php echo S3_BUCKET;?>'+resp.data[i].remark_user_profile_pic+'" class="img-circle  margin-left-30" width="40">'+
	        		'</div>'+
	        		'<div class="col-md-11 col-lg-11 col-sm-11 col-xs-11">'+
	        			'<p class="margin-bottom-0 font-size-14"><b>'+resp.data[i].remark_user_name+'</b><span class="margin-left-20 font-size-12 color-grey-1">'+remar+'</span></p>'+
	        			'<p class="font-size-12 color-grey ">'+user_role+'</p>'+
	        		'</div>'+
	        	'</div>'+
	        	'<div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 col-md-offset-2 ">'+
	        		'<p class="padding-left-5">'+resp.data[i].last_remark+'</p>'+
	        		'<a class="padding-left-5 a-deco load_more" data-toggle="collapse" data-target="#comments'+resp.data[i].case_id+'" aria-controls="comments" data-id-case='+resp.data[i].case_id+'><b>More Comments</b></a>'+
	        	'</div>'+
	        	'<!-- collapsed comments starts here  -->'+
	        	'<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 collapse" id="comments'+resp.data[i].case_id+'">'+
		        		
	        	'</div>'+
	        	'<!-- collapsed comments ends here  -->'+
	        	'<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 margin-top-30 border-bottom"></div>'+
	            '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 margin-top-30 ">'+
	            	'<div class="wrapp">'+
	            		'<!-- <hr class="cust-hr"> -->'+
					  '<div class="links">'+
					    '<div class="assign-my-task-div-case track-case-assigned-active">'+
					      '<img src="<?php echo S3_BUCKET;?>'+resp.data[i].creator_profile_pic+'" class="float-inline user_assign-img"><p class="font-size-12 float-inline margin-top-6 margin-left-10 "><b>Created by '+resp.data[i].creator_user_name+'</b></p>'+
					      '<br><p class="case-timestamp font-size-12 text-muted">'+p+'</p>'+
					    '</div>'+
					    '<div class="assign-my-task-div-case ">'+
					      '<img src="<?php echo S3_BUCKET;?>'+resp.data[i].assigned_user_profile_pic+'" class="float-inline user_assign-img"><p class="font-size-12 float-inline margin-top-6 margin-left-10 "><b>Assigned to '+resp.data[i].assigned_user_name+'</b></p>'+
					      '<br><p class="case-timestamp font-size-12 text-muted">'+q+'</p>'+
					    '</div>';

					    if(resp.data[i].case_state == 0){

  htmlText = htmlText + '<div class="assign-my-task-div-case track-case-assigned-inactive">'+
					     '<img src="<?php echo S3_BUCKET;?>'+resp.data[i].cancelling_user_profile_pic+'" class="float-inline user_assign-img"><p class="font-size-12 float-inline margin-top-6 margin-left-10 "><b>Cancelled by '+resp.data[i].cancelling_user_name+'</b></p>'+
					      '<br><p class="case-timestamp font-size-12 text-muted">'+s+'</p>'+
					    '</div>';

					    }else{
					    }

					    if(resp.data[i].status == 0 && resp.data[i].case_state == 1){

  htmlText = htmlText + '<div class="assign-my-task-div-case track-case-assigned-inactive">'+
					     '<img src="<?php echo S3_BUCKET;?>'+resp.data[i].closing_user_profile_pic+'" class="float-inline user_assign-img"><p class="font-size-12 float-inline margin-top-6 margin-left-10 "><b>Closed by '+resp.data[i].closing_user_name+'</b></p>'+
					      '<br><p class="case-timestamp font-size-12 text-muted">'+r+'</p>'+
					    '</div>';

					    }else{}

  htmlText = htmlText + '</div>'+
					'</div>'+
	            '</div>'+
	         '</div>';
                                  	} 

                                  }
                                  $("#main_case").append(htmlText);
                          $('[data-toggle="tooltip"]').tooltip();
                                  
                                  
                        }
                        else
                         {
                          toast_it("NO DATA TO SHOW");
                          $('.comment_loader_message').hide();
                         }
                    });



  	 }




  	
                   $('#assign-agent-drop').select2({
        ajax: {
              url: "<?php echo ROOT_URI; ?>/resources/services/tag_api.php",
              dataType: 'json',
              delay: 350,
              data: function (params) {
                  return {
                      q: params.term, // search term
                        page: params.page || 1,
                        token: sessionStorage.getItem("token"),
                        dist_id:sessionStorage.getItem('dist_id')

                  };
              },
              processResults: function (data, params) {
                   params.page = params.page || 1;
                  return {
                      results: data.results,
                      pagination: {
                        more: (params.page * 5) < data.total_count
                      }

                  };
              },
              cache: true
          },
          
          tags: true,
          placeholder: "Select Customer"
  });
                   $('#assign_customer_modal').select2({
         ajax: {
              url: "<?php echo ROOT_URI; ?>/resources/services/display_all_customer.php",
              dataType: 'json',
              delay: 350,
              data: function (params) {
                  return {
                      q: params.term, // search term
                        page: params.page || 1,
                        user_id:sessionStorage.getItem('user_id'),
                        role:sessionStorage.getItem('role')
                  };
              },
              processResults: function (data, params) {
                   params.page = params.page || 1;
                  return {
                      results: data.results,
                      pagination: {
                        more: (params.page * 5) < data.total_count
                      }

                  };
              },
              cache: true
          },
          
          tags: true,
          placeholder: "Select team member"
  });
      	$('#check_emp').change(function() {
   if ($(this).is(':checked'))
   {
    $('#assign_div :input').attr('disabled', true);
    } 
    else 
    {
        $('#assign_div :input').removeAttr('disabled');
    }   
});
      	$('#agent_display').select2({
        ajax: {
              url: "<?php echo ROOT_URI; ?>/resources/services/display_all_customer.php",
              dataType: 'json',
              delay: 350,
              data: function (params) {
                  return {
                      q: params.term, // search term
                        page: params.page || 1,
                        user_id:sessionStorage.getItem("user_id"),
                        role:sessionStorage.getItem('role')
                  };
              },
              processResults: function (data, params) {
                   params.page = params.page || 1;
                  return {
                      results: data.results,
                      pagination: {
                        more: (params.page * 5) < data.total_count
                      }

                  };
              },
              cache: true
          },
          
          tags: true,
          placeholder: "Select team member"
  });
      	$(document).on('click', '.re-assign-pop', function () {
  		var id_task=($(this).attr("data-id-task"));
  		$(document).on('click', '#confirm_reassign', function () {
			var user_id=$('#assign_customer_modal').val();

  		
  			 $.ajax({
              dataType :'json',
                       method : 'POST',
                       async:false,
                  url : "<?php echo ROOT_URI; ?>/resources/services/case_main.php",
                  data: "{\n\"action\":\"reassign_case\",\n\"case_id\":\""+id_task+"\",\n\"assigned_user_id\":\""+user_id+"\"}",}).success(function(resp) {
                    toast_it(resp.response_message);
                      }); 
                  $('#reassign-modal').modal('toggle');
                  setInterval(function(){ location.reload();}, 1000);
                  


  		});
});
      	 	$(document).on('click', '.close-pop', function () {
  		var id_task=($(this).attr("data-id-task"));
  		$(document).on('click', '#confirm_close', function () {
  			 $.ajax({
              dataType :'json',
                       method : 'POST',
                       async:false,
                  url : "<?php echo ROOT_URI; ?>/resources/services/case_main.php",
                  data: "{\n\"action\":\"close_case\",\n\"closing_user_id\":\""+sessionStorage.getItem('user_id')+"\",\n\"case_id\":\""+id_task+"\"}",})
  			 .success(function(resp) {
                    toast_it(resp.response_message);
                    //console.log(resp);

                      }); 
                  $('#case_closed-modal').modal('toggle');
                  setInterval(function(){ location.reload();}, 1000);
                  


  		});
});
      	 	$(document).on('click', '.cancel-pop', function () {
  		var id_task=($(this).attr("data-id-task"));
  		$(document).on('click', '#confirm_cancel', function () {
  			 $.ajax({
              dataType :'json',
                       method : 'POST',
                       async:false,
                  url : "<?php echo ROOT_URI; ?>/resources/services/case_main.php",
                  data: "{\n\"action\":\"cancel_case\",\n\"cancelled_user_id\":\""+sessionStorage.getItem('user_id')+"\",\n\"case_id\":\""+id_task+"\"}",})
  			 .success(function(resp) {
                    toast_it(resp.response_message);
                    //console.log(resp);

                      }); 
                  $('#case_cancelled-modal').modal('toggle');
                  setInterval(function(){ location.reload();}, 1000);
                  


  		});
});
 	$(document).on('click', '#create_case_ajax', function () {
  			var user_id=$('#assign-agent-drop').val();
	  		var case_name=$('#case_name').val();
	  		 

    		  var emp_id    ='';
                  if ($('#check_emp').is(':checked'))
                    {
                       emp_id=sessionStorage.getItem('user_id');
                    } 
                  else {
                      emp_id=$('#agent_display').val();
                    } 
                  if(emp_id==''){
                     emp_id=sessionStorage.getItem('user_id');
                    // return false;
                  } 
                 // alert(emp_id);
                  if(emp_id==null){
                    emp_id=sessionStorage.getItem('user_id');
                    //return false;
                  }    
  		 $.ajax({
              dataType :'json',
                       method : 'POST',
                       async:false,
                  url : "<?php echo ROOT_URI; ?>/resources/services/case_main.php",
                  data: "{\n\"action\":\"add_case\",\n\"casename\":\""+case_name+"\",\n\"empid\":\""+emp_id+"\",\n\"user\":\""+user_id+"\",\n\"current_empid\":\""+sessionStorage.getItem('user_id')+"\"}",}).success(function(resp) {
                    toast_it(resp.response_message);
                      }); 
                  $('#create_case_modal').modal('toggle');
                  setInterval(function(){ location.reload();}, 1000);

});

 	$(document).on('click','.comments_save',function(){

 		var case_id = $(this).attr('data-case_id_save');
 		var comment = $(this).parent().siblings().children('.comments_text').val();

 		//toast_it(case_id);
 		//console.log(comment);

 		$.ajax({
	       url : "<?php echo ROOT_URI; ?>/resources/services/case_main.php",
	       type : 'POST',
	       data : "{\n\"case_id\":\""+case_id+"\",\n\"comment\":\""+comment+"\",\n\"user_id\":\""+sessionStorage.getItem('user_id')+"\",\n\"action\":\"comments_save\"\n}",

	       processData: false,  // tell jQuery not to process the data
	       contentType: false,  // tell jQuery not to set contentType 
	       dataType: 'JSON',
	             success: function(response) {

	              if(response.response_code == 200){
	                toast_it("Saved");
	                  setInterval(function(){ location.reload();}, 1200);
	              }
	              
	             }
	             // error: function() {  
	             //   toast_it("There was an error.");
	             // }
	       }); //ajax close

 	});

 });
</script>
 

 