	 <?php 
  $path_init = $_SERVER['DOCUMENT_ROOT']."/crm/resources/init.php";
include_once($path_init);

 ?>

<link href="<?php echo ROOT_URI; ?>/resources/lib/select2/select2.min.css" rel="stylesheet" />
<link href="<?php echo ROOT_URI; ?>/assets/css/team.css" rel="stylesheet" type="text/css">
<script src="<?php echo ROOT_URI; ?>/resources/lib/select2/select2.min.js"></script>
<link href="<?php echo ROOT_URI; ?>/resources/lib/timepicker/mdtimepicker.css" rel="stylesheet" type="text/css"

<!-- right side main body starts -->
<div class="col-md-10 col-lg-10 col-sm-10 col-xs-10 col-lg-offset-2 col-md-offset-2 col-sm-offset-2 col-xs-offset-2 no-lr-pad back-color-grey">
	<!-- main section starts here contains upper part and lower part of cases page, cases detail section is outside this section -->
	<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad " id="main-section-cases">
		 <!-- for upper part -->
	
	    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 back-color-white padding-top-13 border-bottom padding-left-40">
	        <p class="font-size-24 float-inline"><b>Reports</b></p>
	    </div>
	   <!-- upper part ends -->

	     <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 margin-top-10 padding-left-40 padding-right-40" id="main-section-reports">
	     	<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 ">
	     		<div class="back-color-white main-item-for-tiles-reports a-deco ">	     			
		     		<img src="<?php echo ROOT_URI; ?>/assets/img/assets/Task-Report.svg" class="report-tile-icon ">
		     		<p class="text-center text-bold font-size-20 margin-top-10">Task Reports</p>
		     		<a class="report-link a-deco" id="download_task_report">Download <img src="<?php echo ROOT_URI; ?>/assets/img/assets/download-now.svg " class="" width="10"></a>
	     		</div>
	     	</div>
	     	<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 ">
	     		<div class="back-color-white main-item-for-tiles-reports a-deco ">	     			
		     		<img src="<?php echo ROOT_URI; ?>/assets/img/assets/Case-Report.svg" class="report-tile-icon ">
		     		<p class="text-center text-bold font-size-20  margin-top-10">Case Reports</p>
		     		<a class="report-link a-deco" id="download_case_report">Download <img src="<?php echo ROOT_URI; ?>/assets/img/assets/download-now.svg " class="" width="10"></a>	
	     		</div>
	     	</div>
	     	<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 ">
	     		<div class="back-color-white main-item-for-tiles-reports a-deco ">	     			
		     		<img src="<?php echo ROOT_URI; ?>/assets/img/assets/Call-Report.svg" class="report-tile-icon ">
		     		<p class="text-center text-bold font-size-20 margin-top-10">Call Reports</p>
		     		<a class="report-link a-deco" id="download_call_report">Download <img src="<?php echo ROOT_URI; ?>/assets/img/assets/download-now.svg " class="" width="10"></a>
	     		</div>
	     	</div>
	     	<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 ">
	     		<div class="back-color-white main-item-for-tiles-reports a-deco ">	     			
		     		<img src="<?php echo ROOT_URI; ?>/assets/img/assets/Order-Report.svg" class="report-tile-icon ">
		     		<p class="text-center text-bold font-size-20 margin-top-10">Order Reports</p>
		     		<a class="report-link a-deco" id="download_order_report">Download <img src="<?php echo ROOT_URI; ?>/assets/img/assets/download-now.svg " class="" width="10"></a>
	     		</div>
	     	</div>
	     	<!-- <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 ">
	     		<div class="back-color-white main-item-for-tiles-reports a-deco ">	  
	     			<div class="report-icon-div">
	     				<img src="<?php //echo ROOT_URI; ?>/assets/img/assets/report-5.svg" class="report-icon">	
	     			</div>   					     		
		     		<p class="text-center text-bold font-size-20 margin-top-10">Report 5</p>
		     		<a class="report-link a-deco">Download <img src="<?php// echo ROOT_URI; ?>/assets/img/assets/download-now.svg" class="" width="10"></a>	
	     		</div>
	     	</div> -->
	     </div>
	</div>
</div>

<script src="<?php echo ROOT_URI; ?>/resources/lib/timepicker/mdtimepicker.js"></script>
<script type="text/javascript">
  $(document).ready(function() {


  		   $(document).on('click','#download_task_report',function(){


            var user_role = sessionStorage.getItem("role");
            var user_id   = sessionStorage.getItem("user_id");

    		// console.log(company_id);
    		var arr = new Array();
    		$.ajax({
            url : "<?php echo ROOT_URI; ?>/resources/services/reports_api.php",
            type : 'POST',
            data :"{\"user_id\":\""+user_id+"\",\n\"token\":\""+sessionStorage.getItem("token")+"\",\"user_role\":\""+user_role+"\",\"action\":\"load_all_task\"}\n\n",

            processData: false,  // tell jQuery not to process the data
            contentType: false,  // tell jQuery not to set contentType 
            dataType: 'JSON',
                     success: function(response) {
	                     	var result = response.data;
	                     	//console.log(result);
	                     	var arr = [];
	                		for(var arr1 in result){
	                  		arr.push(result[arr1]);
	                		}
	                  		var csvContent = "data:text/csv;charset=utf-8,";
	                  		arr.forEach(function(infoArray, index){
	                     	dataString = infoArray+",";
	                     	csvContent += index < arr.length ? dataString+ "\n" : dataString;
	                  		}); 
	                  		var encodedUri = encodeURI(csvContent);
	                     	var link = document.createElement("a");
	                 	 	link.setAttribute("href", encodedUri);
	                  		link.setAttribute("download", "Task_Report.csv");
	                  		document.body.appendChild(link);  
	                  		link.click();   
						 },
	                    
                    
            }); //ajax close


    	}); // download all registered users close.


  		$(document).on('click','#download_case_report',function(){


            var user_role = sessionStorage.getItem("role");
            var user_id   = sessionStorage.getItem("user_id");

    		// console.log(company_id);
    		var arr = new Array();
    		$.ajax({
            url : "<?php echo ROOT_URI; ?>/resources/services/reports_api.php",
            type : 'POST',
            data :"{\"user_id\":\""+user_id+"\",\n\"token\":\""+sessionStorage.getItem("token")+"\",\"user_role\":\""+user_role+"\",\"action\":\"load_all_case\"}\n\n",

            processData: false,  // tell jQuery not to process the data
            contentType: false,  // tell jQuery not to set contentType 
            dataType: 'JSON',
                     success: function(response) {
	                     	var result = response.data;
	                     	//console.log(result);
	                     	var arr = [];
	                		for(var arr1 in result){
	                  		arr.push(result[arr1]);
	                		}
	                  		var csvContent = "data:text/csv;charset=utf-8,";
	                  		arr.forEach(function(infoArray, index){
	                     	dataString = infoArray+",";
	                     	csvContent += index < arr.length ? dataString+ "\n" : dataString;
	                  		}); 
	                  		var encodedUri = encodeURI(csvContent);
	                     	var link = document.createElement("a");
	                 	 	link.setAttribute("href", encodedUri);
	                  		link.setAttribute("download", "Case_Report.csv");
	                  		document.body.appendChild(link);  
	                  		link.click();   
						 },
	                    
                    
            }); //ajax close


    	}); // download all registered users close.

  		$(document).on('click','#download_call_report',function(){


            var user_role = sessionStorage.getItem("role");
            var user_id   = sessionStorage.getItem("user_id");

    		// console.log(company_id);
    		var arr = new Array();
    		$.ajax({
            url : "<?php echo ROOT_URI; ?>/resources/services/reports_api.php",
            type : 'POST',
            data :"{\"user_id\":\""+user_id+"\",\n\"token\":\""+sessionStorage.getItem("token")+"\",\"user_role\":\""+user_role+"\",\"action\":\"load_all_call\"}\n\n",

            processData: false,  // tell jQuery not to process the data
            contentType: false,  // tell jQuery not to set contentType 
            dataType: 'JSON',
                     success: function(response) {
	                     	var result = response.data;
	                     	//console.log(result);
	                     	var arr = [];
	                		for(var arr1 in result){
	                  		arr.push(result[arr1]);
	                		}
	                  		var csvContent = "data:text/csv;charset=utf-8,";
	                  		arr.forEach(function(infoArray, index){
	                     	dataString = infoArray+",";
	                     	csvContent += index < arr.length ? dataString+ "\n" : dataString;
	                  		}); 
	                  		var encodedUri = encodeURI(csvContent);
	                     	var link = document.createElement("a");
	                 	 	link.setAttribute("href", encodedUri);
	                  		link.setAttribute("download", "Call_Report.csv");
	                  		document.body.appendChild(link);  
	                  		link.click();   
						 },
	                    
                    
            }); //ajax close


    	}); // download all registered users close.

    	$(document).on('click','#download_order_report',function(){


            var user_role = sessionStorage.getItem("role");
            var user_id   = sessionStorage.getItem("user_id");

    		// console.log(company_id);
    		var arr = new Array();
    		$.ajax({
            url : "<?php echo ROOT_URI; ?>/resources/services/reports_api.php",
            type : 'POST',
            data :"{\"dist_id\":\""+sessionStorage.getItem("dist_id")+"\",\n\"token\":\""+sessionStorage.getItem("token")+"\",\"action\":\"load_all_order\"}\n\n",

            processData: false,  // tell jQuery not to process the data
            contentType: false,  // tell jQuery not to set contentType 
            dataType: 'JSON',
                     success: function(response) {
	                     	var result = response.data;
	                     	//console.log(result);
	                     	var arr = [];
	                		for(var arr1 in result){
	                  		arr.push(result[arr1]);
	                		}
	                  		var csvContent = "data:text/csv;charset=utf-8,";
	                  		arr.forEach(function(infoArray, index){
	                     	dataString = infoArray+",";
	                     	csvContent += index < arr.length ? dataString+ "\n" : dataString;
	                  		}); 
	                  		var encodedUri = encodeURI(csvContent);
	                     	var link = document.createElement("a");
	                 	 	link.setAttribute("href", encodedUri);
	                  		link.setAttribute("download", "Order_Report.csv");
	                  		document.body.appendChild(link);  
	                  		link.click();   
						 },
	                    
                    
            }); //ajax close


    	}); // download all registered users close.

 	 
  });
 
</script>
 

 