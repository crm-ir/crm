 <?php 
  $path_init = $_SERVER['DOCUMENT_ROOT']."/crm/resources/init.php";
include_once($path_init);

 ?>

   <link href="<?php echo ROOT_URI; ?>/resources/lib/select2/select2.min.css" rel="stylesheet" />

   <link href="<?php echo ROOT_URI; ?>/assets/css/team.css" rel="stylesheet" type="text/css">
  <script src="<?php echo ROOT_URI; ?>/resources/lib/select2/select2.min.js"></script>
  <script src="<?php echo ROOT_URI; ?>/resources/lib/time_ago/tinyAgo.min.js"></script>
  <link href="<?php echo ROOT_URI; ?>/resources/lib/timepicker/mdtimepicker.css" rel="stylesheet" type="text/css">
  <script type="text/javascript">
var user_role1 = sessionStorage.getItem("role");
 
if(user_role1==1){
   
  window.location.href =  location.protocol + '//' + location.host + location.pathname+"?panel=10";
}
</script>
<!-- right side main body starts -->
<div class="col-md-10 col-lg-10 col-sm-10 col-xs-10 col-lg-offset-2 col-md-offset-2 col-sm-offset-2 col-xs-offset-2 no-lr-pad back-color-grey">
  <!-- main section starts here contains upper part and lower part of task page, task detail section is outside this section -->
  <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad " id="main-section-task">
   <!-- for upper part -->
  
      <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 back-color-white padding-top-13 border-bottom padding-left-40">
          <p class="font-size-24 float-inline"><b>My Tasks</b></p>
          <div class="margin-top-6">
            <a data-toggle="modal" data-target="#create_task_modal" class="a-deco color-brand margin-left-16"><img class="margin-right-8" src="<?php echo ROOT_URI; ?>/assets/img/assets/create-new-task.svg">Create New Task</a>

          </div>

          <div class="pull-right margin-top--20">
            <H3 class="float-inline margin-top--10"><img src="" id="paste_phone_icon" width="40"><span id="paste_phone_number_customer_screen" class="margin---10"></span></h3>

            <button class="a-deco btn btn-success float-inline margin-left-25 margin-top--10" id="reassign_tasks_user">Reassign Tasks</button>
            <a class="a-deco color-grey-1 float-inline margin-left-25 display-task-active task_tabs" id="created_first_tab">Created First</a>
            <a class="a-deco color-grey-1 float-inline margin-left-25 task_tabs" id="last_contacted_tab">Last Contacted</a>
            <!-- <a class="a-deco color-grey-1 float-inline margin-left-25 task_tabs" id="last_ordered_tab">Last Ordered</a> -->
            <!-- <a class="a-deco color-grey-1 float-inline margin-left-25 task_tabs" id="order_frequency_tab">Order Frequency</a> -->
            <div class="dropdown float-inline margin-left-25 margin-top--10">
                  <button class="btn btn-default brand-btn-white add-mem-btn1 btn-drop-dwn width-107  dropdown-toggle" type="button" data-toggle="dropdown" id="drop-d-action-task"><img class="" src="<?php echo ROOT_URI; ?>/assets/img/assets/filter.svg"> <span class="padding-left-5">Filters</span>
                    
                  <!-- <span class="caret"></span> --></button>
                  <ul class="dropdown-menu pull-right check_box">
                    <li>
                      <a class="drop-sub-chl " >
                        <div class="filt-chk checkbox margin-top-bot-2 ">
                <label><input class="filter_condition" data-sort_selected="1" type="checkbox" data-condition_value="1"><span class="margin-left-10">New</span></label>
              </div>
                      </a>
                    </li>
                     <li>
                      <a class="drop-sub-chl " >
                        <div class="filt-chk checkbox margin-top-bot-2">
                <label><input class="filter_condition" data-sort_selected="1" type="checkbox" data-condition_value="2"><span class="margin-left-10">Open</span></label>
              </div>
                      </a>
                    </li>
                     <li>
                      <a class="drop-sub-chl " >
                        <div class="filt-chk checkbox margin-top-bot-2">
                <label><input class="filter_condition" type="checkbox" data-condition_value="3"><span class="margin-left-10">Closed</span></label>
              </div>
                      </a>
                    </li>
                    <li>
                      <a class="drop-sub-chl " >
                        <div class="filt-chk checkbox margin-top-bot-2">
                <label><input class="filter_condition" data-sort_selected="1" type="checkbox" data-condition_value="4"><span class="margin-left-10">Expired</span></label>
              </div>
                      </a>
                    </li>
                  </ul>
                </div>
          </div>
      </div>
     <!-- upper part ends -->
     <!-- lower part/main part starts here -->
     <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 margin-top-10 padding-left-40 padding-right-40" id="main_task">

          <!-- items tiles starts here  -->
          
          <!-- items tiles starts here  -->

     </div>
     <a class="comment_loader_message margin-top-10 margin-bottom-10 col-sm-12 col-xs-12 col-md-12 col-lg-12 a-deco-grey text-center font-size-12"><u>Load More</u></a>
     <!-- lower part/main part ends here -->
  </div>
  <!-- main section ends here -->
   <!-- task detail section starts from here -->
    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 margin-top-10 padding-right-0 display-none" id="task-detail-section">
      <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6 no-lr-pad">
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
          <button class="btn btn-default back-btn-task" id="back-btn-task"> <img class="" src="<?php echo ROOT_URI; ?>/assets/img/assets/back.svg"><span class="margin-left-10">Back To My Tasks</span></button>
        </div>
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad task-detail-item-tiles-mytask  box-shadow1 margin-top-25 back-color-white" id="cust_detail">
          
        </div>
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad task-detail-item-tiles-mytask  box-shadow1 margin-top-15 back-color-white">
          <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 border-bottom">
            <p class="font-size-18 margin-top-15"><b>Last 5 Orders</b></p>
          <!--   <a class="color-grey-1 pull-right margin-top--30" >See All <img class="margin-right-8  " src="<?php echo ROOT_URI; ?>/assets/img/assets/see-all.svg"></a> -->
          </div>
          <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 border-bottom">
            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 margin-top-10">
            <div class="dropdown">
              <p class="dropdown-toggle a-deco" style="position: initial;" type="button" id="menu1" data-toggle="dropdown">Last 5 orders
              <span class="caret"></span></p>
              <ul class="dropdown-menu" role="menu" style="width: 100%;" aria-labelledby="menu1" id="append_last_5_orders">
                
              </ul>
            </div>
            </div>
          </div>
          <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 border-bottom pad-10" id="last_5_order_details_items">                           
          </div>
        </div>
        <!-- 20 most ordered products -->
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad task-detail-item-tiles-mytask  box-shadow1 margin-top-15 back-color-white">
          <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 border-bottom">
            <p class="font-size-18 margin-top-15"><b>20 Most Ordered Products</b></p>
            <!-- <a href="" class="color-grey-1 pull-right margin-top--30">See All <img class="margin-right-8  " src="<?php echo ROOT_URI; ?>/assets/img/assets/see-all.svg"></a> -->
          </div>
         
          <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 border-bottom">
              <table class="table border-bottom">   
              <tbody id="20_most_ordered_product">
              </tbody>
            </table> 
          </div>
        </div>
        <!-- 20 Most Ordered Companies-->
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad task-detail-item-tiles-mytask  box-shadow1 margin-top-15 back-color-white">
          <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 border-bottom">
            <p class="font-size-18 margin-top-15"><b>20 Most Ordered Companies</b></p>
           <!--  <a href="" class="color-grey-1 pull-right margin-top--30">See All <img class="margin-right-8  " src="<?php echo ROOT_URI; ?>/assets/img/assets/see-all.svg"></a> -->
          </div>
           
          <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 border-bottom margin-top-10 padding-bottom-10" id="20_most_ordered">
          </div>
        </div>
        <!-- Call History With Customer -->
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad task-detail-item-tiles-mytask  box-shadow1 margin-top-15 back-color-white margin-bottom-20">
          <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 border-bottom ">
            <p class="font-size-18 margin-top-15"><b>Call History With Customer</b></p>
        <!--     <a href="" class="color-grey-1 pull-right margin-top--30">See All <img class="margin-right-8  " src="<?php echo ROOT_URI; ?>/assets/img/assets/see-all.svg"></a> -->
          </div>
         
          <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 border-bottom">
              <table class="table border-bottom">   
              <tbody id="body_call_display">
              </tbody>
            </table> 
          </div>
        </div>
      </div>
      <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6 padding-right-0">
        
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 back-color-white main-call-section margin-top--10 no-lr-pad">
          <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 back-color-brand height-48 ">
            <p class="font-size-12 color-white margin-top-13 ">Click on call button to connect with customer</p>
          </div>
          <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 margin-top-30">
            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
              <p class="color-grey"><b><span id="cust_name_call_panel"></span></b><br><br></p>
            </div>

              <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 " id="">
               <p class="margin-bottom-10 text-bold">Last Contacted number:  <span id="last_contacted_number"></span><br> </p>
               <p class="margin-bottom-10 text-bold"><span class="float-inline ">CALL ON: </span>
                  <input type="text" class="float-inline margin-left-10" name="" id="cust_ph_call_panel" val=""  placeholder="Calling Number">
              
                  <button type="button" class="btn btn-default brand-btn float-inline add-mem-btn1 call-customer margin-left-25 margin-top--5">Call</button>
            
               </p>
              <!-- <p class="font-size-12 margin-top-13 ">Call is being connected</p> -->
              </div>

            
          </div>
          <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 back-color-brand height-48 display-none margin-top-15" id="connected_msg">
            <p class="font-size-12 color-white margin-top-13 ">Call is being connected</p>
          </div>
          <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 margin-top-30">
            <!-- <div class="col-md-4 col-lg-4 col-sm-4 col-xs-4"> -->
              <!-- <div class="dropdown">
                      <button class="btn btn-default brand-btn-white add-mem-btn1 btn-drop-dwn dropdown-toggle" type="button" data-toggle="dropdown" id="drop-d-action-task1">Select Action
                        <img class="margin-left-10" src="<?php //echo ROOT_URI; ?>/assets/img/assets/dropdown-arrow.svg">
                      </button>
                      <ul class="dropdown-menu pull-right">
                        <li ><a class="drop-sub-chl re-assign-pop" data-toggle="modal" data-target="#reassign-modal">Re-assign</a></li>
                        <li ><a class="drop-sub-chl" data-toggle="modal" data-target="#followup-modal" >Follow Up</a></li>
                        <li ><a class="drop-sub-chl" data-toggle="modal" data-target="#closetask-modal"  >Close</a></li>
                      </ul>
                  </div> -->
            <!-- </div> -->
             <div class="col-md-4 col-lg-4 col-sm-4 col-xs-4">
              <button type="button" class="btn btn-default brand-btn pull-left add-mem-btn1 width-90 place_order" data-dist_id="" data-cust_erpid="" data-cust_erpcode="" data-login_id="" data-user_pwd="" >Place Order</button>
            </div>
             <div class="col-md-4 col-lg-4 col-sm-4 col-xs-4 text-right">
              <button type="button" class="btn btn-danger add-mem-btn1 width-90 close-pop" data-toggle="modal" data-target="#closetask-modal" data-id-task="" id="end_task_button">End Task</button>
            </div>
            <div class="col-md-4 col-lg-4 col-sm-4 col-xs-4 text-right">
              <button type="button" class="btn btn-default brand-btn-white add-mem-btn1 width-90 display-none"  id="call_again">Call Again</button>
            </div>
           
          </div>
          <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad margin-top-50 remark_height_modal" id="remark_call">
                  
                </div>
          <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 margin-top-30">
              
              <div class="input-group search-input-grp">
                        <textarea type="text" class="resize-none form-control remark-add-box remark_text" rows="2" placeholder="Write something" ></textarea>
                        <div class="input-group-btn">
                          <button class="btn btn-default add_remark_call">
                            <img class="margin-right-8" src="<?php echo ROOT_URI; ?>/assets/img/assets/add-remark.svg" height="40">
                          </button>
                        </div>
                      </div>
         
          </div>
        </div>


      </div>
    </div>
    <!-- task detail section ends here -->
   <!-- modals -->


  <!-- Modal -->
  <div id="create_task_modal" class="modal fade" role="dialog">
    <div class="modal-dialog margin-top-70">

      <!-- Modal content-->
      <div class="modal-content col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
        <div class="modal-header col-md-12 col-lg-12 col-sm-12 col-xs-12 ">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <p class="font-size-20 margin-left-10"><b>Create New Task</b></p>
        </div>
        <div class="modal-body col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
          <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                <!-- <div class="form-group col-md-12 col-lg-12 col-sm-12 col-xs-12" >
                  <span class="font-size-12 color-grey-1" for="username">Username:</span>
                  <input type="text" name="username" class="form-control" id="username">
                </div> -->
                <div class="form-group col-md-12 col-lg-12 col-sm-12 col-xs-12">
                  <p class="font-size-12 color-grey-1 margin-bottom--15"  for="customer_sel">Customer</p><br>
          <select class=" form-control tag-for-cust  js-data-select-ajax width-100" name="tags_for_create[]" id="customer_display">
                    </select>
                </div>
                <div class="form-group col-md-12 col-lg-12 col-sm-12 col-xs-12">
                  <span class="font-size-12 color-grey-1"  for="lastName">Name</span>
                  <input type="text" name="task_name" class="form-control margin-top-5" placeholder="Enter task name" id="task_name">
                </div>
                <div class="form-group col-md-12 col-lg-12 col-sm-12 col-xs-12">
                    <div class="checkbox">
            <label class="color-grey"><input type="checkbox" class="assign-chk" value="me" id="check_emp">Assign to Myself</label>
          </div> 
                </div>
                <div class="form-group col-md-12 col-lg-12 col-sm-12 col-xs-12 select-ajax-assign" id="assign_div">
                  <p class="font-size-12 color-grey-1 margin-bottom--15"  for="customer_sel">Assign To</p><br>
          <select class=" form-control tag-for-cust  js-data-select-ajax-assign width-100" name="tags_for_assign[]" id="agent_display">
                    </select>
                </div>
                
                <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
                  <!-- <input type="time" name="task_date" class="form-control margin-top-25" placeholder="Select date" id="task_date">        -->
                <input name="task_date" class="form-control margin-top-25 back-color-white" placeholder="Select time" type="text" id="timepicker"/>
                </div>
          </div>
        </div>
        <div class="modal-footer col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
          <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 padding-left-30 padding-right-30">
              <button type="button" class="btn btn-default pull-left add-mem-btn1" data-dismiss="modal">Cancel</button>
              <button type="button" class="btn btn-default pull-right brand-btn add-mem-btn"  id="create_task_ajax">Create</button>
          </div>
        </div>
      </div>

    </div>
  </div>
  <!-- modals ends here -->
  <!-- Modal -->
  <div id="reassign-modal" class="modal fade" role="dialog">
    <div class="modal-dialog margin-top-70">

      <!-- Modal content-->
      <div class="modal-content col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
        <div class="modal-header col-md-12 col-lg-12 col-sm-12 col-xs-12 ">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <p class="font-size-20 margin-left-10"><b>Re-assign</b></p>
        </div>
        <div class="modal-body col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
          <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                 
                
                <div class="form-group col-md-12 col-lg-12 col-sm-12 col-xs-12 select-ajax-assign">
                  <p class="font-size-12 color-grey-1 margin-bottom--15"  for="customer_sel">Assign To</p><br>
          <select class=" form-control tag-for-cust  js-data-select-ajax-assign width-100 assign_customer_modal" name="tags_for_assign[]">
                    </select>
                </div>
                
                <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
                  <!-- <input type="time" name="task_date" class="form-control margin-top-25" placeholder="Select date" id="task_date">        -->
                <input name="task_date" class="form-control margin-top-25 back-color-white" placeholder="Select time" type="text" id="timepicker1"/>
                </div>
          </div>
        </div>
        <div class="modal-footer col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
          <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 padding-left-30 padding-right-30">
              <button type="button" class="btn btn-default pull-left add-mem-btn1" data-dismiss="modal">Cancel</button>
              <button type="button" class="btn btn-default pull-right brand-btn add-mem-btn width-88" id="confirm_reassign">Confirm</button>
          </div>
        </div>
      </div>

    </div>
  </div>
  <!-- modal1 ends here -->

    <div id="reassign_tasks_user_modal" class="modal fade" role="dialog">
      <div class="modal-dialog margin-top-70">

        <!-- Modal content-->
        <div class="modal-content col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
          <div class="modal-header col-md-12 col-lg-12 col-sm-12 col-xs-12 ">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <p class="font-size-20 margin-left-10"><b>Reassign Tasks</b></p>
          </div>
          <div class="modal-body col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                   
                  
                  <div class="form-group col-md-12 col-lg-12 col-sm-12 col-xs-12 margin-right-20 margin-left-10 margin-top-10">
                    <p class="font-size-14 color-grey-1 margin-bottom--15"  for="customer_sel">Currently Assigned To</p><br>
                      <select class=" form-control tag-for-cust js-data-select-ajax-assign width-100 currently_assigned assign_customer_modal"  name="tags_for_assign[]">
                      </select>
                      <br />
                  </div>

                  <div class="form-group col-md-12 col-lg-12 col-sm-12 col-xs-12 margin-right-20 margin-left-10">
                    <p class="font-size-14 color-grey-1 margin-bottom--15"  for="customer_sel">Assign To</p><br>
                      <select class=" form-control tag-for-cust  js-data-select-ajax-assign width-100 new_assign_tasks_to assign_customer_modal" name="tags_for_assign[]">
                      </select>
                  </div>
                  
            </div>
          </div>
          <div class="modal-footer col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 padding-left-30 padding-right-30">
                <button type="button" class="btn btn-default pull-left add-mem-btn1" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-default pull-right brand-btn add-mem-btn width-88" id="confirm_reassign_all_tasks">Confirm</button>
            </div>
          </div>
        </div>

      </div>
    </div>

    <!-- Modal -->
  <div id="followup-modal" class="modal fade" role="dialog">
    <div class="modal-dialog margin-top-70">

      <!-- Modal content-->
      <div class="modal-content col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
        <div class="modal-header col-md-12 col-lg-12 col-sm-12 col-xs-12 ">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <p class="font-size-20 margin-left-10"><b>Follow Up</b></p>
        </div>
        <div class="modal-body col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
          <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                
                <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
                  <!-- <input type="time" name="task_date" class="form-control margin-top-25" placeholder="Select date" id="task_date">        -->
                <input name="task_date" class="form-control margin-top-25 back-color-white" placeholder="Select time" type="text" id="timepicker2"/>
                </div>
          </div>
        </div>
        <div class="modal-footer col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
          <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 padding-left-30 padding-right-30">
              <button type="button" class="btn btn-default pull-left add-mem-btn1" data-dismiss="modal">Cancel</button>
              <button type="button" class="btn btn-default pull-right brand-btn add-mem-btn width-88" id="confirm_follow">Confirm</button>
          </div>
        </div>
      </div>

    </div>
  </div>
  <!-- modal2 ends here -->
  <div id="followup_time_popup" class="modal fade" role="dialog">
    <div class="modal-dialog margin-top-70">

      <!-- Modal content-->
      <div class="modal-content col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
        <div class="modal-header col-md-12 col-lg-12 col-sm-12 col-xs-12 ">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <p class="font-size-20 margin-left-10"><b>Follow Up</b></p>
        </div>
        <div class="modal-body col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
            <p class="font-size-14 text-bold text-center"><span class="font-size-14 paste_followup_time"></span></p>
        </div>
        <div class="modal-footer col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
          <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 padding-left-30 padding-right-30">
              <button type="button" class="btn btn-default pull-left add-mem-btn1" data-dismiss="modal">Cancel</button>
              <!-- <button type="button" class="btn btn-default pull-right brand-btn add-mem-btn width-88" data-task_id="" id="confirm_follow_up_later">Confirm</button>             -->
          </div>
        </div>
      </div>

    </div>
  </div>
   <!-- Modal -->
  <div id="closetask-modal" class="modal fade" role="dialog">
    <div class="modal-dialog margin-top-70">

      <!-- Modal content-->
      <div class="modal-content col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
        <div class="modal-header col-md-12 col-lg-12 col-sm-12 col-xs-12 ">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <p class="font-size-20 margin-left-10"><b>Close This Task</b></p>
        </div>
        <div class="modal-body col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
          <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                
                <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                  <p class="font-size-12 color-grey">This task will not appear in your task list for today and we will mark this task closed without calling</p>   
                </div>
               
          </div>
        </div>
        <div class="modal-footer col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
          <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 padding-left-30 padding-right-30">
              <button type="button" class="btn btn-default pull-left add-mem-btn1" data-dismiss="modal">Cancel</button>
              <button type="button" class="btn btn-default pull-right brand-btn add-mem-btn width-88" id="confirm_close">Confirm</button>
          </div>
        </div>
      </div>

    </div>
  </div>
  <!-- modal4 ends here -->
     <!-- Modal -->
  <div id="add_remark-modal" class="modal fade" role="dialog">
    <div class="modal-dialog margin-top-70">

      <!-- Modal content-->
      <div class="modal-content col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
        <div class="modal-header col-md-12 col-lg-12 col-sm-12 col-xs-12 ">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <p class="font-size-20 margin-left-10"><b>Add Remarks</b></p>
        </div>
        <div class="modal-body col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
          <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                
                <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad remark_height_modal" id="1234">
                </div>

                <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                  <div class="form-group"> 
            <textarea class="form-control resize-none remark-comment" rows="4" id="comment" placeholder="Write your remarks" ></textarea>
          </div>
                </div>
               
          </div>
        </div>
        <div class="modal-footer col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
          <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 padding-left-30 padding-right-30">
              <button type="button" class="btn btn-default pull-left add-mem-btn1" data-dismiss="modal">Cancel</button>
              <button type="button" class="btn btn-default pull-right brand-btn add-mem-btn" id="add_comment">Add</button>
          </div>
        </div>
      </div>

    </div>
  </div>
  <!-- modal3 ends here -->
</div>
 <!-- right side main body ends -->
      
 <script src="<?php echo ROOT_URI; ?>/resources/lib/timepicker/mdtimepicker.js"></script>
<script type="text/javascript">

  $(document).ready(function() {
    console.log(sessionStorage.getItem("token"));
  	$(document).on('click', '.follow-up-pop', function () {
  		var id_task=($(this).attr("data-id-task"));
  		$(document).on('click', '#confirm_follow', function () {
  		var time=$('#timepicker2').val();

      if(!time){
        toast_it("Select Time");
        return false;
      }

  		var hours = Number(time.match(/^(\d+)/)[1]);
			var minutes = Number(time.match(/:(\d+)/)[1]);
			var AMPM = time.match(/\s(.*)$/)[1];
			if(AMPM == "PM" && hours<12) hours = hours+12;
			if(AMPM == "AM" && hours==12) hours = hours-12;
			var sHours = hours.toString();
			var sMinutes = minutes.toString();
			if(hours<10) sHours = "0" + sHours;
			if(minutes<10) sMinutes = "0" + sMinutes;
			var time=sHours + ":" + sMinutes+":"+"00";
  		 $.ajax({
              dataType :'json',
                       method : 'POST',
                       async:false,
                  url : "<?php echo ROOT_URI; ?>/resources/services/task_main.php",
                  data: "{\n\"action\":\"follow_up\",\n\"task_id\":\""+id_task+"\",\n\"follow_up\":\""+time+"\"}",}).success(function(resp) {
                    toast_it(resp.response_message);
                      }); 
                  $('#followup-modal').modal('toggle');
                  setInterval(function(){ location.reload();}, 1000);

});
  		});

  		$(document).on('click', '#create_task_ajax', function () {
  			var user_id=$('#customer_display').val();
	  		var time=$('#timepicker').val();
	  		var task_name=$('#task_name').val();

        // alert(user_id);

       if(user_id==null){
          toast_it("Select customer");
          return false;
       }
      if(task_name==''){
          toast_it("Enter task name");
          return false;
       }
       if(time==''){
          toast_it("Select time");
          return false;
       }
      
       // alert(time);
      
       // if()



	  		var emp_id='';
	  		if ($('#check_emp').is(':checked'))
   				{
   					 emp_id=sessionStorage.getItem('user_id');
    			} 
    		else {
        		emp_id=$('#agent_display').val();
    			} 
        if(emp_id==''){
           emp_id=sessionStorage.getItem('user_id');
          // return false;
       } 
       // alert(emp_id);
       if(emp_id==null){
          emp_id=sessionStorage.getItem('user_id');
          //return false;
       } 
	  		var hours = Number(time.match(/^(\d+)/)[1]);
				var minutes = Number(time.match(/:(\d+)/)[1]);
				var AMPM = time.match(/\s(.*)$/)[1];
				if(AMPM == "PM" && hours<12) hours = hours+12;
				if(AMPM == "AM" && hours==12) hours = hours-12;
				var sHours = hours.toString();
				var sMinutes = minutes.toString();
				if(hours<10) sHours = "0" + sHours;
				if(minutes<10) sMinutes = "0" + sMinutes;
				var time=sHours + ":" + sMinutes+":"+"00";

  		 $.ajax({
              dataType :'json',
                       method : 'POST',
                       async:false,
                  url : "<?php echo ROOT_URI; ?>/resources/services/task_main.php",
                  data: "{\n\"action\":\"add_task\",\n\"taskname\":\""+task_name+"\",\n\"emp_id\":\""+emp_id+"\",\n\"cust_id\":\""+user_id+"\",\n\"current_cust_id\":\""+sessionStorage.getItem('user_id')+"\",\n\"task_when\":\""+time+"\"}",}).success(function(resp) {
                    toast_it(resp.response_message);
                      }); 
                  $('#create_task_modal').modal('toggle');
                  setInterval(function(){ location.reload();}, 1000);

});
$(document).on('click', '.re-assign-pop', function () {
  		var id_task=($(this).attr("data-id-task"));
  		$(document).on('click', '#confirm_reassign', function () {
  			var time=$('#timepicker1').val();

        if(!time){
          toast_it("Select Time");
          return false;
        }
  			var hours = Number(time.match(/^(\d+)/)[1]);
			var minutes = Number(time.match(/:(\d+)/)[1]);
			var AMPM = time.match(/\s(.*)$/)[1];
			if(AMPM == "PM" && hours<12) hours = hours+12;
			if(AMPM == "AM" && hours==12) hours = hours-12;
			var sHours = hours.toString();
			var sMinutes = minutes.toString();
			if(hours<10) sHours = "0" + sHours;
			if(minutes<10) sMinutes = "0" + sMinutes;
			var time=sHours + ":" + sMinutes+":"+"00";
			var user_id=$('.assign_customer_modal').val();



  		
  			 $.ajax({
              dataType :'json',
                       method : 'POST',
                       async:false,
                  url : "<?php echo ROOT_URI; ?>/resources/services/task_main.php",
                  data: "{\n\"action\":\"reassign_user\",\n\"task_id\":\""+id_task+"\",\n\"assigned_user_id\":\""+user_id+"\",\n\"date_time\":\""+time+"\"\n}",}).success(function(resp) {
                    toast_it(resp.response_message);
                      }); 
                  $('#reassign-modal').modal('toggle');
                  setInterval(function(){ location.reload();}, 1000);
                  


  		});
});
  	$('#check_emp').change(function() {
   if ($(this).is(':checked'))
   {
    $('#assign_div :input').attr('disabled', true);
    } 
    else 
    {
        $('#assign_div :input').removeAttr('disabled');
    }   
});
 $(document).on('click', '.remark_button', function () {
      var id_task=$(this).attr("data-id-task");
      $("#add_comment").attr("data-id-task",id_task);


      var entity_id=1;
      var flag=0;
       $.ajax({
              dataType :'json',
                       method : 'POST',
                       async:false,
                  url : "<?php echo ROOT_URI; ?>/resources/services/remark_api.php",
                  data: "{\n\"action\":\"display_remarks\",\n\"entity_id\":\""+id_task+"\",\n\"entity_type\":\""+entity_id+"\",\n\"page_no\":\"1\"\n}",}).success(function(resp) {
                    var html='';
                    if(resp.response_code==400)
                    {
                      flag=1;
                      html='<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 margin-top-20 text-center margin-bottom-20"><p class="text-center">'+
                      'No Remarks For This Task</p></div>';
                    }
                    else{

                       for(i=0;i<resp.data.length;i++){
                        var d = new Date(resp.data[i].created_date);
                  var p=(ago(d.getTime()));
                      html+='<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">'+

                    '<div class="col-md-1 col-lg-1 col-sm-1 col-xs-1">'+
                      '<img src="<?php echo S3_BUCKET; ?>'+resp.data[i].user_profile_pic+'" class="pull-right user_assign-img">'+
                    '</div>'+
                    '<div class="col-md-11 col-lg-11 col-sm-11 col-xs-11 padding-left-0">'+
                      
                      '<p class="font-size-12">'+resp.data[i].remark+'</p>'+
                      '<p class="font-size-12 color-grey">'+p+' ago</p>'+
                      '<hr>'+
                    '</div>'+
                    
                  '</div>';
                }
                    }
                    //+alert(html);
                    $("#1234").html(html);
                    $("#add_comment").attr("flag",flag); 

                      });
                });
      $(document).on('click', '#add_comment', function () {
        var comment=$('.remark-comment').val();
        if(comment=='' || comment==null || comment==' '){
          toast_it("Remark can't be empty");
          return false;
        }
        var id=$('#add_comment').attr('data-id-task');
        var flag=$('#add_comment').attr('flag');
         $.ajax({
              dataType :'json',
                       method : 'POST',
                       async:false,
                  url : "<?php echo ROOT_URI; ?>/resources/services/remark_api.php",
                  data: "{\n\"action\":\"add_remarks\",\n\"user_id\":\""+sessionStorage.getItem('user_id')+"\",\n\"entity_id\":\""+id+"\",\n\"entity_type\":\"1\",\n\"remark\":\""+comment+"\"\n}",}).success(function(resp) {
                    var html='';
                    if(resp.response_code==200)
                    {
                       html+='<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">'+

                    '<div class="col-md-1 col-lg-1 col-sm-1 col-xs-1">'+
                      '<img src="https://s3.ap-south-1.amazonaws.com/entero-crm/'+sessionStorage.getItem('profile_pic')+'" class="pull-right user_assign-img">'+
                    '</div>'+
                    '<div class="col-md-11 col-lg-11 col-sm-11 col-xs-11 padding-left-0">'+
                      
                      '<p class="font-size-12">'+comment+'</p>'+
                      '<p class="font-size-12 color-grey">just now</p>'+
                      '<hr>'+
                    '</div>'+
                    
                  '</div>';
                  if(flag==1)
                  {
                      $("#1234").html(html); 
                      $('.remark-comment').val('');
                       $("#add_comment").attr("flag",flag);
                  }
                   else
                   {
                       $("#1234").append(html); 
                      $('.remark-comment').val('');
                  }
                }
                else
                    {
                      $('.remark-comment').val('');
                      toast_it(resp.response_message);
                      $('#add_remark-modal').modal('toggle');


                    }
                      }); 


      }); 
      
        var task_id='';
        var customer_id='';
       $(document).on('click', '.call_connect', function () {	
          task_id=$(this).attr("data-id-task");
          $('.add_remark_call').attr("data-id-task",task_id);
          customer_id=$(this).attr("customer-id");
        $('#last_contacted_number').html($(this).attr('data-last_contacted'));
        $('.place_order').attr('data-user_login',$(this).attr('data-user_login'));
        $('.place_order').attr('data-cust_erpid',$(this).attr('data-cust_erpid'));
        $('.place_order').attr('data-cust_erpcode',$(this).attr('data-cust_erpcode'));
        $('.place_order').attr('data-dist_id',$(this).attr('data-dist_id'));
        $('.place_order').attr('data-user_pwd',$(this).attr('data-user_pwd'));
        $('#call_again').attr("data-id-task",task_id);
        $('#end_task_button').attr("data-id-task",task_id);

        if($(this).attr('data-task_status') == 0){
          $('#end_task_button').hide();
        }
        $('#drop-d-action-task1').hide();
        $('#call_again').html("Check Status");
        $('#call_again').hide();
        $('#connected_msg').hide();
        $('.call-customer').removeAttr('disabled');
        var token=sessionStorage.getItem("token");
         $.ajax({
              dataType :'json',
                       method : 'POST',
                       async:false,
                  url : "<?php echo ROOT_URI; ?>/resources/services/remark_api.php",
                  data: "{\n\"action\":\"display_remarks_all\",\n\"entity_id\":\""+task_id+"\",\n\"entity_type\":\"1\"\n}",}).success(function(resp) {
                    var html='';
                    if(resp.response_code==400)
                    {
                      flag=1;
                      html='';
                    }
                    else{
                      for(i=0;i<resp.data.length;i++){
                        var d = new Date(resp.data[i].created_date);
                  var p=(ago(d.getTime()));
                      html+='<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">'+

                    '<div class="col-md-1 col-lg-1 col-sm-1 col-xs-1">'+
                      '<img src="https://s3.ap-south-1.amazonaws.com/entero-crm/'+resp.data[i].user_profile_pic+'" class="pull-right user_assign-img">'+
                    '</div>'+
                    '<div class="col-md-11 col-lg-11 col-sm-11 col-xs-11 padding-left-0">'+
                      
                      '<p class="font-size-12">'+resp.data[i].remark+'</p>'+
                      '<p class="font-size-12 color-grey">'+p+' ago</p>'+
                      '<hr>'+
                    '</div>'+
                    
                  '</div>';

                    }
                  }
                    $('#remark_call').html(html);
                  });


        $.ajax({
              dataType :'json',
                       method : 'POST',
                       async:false,
                  url : "<?php echo ROOT_URI; ?>/resources/services/details_task_page.php",
                  data: "{\n\"action\":\"20_most_ordered_company\",\n\"token\":\""+token+"\",\n\"customer_id\":"+customer_id+"\n}",
                       }).success(function(resp){

                        console.log(resp);
                        if(resp.response_code==200)
                    {
                      html='';
                      if(resp.data.length==0)
                      {
                        html='<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">'+
            '<p class="text-center font-size-12 margin-top-25"><b>No Company to Show</b></p>'+
          '</div>';
                      }
                      else
                      {
                      for(i=0;i<resp.data.length;i++){
                       html+='<div class="font-size-12 tag-box-for-comp float-inline">'+resp.data[i].Company+'</div>'
                      ;
                     }
                   }
                     $('#20_most_ordered').html(html);
                   }
                    
                      });   
                       $.ajax({
              dataType :'json',
                       method : 'POST',
                       async:false,
                  url : "<?php echo ROOT_URI; ?>/resources/services/details_task_page.php",
                  data: "{\n\"action\":\"20_most_ordered_product\",\n\"token\":\""+token+"\",\n\"customer_id\":"+customer_id+"\n}",
                       }).success(function(resp){

                        console.log(resp);
                        if(resp.response_code==200)
                    {
                      html='';
                       if(resp.data.length==0)
                      {
                        html='<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">'+
            '<p class="text-center font-size-12 margin-top-25"><b>No Products Ordered</b></p>'+
          '</div>';
                      }
                      else
                      {
                      for(i=0;i<resp.data.length;i++){
                       html+='<tr>'+
                  '<td><b>'+resp.data[i].ProductName+'</b></td>'+
                  '<td class="font-size-12">'+resp.data[i].Company+'</td>'+
                  '<td class="font-size-12">'+resp.data[i].Quntity+'</td>'+
                '</tr>';
                     }
                   }
                     $('#20_most_ordered_product').html(html);
                   }
                    
                      });

                    $.ajax({
                       dataType :'json',
                       method : 'POST',
                       async:false,
                       url : "<?php echo ROOT_URI; ?>/resources/services/details_task_page.php",
                       data: "{\n\"action\":\"last_5_order_details\",\n\"token\":\""+token+"\",\n\"customer_id\":"+customer_id+"\n}",
                       }).success(function(resp){
                      if(resp.response_code==200)
                      {
                          var order = '';

                          order_list = resp.data;

                          order_list.forEach(function(entry){

                            order = order + '<li role="presentation"><a role="menuitem" tabindex="-1" class="a-deco order_list_click" data-order_id='+entry._OrderID+' data-order_date="'+entry._Date+'">Order ID : '+entry._OrderID+' Order Date : '+entry._Date+'</a></li>';
                            $('#append_last_5_orders').html(order);
                          });

                            $('#last_5_order_details_items').html(order);



                      }                   
                            
                    });

                    $(document).on('click','.order_list_click',function(){

                      var order_id = $(this).attr('data-order_id');
                      var order_date = $(this).attr('data-order_date');

                      $.ajax({
                       dataType :'json',
                       method : 'POST',
                       async:false,
                       url : "<?php echo ROOT_URI; ?>/resources/services/details_task_page.php",
                       data: "{\n\"action\":\"last_5_order_items\",\n\"token\":\""+token+"\",\n\"order_id\":"+order_id+"\n,\n\"customer_id\":"+customer_id+"\n}",
                       }).success(function(resp){
                          if(resp.response_code==200)
                          {
                          $('#last_5_order_details_items').show(); 
                          html='<div class="font-size-14 margin-left-10 margin-bottom-10 text-bold">Order ID - '+order_id+' &emsp; Order Date - '+order_date+'</div>'+
                          '<table class="table border-bottom">'+
                              '<thead>'+
                                '<tr>'+
                                  '<th>ITEM</th>'+
                                  '<th>UNIT PRICE</th>'+
                                  '<th>QTY</th>'+
                                  '<th>TOTAL</th>'+
                                '</tr>'+
                              '</thead>'+
                              '<tbody >';
                          var order = '';

                          order_list = resp.data;

                          order_list.forEach(function(entry){
                           html+='<tr>'+
                          '<td><span class="color-grey font-size-12">'+entry.Manufacturer+'</span><br>'+entry.Product_Desc+'</td>'+
                          '<td>Rs.'+entry.PTR+'</td>'+
                          '<td>'+entry.Qty+'</td>'+
                          '<td><b>Rs.'+ entry.Total+'</b><br><span class="color-save">-('+entry.TotalSaving+'%) Rs.'+entry.Total+'</span></td>'+
                          '</tr>';
                           });  // close for each
                          html+='</tbody>'+
                          '</table>';
               
                         $('#last_5_order_details_items').html(html);
                       }  
                       else{
                         $('#last_5_order_details_items').show();
                         $('#last_5_order_details_items').html('<p class="text-center">No Items</p>');

                       }
                            
                       });   // close success


                    });    /// close on click order list


                        $.ajax({
                          dataType :'json',
                          method : 'POST',
                          async:false,
                          url : "<?php echo ROOT_URI; ?>/resources/services/details_task_page.php",
                          data: "{\n\"action\":\"customer_details\",\n\"token\":\""+token+"\",\n\"customer_id\":"+customer_id+"\n}",
                          }).success(function(resp){

                           console.log(resp);
                           if(resp.response_code==200)
                           {
                            html='';
                            $('#cust_name_call_panel').html(resp.data[0].S_Name);
                            if(!resp.data[0].S_MobContact){
                                resp.data[0].S_MobContact='';
                           }
                           if(!resp.data[0].S_AvgPrice){
                            resp.data[0].S_AvgPrice='NA';
                          }
                          if(!resp.data[0].OrderFrq){
                            resp.data[0].OrderFrq='NA';
                          }else{
                            resp.data[0].OrderFrq=resp.data[0].OrderFrq+' times per week';
                          }

                            $('#cust_ph_call_panel').val(resp.data[0].S_Tel_No);
                             html+='<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">'+
                             '<p class="text-center font-size-24 margin-top-25"><b>'+resp.data[0].S_Name+'</b></p>'+
                             '</div>'+
                            '<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6 padding-right-5">'+
                         
                          '<p class="float-inline pull-right margin-top--2">'+resp.data[0].S_Email+'</p>'+
                           '<img class="margin-right-8 float-inline pull-right" src="<?php echo ROOT_URI; ?>/assets/img/assets/email.svg">'+
                      '</div>'+
                      '<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6 padding-left-5">'+
                          '<img class="margin-right-8 float-inline" src="<?php echo ROOT_URI; ?>/assets/img/assets/phone.svg">'+
                          '<p class="float-inline margin-top--2">'+resp.data[0].S_Tel_No+'</p>'+
                      '</div>'+
                      '<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 margin-top-25">'+
                        '<div class="col-md-4 col-lg-4 col-sm-4 col-xs-4 text-right">'+
                            '<p class="font-size-12 color-grey">Cutomer Since</p>'+
                            '<p class=""><b>'+resp.data[0].S_CreateDate+'</b></p>'+
                        '</div>'+
                        '<div class="col-md-4 col-lg-4 col-sm-4 col-xs-4 text-center">'+
                            '<p class="font-size-12 color-grey">Avg. Order Size</p>'+
                            '<p class=""><b>Rs.'+resp.data[0].S_AvgPrice+'</b></p>'+
                        '</div>'+
                        '<div class="col-md-4 col-lg-4 col-sm-4 col-xs-4">'+
                            '<p class="font-size-12 color-grey">Order Frequency</p>'+
                            '<p class=""><b>'+resp.data[0].OrderFrq+'</b></p>'+
                        '</div>'+
                      '</div>';
                           
                           $('#cust_detail').html(html);
                         }
                    
                      }); 

                       $.ajax({
              dataType :'json',
                       method : 'POST',
                       async:false,
                  url : "<?php echo ROOT_URI; ?>/resources/services/call_history.php",
                  data: '{\n\"action\":\"call_history\",\n\"cust_id\":\"'+customer_id+'\",\n\"page_no\":1\n}',
                       }).success(function(resp){
                         if(resp.response_code==200)
                    {
                      html='';
                      for(i=0;i<resp.data.length;i++){
                       html+='<tr>'+
                  '<td><b>'+resp.data[i].start_date+'</b></td>'+
                  '<td class="font-size-12"><span class="color-grey">Time:</span> <b>'+resp.data[i].time+'</b></td>'+
                  '<td class="font-size-12"><span class="color-grey">Call Duration: </span> <b>'+resp.data[i].call_duration+'</b></td>'+
                '</tr>';
                     }
                     $('#body_call_display').html(html);
                   }
                    
                      });   

        
    $('.call-customer').on("click",function(){

            var calling_number = $('#cust_ph_call_panel').val();

            // alert("hello");
            $.ajax({
              dataType :'json',
                       method : 'POST',
                       async:false,
                  url : "<?php echo ROOT_URI; ?>/resources/services/call_services.php",
                  data: "{\n\"User_id\":\""+sessionStorage.getItem('user_id')+"\",\n\"token\":\""+sessionStorage.getItem('token')+"\",\n\"task_id\":\""+task_id+"\",\n\"Customer_User_id\":\""+customer_id+"\",\n\"to\":\""+calling_number+"\"}",}).success(function(resp) {
                        if(resp.response_status==2){
                             toast_it("Customer contact number is not available"); 
                          }
                          else if(resp.response_status==0){
                             toast_it("Couldn't place call, please try again"); 
                          }
                          else if(resp.response_status==1){
                            if(resp.data.Call.Status=="in-progress"){

                                sessionStorage.setItem("number_to_send_order",calling_number);

                                $('.call-customer').attr('disabled','disabled');
                                toast_it("Call is being connected to :"+resp.data.Call.To);
                                $('#call_again').attr("call_sid",resp.data.Call.Sid);
                                // $('#call_again').show()
                            }
                          }
                      });   
          });
     });

      var page_count = 1;
      //var comment_flag_load = 1;
      //var current_clicked_modal_content_id = 0;
      //var current_clicked_modal_content_type = 0;
      $(document).on('click', '.comment_loader_message', function () {
            var conditon=$('.filter_condition').attr('data-sort_selected');
            var filter=$('.task_tabs').attr('data-filter_selected');
              task_display(conditon,filter,++page_count);       
            
      });

      $(document).on('click','#created_first_tab',function(){

          $('#main_task').html('');
          $('.task_tabs').removeClass('display-task-active');
          $(this).addClass('display-task-active');
          $('.filter_condition').attr('data-sort_selected',1);
          page_count=1;
            task_display(1,$(this).attr('data-filter_selected'),1);
            
      });
      $(document).on('click','#last_contacted_tab',function(){

          $('#main_task').html('');
          $('.task_tabs').removeClass('display-task-active');
          $(this).addClass('display-task-active');
          $('.filter_condition').attr('data-sort_selected',2);
          page_count=1;
            task_display(2,$(this).attr('data-filter_selected'),1);
            
      });

      $(document).on('click','#last_ordered_tab',function(){

          $('#main_task').html('');
          $('.task_tabs').removeClass('display-task-active');
          $(this).addClass('display-task-active');
          $('.filter_condition').attr('data-sort_selected',3);
          page_count=1;
          task_display(3,$(this).attr('data-filter_selected'),1);
          


      });
      $(document).on('click','#order_frequency_tab',function(){

          $('#main_task').html('');
          $('.task_tabs').removeClass('display-task-active');
          $(this).addClass('display-task-active');
          $('.filter_condition').attr('data-sort_selected',3);
          page_count=1;
          task_display(4,$(this).attr('data-filter_selected'),1);
          


      });
      $('input.filter_condition').on('change', function() {
    
      $('input.filter_condition').not(this).prop('checked', false);  
  
  });

    $(document).on('click','input[type="checkbox"]',function(){

      if($(this).attr('data-condition_value') == 1&& $(this).is(':checked')){  // not associated with me
        //toast_it('not');
        $('.task_tabs').attr('data-filter_selected',1);
        $('#main_task').html('');
        page_count=1;
        task_display($(this).attr('data-sort_selected'),1,1);
        
      }
if($(this).attr('data-condition_value') == 2&& $(this).is(':checked')){  // associated with me 
        //toast_it('with');
        $('#main_task').html('');
        $(this).attr('data-sort_selected');
        $('.task_tabs').attr('data-filter_selected',2);
        page_count=1;
        task_display($(this).attr('data-sort_selected'),2,1);
        

      }
      if($(this).attr('data-condition_value') == 3 && $(this).is(':checked')){  // associated with me 
        //toast_it('with');
        $('#main_task').html('');
        $(this).attr('data-sort_selected');
        $('.task_tabs').attr('data-filter_selected',3);
        page_count=1;
        task_display($(this).attr('data-sort_selected'),3,1);
        

      }

      if($(this).attr('data-condition_value') == 4 && $(this).is(':checked')){  // associated with me 
        //toast_it('with');
        $('#main_task').html('');
        $(this).attr('data-sort_selected');
        $('.task_tabs').attr('data-filter_selected',4);
        page_count=1;
        task_display($(this).attr('data-sort_selected'),4,1);
        

      }

      if($(this).is(':not(:checked)')){
        //toast_it('none');
          $('#main_task').html('');
          $('.tab_task').removeClass('display-task-active');
          $('#all_case_tab').addClass('display-task-active');
          $('.task_tabs').attr('data-filter_selected','');    
          page_count=1;   
        task_display($('.filter_condition').attr('data-sort_selected'),'',1);
        
      }


    });

      var follow_up_array = {'time':[],'customer_name':[],'task_id':[]};

      // console.log(cur_time);

      task_display(1,5,1);

      setInterval(function() {

          check_follow_up();

      }, 40 * 1000);

      function check_follow_up(){

          var date = new Date;

          var minutes = ('0'+date.getMinutes()).slice(-2);
          var hour = ('0'+date.getHours()).slice(-2);

          var cur_time = hour+':'+(parseInt(minutes)+2);

          for(var i = 0; i<follow_up_array.task_id.length; i++){
              // console.log(follow_up_array.time[i]+"----"+cur_time);

            if(follow_up_array.time[i] == cur_time ){
              // console.log(cur_time);
              $('.paste_followup_time').html(follow_up_array.customer_name[i]+" - Follow Up time: "+cur_time);
              $('#followup_time_popup').modal('show');

              $('#confirm_follow_up_later').attr('data-task_id',follow_up_array.task_id[i]);

            }
 
          }

      }  // close function check follow up

      var paste_phone_number_customer_screen = sessionStorage.getItem('number_to_send_order');
    // toast_it(paste_phone_number_customer_screen);

      if(paste_phone_number_customer_screen){
          $('#paste_phone_number_customer_screen').html(paste_phone_number_customer_screen);       
          $('#paste_phone_icon').attr('src','<?php echo ROOT_URI; ?>/assets/img/assets/call-blue.svg');  
      }


      $(document).on('click','#reassign_tasks_user',function(){

          $('#reassign_tasks_user_modal').modal('show');

      }); 

      $(document).on('click','#confirm_follow_up_later',function(){

          $('#followup-modal').modal('show');

      });  

      function task_display(condition,filter,page_no){

                     $.ajax({
                       url: "<?php echo ROOT_URI; ?>/resources/services/task_main.php",
                       dataType :'json',
                       method : 'POST',
                       async:false,
                       data: '{\n\"action\":\"display_user\",\n\"condition\":\"'+condition+'\",\n\"filter\":\"'+filter+'\",\n\"token\":\"'+sessionStorage.getItem("token")+'\",\n\"user_id\":\"'+sessionStorage.getItem('user_id')+'\",\n\"dist_id\":\"'+sessionStorage.getItem('dist_id')+'\",\n\"role\":'+sessionStorage.getItem('role')+',\n\"page_no\":'+page_no+'\n}',
                       }).success(function(resp){
                        console.log(resp);
                        if(resp.response_code==200)
                        {

                          
                                  var htmlText ="";
                                  $('.comment_loader_message').show();
                                  for(i=0;i<resp.data.length;i++)
                                  {
                                    var follow_up=resp.data[i].follow_up;
                                    var [hours,minutes,seconds]=follow_up.split(':');
                                    var tf="";

                                    if(follow_up=='00:00:00')
                                    {
                                        
                                    }
                                    else
                                    { 
                                      follow_up_array['time'].push(hours+":"+minutes);
                                      follow_up_array['customer_name'].push(resp.data[i].customer_name);
                                      follow_up_array['task_id'].push(resp.data[i].task_id);
                                    }

                                    if(hours>11)
                                    {
                                        tf="P.M";
                                        hours=hours-12;
                                    }
                                    else
                                    {
                                      tf="A.M";
                                    }
                                    var button_text='';
                                    if(follow_up=='00:00:00')
                                    {
                                        button_text="Select Action";
                                      }
                                    else
                                    { 
                                      button_text="Follow Up @"+hours+":"+minutes+tf;                  
                                    }

                                    if(resp.data[i].task_when){
                                      var task_when = resp.data[i].task_when;
                                    }else{
                                      var task_when = "";
                                    }

                                    if(resp.data[i].time_to){
                                      var time_to = resp.data[i].time_to;
                                    }else{
                                      var time_to = "";
                                    }

                   if(resp.data[i].last_contacted == ""){
                          var p = "NA"
                   }else{
                          var d = new Date(resp.data[i].last_contacted);
                          var p =(ago(d.getTime()));
                              p = p + ' ago';
                        }

                    var last_order_placed = "";

                    if(resp.data[i].last_order_placed){
                      
                        last_order_placed = resp.data[i].last_order_placed;
                    }else{
                        last_order_placed = "NA";
                    }
                    if(resp.data[i].last_contacted_number){
                      var last_contacted_number = resp.data[i].last_contacted_number;
                    }else{
                      var last_contacted_number = 'NA';
                    }
                  // var days = new Date('2018-10-27 10:48:55');
                    // var n = d.getHours();
                    // n1="";
                    // if(n>12)
                    // {
                    //   n1=(n-12)+" P.M";
                    // }
                    // else
                    // {
                    //   n1=n+" A.M";
                    // }
                    var customer_blocked = '';
                    var Block_Reason = '';
                    var block_class = 'display-none';

                    if(resp.data[i].isBlocked == 0){
                       customer_blocked = 'Blocked';
                       Block_Reason = resp.data[i].Block_Reason;
                       block_class = '';
                    }


                    var clas='';
                    if(resp.data[i].status_label=="EXPIRED")
                    {
                      clas='label-default';
                    }
                    else if(resp.data[i].status_label=="NEW")
                    {
                      clas="label-success";
                    }
                    else
                    {
                      clas='label-danger';
                    }
                      htmlText+='<div class="back-color-white col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad main-item-for-tiles-mytask box-shadow1 margin-top-16">'+
                            '<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">'+
                            
                                '<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">'+
                                     '<p class="font-size-14 pull-right margin-right-10 float-inline text-danger margin-top-24 '+block_class+'">'+customer_blocked+'<i class="margin-left-10 fa fa-info-circle " aria-hidden="true" data-original-title="'+Block_Reason+'" data-toggle="tooltip"></i></p>'+

                                    '<p class="font-size-16  margin-top-24 margin-right-8 ellipsis"><b>'+resp.data[i].customer_name+'</b> <span class="label '+clas+' margin-left-10">'+resp.data[i].status_label+'</span></p><!--user classes label-default label-danger for other states of task-->'+
                                '</div>'+
            
                                '<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">'+
                                    '<p class="color-grey-1 ellipsis" data-toggle="tooltip" title="'+resp.data[i].customer_address+'"><i class="fa fa-map-marker" aria-hidden="true"></i> '+resp.data[i].customer_address+'</p>'+
                                '</div>'+
                                '<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">'+
                                    '<p class="font-size-14  float-inline margin-right-8 "><b>'+resp.data[i].task_name+'</b> '+
                                '</div>'+
                                '<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">'+
                                    '<p class="float-inline font-size-12 color-grey"><b>Contacted : </b> '+ p +' </p>'+
                                    '<p class="float-inline font-size-12 margin-left-16 color-grey"><b>Last Order Placed : </b>'+last_order_placed+'</p>'+
                                    '<a class="float-inline font-size-12 margin-left-16 a-deco color-brand remark_button" data-target="#add_remark-modal" data-toggle="modal" data-id-task="'+resp.data[i].task_id+'"><b>Add Remarks('+resp.data[i].total_remark+')</b></a>'+
                                '</div>'+
                                '<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 margin-top-5">'+

                                  '<div class="assign-my-task-div"><img src="<?php echo S3_BUCKET; ?>'+resp.data[i].assigned_user_profile_pic+'" class="float-inline user_assign-img"><p class="font-size-12 float-inline margin-top-6 margin-left-10 "><b>Assigned To '+resp.data[i].assigned_user_name+'</b></p></div>'+
                                  '<!-- use class "assign-my-task-div-other" for assigned task to other than me -->'+
                                '</div>'+
                                 
                            '</div>'+
                            
                           
                            '<div class="col-md-3 col-lg-3 col-sm-3 col-xs-3 margin-top-60 text-right padding-right-0">'+
                                 '<button class="btn btn-default brand-btn add-mem-btn1 width-btn-120 margin-top-10 call-btn-task-tiles call_connect" data-last_contacted="'+last_contacted_number+'" customer-id="'+resp.data[i].customer_id+'" data-id-task="'+resp.data[i].task_id+'" data-dist_id="'+resp.data[i].dist_id+'" data-cust_erpid="'+resp.data[i].cust_erpid+'" data-cust_erpcode="'+resp.data[i].cust_erpcode+'" data-user_login="'+resp.data[i].login_id+'" data-user_pwd="'+resp.data[i].user_pwd+'" data-task_status="'+resp.data[i].status+'">Call</button>'+                            

                                 '<div class="margin-right-20 margin-bottom-0 margin-left-16"></div><div><p class="margin-top-10 margin-right-20 color-brand"></p><p class="color-green">'+task_when+' - '+time_to+'</p></div><br>'+
                                 
                               
                            '</div>'+
                            '<div class="col-md-3 col-lg-3 col-sm-3 col-xs-3 padding-right-30  margin-top-40 text-right padding-left-0">'+
                                '<div class="dropdown">'+
                                '<button class="btn btn-default brand-btn-white add-mem-btn1 btn-drop-dwn dropdown-toggle" type="button" data-toggle="dropdown" id="drop-d-action-task">'+button_text+
                                  '<img class="margin-left-10" src="<?php echo ROOT_URI; ?>/assets/img/assets/dropdown-arrow.svg">'+
                                '<!-- <span class="caret"></span> --></button>'+
                                '<ul class="dropdown-menu pull-right">'+
                                  '<li ><a class="drop-sub-chl re-assign-pop" data-toggle="modal" data-target="#reassign-modal" data-id-task="'+resp.data[i].task_id+'">Re-assign</a></li>'+
                                  '<li ><a class="drop-sub-chl follow-up-pop" data-toggle="modal" data-target="#followup-modal" data-id-task="'+resp.data[i].task_id+'">Follow up</a></li>';

                                  if(resp.data[i].status == 0){

                                  }else{
                    htmlText = htmlText + '<li ><a class="drop-sub-chl close-pop" data-toggle="modal" data-target="#closetask-modal" data-id-task="'+resp.data[i].task_id+'">Close</a></li>';
                                  }                
                   htmlText = htmlText +  '</ul>'+
                              '</div> <br>'+

                              '<button class="btn btn-success  width-btn-120 margin-right-20 place_order" data-last_contacted="'+last_contacted_number+'" customer-id="'+resp.data[i].customer_id+'" data-id-task="'+resp.data[i].task_id+'" data-dist_id="'+resp.data[i].dist_id+'" data-cust_erpid="'+resp.data[i].cust_erpid+'" data-cust_erpcode="'+resp.data[i].cust_erpcode+'" data-user_login="'+resp.data[i].login_id+'" data-user_pwd="'+resp.data[i].user_pwd+'" data-task_status="'+resp.data[i].status+'">Place Order</button>'+
                            '</div>'+
                           

                        '</div>';

                                }
                                //alert(htmlText);
                                 $("#main_task").append(htmlText); 
                          $('[data-toggle="tooltip"]').tooltip();
                                 
                         }
                         else
                         {
                          toast_it("NO DATA TO SHOW");
                          $('.comment_loader_message').hide();
                         }

                                      });

              console.log(follow_up_array);


      }

$(document).on('click','.place_order',function(){

 var user_loginid = $(this).attr('data-user_login');
 var user_pwd = $(this).attr('data-user_pwd');
 var stk_id = $(this).attr('data-dist_id');
 var cust_erpid = $(this).attr('data-cust_erpid');
 var cust_erpcode = $(this).attr('data-cust_erpcode');
 var user_id = sessionStorage.getItem("og_id");
 var number_order = sessionStorage.getItem("number_to_send_order");

 var url = "http://crm.enterodirect.com/login?email="+user_loginid+"&password="+user_pwd+"&distributor="+stk_id+"&customer_erpid="+cust_erpid+"&customer_erpcode="+cust_erpcode+"&crmuserid=51&salesman_erpcode="+user_id+"&callerInfo="+number_order;

    window.open(url, '_blank'); 

});  // close 

$(document).on('click', '#call_again', function () {
  var text=$(this).html();
  var call_id=$(this).attr('call_sid');

 // call_id='da2ea84e4e47558e05014d30236e1cc5';
  if(text=='Check Status')
  {
    $.ajax({
              dataType :'json',
                       method : 'POST',
                       async:false,
                  url : "<?php echo ROOT_URI; ?>/resources/services/call_history.php",
                  data: '{\n\"action\":\"check_call",\n\"call_id\":\"'+call_id+'\"\n}',
                       }).success(function(resp){
                         if(resp.response_code==200)
                    {
                      $('#call_again').html("Call Again");
                      $('#drop-d-action-task1').show();
                      $('#connected_msg').html("CALL COMPLETED");
                      $('#connected_msg').show();
                    }
                    else
                    {
                      $('#connected_msg').show();
                    }

  });
}
if(text=='Call Again')
{
  var task_id=$('#call_again').attr('data-id-task');
  var customer_id=$('.call_connect').attr('customer-id');
  var calling_number = $('#cust_ph_call_panel').val();

            $.ajax({
              dataType :'json',
                       method : 'POST',
                       async:false,
                  url : "<?php echo ROOT_URI; ?>/resources/services/call_services.php",
                  data: "{\n\"User_id\":\""+sessionStorage.getItem('user_id')+"\",\n\"token\":\""+sessionStorage.getItem('token')+"\",\n\"task_id\":\""+task_id+"\",\n\"Customer_User_id\":\""+customer_id+"\",\n\"to\":\""+calling_number+"\"}",}).success(function(resp) {
                        if(resp.response_status==2){
                             toast_it("Customer contact number is not available"); 
                          }
                          else if(resp.response_status==0){
                             toast_it("Couldn't place call, please try again"); 
                          }
                          else if(resp.response_status==1){
                            if(resp.data.Call.Status=="in-progress"){
                                $('.call-customer').attr('disabled','disabled');
                                toast_it("Call is being connected to :"+resp.data.Call.To);
                                $('#call_again').attr("call_sid",resp.data.Call.Sid);
                                // $('#call_again').show()
                            }
                          }
                      });     
}

});


    $('#timepicker').mdtimepicker();
    $('#timepicker1').mdtimepicker();
    $('#timepicker2').mdtimepicker();

  $('#create_task_modal').on('shown.bs.modal', function (e) {
     
      $('.select2-selection__arrow b').hide();
      $('.select2-selection__arrow b').hide();
      $('.select2-selection__arrow').append('   <img class="margin-top-10 margin-right-8" src="<?php echo ROOT_URI; ?>/assets/img/assets/dropdown-arrow.svg">');
  });

  // $('.select2-arrow').append('<i class="fa fa-angle-down"></i>');

   
      $('#customer_display').select2({
        ajax: {
             url: "<?php echo ROOT_URI; ?>/resources/services/tag_api.php",
             dataType: 'json',
             delay: 350,
             data: function (params) {
                 return {
                     q: params.term, // search term
                       page: params.page || 1,
                       token: sessionStorage.getItem("token"),
                       dist_id:sessionStorage.getItem('dist_id')
                 };
             },
             processResults: function (data, params) {
                  params.page = params.page || 1;
                 return {
                     results: data.results,
                     pagination: {
                       more: (params.page * 10) < data.total_count
                     }

                 };
             },
             cache: true
         },
         
         tags: true,
         placeholder: "Select Customer"
 }); 
   

  // second select2 for assign-to
  $('.assign_customer_modal').select2({
         ajax: {
              url: "<?php echo ROOT_URI; ?>/resources/services/display_all_customer.php",
              dataType: 'json',
              delay: 350,
              data: function (params) {
                  return {
                      q: params.term, // search term
                        page: params.page || 1,
                        user_id:sessionStorage.getItem('user_id'),
                        role:sessionStorage.getItem('role')
                  };
              },
              processResults: function (data, params) {
                   params.page = params.page || 1;
                  return {
                      results: data.results,
                      pagination: {
                        more: (params.page * 5) < data.total_count
                      }

                  };
              },
              cache: true
          },
          
          tags: true,
          placeholder: "Select team member"
  });


  $('.assign_customer_modal').select2({
         ajax: {
              url: "<?php echo ROOT_URI; ?>/resources/services/display_all_customer.php",
              dataType: 'json',
              delay: 350,
              data: function (params) {
                  return {
                      q: params.term, // search term
                        page: params.page || 1,
                        user_id:sessionStorage.getItem('user_id'),
                        role:sessionStorage.getItem('role')
                  };
              },
              processResults: function (data, params) {
                   params.page = params.page || 1;
                  return {
                      results: data.results,
                      pagination: {
                        more: (params.page * 5) < data.total_count
                      }

                  };
              },
              cache: true
          },
          
          tags: true,
          placeholder: "Select team member"
  });

  
  $('#agent_display').select2({
        ajax: {
              url: "<?php echo ROOT_URI; ?>/resources/services/display_all_customer.php",
              dataType: 'json',
              delay: 350,
              data: function (params) {
                  return {
                      q: params.term, // search term
                        page: params.page || 1,
                        user_id:sessionStorage.getItem('user_id'),
                        role:sessionStorage.getItem('role')
                  };
              },
              processResults: function (data, params) {
                   params.page = params.page || 1;
                  return {
                      results: data.results,
                      pagination: {
                        more: (params.page * 5) < data.total_count
                      }

                  };
              },
              cache: true
          },
          
          tags: true,
          placeholder: "Select team member"
  });

  $(document).on('click','#confirm_reassign_all_tasks',function(){

    var currently_assigned = $('.currently_assigned').val();
    var  new_assign_tasks_to = $('.new_assign_tasks_to').val();

      if(currently_assigned == new_assign_tasks_to){

        toast_it("Assign tasks to different user");
        return false;
      }

            $.ajax({
               url : "<?php echo ROOT_URI; ?>/resources/services/task_main.php",
               type : 'POST',
               data : "{\n\"currently_assigned\":\""+currently_assigned+"\",\n\"new_assign_tasks_to\":\""+new_assign_tasks_to+"\",\n\"action\":\"reassign_all_tasks\"\n}",

               processData: false,  // tell jQuery not to process the data
               contentType: false,  // tell jQuery not to set contentType 
               dataType: 'JSON',
                     success: function(response) {

                      if(response.response_code == 200){

                        toast_it("Tasks Reassigned");
                        setInterval(function(){ location.reload();}, 1000);
                        

                      }  //  close if response code 200
                      
                     }  //  close success 
                    
            }); //ajax close

  });

  $(document).on('click', '.add_remark_call', function () {
        var comment=$('.remark_text').val();
        if(comment=='' || comment==null || comment==' '){
          toast_it("Remark can't be empty");
          return false;
        }
        var id=$('.add_remark_call').attr('data-id-task');
        var user_id=sessionStorage.getItem('user_id');
        //var profile_pic=sessionStorage.getItem('')
        //var flag=$('#add_comment').attr('flag');
         $.ajax({
              dataType :'json',
                       method : 'POST',
                       async:false,
                  url : "<?php echo ROOT_URI; ?>/resources/services/remark_api.php",
                  data: "{\n\"action\":\"add_remarks\",\n\"user_id\":\""+user_id+"\",\n\"entity_id\":\""+id+"\",\n\"entity_type\":\"1\",\n\"remark\":\""+comment+"\"\n}",}).success(function(resp) {
                    var html='';
                    if(resp.response_code==200)
                    {
                    
                       html+='<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">'+

                    '<div class="col-md-1 col-lg-1 col-sm-1 col-xs-1">'+
                      '<img src="<?php echo S3_BUCKET; ?>'+sessionStorage.getItem("profile_pic")+'" class="pull-right user_assign-img">'+
                '</div>'+
                    '<div class="col-md-11 col-lg-11 col-sm-11 col-xs-11 padding-left-0">'+
                      
                      '<p class="font-size-12">'+comment+'</p>'+
                      '<p class="font-size-12 color-grey">just now</p>'+
                      '<hr>'+
                    '</div>'+
                    
                  '</div>';
                }
                $('#remark_call').append(html);
                $('.remark_text').val('');
              });
                });
  $(document).on('click', '.close-pop', function () {
      var id_task=($(this).attr("data-id-task"));
      $(document).on('click', '#confirm_close', function () {
         $.ajax({
              dataType :'json',
                       method : 'POST',
                       async:false,
                  url : "<?php echo ROOT_URI; ?>/resources/services/task_main.php",
                  data: "{\n\"action\":\"close_task\",\n\"task_id\":\""+id_task+"\"}",})
         .success(function(resp) {
                    toast_it(resp.response_message);
                    //console.log(resp);

                      }); 
                  $('#closetask-modal').modal('toggle');
                  setInterval(function(){ location.reload();}, 1000);
                  


      });
});
  $(document).on('click','.call-btn-task-tiles',function(){
    $("#main-section-task").hide();
    $("#task-detail-section").show();
  });
  $(document).on('click','#back-btn-task',function(){
    $("#task-detail-section").hide();
    $("#main-section-task").show();
    setInterval(function(){ location.reload();}, 100);

    
  });
  
});
 
</script>
 

 