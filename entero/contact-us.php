<?php 
// print_r($_POST);die;
$msg_q_c ='';
if(isset($_POST['frm_submit'])){
	// print_r($_POST);die;
	// mail code starts from here
	if(!class_exists('PHPMailer',false))
		{
			require_once 'PHPMailer-master/class.phpmailer.php';
		}

		$mail = new PHPMailer;

		$mail->ClearAddresses();
		$mail->isSMTP();                                      			// Set mailer to use SMTP
		$mail->Host = 'ssl://smtp.gmail.com';	    		  			// Specify main and backup server
		$mail->Port = 465;
		$mail->SMTPAuth = true;                               			// Enable SMTP authentication
		$mail->Username = 'entero.tech@gmail.com';                        // SMTP username
		$mail->Password = 'Entero123';                     // SMTP password
	      $mail->SMTPSecure = 'ssl';                      	        // Enable encryption, 'ssl' also accepted
		$mail->From = 'entero.tech@gmail.com';
		$mail->FromName = 'Enterohealthcare.com';
		$mail->addAddress('info@enterohealthcare.com');               							// Name is optional

		$mail->WordWrap = 50;                              				// Set word wrap to 50 characters

		$mail->isHTML(true);                                  			// Set email format to HTML
		
		// $file_to_attach = 'logo.png';

		// $mail->AddAttachment( 'test.php' , 'NameOfFile.php' );
		
		$mail->Subject = 'Contact query from '.@$_POST['name'].' : '.@$_POST['sbjct'];
		$body='Hi, We have received a query from '.@$_POST['name'].'<br><br>Company name: '.@$_POST['c_name'].'<br> Email: '.@$_POST['mail'].'<br> Phone: '.@$_POST['phone'].'<br> Department: '.@$_POST['dpt'].'<br> Message: '.@$_POST['msg'].'<br>';
		$mail->Body= $body;
		$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

		if(!$mail->send()) {
			$msg_q_c = $mail->ErrorInfo;
		    // $msg_q_c = 'Something went wrong, please try again';
		}
		else{
			$msg_q_c = 'Thank-you for reaching out to us. We will get back to you shortly';
		}
			 
	// mail ends here
}

 ?>
<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width,initial-scale=1.0">
	<title>Entero Healthcare: Contact Us</title>
	<meta name="robots" content="noindex, nofollow" />
	<link href="https://fonts.googleapis.com/css?family=Istok+Web" rel="stylesheet">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
 <link href="css/animation.css" rel="stylesheet" />
<link rel="shortcut icon" type="image/png" href="img/favicon.png"/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="owlcarousel/owl.carousel.css">
<link rel="stylesheet" href="owlcarousel/owl.theme.default.css">
<link rel="shortcut icon" type="image/png" href="img/favicon.png"/>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<link rel="stylesheet" href="css/style.css" />
</head>

<body>
<div class="main_hdr">
<header>
<div class="head">
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<div class="logo">
					<a href="home.html"><img src="img/logo.png" /></a>
				</div>
			</div>
			<div class="col-md-9">
				<nav class="navbar navbar-expand-lg">
				<button type="button" class="navbar-toggler" >
      				<span class="fa fa-bars"></span>
    			</button>
    			<div class="collapse navbar-collapse" id="mn">
				<ul class="navbar nav">
					<li class="nav-item"><a class="nav-link" href="who-we-are.html">who we are</a></li>
					<li class="nav-item"><a class="nav-link" href="what-we-do.html">what we do</a></li>
					<li class="nav-item"><a class="nav-link" href="why-choose-us.html">why choose us</a></li>
					<!-- <li class="nav-item"><a class="nav-link" href="#">our products</a></li> -->
					<li class="nav-item"><a class="nav-link" href="careers.php">careers</a></li>
					<!-- <li class="nav-item"><a class="nav-link" href="news.html">news</a></li> -->
					<li class="nav-item"><a class="nav-link active" href="contact-us.php">contact us<i class="fa fa-angle-down myang"></i></a></li>
				</ul>
			</div>
			</nav>
			</div>
		</div>
	</div>
</div>
</header>
</div>
<section>
	<div class="contact_us">
		<div class="row m-0">
			<div class="col-md-4 p-0 contct">
				<div class="image pt-4">
					<img src="img/contact.png" />
				</div>
			</div>
			<div class="col-md-8">
				<div class="cntct_sec pt-4 pb-5">
				<div class="row">

					<div class="col-md-12">
						<h2 class="c_hdng pt-4 pb-2 mb-4">CORPORATE OFFICE</h2>
						<p style="font-size: 18px;"><strong>Entero Healthcare Solutions Pvt Ltd</strong><br />
						Unit No. 511, 5th floor, BKC Trade centre, Bandra East, Mumbai 400051</p>
						<p class="float-left mr-4"><i class="fa fa-phone"></i>022-26529100</p>
						<p class="float-left"><i class="fa fa-envelope"></i>info@enterohealthcare.com</p>
					</div>
				</div>
				<div class="row">
				<div class="col-md-12">
					<h2 class="c_hdng pt-4 pb-2 mb-4">SEND QUERY</h2>
				<div class="frm">
					<form action="" method="POST">
						<?php
							 
							if(!empty($msg_q_c)){
						 ?>
						   <div class="alert alert-success">
								 
							    <?php echo $msg_q_c; ?>
							  </div>
						 <?php } ?>
						<input required type="text" name="name" id="name" placeholder="Name" class="form-control mb-4"  />
						<input type="text" name="c_name" id="c_name" placeholder="Company Name" class="form-control my-4" />
						<input required type="email" name="mail" id="mail" placeholder="Email Id" class="form-control" />
						<input type="text" name="phone" id="phone" placeholder="Contact no." class="form-control my-4" />
						
						 
  						<div class="form-group">
						<!--   <label for="sel1">Select list:</label> -->
						  <select class="form-control" name="dpt" id="dpt">
						    <option>Select Department</option>
						    <option value="Customer Service">Customer Service</option>
						    <option value="Partnership">Partnership</option>
						    <option value="Careers">Careers</option>
						    <option value="General Queries">General Queries</option>
						  </select>
						</div>
						<input type="text" name="sbjct" id="sbjct" placeholder="Subject" class="form-control my-4" />
						<textarea class="form-control my-4" name="msg" id="msg" cols="11" placeholder="Message"></textarea>
						<button type="submit" name="frm_submit" class="btn mybtn1 waves-effect waves-light"><span style="color:red; font-weight: bold;">Send Query</span> <i class="fa fa-angle-right fa-2x"></i></button><br><br>
						
					</form>
				</div>
				</div>
				</div>
			</div>
			</div>
		</div>
	</div>
</section>




<footer>
	<div class="ftr">
		<div class="container">
			<div class="row">
				<div class="col-md-12 p-2">
					<p class="mt-2 float-right">
<span class="mr-3">CONNECT WITH US</span>
<a href="https://www.linkedin.com/company/entero-healthcare/about/" target="new"><i class="fa fa-linkedin"></i></a>
</p>
				</div>
			</div>
		</div>
	</div>
	<div class="copyright p-1">
		<p class="text-center mt-2">Copyright 2019 @ Entero Healthcare</p>
	</div>
</footer>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" ></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" ></script>
<script type="text/javascript" src="js/mdb.min.js"></script>
<script src="js/jquery.countTo.js"></script>

<script src="js/custom.js"></script>
</body>
</html>