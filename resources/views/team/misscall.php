 <?php 
  $path_init = $_SERVER['DOCUMENT_ROOT']."/crm/resources/init.php";
include_once($path_init);

 ?>

  <link href="<?php echo ROOT_URI; ?>/assets/css/temp.css" rel="stylesheet" type="text/css">
  <link href="<?php echo ROOT_URI; ?>/assets/css/team.css" rel="stylesheet" type="text/css">

<div class="col-md-10 col-lg-10 col-sm-10 col-xs-10 col-lg-offset-2 col-md-offset-2 col-sm-offset-2 col-xs-offset-2 no-lr-pad back-color-grey">
    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad " id="main-section-task">
   
           <!-- for upper part -->
  
            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 back-color-white padding-top-13 border-bottom padding-left-40">
                <p class="font-size-24 float-inline"><b>Missed Calls</b></p>
                <div class="dropdown pull-right margin-right-30">
                 
                </div>
            </div>
           <!-- upper part ends -->
    </div>
    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad ">
          <table class="table table-bordered">
            <thead>
              <tr>
                <th>Serial</th>
                <th>Customer Number</th>
                <th>Agent Number</th>
                <th>Agent Name</th>
                <th>Call Direction</th>
                <th>Call Status</th>
                <th>Duration</th>
                <th>Recording</th>
                <th>When</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody id="append_call_logs">

  
            </tbody>
          </table>
    </div>
        

</div>  
    <script type="text/javascript">
    $(document).ready(function() {

        var call_status = 'misscall';

        // call_logs_display(call_status);

        // $(document).on('click','.call_log_filter',function(){

        //   call_status = $(this).attr('data-call_status');

        //   $('#append_call_logs').html('');
          call_logs_display(call_status);
          // mark_misscall();
        // });
            

        function call_logs_display(call_status){   

            $.ajax({
              url: '<?php echo ROOT_URI; ?>/resources/services/call_history.php',
              method: 'POST',
              data: "{\n\"user_id\":\""+sessionStorage.getItem('user_id')+"\",\n\"dist_id\":\""+sessionStorage.getItem('dist_id')+"\",\n\"user_role\":\""+sessionStorage.getItem('role')+"\",\n\"call_status\":\""+call_status+"\",\n\"action\":\"call_logs_display\"\n}",
              dataType: 'json', 
            }).success(function(resp) {
                    
                    if(resp.response_code == 200){

                        var ele = '';
                        var call_log = '';
                        var i = 1;
                        var customer_number = '';
                        var agent_number = '';

                        call_log = resp.data;

                        call_log.forEach(function(entry){

                          var call_class = '';
                          var call_duration = '';

                          if(entry.call_direction == 'incoming'){

                            customer_number = entry.from_number;
                            agent_number = entry.to_number;
                            call_duration = entry.call_duration+" seconds";

                          }else{

                            customer_number = entry.to_number;
                            agent_number = entry.from_number;
                            call_duration = entry.call_duration;
                          }

                          if(entry.call_direction == 'incoming' && entry.call_status == 'no-answer'){

                            call_class = '';

                          }else{
                            call_class = 'display-none';
                          }

                          var recording_url = '';

                          if(entry.recording_url == 'null'){

                            recording_url = '';

                          }else{

                            recording_url = 'href="'+entry.recording_url+'" target="_blank"';
                            return false;
                          }

                          // toast_it("dfd");

                          ele = ele + 

                               '<tr>'+
                                '<td>'+i+'</td>'+
                                '<td>'+customer_number+'</td>'+
                                '<td>'+agent_number+'</td>'+
                                '<td>'+entry.name+'</td>'+
                                '<td>'+entry.call_direction+'</td>'+
                                '<td>'+entry.call_status+'</td>'+
                                '<td>'+call_duration+'</td>';
                                if(entry.call_status == 'completed'){
                                  ele = ele + '<td><a href="'+entry.recording_url+'" target="_blank">Recording URL</a</td>';
                                }else{
                                  ele = ele + '<td></td>';
                                }
                                
                     ele = ele +'<td>'+entry.created_date+'</td>'+
                                '<td><button class="btn btn-default btn-success call-customer '+call_class+'" data-customer_number='+customer_number+' data-callsid="'+entry.callsid+'">Call</button></td>'+
                              '</tr>';

                              i++;
                        });  // close for each 

                        $('#append_call_logs').html(ele);

                    }  // close if 200
                    else{

                         $('#append_call_logs').html('<tr class="text-center text-bold text-danger"><td>No Log</td></tr>');

                    }

            });//ajax 

        }  // close function call logs display  

        $(document).on('click','.call-customer',function(){

            var calling_number = $(this).attr('data-customer_number');
            var callsid = $(this).attr('data-callsid');
            var customer_id = 0;
            var task_id = 0;

            // alert("hello");
            $.ajax({
                  dataType :'json',
                  method : 'POST',
                  async:false,
                  url : "<?php echo ROOT_URI; ?>/resources/services/call_services.php",
                  data: "{\n\"User_id\":\""+sessionStorage.getItem('user_id')+"\",\n\"token\":\""+sessionStorage.getItem('token')+"\",\n\"task_id\":\""+task_id+"\",\n\"Customer_User_id\":\""+customer_id+"\",\n\"to\":\""+calling_number+"\"}",}).success(function(resp){
                          
                          if(resp.response_status==2){
                             toast_it("Customer contact number is not available"); 
                          }
                          else if(resp.response_status==0){
                             toast_it("Couldn't place call, please try again"); 
                          }
                          else if(resp.response_status==1){
                                
                                if(resp.data.Call.Status=="in-progress"){
                                sessionStorage.setItem("number_to_send_order",calling_number);
                                $('.call-customer').attr('disabled','disabled');
                                toast_it("Call is being connected to :"+resp.data.Call.To);
                                mark_calledback(callsid);
                                // $('#call_again').show()
                            }
                          }
                      });   
          });  // close function on click  

          function mark_calledback(callsid){

              $.ajax({
                 url : "<?php echo ROOT_URI; ?>/resources/services/call_history.php",
                 type : 'POST',
                 data : "{\n\"callsid\":\""+callsid+"\",\n\"action\":\"mark_calledback\"\n}",

                 processData: false,  // tell jQuery not to process the data
                 contentType: false,  // tell jQuery not to set contentType 
                 dataType: 'JSON',
                       success: function(response) {

                        if(response.response_code == 200){

                        // toast_it("Request Raised");
                        setInterval(function(){ location.reload();}, 1000);
                          

                        }  //  close if response code 200
                        
                       }  //  close success 
                      
              }); //ajax close


          }  

          // function mark_misscall(){

          //     $.ajax({
          //        url : "<?php echo ROOT_URI; ?>/resources/services/call_history.php",
          //        type : 'POST',
          //        data : "{\n\"dist_id\":\""+sessionStorage.getItem('dist_id')+"\",\n\"action\":\"mark_misscall\"\n}",

          //        processData: false,  // tell jQuery not to process the data
          //        contentType: false,  // tell jQuery not to set contentType 
          //        dataType: 'JSON',
          //              success: function(response) {

          //               if(response.response_code == 200){

          //               // toast_it("Request Raised");
          //               // setInterval(function(){ location.reload();}, 1000);
                          

          //               }  //  close if response code 200
                        
          //              }  //  close success 
                      
          //     }); //ajax close


          // }          

    });  // close document on ready
     
    </script>
    </body>
</html>