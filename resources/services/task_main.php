<?php
$path_init = $_SERVER['DOCUMENT_ROOT']."/crm/resources/init.php";
include_once($path_init); 
header('Content-type: application/json');
$data = json_decode(file_get_contents('php://input')); 
$obj_task=new task_class();
$obj_call=new call();
$obj_user=new user();


$action=@$data->action;

switch ($action) {
	

	case 'display_user':

		$token=$data->token;
		$user_id=$data->user_id;//user_id of user
		$page_no=$data->page_no;//page no 
		$role=$data->role;//role of user
		$limit=10;
		$condition =@$data->condition;
		$dist_id_user =@$data->dist_id;
		$filter = @$data->filter;
		//print_r($data);
		$response=array();

		$dist_id = $obj_user->get_distributor_id($dist_id_user);

		$result=$obj_task->task_details($user_id,$condition,$filter,$role,($page_no-1)*$limit,$limit,$token,$dist_id);
		//print_r($result);
		$res=array();
		if(empty($result))
		{
			$response['response_code']=400;
			$response['response_message']="Something Went Wrong";

			echo json_encode($response);
			break;
		}
		else
		{
			$customer_ids='';
			foreach($result as $keys=>$pair)
			{
				$customer_ids.=','.$pair->customer_id;
			}
			$customer_ids= ltrim($customer_ids,',');
			//echo $customer_ids;
			//echo strlen($customer_ids);
			//die;
		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => ORDERGINI_API."/api/CRMServices/GetChemistDetails",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => "{\"ChemistID\":[".$customer_ids."]}",
		  CURLOPT_HTTPHEADER => array(
		    "Authorization: Bearer ".$token,
		    "Content-Type: application/json",
		    "Postman-Token: ee2b2d47-87ad-48e8-a522-8c96fb6c31a1",
		    "cache-control: no-cache"
		  ),
		));

		$response_cus_id= curl_exec($curl);
	    // print_r($response_cus_id->message); die();

		$err = curl_error($curl);

		curl_close($curl);

		if (!$err) {
		$response_cus_id=json_decode($response_cus_id);
		 // print_r($response_cus_id);
		 // die();
		$i=0;
		foreach ($result as $key => $row) 
			{
		$call_detail=$obj_call->last_contracted(@$response_cus_id->result[$key]->ESCID);
		$total_remark=$obj_task->count_remark_all($row->task_id,1);
		$user_details=$obj_user->get_user_information($row->assigned_user_id);
		$state=$row->status;
		// print_r($call_detail); die();
		if($state==1)
		{
		
		$diff = time()-strtotime($row->created_date);

		//$hr_diff=$interval->format('%H');
		if($diff<3*3600)
		{
			$status_label="NEW";
		}
		elseif($diff>12*3600) 
		{
			$status_label="EXPIRED";
		}
		else
		{
			$status_label="OPEN";
		}
		}
		else
		{
			$status_label="CLOSED";
		}
		$c_detail='';
		if(isset($call_detail->created_date))
		{
			$c_detail=$call_detail->created_date;
		}

		$tempArray = array(
	                            "task_name"=>$row->task,
	                             "assigned_user_id"=>$row->assigned_user_id,
	                             "task_when"=>@$row->task_when,
	                             "time_to"=>@$row->time_to,
	                             "total_remark"=>$total_remark,
	                             "last_contacted"=>$c_detail,
	                             "assigned_user_name"=>$user_details->name,
	                             "assigned_user_profile_pic"=>$user_details->profile_picture,
	                             "customer_id"=>@$response_cus_id->result[$key]->ESCID,
	                             "customer_name"=>@$response_cus_id->result[$key]->S_Name,
	                             "customer_address"=>@$response_cus_id->result[$key]->Address,
	                             "status"=>$state,
	                             "status_label"=>$status_label,
	                             "last_order_placed"=>@$response_cus_id->result[$key]->LastOrderDate,
	                             "task_id"=>$row->task_id,
	                             "follow_up"=>$row->follow_up,
	                             "task_created_date"=>$row->created_date,
	                             "last_contacted_number"=>@$call_detail->to_number,
	                             "dist_id"=>@$response_cus_id->result[$key]->DistID,
	                             "cust_erpid"=>@$response_cus_id->result[$key]->ESCID,
	                             "cust_erpcode"=>@$response_cus_id->result[$key]->cust_erpcode,
	                             "login_id"=>@$response_cus_id->result[$key]->asuser_loginid,
	                             "user_pwd"=>@$response_cus_id->result[$key]->user_pwd,
	                             "isBlocked"=>@$response_cus_id->result[$key]->isBlocked,
					             "Block_Reason"=>@$response_cus_id->result[$key]->Block_Reason
	                             
	                        );
				$result_array[$i++]=$tempArray;
			}

			
			$response['response_code']=200;
			$response['response_message']="Success";
			$response['data']=$result_array;
		}
		else
		{
			$response['response_code']=400;
			$response['response_message']="Somethng Went Wrong";
		}
		}
	
		echo json_encode($response);

	break;


	case 'close_task':

		$task_id=$data->task_id;
		$result=$obj_task->close_task($task_id);
		if($result==1)//if task closed sucessfully 
			{
			$response['response_message']= "TASK CLOSED";
			$response['response_code'] = 200;
			}
		if($result==0)//if error occurs
			{
			$response['response_message']= "UNSECUSSFUL";
			$response['response_code'] = 400;
			}
			
		echo json_encode($response);

	break;


	case 'reassign_user': 

		$task_id=$data->task_id;
		$assign_cust_id=$data->assigned_user_id;
		$date_time=$data->date_time;
		$result=$obj_task->reassign_task($task_id,$assign_cust_id,$date_time);
		$follow_up='00:00:00';
		if($result==1)//if task closed sucessfully 
			{
			$result_follow_up=$obj_task->follow_up($task_id,$follow_up);	
			$response['response_message']= "TASK REASSIGNED";
			$response['response_code'] = 200;
			}
		if($result==0)//if error occurs
			{
			$response['response_message']= "UNSECUSSFUL";
			$response['response_code'] = 400;
			}
		echo json_encode($response);	

	break;
	
	case 'follow_up': 

		$task_id=$data->task_id;
		$follow_up=$data->follow_up;
		
		$result=$obj_task->follow_up($task_id,$follow_up);
		if($result==1)//if task closed sucessfully 
			{
			$response['response_message']= "TASK FOLLOWED";
			$response['response_code'] = 200;
			}
		if($result==0)//if error occurs
			{
			$response['response_message']= "UNSECUSSFUL";
			$response['response_code'] = 400;
			}
			echo json_encode($response);
			
	break;
	
	case 'add_task':

		$cusid=$data->cust_id;//cutomer id 
		$empid=$data->emp_id;//employe name
		$task=$data->taskname;//taskname
		$dte=$data->task_when;//when_time
		$current_id=$data->current_cust_id;
		$result=$obj_task->add_task($task,$current_id,$empid,$cusid,$dte);//add task function called which add task 
			if($result==1)//if task added sucessfully 
			{
			$response['response_message']= "TASK ADDED";
			$response['response_code'] = 200;
				}
				if($result==0)//if error occurs
			{
			$response['response_message']= "UNSECUSSFUL";
					$response['response_code'] = 400;
				}

			echo json_encode($response);

		break;

		case 'get_user_task':

			$user_id  = $data->user_id;

			$result = $obj_task->get_user_task($user_id);

			if($result)
			{
			$response['response_message']= "Success";
			$response['response_code'] = 200;
			$response['data'] = $result;
			}
			else
			{
			$response['response_message']= "Something went wrong";
			$response['response_code'] = 400;
			}

			echo json_encode($response);

		break;

		case 'reassign_all_tasks':

			$currently_assigned_to = $data->currently_assigned;
			$new_assign_tasks_to = $data->new_assign_tasks_to;

			$result = $obj_task->reassign_all_tasks($currently_assigned_to,$new_assign_tasks_to);

			if($result)
			{
			$response['response_message']= "Success";
			$response['response_code'] = 200;
			$response['data'] = $result;
			}
			else
			{
			$response['response_message']= "Something went wrong";
			$response['response_code'] = 400;
			}

			echo json_encode($response);

		break;

		default:
	
			$response['response_message']= "Wrong Action ";
			$response['response_code'] = 400;
						# code...
			
			echo json_encode($response);

		break;
}

?>