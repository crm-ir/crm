<?php
$path_init = $_SERVER['DOCUMENT_ROOT']."/crm/resources/init.php";
include_once($path_init); 
header('Content-type: application/json');
$data = json_decode(file_get_contents('php://input')); 
$action=$data->action;
$obj_call=new call();
$obj_user=new user();

switch ($action) {

	case 'call_history':

		$user_id=$data->cust_id;//user_id of user
		$page_no=$data->page_no;//page no 
		$limit=5;
		$call_history=$obj_call->call_history($user_id,($page_no-1)*$limit,$limit);

		if(empty($call_history))
		{
			$response['response_code']=400;
			$response['response_message']="Something Went Wrong";
		}
		else
		{
		$i=0;
		foreach ($call_history as $key => $row) 
			{
				$timestamp = strtotime($row->start_time);
				$date = date('d-m-Y', $timestamp);
				$time = date('H:i:s', $timestamp);
				$tempArray = array(
			                            "start_date"=>$date,
			                             "time"=>$time,
			                             "call_duration"=>$row->duration,

			                        );
				$result_array[$i++]=$tempArray;
			}
			$response['response_code']=200;
			$response['response_message']="Succes";
			$response['data']=$result_array;
			}

	break;

	case 'check_call':

		$call_id=$data->call_id;
		$check=$obj_call->check_call($call_id);
		if($check[0]->ch==1)
		{
			$response['response_code']=200;
			$response['response_message']="Call Completed";
		}
		else
		{
			$response['response_code']=400;
			$response['response_message']="Call in Progress";
		}
	break;

	case 'call_logs_display':

		$user_id = $data->user_id;
		$user_role = $data->user_role;
		$call_status = $data->call_status;
		$dist_id = $data->dist_id;

		$user_ids = $obj_user->get_userid($user_id,$user_role);
		// print_r($)
		$user_str='';
    
	    foreach ($user_ids as $key => $value) {
	        $user_str.=",'".$value."'";
	    }
	    $user_str=ltrim($user_str,',');

		$call_logs = $obj_call->call_logs_display($user_str,$call_status,$dist_id);

		if($call_logs)
		{
			$response['response_code']=200;
			$response['response_message']="Success";
			$response['data']=$call_logs;
		}
		else
		{
			$response['response_code']=400;
			$response['response_message']="Something Went Wrong";
		}
	break;

	case 'mark_calledback':

		$callsid = $data->callsid;

		$result = $obj_call->mark_calledback($callsid);

		if($result)
		{
			$response['response_code']=200;
			$response['response_message']="Success";
			$response['data']=$result;
		}
		else
		{
			$response['response_code']=400;
			$response['response_message']="Something Went Wrong";
		}
	break;

case 'mark_misscall':

		$dist_id = $data->dist_id;

		$result = $obj_call->mark_misscall($dist_id);

		if($result)
		{
			$response['response_code']=200;
			$response['response_message']="Success";
			$response['data']=$result;
		}
		else
		{
			$response['response_code']=400;
			$response['response_message']="Something Went Wrong";
		}
	break;
	}

echo json_encode($response);

?>