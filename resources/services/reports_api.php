<?php
$path_init = $_SERVER['DOCUMENT_ROOT']."/crm/resources/init.php";
include_once($path_init); 
header('Content-type: application/json');
$data = json_decode(file_get_contents('php://input')); 
$obj_task =new task_class();
$obj_call =new call();
$obj_user =new user();
$obj_dashboard =new Dashboard();

$action =$data->action;
	
	switch ($action) {

		case 'load_all_task':

		$token     = $data->token;
		$user_id   = $data->user_id;
		$user_role = $data->user_role;

		$users     = $obj_user->get_userid($user_id,$user_role);
		$res[0]    ='Task ID'.','.'Created By'.','.'Assigned By'.','.'Customer Name'.','.'Created Date'.','.'Status';
		$response_cus_id=array();
		foreach ($users as $key1 => $value1) {
			
			$result = $obj_user->load_all_task($value1);


		    $customer_ids='';
			foreach($result as $keys=>$pair)
			{
				$customer_ids.=','.$pair->customer_id;
			}
			$customer_ids=ltrim($customer_ids,',');
			//echo $customer_ids;
			//echo strlen($customer_ids);
			//die;
			$curl = curl_init();

			curl_setopt_array($curl, array(
			  CURLOPT_URL => ORDERGINI_API."/api/CRMServices/GetAllChemistDetails",
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "POST",
			  CURLOPT_POSTFIELDS => "{\"ChemistDArry\":[".$customer_ids."]}",
			  CURLOPT_HTTPHEADER => array(
			    "Authorization: Bearer ".$token,
			    "Content-Type: application/json",
			    "Postman-Token: ee2b2d47-87ad-48e8-a522-8c96fb6c31a1",
			    "cache-control: no-cache"
			  ),
			));

			$response_cus_id= curl_exec($curl);

			$err = curl_error($curl);

			curl_close($curl);
			// if (!$err) {
			$response_cus_id=json_decode($response_cus_id);
			//print_r($response_cus_id); die();


			 // print_r($response_cus_id->result[$key]->S_NAME); die();

			foreach ($result as $key => $value) {

				// print_r($response_cus_id->result[$key]->S_Name); die();
			$user_info_creator = $obj_user->get_user_information($value->creator_user_id);
			$user_info_assigned = $obj_user->get_user_information($value->assigned_user_id);
			 // print_r($user_info->name);
			if($value->status == 0){
				$status = "Open";
			}
			else{
				$status = "Closed";
			}

			
			$res[]  = $value->task_id.','.$user_info_creator->name.','.$user_info_assigned->name.','.@$response_cus_id->result[$key]->S_Name.','.$value->created_date.','.$status;

			}
			// print_r($response_cus_id);

		 }


			if($res){
			$response['response_code']=200;
			$response['response_message']="Success";
			$response['data']=$res;
			}
			else{
			$response['response_code']=400;
			$response['response_message']="Failure";	
			}

			echo json_encode($response);

		break;

		case 'load_all_case':

			$user_id 	= $data->user_id;
			$user_role  = $data->user_role;
			$token 		= $data->token;

			$users = $obj_user->get_userid($user_id,$user_role);

			$res[0]    ='Case ID'.','.'Case Name'.','.'Created By'.','.'Assigned By'.','.'Customer Name'.','.'Created Date'.','.'Status';

			foreach ($users as $key => $value1) {
				$result = $obj_user->load_all_case($value1);

				$customer_ids='';
				foreach($result as $keys=>$pair)
				{
					$customer_ids.=','.$pair->customer_id;
				}
				$customer_ids=ltrim($customer_ids,',');
				//echo $customer_ids;
				//echo strlen($customer_ids);
				//die;
				$curl = curl_init();

				curl_setopt_array($curl, array(
				  CURLOPT_URL => ORDERGINI_API."/api/CRMServices/GetAllChemistDetails",
				  CURLOPT_RETURNTRANSFER => true,
				  CURLOPT_ENCODING => "",
				  CURLOPT_MAXREDIRS => 10,
				  CURLOPT_TIMEOUT => 30,
				  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				  CURLOPT_CUSTOMREQUEST => "POST",
				  CURLOPT_POSTFIELDS => "{\"ChemistDArry\":[".$customer_ids."]}",
				  CURLOPT_HTTPHEADER => array(
				    "Authorization: Bearer ".$token,
				    "Content-Type: application/json",
				    "Postman-Token: ee2b2d47-87ad-48e8-a522-8c96fb6c31a1",
				    "cache-control: no-cache"
				  ),
				));

				$response_cus_id= curl_exec($curl);

				$err = curl_error($curl);

				curl_close($curl);
				// if (!$err) {
				$response_cus_id=json_decode($response_cus_id);



				foreach ($result as $key => $value) {
				$user_info_creator = $obj_user->get_user_information($value->creator_user_id);
				$user_info_assigned = $obj_user->get_user_information($value->assigned_user_id);
				 // print_r($user_info->name);
				
				if($value->status == 0){
					$status = "Open";
				}
				else{
					$status = "Closed";
				}
				
				$res[]  = $value->case_id.','.$value->case_name.','.$user_info_creator->name.','.$user_info_assigned->name.','.@$response_cus_id->result[$key]->S_Name.','.$value->created_date.','.$status;

				}
			}

			if($res){
			$response['response_code']=200;
			$response['response_message']="Success";
			$response['data']=$res;
			}
			else{
			$response['response_code']=400;
			$response['response_message']="Failure";	
			}

			echo json_encode($response);


		break;

		case 'load_all_call':

			$user_id 	= $data->user_id;
			$user_role  = $data->user_role;
			$token 		= $data->token;

			$users = $obj_user->get_userid($user_id,$user_role);

			$res[0]    ='Call ID'.','.'Called By'.','.'Called From'.','.'Called TO'.','.'Customer Name'.','.'Call Duration'.','.'Status';

			foreach ($users as $key => $value1) {
				$result = $obj_user->load_all_call($value1);

				$customer_ids='';
				foreach($result as $keys=>$pair)
				{
					$customer_ids.=','.$pair->customer_id;
				}
				$customer_ids=ltrim($customer_ids,',');
				//echo $customer_ids;
				//echo strlen($customer_ids);
				//die;
				$curl = curl_init();

				curl_setopt_array($curl, array(
				  CURLOPT_URL => ORDERGINI_API."/api/CRMServices/GetAllChemistDetails",
				  CURLOPT_RETURNTRANSFER => true,
				  CURLOPT_ENCODING => "",
				  CURLOPT_MAXREDIRS => 10,
				  CURLOPT_TIMEOUT => 30,
				  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				  CURLOPT_CUSTOMREQUEST => "POST",
				  CURLOPT_POSTFIELDS => "{\"ChemistDArry\":[".$customer_ids."]}",
				  CURLOPT_HTTPHEADER => array(
				    "Authorization: Bearer ".$token,
				    "Content-Type: application/json",
				    "Postman-Token: ee2b2d47-87ad-48e8-a522-8c96fb6c31a1",
				    "cache-control: no-cache"
				  ),
				));

				$response_cus_id= curl_exec($curl);

				$err = curl_error($curl);

				curl_close($curl);
				// if (!$err) {
				$response_cus_id=json_decode($response_cus_id);

				foreach ($result as $key => $value) {
				 $user_info_caller = $obj_user->get_user_information($value->user_id);
				 // print_r($user_info->name);

				if($value->status == 1)
				{
					$status = "Completed";
				}
				elseif($value->status == 2) {
					$status = "Queued";
				}
				elseif($value->status == -1) {
					$status = "Failed";
				}
				elseif($value->status == 3) {
					$status = "Busy";
				}
				elseif($value->status == 4) {
					$status = "No-answer";
				}
				elseif($value->status == 5) {
					$status = "In-progress";
				}
				
				$res[]  = $value->call_id.','.$user_info_caller->name.','.$value->from_number.','.$value->to_number.','.@$response_cus_id->result[$key]->S_Name.','.$value->duration.','.$status;

				}
			}

			if($res){
			$response['response_code']=200;
			$response['response_message']="Success";
			$response['data']=$res;
			}
			else{
			$response['response_code']=400;
			$response['response_message']="Failure";	
			}

			echo json_encode($response);

		break;

		case 'load_all_order':

			$dist_id 	= $data->dist_id;
			 

			// $users = $obj_user->get_userid($user_id,$user_role);

			$res[0]    ='Order No'.','.'ERP ID'.','.'Order date'.','.'Items'.','.'Amount'.','.'ERP Code'.','.'Order Mode'.','.'Chemist Name'.','.'Address';

			$distributor_id = $obj_user->get_distributor_id($data->dist_id);
 					// $distributor_id =89;
					 

					$curl = curl_init();

					curl_setopt_array($curl, array(
					  CURLOPT_URL => ORDERGINI_API."/api/CRMServices/CRM_Get_OrdersReportbyStockistID",
					  CURLOPT_RETURNTRANSFER => true,
					  CURLOPT_ENCODING => "",
					  CURLOPT_MAXREDIRS => 10,
					  CURLOPT_TIMEOUT => 30,
					  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					  CURLOPT_CUSTOMREQUEST => "POST",
					  CURLOPT_POSTFIELDS => "{\"StockistID\":\"".$distributor_id."\"}",
					  // CURLOPT_POSTFIELDS => "{\"StockistID\":\"89\"}",
					  CURLOPT_HTTPHEADER => array(
					    "Authorization: Bearer ".$data->token,
					    "Content-Type: application/json",
					    "Postman-Token: ee2b2d47-87ad-48e8-a522-8c96fb6c31a1",
					    "cache-control: no-cache"
					  ),
					));

					$response_cus_id= curl_exec($curl);

					
					$err = curl_error($curl);

					curl_close($curl);
					$response = array();
					$response_cus_id=json_decode($response_cus_id);
					// print_r($response_cus_id->chemistdetails[0]->OrderNo);die;
					if($response_cus_id->chemistdetails){
						foreach ($response_cus_id->chemistdetails as $key => $value) {
							 
							 // print_r($value->OrderNo);die;
							$res[]  = $value->OrderNo.','.$value->ERP_ID.','.$value->Orderdate.','.$value->Items.','.$value->Amount.','.$value->ERP_Code.','.$value->Order_Mode.','.$value->Chemist_Legal_Name.','.$value->Address;

							 
						}
					}
					else{
						$res[1]="No data found";
					}

			if($res){
			$response['response_code']=200;
			$response['response_message']="Success";
			$response['data']=$res;
			}
			else{
			$response['response_code']=400;
			$response['response_message']="Failure";	
			}

			echo json_encode($response);



		break;


		

	}


?>