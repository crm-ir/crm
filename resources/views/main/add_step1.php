<html>
<head>
	<title>Add user | Enteroteam.com</title>
  <link href='https://fonts.googleapis.com/css?family=Open Sans:300' rel='stylesheet'>

    <link rel="stylesheet" type="text/css" href="styles.css"/>
<link href="../../lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <script src="../../lib/jquery/jquery-2.2.4.min.js"></script>
    
</head>
<body>
  <div class="container ">
    <div class="row">
        <div class="col-md-2">
            <a href="../main/" class="btn btn-info pull-right margin-top-30 btn-mine">Home</a>
       
        </div>
        <div class="col-md-7">
            <h1 class="text-center margin-top-30">Add user</h1>
            <form action="add_user.php" method="post">
             	<div class="form-group" >
                  <label for="usr">Username:</label>
                  <input type="text" name="username" class="form-control" id="username">
                </div>
                <div class="form-group">
                  <label for="usr">FirstName:</label>
                  <input type="text" name="firstName" class="form-control" id="firstName">
                </div>
                <div class="form-group">
                  <label for="usr">Last Name:</label>
                  <input type="text" name="lastName" class="form-control" id="lastName">
                </div>
                <div class="form-group">
                  <label for="usr">Password:</label>
                  <input type="text" name="password" class="form-control" id="password">
                </div>
                <div class="form-group">
                  <label for="usr">Email:</label>
                  <input type="text" name="email" class="form-control" id="email">
                </div>
                 <input type="submit" class="btn btn-info pull-right margin-top-30 btn-mine" value="submit">
            </form>
        </div>
      </div>
  </div>
</body>
</html>