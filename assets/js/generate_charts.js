
  // var ctx = document.getElementById("callsummary");
  // ctx.height = 120;
  // var myChart = new Chart(ctx, {
  //     type: 'bar',
  //     data: {
  //         labels: ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
  //         datasets: [{
  //             label: '# of Calls',
  //             data: [12, 99, 3, 5, 2, 3],
  //             backgroundColor: [
  //                 'rgba(255, 99, 132, 0.2)',
  //                 'rgba(54, 162, 235, 0.2)',
  //                 'rgba(255, 206, 86, 0.2)',
  //                 'rgba(75, 192, 192, 0.2)',
  //                 'rgba(153, 102, 255, 0.2)',
  //                 'rgba(255, 159, 64, 0.2)'
  //             ],
  //             borderColor: [
  //                 'rgba(255,99,132,1)',
  //                 'rgba(54, 162, 235, 1)',
  //                 'rgba(255, 206, 86, 1)',
  //                 'rgba(75, 192, 192, 1)',
  //                 'rgba(153, 102, 255, 1)',
  //                 'rgba(255, 159, 64, 1)'
  //             ],
  //             borderWidth: 1
  //         }]
  //     },
  //     options: {
  //         scales: {
  //             yAxes: [{
  //                 ticks: {
  //                     beginAtZero:true
  //                 }
  //             }]
  //         }
  //     }
  // });

  // var ctx1 = document.getElementById("performance_chart");
  // ctx1.height = 200;
  // var chart = new Chart(ctx1, {
  //   // The type of chart we want to create
  //   type: 'line',

  //   // The data for our dataset
  //   data: {
  //       labels: ["January", "February", "March", "April", "May", "June", "July"],
  //   datasets: [{
  //       label: "My First dataset",
  //       data: [65, 59, 80, 81, 56, 55, 40],
  //       backgroundColor: [
  //         'rgba(105, 0, 132, .2)',
  //       ],
  //       borderColor: [
  //         'rgba(200, 99, 132, .7)',
  //       ],
  //       borderWidth: 2
  //     },
  //       {
  //       label: "My Second dataset",
  //       data: [28, 48, 40, 19, 86, 27, 90],
  //       backgroundColor: [
  //         'rgba(0, 137, 132, .2)',
  //       ],
  //       borderColor: [
  //         'rgba(0, 10, 130, .7)',
  //       ],
  //       borderWidth: 2
  //     }

  //       ]
  //   },

  //   // Configuration options go here
  //   options: {}
  // }); // Team performance charts 


  // var ctx = document.getElementById("callsummary_1");
  // ctx.height = 170;
  // var myChart = new Chart(ctx, {
  //     type: 'bar',
  //     data: {
  //         labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
  //         datasets: [{
  //             label: '# of Calls',
  //             data: [12, 99, 3, 5, 2, 3],
  //             backgroundColor: [
  //                 'rgba(255, 99, 132, 0.2)',
  //                 'rgba(54, 162, 235, 0.2)',
  //                 'rgba(255, 206, 86, 0.2)',
  //                 'rgba(75, 192, 192, 0.2)',
  //                 'rgba(153, 102, 255, 0.2)',
  //                 'rgba(255, 159, 64, 0.2)'
  //             ],
  //             borderColor: [
  //                 'rgba(255,99,132,1)',
  //                 'rgba(54, 162, 235, 1)',
  //                 'rgba(255, 206, 86, 1)',
  //                 'rgba(75, 192, 192, 1)',
  //                 'rgba(153, 102, 255, 1)',
  //                 'rgba(255, 159, 64, 1)'
  //             ],
  //             borderWidth: 1
  //         }]
  //     },
  //     options: {
  //         scales: {
  //             yAxes: [{
  //                 ticks: {
  //                     beginAtZero:true
  //                 }
  //             }]
  //         }
  //     }
  // });

  // var ctx1 = document.getElementById("performance_chart_1");
  // ctx1.height = 200;
  // var chart = new Chart(ctx1, {
  //   // The type of chart we want to create
  //   type: 'line',

  //   // The data for our dataset
  //   data: {
  //       labels: ["January", "February", "March", "April", "May", "June", "July"],
  //   datasets: [{
  //       label: "My First dataset",
  //       data: [65, 59, 80, 81, 56, 55, 40],
  //       backgroundColor: [
  //         'rgba(105, 0, 132, .2)',
  //       ],
  //       borderColor: [
  //         'rgba(200, 99, 132, .7)',
  //       ],
  //       borderWidth: 2
  //     },
  //       {
  //       label: "My Second dataset",
  //       data: [28, 48, 40, 19, 86, 27, 90],
  //       backgroundColor: [
  //         'rgba(0, 137, 132, .2)',
  //       ],
  //       borderColor: [
  //         'rgba(0, 10, 130, .7)',
  //       ],
  //       borderWidth: 2
  //     }

  //       ]
  //   },

  //   // Configuration options go here
  //   options: {}
  // });

  // window.onload = function() {
  //     var ctx2 = document.getElementById('task-summary_chart_1').getContext('2d');
  //     ctx2.height = 100;
  //     window.myDoughnut = new Chart(ctx2, config);

  //     var task_ctx = document.getElementById('task-summary_chart').getContext('2d');
  //     task_ctx.height = 100;
  //     window.myDoughnut_1 = new Chart(task_ctx, config);
  // };

  // var randomScalingFactor = function() {
  //     return Math.round(Math.random() * 100);
  //   };
  // var config = {
  //     type: 'doughnut',
  //     data: {
  //       datasets: [{
  //         data: [
  //           randomScalingFactor(),
  //           randomScalingFactor(),
  //           randomScalingFactor(),
  //           randomScalingFactor(),
  //           randomScalingFactor(),
  //         ],
  //         backgroundColor: [
  //           'rgb(255,0,0)',
  //           'rgb(0,255,0)',
  //           'rgb(0,0,255)',
  //           'rgb(255,255,0)',
  //           'rgb(0,255,255)',
  //         ],
  //         label: 'Dataset 1'
  //       }],
  //       labels: [
  //         'Red',
  //         'Orange',
  //         'Yellow',
  //         'Green',
  //         'Blue'
  //       ]
  //     },
  //     options: {
  //       responsive: true,
  //       legend: {
  //         position: 'top',
  //       },
  //       title: {
  //         display: true,
  //         text: ''
  //       },
  //       animation: {
  //         animateScale: true,
  //         animateRotate: true
  //       }
  //     }
  //   };

 // Team performance charts ends here 