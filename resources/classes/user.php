<?php
// include 'db.php';
class user
{
      private $_db, $dt,$_user_obj;
      private static $_instance = null;
   public function __construct($user=null)
   {
      $this->_db = DB::getInstance();
     // $this->_user_obj = User::getInstance_of_user();
      date_default_timezone_set('Asia/Calcutta');
        $this->dt = date('Y-m-d H:i:s');
   }
    public static function getInstance_of_user()          //Used to instantiate an object
      {                         //Checks if instance already created else creates a new Instance                                  
      if(!isset(self::$_instance))
      {
        self::$_instance=new user();
      }
      return self::$_instance;
    }

   public function user_login_data_for_globalization($user_id){
   
     $sql="SELECT a.user_id, b.role, a.phone,a.name,a.profile_picture,a.email,a.calling_no,a.og_id FROM users as a INNER JOIN user_role as b ON a.user_id=b.user_id WHERE a.user_id ='$user_id'";
    

     //no user_status condition 
     // dont use same name variable for override purpose
    $result_l=$this->_db->query($sql)->results();

    // print_r($result_l);die;
    $arr=array();
   if(empty($result_l)){
       return 0;
   }
   else{
       foreach($result_l as $key=>$row){  
         $arr['user_id']=$row->user_id;
         $arr['name']=$row->name;
         $arr['profile_pic']=$row->profile_picture;
         $arr['email']=$row->email;
         $arr['phone']=$row->phone;
         $arr['calling_no']=$row->calling_no;
         $arr['og_id']=$row->og_id;
       
         $user_id=$row->user_id;
         $arr['role']=$row->role;
         
         switch($arr['role']){
            case '1':
                     $arr['dist_id']='';
            
            break;
            case '2':
                     $arr['dist_id']=$row->user_id;
            
            break;

            case '3':
                    $arr['l2_id']=$row->user_id;
                    $sql1="SELECT user_id_distributor from user_distributor_mapping WHERE user_id_l2=?";
                         $result_ud=$this->_db->query($sql1,array($user_id))->results();
                      foreach($result_ud as $key=>$row1)
                      {
                        $arr['dist_id']=$row1->user_id_distributor;
                      }
            break;

            case '4':
                    $sql2="SELECT user_id_l2 from l1_l2_mapping WHERE user_id_l1=?";
                      $l2_id='';
                      $result_l1_l2=$this->_db->query($sql2,array($user_id))->results();
                            foreach($result_l1_l2 as $key=>$row2)
                      {
                        $arr['l2_id']=$row2->user_id_l2;
                        $l2_id=$arr['l2_id'];
                      }
                    $sql3="SELECT user_id_distributor from user_distributor_mapping WHERE user_id_l2=?";
                      $result_ud=$this->_db->query($sql3,array($l2_id))->results();
                            foreach($result_ud as $key=>$row3)
                      {
                        $arr['dist_id']=$row3->user_id_distributor;
                    }
            break;


         }

          $sql_p="SELECT dist_phone from distributor_mapping WHERE user_id=?";
          $result_p=$this->_db->query($sql_p,array($arr['dist_id']))->results();
          $arr['dist_phone'] = @$result_p[0]->dist_phone;

          $sql_p="SELECT dynamic_multiple_customer from distributor_mapping WHERE user_id=?";
          $result_p=$this->_db->query($sql_p,array($arr['dist_id']))->results();
          $arr['dynamic_multiple_customer'] = @$result_p[0]->dynamic_multiple_customer;

           $sql_p1="SELECT count(*) as total_misscall from call_logs WHERE dist_id=?";
          $result_p1=$this->_db->query($sql_p1,array($arr['dist_id']))->results();
          $arr['misscall_status'] = @$result_p1[0]->total_misscall;

      }
       
   }
    return $arr;
}
public function set_priority_users($user_id,$new_value){
    $query ="UPDATE users SET priority = ? WHERE user_id = ?";
    $result1 = $this->_db->query($query,array($new_value,$user_id));  
      if($result1->_error)
      {
         return 0;
      }
      else
      {
         return 1;
      }
}
public function set_token_firebase($user_id,$token){
    $query ="UPDATE users SET token = ? WHERE user_id = ?";
    $result1 = $this->_db->query($query,array($token,$user_id));  
      if($result1->_error)
      {
         return 0;
      }
      else
      {
         return 1;
      }
}

public function get_priority_users($dist_id){
      $all_user_id = array();
      $all_user_id = $this->get_userid($dist_id,2);
      $user_ids = '';
      foreach ($all_user_id as $key => $value) {
        if($value!=$dist_id){
           $user_ids.="'".$value."',";
        }
      }
        $user_ids = rtrim($user_ids,",");

      $sql="SELECT a.user_id, b.role, a.phone,a.name,a.profile_picture,a.email,a.status,a.priority FROM users as a INNER JOIN user_role as b ON a.user_id=b.user_id WHERE a.user_id in ($user_ids) order by a.priority asc";
    
  
       $result=$this->_db->query($sql)->results();
       return $result;


}
public function get_misscalls($dist_id){
          $sql_p1="SELECT count(*) as total_misscall from call_logs WHERE dist_id=? AND call_status = 'no-answer' AND call_direction = 'incoming' AND recording_url = 'null'";
          $result_p1=$this->_db->query($sql_p1,array($dist_id))->results();
           return $result_p1[0]->total_misscall;
}

public function l2display()//displays all the l2 users
{
 $query ="SELECT a.name,b.user_id FROM users as a INNER JOIN user_role as b ON a.user_id=b.user_id AND b.role=3";
$result_l=$this->_db->query($query)->results();
if(empty($result_l))
{
   return 0;
}
else
{
 return $result_l;
}
}
public function update_details($id,$name,$calling_no,$profile_pic,$ordergini_id)
{
 $query ="UPDATE users SET name = ?, calling_no = ?,profile_picture = ?,og_id = ? WHERE users.user_id = ?";
$result_up=$this->_db->query($query,array($name,$calling_no,$profile_pic,$ordergini_id,$id))->results();
if($result_up)
{
   return 0;
}
else
{

 return 1;
}
}
public function l1display()//displays all the l1 user
{
 $query = "SELECT a.name,b.user_id FROM users as a INNER JOIN user_role as b ON a.user_id=b.user_id AND b.role=4";
 $result=$this->_db->query($query)->results();
if(empty($result))
{
   return 0;
}
else
{
 return $result;
}

}
public function insert_details_l1($user_id,$mapping_user_id,$name,$email,$phone,$ordergini_id){
   $sql ="INSERT INTO users(user_id,email,name,phone,calling_no,og_id) VALUES(?,?,?,?,?,?)";
   $result = $this->_db->query($sql,array($user_id,$email,$name,$phone,$phone,$ordergini_id));
   // $last_id = $this->_db->last();
   // print_r($result);die;
   if($result->_error)
   {
      return 0;
   }
   else
   {
      $sql1="INSERT INTO user_role (user_id, role) VALUES(?,?)";
      $result1 = $this->_db->query($sql1,array($user_id,4));  
      if($result1->_error)
      {
         return 0;
      }
      else
      {     
         $sql2="INSERT INTO l1_l2_mapping(user_id_l1,user_id_l2) VALUES(?,?)";
         $result1 = $this->_db->query($sql2,array($user_id,$mapping_user_id)); 
         if($result1->_error)
         {
            return 0;
         }
         else
         {
            return 1;
         }
        
      }
         
   }
}

public function insert_details_dis($user_id,$name,$email,$phone,$ordergini_id,$exo)//insert l2 user
{
   $res=array();
   $sql ="INSERT INTO users(user_id,email,name,phone,calling_no,og_id) VALUES(?,?,?,?,?,?)";
   $result = $this->_db->query($sql,array($user_id,$email,$name,$phone,$phone,''));
   // print_r($result);die;
   if($result->_error)
   {
      return 0;

   }
   else
   {
      $sql1="INSERT INTO user_role (user_id, role) VALUES(?,?)";
      $result1 = $this->_db->query($sql1,array($user_id,2));  
      if($result1->_error)
      {
         return 0;
      }
      else
      {
         $sql2="INSERT INTO distributor_mapping(user_id,dist_id,dist_phone) VALUES(?,?,?)";
         $result = $this->_db->query($sql2,array($user_id,$ordergini_id,$exo));
          if($result1->_error)
         {
           return 0;
         }
         else
         {
           return 1;
      
         }
        
      }
         
   }
   return $res;
} 
public function insert_details_l2($user_id,$mapping_user_id,$name,$email,$phone,$ordergini_id)//insert l2 user
{
   $res=array();
   $sql ="INSERT INTO users(user_id,email,name,phone,calling_no,og_id) VALUES(?,?,?,?,?,?)";
   $result = $this->_db->query($sql,array($user_id,$email,$name,$phone,$phone,$ordergini_id));
   if($result->_error)
   {
      return 0;

   }
   else
   {
      $sql1="INSERT INTO user_role (user_id, role) VALUES(?,?)";
      $result1 = $this->_db->query($sql1,array($user_id,3));  
      if($result1->_error)
      {
         return 0;
      }
      else
      {
         $sql2="INSERT INTO user_distributor_mapping(user_id_l2,user_id_distributor) VALUES(?,?)";
         $result = $this->_db->query($sql2,array($user_id,$mapping_user_id));
          if($result1->_error)
         {
           return 0;
         }
         else
         {
           return 1;
      
         }
        
      }
         
   }
   return $res;
} 
public function get_userid($user_id,$role){
  $_id=array($user_id);
  if($role==2)
  {
    $sql_dist="SELECT b.user_id_l2 from user_distributor_mapping as b where b.user_id_distributor=?";
    $result_dist= $this->_db->query($sql_dist,array($user_id))->results();
    if(empty($result_dist))
    {
       return $_id;
    }
    else
    {

      foreach ($result_dist as $key=>$row) 
      {
        array_push($_id,$row->user_id_l2);
        $sql_l2="SELECT b.user_id_l1 from l1_l2_mapping as b where b.user_id_l2=?";
        $result_l2= $this->_db->query($sql_l2,array($row->user_id_l2))->results();
        if(empty($result_dist))
        {
          return $_id;
        }
        else
        {
          foreach ($result_l2 as $key_l2=>$row_l2) 
          {
            array_push($_id,$row_l2->user_id_l1);
          }

        }

      }
      return $_id;
    }
  }
    if($role==1)
  {
        $sql_admin="SELECT a.user_id from users as a where a.user_id!=?";
        $result_admin= $this->_db->query($sql_admin,array($user_id))->results();
        if(empty($result_admin))
        {
          return $_id;
        }
        else
        {
          foreach ($result_admin as $key_admin=>$row_admin) 
          {
            array_push($_id,$row_admin->user_id);
          }
          return $_id;
        }

  }
  if($role==3)
  {
        $sql_l2="SELECT b.user_id_l1 from l1_l2_mapping as b where b.user_id_l2=?";
        $result_l2= $this->_db->query($sql_l2,array($user_id))->results();
        if(empty($result_l2))
        {
          return $_id;
        }
        else
        {
          foreach ($result_l2 as $key_l2=>$row_l2) 
          {
            array_push($_id,$row_l2->user_id_l1);
          }
          return $_id;
        }

  }
  if($role==4)
  {
    return $_id;
  }

}
public function delete_user($user_id){
    $query ="UPDATE users SET status = 0 WHERE user_id = ?";
    $result1 = $this->_db->query($query,array($user_id));  
      if($result1->_error)
      {
         return 0;
      }
      else
      {
         return 1;
      }
}

public function activate_user($user_id){
    $query ="UPDATE users SET status = 1 WHERE user_id = ?";
    $result1 = $this->_db->query($query,array($user_id));  
      if($result1->_error)
      {
         return 0;
      }
      else
      {
         return 1;
      }
}

public function check_customer($customer_id){

  $sql =  "SELECT * FROM user_customer_mapping WHERE customer_id ='$customer_id'";
  $result = $this->_db->query($sql)->results();

  if(empty($result)){
    return 0;
  }else{
    return 1;
  }

}

public function get_user_information($user_id)
   {
      $sql="SELECT a.name,a.profile_picture,a.calling_no,b.role,a.status,a.email,a.user_id,a.og_id from users as a JOIN user_role as b on a.user_id=b.user_id where a.user_id=?";
      $result_user=$this->_db->query($sql,array($user_id))->results();
      if(empty($result_user))
      {
         return 0;
      }
      else
      {
      foreach($result_user as $last_user=>$row_user)
                  {
                     return $row_user;
                  }

   }
}

public function get_user_information_cust($customer_id)
   {
      $sql="SELECT a.name,a.profile_picture,a.calling_no,b.role,a.status,a.email,a.user_id from users as a JOIN user_role as b on a.user_id=b.user_id join user_customer_mapping as c on a.user_id = c.user_id where c.customer_id = ?";
      $result_user=$this->_db->query($sql,array($customer_id))->results();
      if(empty($result_user))
      {
         return 0;
      }
      else
      {
      foreach($result_user as $last_user=>$row_user)
                  {
                     return $row_user;
                  }

   }
}

public function get_open_case_for_cust($customer_id){
      $sql="SELECT count(*) as count_case from cases  where case_state=1 and status=1 and customer_id=?";
      $result_user=$this->_db->query($sql,array($customer_id))->results();
      if(empty($result_user))
      {
         return 0;
      }
      else
      {
        return $result_user[0]->count_case;
      }
      
}
public function get_open_task_for_cust($customer_id){
      $date=date("Y-m-d");
      $sql="SELECT * from tasks  where date(created_date)='$date' and status=1 and customer_id=?";
      $result_user=$this->_db->query($sql,array($customer_id))->results();
      if(empty($result_user))
      {
         return 0;
      }
      else
      {
        return $result_user[0];
      }
      
}
public function get_user_tag_information($user_id,$role,$key1)
   {

    //print_r($role);
    $key1=mb_strtolower($key1);
    $user_str='';
      $user_id_dorm=$this->get_userid($user_id,$role);
     // print_r($user_id_dorm);
     foreach ($user_id_dorm as $key => $value) {
        $user_str.=",'".$value."'";
      }
      $user_str=ltrim($user_str,',');
      $sql="SELECT a.name,a.profile_picture,a.user_id,b.role from users as a INNER JOIN user_role as b on a.user_id=b.user_id WHERE a.status = 1 and lower(name) LIKE '%$key1%' AND a.user_id IN(".$user_str.") order by a.name asc";

      $result_user=$this->_db->query($sql)->results();
      //print_r($result_user);
      if(empty($result_user))
      {
         return 0;
      }
      else
      {
      return $result_user;
   }
}
public function get_all_the_agents($user_id,$role)
   {

    
      $user_str='';
      $user_id_dorm=$this->get_userid($user_id,$role);
     // print_r($user_id_dorm);
     foreach ($user_id_dorm as $key => $value) {
        $user_str.=",'".$value."'";
      }
      $user_str=ltrim($user_str,',');
      $sql="SELECT a.name,a.profile_picture,a.user_id,b.role from users as a INNER JOIN user_role as b on a.user_id=b.user_id WHERE a.status = 1 AND a.user_id IN(".$user_str.") order by a.name asc";

      $result_user=$this->_db->query($sql)->results();
      //print_r($result_user);
      if(empty($result_user))
      {
         return 0;
      }
      else
      {
      return $result_user;
   }
}
 
public function get_count_user_tag_information($user_id,$role,$key){
     $user_id=$this->get_userid($user_id,$role);
      $user_str=implode(",",$user_id);
     $sql = "SELECT count(*) as count_t FROM users WHERE lower(name) LIKE '%$key%' AND user_id IN(".$user_str.")";
     $result = $this->_db->query($sql)->results();
    return $result;
  }
  public function get_user_dist_for_search($key_search){
    $sql="SELECT u.name,u.profile_picture,u.user_id from users as u join user_role as r on u.user_id=r.user_id WHERE r.role=2 AND u.status = 1 and lower(u.name) LIKE '%$key_search%'  order by u.name asc";
      $result_user=$this->_db->query($sql)->results();
      
      if(empty($result_user)){
         return 0;
      }
      else{
        return $result_user;
      }
  }
  public function get_user_lead_team($user_id,$key_search){
      $user_id_tl=$this->get_team_leader_for_task($user_id);
       // print_r($user_id_tl);die;
      $user_id_ts='';
      foreach ($user_id_tl as $key => $value) {

        $user_id_ts.=",'".$value->user_id_l2."'";
      }
       // print_r($user_id_ts);die;
      $user_id_ts=ltrim($user_id_ts,',');
  
      $sql="SELECT u.name,u.profile_picture,u.user_id from users as u join user_role as r on u.user_id=r.user_id WHERE r.role=3 and u.status = 1 and lower(u.name) LIKE '%$key_search%' and u.user_id in($user_id_ts)  order by u.name asc";
      $result_user=$this->_db->query($sql)->results() ;
       // print_r($result_user);die;
      if(empty($result_user)){
         return 0;
      }
      else{
        return $result_user;
      }
  }
  public function get_team_leader_for_task($user_id){
  
      $sql_dist="SELECT b.user_id_l2 from user_distributor_mapping as b where b.user_id_distributor=?";
      $result_dist= $this->_db->query($sql_dist,array($user_id))->results();
      // print_r($result_dist);die;
      if(empty($result_dist))
      {
         return 0;
      }
      else
      { 
           
        return $result_dist;
         
      }

  }//function ends here

    public function get_users_team($user_id,$user_role){

        if($user_role == 1){

            $sql1    = "SELECT us.*, ur.role FROM users as us JOIN user_role as ur on us.user_id = ur.user_id WHERE  ur.role = 2 ";
            $result1 = $this->_db->query($sql1)->results();
            
            if(empty($result1)){
               return 0;
            }else{   
              //print_r($result1); die();
              return $result1; 
            }

        }elseif($user_role == 2){

            $sql2    = "SELECT us.*, ur.role FROM users as us join user_distributor_mapping as udm on us.user_id = udm.user_id_l2 JOIN user_role as ur on us.user_id = ur.user_id WHERE udm.user_id_distributor = ?";
            $result2 = $this->_db->query($sql2,array($user_id))->results();

            if(empty($result2)){
               return 0;
            }else{   
              return $result2; 
            }

        }elseif($user_role == 3){

            $sql3    = "SELECT us.*, ur.role FROM users as us JOIN l1_l2_mapping as lm on us.user_id = lm.user_id_l1 JOIN user_role as ur on us.user_id = ur.user_id WHERE lm.user_id_l2 = ?";
            $result3 = $this->_db->query($sql3,array($user_id))->results();

            if(empty($result3)){
               return 0;
            }else{   
              return $result3; 
            }
        }

    }  // close function get users team.

    public function get_tasks_user($user_id){

      $sql    = "SELECT count(task_id) as task FROM tasks WHERE assigned_user_id = ? AND DATE(created_date) = CURDATE()";
      $result = $this->_db->query($sql,array($user_id))->results();

      return $result;
    }

    public function get_cases_user($user_id){

      $sql    = "SELECT count(case_id) as cases FROM cases WHERE assigned_user_id = ?";
      $result = $this->_db->query($sql,array($user_id))->results();

      return $result;
    }

    public function get_customers_user($user_id){

      $sql = "SELECT count(id) as customer FROM user_customer_mapping WHERE user_id = ?";
      $result = $this->_db->query($sql,array($user_id))->results();

      return $result;
    }

    public function change_role($user_id,$user_role,$user_id_dist){

      if($user_role == 4){
        $sql1    = "UPDATE user_role SET role = 3 WHERE user_id = ?";
        $result1 = $this->_db->query($sql1,array($user_id));
        //print_r($result1);  

        $sql2    = "DELETE FROM l1_l2_mapping WHERE user_id_l1 = ?";
        $result2 = $this->_db->query($sql2,array($user_id));  
       // print_r($result2);

        $sql3    = "INSERT INTO user_distributor_mapping(user_id_l2,user_id_distributor) VALUES('$user_id','$user_id_dist')";
        $result3 = $this->_db->query($sql3,array($user_id));  
//        print_r($result3);

        if($result1->_error){
         return 0;
        }   
        else{
         return 1;
        }

      } // close if 

      

    } // close function change role

    public function help_and_support($user_id,$query){

      $sql = "INSERT INTO contact_us(query,created_user_id) VALUES(?,?)";
      $result = $this->_db->query($sql,array($query,$user_id));

      if($result->_error)
      {
        return 0;
      }
      else
      {
        return 1;
      }


    }  // close function help and support

    public function load_all_task($user_id){

      $sql = "SELECT task_id,creator_user_id,assigned_user_id,customer_id,created_date,status FROM tasks WHERE creator_user_id = '$user_id'";
      $result = $this->_db->query($sql)->results();

      // print_r($result); die();

      return $result;
    }

    public function load_all_case($user_id){

      $sql = "SELECT case_id,case_name,creator_user_id,assigned_user_id,customer_id,created_date,status FROM cases WHERE creator_user_id = '$user_id'";
      $result = $this->_db->query($sql)->results();

      // print_r($result); die();
      return $result;
    }

    public function load_all_call($user_id){

      $sql = "SELECT ca.user_id,ca.call_id,cd.from_number,cd.to_number,ca.customer_id,cd.duration,ca.status FROM calls as ca JOIN call_details as cd on ca.call_id = cd.call_id WHERE ca.user_id = '$user_id'";

      $result = $this->_db->query($sql)->results();

      // print_r($result); die();
      return $result;

    }

    public function get_members_count($user_id,$user_role){

      if($user_role == 2){
            $sql = "SELECT count(id) as members FROM user_distributor_mapping WHERE user_id_distributor = '$user_id'";
            $result = $this->_db->query($sql)->results();
            return $result[0]->members;
      }
      elseif($user_role  = 3){

            $sql = "SELECT count(id) as members FROM l1_l2_mapping WHERE user_id_l2 ='$user_id'";
            $result = $this->_db->query($sql)->results();
            return $result[0]->members;
      }

    } // close function get_members_count

    public function reassign_multiple_agent($user_id_team_lead,$user_id_agents){

      foreach ($user_id_agents as $key => $value) {
          
        $sql1 = "SELECT id FROM l1_l2_mapping WHERE user_id_l1 = '$value'";
        $result = $this->_db->query($sql1)->results();
        // print_r($result);

        if(empty($result)){

          $sql2 = "INSERT INTO l1_l2_mapping(user_id_l1,user_id_l2) VALUES(?,?)";
          $result2 = $this->_db->query($sql2,array($value,$user_id_team_lead));

        }
        else{

          $sql3 = "UPDATE l1_l2_mapping SET user_id_l2 = '$user_id_team_lead' WHERE user_id_l1 = '$value'";
          $result3 = $this->_db->query($sql3);
          // print_r($result3);
        }

      }  // close foreach

      return 1;

    } // close functin reassign

    public function get_distributor_id($user_id){

      $sql = "SELECT dist_id FROM distributor_mapping WHERE user_id = '$user_id'";
      $result = $this->_db->query($sql)->results();
      // print_r($result); die();

      return $result[0]->dist_id; 
    }

    public function get_ogid($user_id){
        
        $sql = "SELECT og_id from users WHERE user_id = '$user_id'";
        $result = $this->_db->query($sql)->results();

        if(empty($result)){
          return 0;
        }else{
          return $result[0]->og_id;
        }

    }  // close function get ogid

    public function change_ed_id($user_id,$ed_id){

      $sql = "UPDATE users SET og_id = '$ed_id' WHERE user_id = '$user_id'";
      $result = $this->_db->query($sql);

      if($result->_error){
        return 0;
      }else{
        return 1;
      }


    }

     
}  // class close
?>