 <?php 
  $path_init = $_SERVER['DOCUMENT_ROOT']."/crm/resources/init.php";
include_once($path_init);

 ?>

   <link href="<?php echo ROOT_URI; ?>/resources/lib/select2/select2.min.css" rel="stylesheet" />
   <link href="<?php echo ROOT_URI; ?>/assets/css/team.css" rel="stylesheet" type="text/css">
  <script src="<?php echo ROOT_URI; ?>/resources/lib/select2/select2.min.js"></script>
   <script src="<?php echo ROOT_URI; ?>/resources/lib/time_ago/tinyAgo.min.js"></script>
  <link href="<?php echo ROOT_URI; ?>/resources/lib/timepicker/mdtimepicker.css" rel="stylesheet" type="text/css">
   <script src="<?php echo ROOT_URI; ?>/assets/js/rating.js" type="text/javascript"></script> 

  

<!-- right side main body starts -->
<div class="col-md-10 col-lg-10 col-sm-10 col-xs-10 col-lg-offset-2 col-md-offset-2 col-sm-offset-2 col-xs-offset-2 no-lr-pad back-color-grey">
	<!-- main section starts here contains upper part and lower part of task page, task detail section is outside this section -->
	<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad " id="main-section-task">
   <!-- for upper part -->
	
	    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 back-color-white padding-top-13 border-bottom padding-left-40">
	        <p class="font-size-24 float-inline"><b>My Customers</b></p>

          <!-- <button class="btn brand-btn margin-left-10" id="open_promotions_modal"><span class="margin-left-10 margin-right-10">Promotions</span></button> -->
          <a class="btn brand-btn margin-left-10 a-deco width-auto" href="<?php echo ROOT_URI; ?>/resources/views/team/sidebar.php?panel=7"><span class="margin-left-10 margin-right-10">Customer Signup</span></a>


	        
	        <div class="pull-right margin-top-8">
	             <button class="btn btn-default brand-btn-white float-inline  add-mem-btn1 width-auto margin-top--10 color-brand" id="assign_multiple_customer"><b>Assign/Re-assign Customer</b></button>


	        	
	        	<!-- <div class="dropdown float-inline margin-left-25 margin-top--10">
	                <button class="btn btn-default brand-btn-white add-mem-btn1 btn-drop-dwn width-107  dropdown-toggle" type="button" data-toggle="dropdown" id="drop-d-action-task"><img class="" src="<?php// echo ROOT_URI; ?>/assets/img/assets/filter.svg"> <span class="padding-left-5">Filters</span> -->
	                  
	                <!-- <span class="caret"></span></button> -->
	                <!-- <ul class="dropdown-menu pull-right">
	                  <li>
	                  	<a class="drop-sub-chl " >
		                  	<div class="filt-chk checkbox margin-top-bot-2">
							  <label><input type="checkbox" value=""><span class="margin-left-10">New</span></label>
							</div>
	                  	</a>
	                  </li>
	                   <li>
	                  	<a class="drop-sub-chl " >
		                  	<div class="filt-chk checkbox margin-top-bot-2">
							  <label><input type="checkbox" value=""><span class="margin-left-10">Open</span></label>
							</div>
	                  	</a>
	                  </li>
	                   <li>
	                  	<a class="drop-sub-chl " >
		                  	<div class="filt-chk checkbox margin-top-bot-2">
							  <label><input type="checkbox" value=""><span class="margin-left-10">Closed</span></label>
							</div>
	                  	</a>
	                  </li>
	                </ul> -->
	              <!-- </div> -->
	        </div>
	    </div>
	   <!-- upper part ends -->
     <div class="form-group col-md-6 col-lg-6 col-sm-6 col-xs-6 margin-top-20 padding-left-40" id="assign_div">
                    
                      <select class="form-control tag-for-cust js-data-select-ajax-assign  width-100" name="tags_for_assign[]" id="customer_display">
                      </select>
     </div>
     <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6 padding-left-40 padding-right-40" >


      <H2 class="pull-right"><img src="" id="paste_phone_icon" width="40"><span id="paste_phone_number_customer_screen" class="margin-top-10"></span></h2>
    </div>
	   <!-- lower part/main part starts here -->
	   <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 padding-left-40 padding-right-40" id="append_customer">
	        <!-- items tiles starts here  -->
	         
	        <!-- items tiles starts here  -->

	   </div>
     <a class="comment_loader_message margin-top-10 margin-bottom-10 col-sm-12 col-xs-12 col-md-12 col-lg-12 a-deco-grey text-center font-size-12"><u>Load More</u></a>
	   <!-- lower part/main part ends here -->
	</div>
	<!-- main section ends here -->
   <!-- task detail section starts from here -->
   	   <!-- task detail section starts from here -->
    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 margin-top-10 padding-right-0 display-none" id="task-detail-section">
      <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6 no-lr-pad">
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
          <button class="btn btn-default back-btn-task width-auto" id="back-btn-task"> <img class="" src="<?php echo ROOT_URI; ?>/assets/img/assets/back.svg"><span class="margin-left-10">Back To My Customers</span></button>
        </div>
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad task-detail-item-tiles-mytask  box-shadow1 margin-top-15 back-color-white" id="cust_detail">
          
        </div>
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 task-detail-item-tiles-mytask  box-shadow1 margin-top-15 back-color-white">
          <h4 class="margin-bottom--10"><b>Customer Other Information</b></h4><hr class="margin-bottom-0">
          <h4 class="float-inline margin-left-10" style="line-height: 35px;">
              <small>Credit Days</small><br><b><span id="credit_days_c"></span></b>
          </h4>
          <h4 class="float-inline margin-left-20" style="line-height: 35px;">
              <small>Credit Limit</small><br><b>&#x20a8 <span id="credit_limit_c"></span></b>
          </h4>
          <h4 class="float-inline margin-left-20" style="line-height: 35px;">
              <small>Total Outstandings</small><br><b><span id="credit_outstanding_c"></span></b>
          </h4>
          <h4 class="float-inline margin-left-20" style="line-height: 35px;">
              <small>Remaining Credit Limit</small><br><b>&#x20a8 <span id="credit_re_limit_c"></span></b>
          </h4>
          
        </div>
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad task-detail-item-tiles-mytask  box-shadow1 margin-top-15 back-color-white">
          <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 border-bottom">
            <p class="font-size-18 margin-top-15"><b>Last 5 Orders</b></p>
            <!-- <a class="color-grey-1 pull-right margin-top--30" >See All <img class="margin-right-8  " src="<?php echo ROOT_URI; ?>/assets/img/assets/see-all.svg"></a> -->
          </div>
          <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 border-bottom">
            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 margin-top-10">
            <div class="dropdown">
              <p class="dropdown-toggle a-deco" style="position: initial;" type="button" id="menu1" data-toggle="dropdown">Last 5 orders
              <span class="caret"></span></p>
              <ul class="dropdown-menu" role="menu" style="width: 100%;" aria-labelledby="menu1" id="append_last_5_orders">
                
              </ul>
            </div>
            </div>
          </div>
          <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 border-bottom pad-10" id="last_5_order_details_items">                           
          </div>
        </div>
        
        <!-- 20 Most Ordered Companies-->
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad task-detail-item-tiles-mytask  box-shadow1 margin-top-15 back-color-white">
          <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 border-bottom">
            <p class="font-size-18 margin-top-15"><b>20 Most Ordered Companies</b></p>
            <!-- <a href="" class="color-grey-1 pull-right margin-top--30">See All <img class="margin-right-8  " src="<?php echo ROOT_URI; ?>/assets/img/assets/see-all.svg"></a> -->
          </div>
           
          <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 border-bottom margin-top-10 padding-bottom-10" id="20_most_ordered">
          </div>
        </div>
        <!-- Call History With Customer -->
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad task-detail-item-tiles-mytask  box-shadow1 margin-top-15 back-color-white margin-bottom-20">
          <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 border-bottom ">
            <p class="font-size-18 margin-top-15"><b>Call History With Customer</b></p>
            <!-- <a href="" class="color-grey-1 pull-right margin-top--30">See All <img class="margin-right-8  " src="<?php echo ROOT_URI; ?>/assets/img/assets/see-all.svg"></a> -->
          </div>
         
          <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 border-bottom">
              <table class="table border-bottom">   
              <tbody id="body_call_display">
              </tbody>
            </table> 
          </div>
        </div>
      </div>
      <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6 padding-right-0">
        
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 back-color-white main-call-section margin-top--10 no-lr-pad">
          <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 back-color-brand height-48 ">
            <p class="font-size-12 color-white margin-top-13 ">Click on call button to connect with customer</p>
          </div>
          <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 margin-top-30">
            <div class="col-md-10 col-lg-10 col-sm-10 col-xs-10">
              <p class="color-grey"><b><span id="cust_name_call_panel"></span></b><br><br></p>
            </div>

              <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 " id="">
               <p class="margin-bottom-10 text-bold">Last Contacted number:  <span id="last_contacted_number"></span><br> </p>
               <p class="margin-bottom-10 text-bold"><span class="float-inline ">CALL ON: </span>
                  <input type="text" class="float-inline margin-left-10" name="" id="cust_ph_call_panel" val=""  placeholder="Calling Number">
              
                  <button type="button" class="btn btn-default brand-btn float-inline add-mem-btn1 call-customer margin-left-25 margin-top--5">Call</button>

                   <button class="btn btn-default brand-btn pull-left add-mem-btn1 width-auto margin-top--5 margin-left-10 place_order" data-dist_id="" data-cust_erpid="" data-cust_erpcode="" data-user_login="" data-user_pwd="" >Place Order</button>
            
               </p>
              <!-- <p class="font-size-12 margin-top-13 ">Call is being connected</p> -->
              </div>

          </div>
          <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 back-color-brand height-48 display-none margin-top-10" id="connected_msg">
            <p class="font-size-12 color-white margin-top-13 ">Call is being connected</p>
          </div>
          <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 margin-top-30">
            <div class="col-md-4 col-lg-4 col-sm-4 col-xs-4">
              <!-- <div class="dropdown">
                      <button class="btn btn-default brand-btn-white add-mem-btn1 btn-drop-dwn dropdown-toggle" type="button" data-toggle="dropdown" id="drop-d-action-task1">Select Action
                        <img class="margin-left-10" src="<?php echo ROOT_URI; ?>/assets/img/assets/dropdown-arrow.svg">
                      </button>
                      <ul class="dropdown-menu pull-right">
                        <li ><a class="drop-sub-chl re-assign-pop" data-toggle="modal" data-target="#reassign-modal">Re-assign</a></li>
                        <li ><a class="drop-sub-chl" data-toggle="modal" data-target="#followup-modal" >Follow Up</a></li>
                        <li ><a class="drop-sub-chl" data-toggle="modal" data-target="#closetask-modal"  >Close</a></li>
                      </ul>
                  </div> -->
            </div>
            
            <div class="col-md-4 col-lg-4 col-sm-4 col-xs-4">
             
            </div>
            <div class="col-md-4 col-lg-4 col-sm-4 col-xs-4 text-right">
              <button type="button" class="btn btn-default brand-btn-white add-mem-btn1 width-90 display-none"  id="call_again">Call Again</button>
            </div>
          </div>
          <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad margin-top-10 remark_height_modal" style="max-height: 150px !important;" id="remark_call">
                  
                </div>
          <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 margin-top-30">
              
              <div class="input-group search-input-grp padding-bottom-10">
                        <textarea type="text" class="resize-none form-control remark-add-box remark_text" rows="2" placeholder="Write something" ></textarea>
                        <div class="input-group-btn">
                          <button class="btn btn-default add_remark_call" >
                            <img class="margin-right-8" src="<?php echo ROOT_URI; ?>/assets/img/assets/add-remark.svg" height="40">
                          </button>
                        </div>
              </div>
         
          </div>
           <!-- 20 most ordered products -->
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad task-detail-item-tiles-mytask  box-shadow1 back-color-grey padding-top-10">

          <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 border-bottom back-color-white">
            <p class="font-size-18 margin-top-15"><b>20 Most Ordered Products</b></p>
            <!-- <a href="" class="color-grey-1 pull-right margin-top--30">See All <img class="margin-right-8  " src="<?php echo ROOT_URI; ?>/assets/img/assets/see-all.svg"></a> -->
          </div>
         
          <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 border-bottom back-color-white">
              <table class="table border-bottom">   
              <tbody id="20_most_ordered_product">
              </tbody>
            </table> 
          </div>
        </div>
        </div>


      </div>
    </div>
    <!-- task detail section ends here -->

   <!-- task detail section ends  here -->
  
   <!-- modals -->
   
     <div id="invoice_modal" class="modal fade" role="dialog">
        <div class="modal-dialog margin-top-10 modal-lg">

          <!-- Modal content-->
          <div class="modal-content col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
            <div class="modal-header col-md-12 col-lg-12 col-sm-12 col-xs-12 ">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <p class="font-size-20 margin-left-10"><b>Total Outstandings (&#x20a8 <span class="amount_modal"></span>)</b></p>
            </div>

            <div class="modal-body col-md-12 col-lg-12 col-sm-12 col-xs-12" style="max-height: 75vh;overflow: auto;">
                <table class="table table-condensed text-left">
                  <thead>
                    <tr>
                      <th class="text-left">Doc Type/No</th>
                      <th>Doc Date</th>
                      <th>Invoice Amount</th>
                      <th>Paid Amount</th>
                      <th>Outstanding Amount</th>
                      <th>Days To Pay</th>
                    </tr>
                  </thead>
                  <tbody class="invoice_data">
                    
                  </tbody>
                </table>
            </div> 
        <div class="modal-footer col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
          <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 padding-left-30 padding-right-30">
              <button type="button" class="btn btn-default pull-right add-mem-btn1" data-dismiss="modal">Cancel</button>
              
          </div>
        </div>
      </div>

    </div>
  
   <!-- Modal -->
 </div>
   
  <div id="create_task_modal" class="modal fade" role="dialog">
    <div class="modal-dialog margin-top-70">

      <!-- Modal content-->
      <div class="modal-content col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
        <div class="modal-header col-md-12 col-lg-12 col-sm-12 col-xs-12 ">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <p class="font-size-20 margin-left-10"><b>Create New Task</b></p>
        </div>
        <div class="modal-body col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
          <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                <!-- <div class="form-group col-md-12 col-lg-12 col-sm-12 col-xs-12" >
                  <span class="font-size-12 color-grey-1" for="username">Username:</span>
                  <input type="text" name="username" class="form-control" id="username">
                </div> -->

                <div class="form-group col-md-12 col-lg-12 col-sm-12 col-xs-12">
                  <p class="font-size-16  margin-bottom--15" id="customer_name_task" data-customer_id_task="" for="customer_sel"></p> <br>
                </div>
                <div class="form-group col-md-12 col-lg-12 col-sm-12 col-xs-12">
                  <span class="font-size-12 color-grey-1"  for="lastName">Name</span>
                  <input type="text" name="task_name" class="form-control margin-top-5" placeholder="Enter task name" id="task_name">
                </div>
                <div class="form-group col-md-12 col-lg-12 col-sm-12 col-xs-12">
                   	<div class="checkbox">
					           <label class="color-grey"><input type="checkbox" class="assign-chk" value="me" id="check_emp_task">Assign to Myself</label>
					          </div> 
                </div>
               	<div class="form-group col-md-12 col-lg-12 col-sm-12 col-xs-12 select-ajax-assign" id="assign_div_task">
                  <p class="font-size-12 color-grey-1 margin-bottom--15"  for="customer_sel">Assign To</p><br>
					          <select class=" form-control tag-for-cust  js-data-select-ajax-assign width-100 agent_display" id="assigned_user_task" name="tags_for_assign[]">
                    </select>
                </div>
                
                <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
                	<span class="font-size-12 color-grey-1"  for="lastName">When</span>
                  	<input type="date" name="task_date" class="form-control margin-top-5" placeholder="Select date" id="task_date">       
                </div>
                <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
                	<!-- <input type="time" name="task_date" class="form-control margin-top-25" placeholder="Select date" id="task_date">        -->
               	<input name="task_date" class="form-control margin-top-25 back-color-white" placeholder="Select time" type="text" id="timepicker"/>
                </div>
          </div>
        </div>
        <div class="modal-footer col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
          <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 padding-left-30 padding-right-30">
              <button type="button" class="btn btn-default pull-left add-mem-btn1" data-dismiss="modal">Cancel</button>
              <button type="button" class="btn btn-default pull-right brand-btn add-mem-btn" data-user_id_task_modal=""  id="create_task_ajax">Create</button>
          </div>
        </div>
      </div>

    </div>
  </div>
   <!-- modals ends here -->
   <!-- Modal -->
   <div id="reassign-modal" class="modal fade" role="dialog">
     <div class="modal-dialog margin-top-70">
      <!-- Modal content-->
      <div class="modal-content col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
        <div class="modal-header col-md-12 col-lg-12 col-sm-12 col-xs-12 ">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <p class="font-size-20 margin-left-10"><b>Re-assign</b></p>
        </div>
        <div class="modal-body col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
          <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                   
               	<div class="form-group col-md-12 col-lg-12 col-sm-12 col-xs-12 select-ajax-assign">
                  <p class="font-size-12 color-grey-1 margin-bottom--15"  for="customer_sel">Assign To</p><br>
					          <select class=" form-control tag-for-cust  js-data-select-ajax-assign width-100 agent_display" id="user_id_assign" name="tags_for_assign[]">
                    </select>
                </div>
                                 
          </div>
        </div>
        <div class="modal-footer col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
          <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 padding-left-30 padding-right-30">
              <button type="button" class="btn btn-default pull-left add-mem-btn1" data-dismiss="modal">Cancel</button>
              <button type="button" class="btn btn-default pull-right brand-btn add-mem-btn width-88 confirm_assign_button" data-customer_id="" data-flag="" >Confirm</button>
          </div>
        </div>
      </div>

    </div>
  </div>
  <!-- modal1 ends here -->

  <!-- Modal -->
   <div id="check_incoming_call_modal" class="modal fade" role="dialog">
     <div class="modal-dialog margin-top-70">
      <!-- Modal content-->
      <div class="modal-content col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
        <div class="modal-header col-md-12 col-lg-12 col-sm-12 col-xs-12 ">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <p class="font-size-20 margin-left-10"><b>Incoming Call</b></p>
        </div>
        <div class="modal-body col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
          <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                   
                <div class="form-group col-md-12 col-lg-12 col-sm-12 col-xs-12 text-center">
                 <label for="calling_from" class="">Incoming Call From</label>
                 <p class="paste_customer_number"></p>
                </div>
                                 
          </div>
        </div>
        <div class="modal-footer col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
          <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 padding-left-30 padding-right-30">
              <button type="button" class="btn btn-default pull-left add-mem-btn1" data-dismiss="modal">Cancel</button>
          </div>
        </div>
      </div>

    </div>
  </div>
  <!-- modal1 ends here -->
    <!-- Modal -->
  <div id="rate_modal" class="modal fade" role="dialog">
    <div class="modal-dialog margin-top-70">

      <!-- Modal content-->
      <div class="modal-content col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
        <div class="modal-header col-md-12 col-lg-12 col-sm-12 col-xs-12 ">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <p class="font-size-20 margin-left-10"><b>Rate Customers</b></p>
        </div>
        <div class="modal-body col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
          <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 text-center">
                <p>Rating will help you to sort out customer easily</p>
                <div id="stars-existing" class=" starrr large-rating margin-top-25 "   data-rating='4'>
		        </div>
                 
          </div>
        </div>
        <div class="modal-footer col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
          <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 padding-left-30 padding-right-30">
              <button type="button" class="btn btn-default pull-left add-mem-btn1" data-dismiss="modal">Cancel</button>
              <button type="button" class="btn btn-default pull-right brand-btn add-mem-btn width-88 save_rating_button" data-customer_id="">Save</button>	
          </div>
        </div>
      </div>

    </div>
  </div>
  <!-- modal2 ends here -->

  <!-- create a new case modal  -->
            <div id="create_case_modal" class="modal fade" role="dialog">
              <div class="modal-dialog margin-top-70">

                <!-- Modal content-->
                <div class="modal-content col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
                  <div class="modal-header col-md-12 col-lg-12 col-sm-12 col-xs-12 ">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <p class="font-size-20 margin-left-10"><b>Create New Case</b></p>
                  </div>
                  <div class="modal-body col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                         
                          <div class="form-group col-md-12 col-lg-12 col-sm-12 col-xs-12">
                            <p class="font-size-12 color-grey-1 margin-bottom--15" id="customer_name_case" data-customer_id_case=""  for="customer_sel">Customer</p><br>
                              
                          </div>
                          <div class="form-group col-md-12 col-lg-12 col-sm-12 col-xs-12">
                            <span class="font-size-12 color-grey-1"  for="case">Case</span>
                            <input type="text" name="case" class="form-control margin-top-5" placeholder="Enter case detail" id="case_name">
                          </div>
                          <div class="form-group col-md-12 col-lg-12 col-sm-12 col-xs-12">
                              <div class="checkbox">
                                <label class="color-grey"><input type="checkbox" class="assign-chk" id="check_emp_case" value="me">Assign to Myself</label>
                              </div> 
                          </div>
                          <div class="form-group col-md-12 col-lg-12 col-sm-12 col-xs-12 select-ajax-assign" id="assign_div_case">
                             <p class="font-size-12 color-grey-1 margin-bottom--15"  for="customer_sel">Assign To</p><br>
                             <select class=" form-control tag-for-cust  js-data-select-ajax-assign width-100 agent_display" id="assigned_user_case" name="tags_for_assign[]">
                             </select>
                          </div> 
                    </div>
                  </div>
                  <div class="modal-footer col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 padding-left-30 padding-right-30">
                        <button type="button" class="btn btn-default pull-left add-mem-btn1" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-default pull-right brand-btn add-mem-btn" data-user_id_case_modal=""  id="create_case_ajax">Create</button>
                    </div>
                  </div>
                </div>

              </div>
           </div>

          <!-- create a new case modal ends here  -->

          <!-- Add Remark Modal -->

            <div id="add_remark-modal" class="modal fade" role="dialog">
              <div class="modal-dialog margin-top-70">

                <!-- Modal content-->
                <div class="modal-content col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
                  <div class="modal-header col-md-12 col-lg-12 col-sm-12 col-xs-12 ">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <p class="font-size-20 margin-left-10"><b>Add Remarks</b></p>
                  </div>
                  <div class="modal-body col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                          
                          <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad remark_height_modal" id="1234">
                          </div>

                          <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                            <div class="form-group"> 
                      <textarea class="form-control resize-none remark-comment" rows="4" id="comment" placeholder="Write your remarks" ></textarea>
                    </div>
                          </div>
                         
                    </div>
                  </div>
                  <div class="modal-footer col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 padding-left-30 padding-right-30">
                        <button type="button" class="btn btn-default pull-left add-mem-btn1" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-default pull-right brand-btn add-mem-btn" data-customer_id="" id="add_comment_cust">Add</button>
                    </div>
                  </div>
                </div>

              </div>
            </div>



          <!-- Add remark modal close -->
    <!-- Modal Start -->
  <div id="promotions_modal" class="modal fade" role="dialog">
    <div class="modal-dialog margin-top-70 modal-lg">

      <!-- Modal content-->
      <div class="modal-content col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
        <div class="modal-header col-md-12 col-lg-12 col-sm-12 col-xs-12 ">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <p class="font-size-20 margin-left-10"><b>Promotions</b></p>
        </div>
        <div class="modal-body col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">

              <div class="margin-bottom-20"> 

                <button class="btn btn-success pull-left margin-left-10" id="delete_promotions_csv">Delete Promotions</button>
                <button class="btn btn-success pull-right margin-right-10" id="add_promotions_csv">Add Promotions</button>
                <button class="btn btn-success pull-left display-none margin-left-10" id="delete_promotions_final">Delete Selected Promotions</button>
                
                <div class="display-none"><input type="file" name="filename" id="add_file_csv"></div>


              </div>

              <br>

              <div class="data_cont">
                <!--start row-->
                <div class="row no-lr-margin tbl">
              <div class="table-responsive ">
              <table class="table table-bordered back-color-white margin-bottom-0">
                  
                <thead>
                  <td class="delete_promotions_checkbox display-none"></td>
                  <td class="margin-right-10 margin-left-10 padding-right-10 back-color-brand color-white pad-10 font-size-12 box-shadow1 padding-left-5">Product Code</td>
                  <td class="margin-right-10 margin-left-10 padding-right-10 back-color-brand color-white pad-10 font-size-12 box-shadow1 padding-left-5">Product Name</td>
                  <td class="margin-right-10 margin-left-10 padding-right-10 back-color-brand color-white pad-10 font-size-12 box-shadow1 padding-left-5">Promotion Type</td>
                  <td class="margin-right-10 margin-left-10 padding-right-10 back-color-brand color-white pad-10 font-size-12 box-shadow1 padding-left-5">Scheme</td>
                  <td class="margin-right-10 margin-left-10 padding-right-10 back-color-brand color-white pad-10 font-size-12 box-shadow1 padding-left-5">Sales Incentive</td>
                  <td class="margin-right-10 margin-left-10 padding-right-10 back-color-brand color-white pad-10 font-size-12 box-shadow1 padding-left-5">Sales incentive Validity start date</td>
                  <td class="margin-right-10 margin-left-10 padding-right-10 back-color-brand color-white pad-10 font-size-12 box-shadow1 padding-left-5">Sales incentive Validity end date</td>
                  <!-- <td class="margin-right-10 margin-left-10 padding-right-10 back-color-brand color-white pad-10 font-size-12 box-shadow1 padding-left-5">Promotion end date </td> -->
                </thead>

                <tbody>

                  <?php for($i=0;$i<8;$i++){ ?>
                  <tr>
                    <td class="delete_promotions_checkbox display-none"><input class="checkbox_check" type="checkbox" name=""></td>
                    <td class="margin-left-10 margin-right-10 padding-left-5 padding-right-10 back-color-grey font-size-16">ED12345</td>
                    <td class="margin-left-10 margin-right-10 padding-left-5 padding-right-10 back-color-grey font-size-16">AIROSUCK YANKEURS </td>
                    <td class="margin-left-10 margin-right-10 padding-left-5 padding-right-10 back-color-grey font-size-16">Dump Product</td>
                    <td class="margin-left-10 margin-right-10 padding-left-5 padding-right-10 back-color-grey font-size-16">1 Carent HCG pregnancy card+3 Carent HCG pregnancy card+1 entair JK15 mesh nebulizer</td>
                    <td class="margin-left-10 margin-right-10 padding-left-5 padding-right-10 back-color-grey font-size-16">1% commission</td>
                    <td class="margin-left-10 margin-right-10 padding-left-5 padding-right-10 back-color-grey font-size-16">07-19</td>
                    <td class="margin-left-10 margin-right-10 padding-left-5 padding-right-10 back-color-grey font-size-16">08-19</td>
                    <!-- <td class="margin-left-10 margin-right-10 padding-left-5 padding-right-10 back-color-grey font-size-16">xxx</td> -->
                  </tr>

                <?php } ?>
                  
                </tbody>
 
              </table>

            </div>
          </div>
        </div>

            </div>          
        </div>
        <div class="modal-footer col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
          <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 padding-left-30 padding-right-30">
              <button type="button" class="btn btn-default pull-left add-mem-btn1" data-dismiss="modal">Cancel</button>
              <button type="button" class="btn btn-default pull-right brand-btn add-mem-btn" >Create</button>
          </div>
        </div>
      </div>

    </div>
  </div>
   <!-- modals ends here -->
 </div>
   <!-- modal for multiple customer selection -->
   <div id="multiple_cust_modal" class="modal fade" role="dialog">
              <div class="modal-dialog margin-top-70 modal-lg">

                <!-- Modal content-->
                <div class="modal-content col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
                  <div class="modal-header col-md-12 col-lg-12 col-sm-12 col-xs-12 ">
                    
                    <p class="font-size-20 margin-left-10"><b>Select Customer</b></p>
                  </div>
                  <div class="modal-body col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                          
                           
                          <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12" id="data_append_for_multiple">
                          
            
                          </div>
                         
                    </div>
                  </div>
                  <div class="modal-footer col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 padding-left-30 padding-right-30">
                      
                    </div>
                  </div>
                </div>

              </div>
            </div>
      <!-- modal ends for multiple cust -->



</div>
 <!-- right side main body ends -->
      
 <script src="<?php echo ROOT_URI; ?>/resources/lib/timepicker/mdtimepicker.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
 // $('[data-toggle="tooltip"]').tooltip();
  	// main ajax to load data
    var customer_id_search = '';

    var role = sessionStorage.getItem('role');
    var dynamic_multiple_customer = sessionStorage.getItem('dynamic_multiple_customer');
    if(dynamic_multiple_customer==1){
     // alert("test");
      $.ajax({
           url : "<?php echo ROOT_URI; ?>/resources/services/customer_api.php",
           type : 'POST',
           data : "{\n\"action\":\"get_multiple_customer\"\n}",

           processData: false,  // tell jQuery not to process the data
           contentType: false,  // tell jQuery not to set contentType 
           dataType: 'JSON',
                 success: function(response) {

                  if(response.response_code == 200){
                      var html_text = '';
                     response.data.forEach(function(entry){
                        html_text += ' <a  class="btn btn-default pull-left add-mem-btn1 width-auto select_multiple" data-id="'+entry.customer_value+'" data-dismiss="modal" style="height:auto;padding:30px;font-size:30px;margin:10px;background:#666;color:#fff;">'+entry.customer_name+'</a>'; 
                      });


                      $("#data_append_for_multiple").html(html_text);

                  }  
                  else{

                     
                  }
                  
                 }  //  close success 
                      
      }); //ajax close

      $('#multiple_cust_modal').modal({
                        backdrop: 'static',
                        keyboard: true, 
                        show: true
                }); 
    }
    var select_multiple = '';
    $(document).on('click','.select_multiple',function(){
       select_multiple = $(this).attr("data-id");
    });
    var paste_phone_number_customer_screen = sessionStorage.getItem('number_to_send_order');
    // toast_it(paste_phone_number_customer_screen);

    if(paste_phone_number_customer_screen){
        $('#paste_phone_number_customer_screen').html(paste_phone_number_customer_screen);       
        $('#paste_phone_icon').attr('src','<?php echo ROOT_URI; ?>/assets/img/assets/call-blue.svg');  
    }


    // if(role != 2){
    //   $('#assign_div').hide();
    // }

    display_customer(1,customer_id_search);
    var page_count = 1;
          //var comment_flag_load = 1;
          //var current_clicked_modal_content_id = 0;
          //var current_clicked_modal_content_type = 0;
          $(document).on('click', '.comment_loader_message', function () {
                
                  display_customer(++page_count,customer_id_search);
                  
                
          });


          $(document).on('click','#check_incoming_call_modal_open',function(){

              $('#check_incoming_call_modal').modal('show');
              $('.paste_customer_number').text('');

              $.ajax({
                 url : "<?php echo ROOT_URI; ?>/resources/services/customer_api.php",
                 type : 'POST',
                 data : "{\n\"user_id\":\""+sessionStorage.getItem('user_id')+"\",\n\"action\":\"get_customer_number\"\n}",

                 processData: false,  // tell jQuery not to process the data
                 contentType: false,  // tell jQuery not to set contentType 
                 dataType: 'JSON',
                       success: function(response) {

                        if(response.response_code == 200){

                          // toast_it("Request Dates Changed");
                          // setInterval(function(){ location.reload();}, 800);
                          $('.paste_customer_number').text(response.data);

                        }  //  close if response code 200
                        else{

                          $('.paste_customer_number').text('NA');

                        }
                        
                       }  //  close success 
                      
              }); //ajax close

          });  // close function 

   function display_customer(page_no,customer_id_search)
                {
      $.ajax({
                       url: "<?php echo ROOT_URI; ?>/resources/services/customer_api.php",
                       dataType :'json',
                       method : 'POST',
                       async:false,
                       data: '{\n\"action\":\"display_custm\",\n\"customer_id_search\":\"'+customer_id_search+'\",\n\"token\":\"'+sessionStorage.getItem("token")+'\",\n\"user_id\":\"'+sessionStorage.getItem('user_id')+'\",\n\"dist_id\":\"'+sessionStorage.getItem('dist_id')+'\",\n\"role\":'+sessionStorage.getItem('role')+',\n\"page_no\":'+page_no+'}',
                       }).success(function(resp){
                        console.log(resp);
                        if(resp.response_code==200)
                        {
                        $('.comment_loader_message').show();

                        if(!(resp.data).length){
                           toast_it("No more data to show");
                        }

                          
                                  var htmlText ="";

                                  for(i=0;i<resp.data.length;i++)
                                  {
                                  	 
                                    var d = new Date(resp.data[i].last_contacted);
                  var p=(ago(d.getTime()));
                  // alert(p);
                  var days = new Date('2018-10-27 10:48:55');
                    var n = d.getHours();
                    n1="";
                    if(n>12)
                    {
                      n1=(n-12)+" P.M";
                    }
                    else
                    {
                      n1=n+" A.M";
                    }
                    var clas='';
                    if(resp.data[i].is_task){


	                    if(resp.data[i].status_label=="EXPIRED")
	                    {
	                      clas='label-default';
	                    }
	                    else if(resp.data[i].status_label=="NEW")
	                    {
	                      clas="label-success";
	                    }
	                    else
	                    {
	                      clas='label-danger';
	                    }

	                }

                  var customer_blocked = '';
                  var Block_Reason = '';
                  var block_class = 'display-none';

                  if(resp.data[i].isBlocked == 0){
                     customer_blocked = 'Blocked';
                     Block_Reason = resp.data[i].Block_Reason;
                     block_class = '';
                  }

                  if(resp.data[i].last_order_placed){
                    var last_order_placed = resp.data[i].last_order_placed;
                  }else{
                    var last_order_placed = 'NA';
                  }

                  if(resp.data[i].order_frequency){
                    var order_frequency = resp.data[i].order_frequency+' times per week';
                  }else{
                    var order_frequency = 'NA';
                  }

                  if(resp.data[i].order_avg_price){
                    var order_avg_price = resp.data[i].order_avg_price;
                  }else{
                    var order_avg_price = 'NA';
                  }

                  if(resp.data[i].last_contacted){
                    var last_contacted = resp.data[i].last_contacted;
                  }else{
                    var last_contacted = 'NA';
                  }

                  if(resp.data[i].last_contacted_number){
                    var last_contacted_number = resp.data[i].last_contacted_number;
                  }else{
                    var last_contacted_number = 'NA';
                  }

                      htmlText+=
                      			    '<div class="back-color-white col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad main-item-for-tiles-mycustomer box-shadow1 margin-top-10">'+
							            '<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 border-bottom">'+
							            	'<div class="col-md-4 col-lg-4 col-sm-4 col-xs-4">'+
							            
								                '<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">'+
								                	'<div class="filt-chk checkbox   margin-top-24 float-inline">'+
														       '<label><input class="assign_checkbox" type="checkbox" value="'+resp.data[i].customer_id+'"></label>'+
														      '</div>'+
                                  '<div>'+
                                    '<p class="font-size-14 pull-right margin-right-10 float-inline text-danger '+block_class+'">'+customer_blocked+'<i class="margin-left-10 fa fa-info-circle " aria-hidden="true" data-original-title="'+Block_Reason+'" data-toggle="tooltip"></i></p>'+

								                    '<p class="font-size-16 margin-top-24 margin-right-8 margin-left-10 ellipsis" data-original-title="'+resp.data[i].customer_name+'" data-toggle="tooltip"><b>'+resp.data[i].customer_name+'</b></p>'+ 
                                  '</div>'+
								                '</div>'+
								                '<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 text-left ">'+
								                    '<p    class="  color-grey-1 margin-left-30 custm_p_add ellipsis"   data-original-title="'+resp.data[i].customer_address+'" data-toggle="tooltip"><i class="fa fa-map-marker" aria-hidden="true"></i> '+resp.data[i].customer_address+'</p>'+
								                '</div>'+

								            '</div>'+
								            
								            '<div class="col-md-8 col-lg-8 col-sm-8 col-xs-8">'+
								             //    '<div class="col-md-2 col-lg-2 col-sm-2 col-xs-2 no-lr-pad">'+
										           //  '<p class="font-size-12 color-grey-1 margin-top-25 text-center">Customer Since</p>'+
										           //  '<p class="font-size-14 margin-top--5 text-center"><b>'+resp.data[i].customer_since+'</b></p>'+
									            // '</div>'+
									            '<div class="col-md-4 col-lg-4 col-sm-4 col-xs-4 no-lr-pad">'+
										            '<p class="font-size-12 color-grey-1 margin-top-25 text-center">Avg. Order Size</p>'+
										            '<p class="font-size-14 margin-top--5 text-center"><b>'+order_avg_price+' </b></p>'+
									            '</div>'+
									            '<div class="col-md-3 col-lg-3 col-sm-3 col-xs-3 no-lr-pad">'+
										            '<p class="font-size-12 color-grey-1 margin-top-25 text-center">Order Frequency</p>'+
										            '<p class="font-size-14 margin-top--5 text-center"><b>'+order_frequency+'</b></p>'+
									           ' </div>'+
									            '<div class="col-md-5 col-lg-5 col-sm-5 col-xs-5 no-lr-pad">'+
										            '<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6 no-lr-pad">'+
											            '<p class="font-size-12 color-grey-1 margin-top-25 text-center">Last Contacted</p>'+
											            '<p class="font-size-14 margin-top--5 text-center"><b>'+last_contacted+'</b></p>'+
										            '</div>'+
										            '<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6 no-lr-pad">'+
											            '<p class="font-size-12 color-grey-1 margin-top-25 text-center">Last Order Placed</p>'+
											            '<p class="font-size-14 margin-top--5 text-center"><b>'+last_order_placed+'</b></p>'+
										            '</div>'+
										        '</div>'+
									        '</div>'+
							                 
							            '</div>'+
							           
							            '<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12   margin-top-10 ">'+
							            	'<div class="col-md-4 col-lg-4 col-sm-4 col-xs-4 margin-top-5">';

                                if(resp.data[i].assigned_user_id){

							                	  htmlText += '<div class="assign-my-task-div float-inline"><img src="<?php echo S3_BUCKET; ?>'+resp.data[i].assigned_user_profile_pic+'" class="float-inline user_assign-img"><p class="font-size-12 float-inline margin-top-6 margin-left-10 "><b>Assigned To '+resp.data[i].assigned_user_name+'</b></p></div>';
                                }else{

                                  htmlText += '<div class="assign-my-task-div float-inline resassign_modal_cust" data-customer_id="'+resp.data[i].customer_id+'"><p class="font-size-12 float-inline margin-top-6 margin-left-10 a-deco text-danger"><b>Assign Customer</b></p></div>';
                                }

							                	if(resp.data[i].is_task){
							                		htmlText+='<div class="label '+clas+' font-size-12 margin-left-30 margin-top-5 float-inline">'+resp.data[i].status_label+' Task for Today</div> <!--use classes label-default label-danger for other states of task-->';
							                	}
							                	
							                	if(resp.data[i].case_count!=0  ){
							                		htmlText+='<div class="label label-blue-case font-size-12 margin-left-30 margin-top-5 float-inline">'+resp.data[i].case_count+' Open Case</div>';
							                	}
							                	
							                	htmlText+='<!-- use class "assign-my-task-div-other" for assigned task to other than me -->'+
                          
							                '</div>'+

                              '<div class="col-md-2 col-lg-2 col-sm-2 col-xs-2 margin-top-0 text-center no-lr-pad ">'+
                                  '<button class="btn btn-success add-mem-btn1 width-btn-120 margin-top-10 place_order" data-last_contacted="'+last_contacted_number+'" customer-id="'+resp.data[i].customer_id+'" data-dist_id="'+resp.data[i].dist_id+'" data-cust_erpid="'+resp.data[i].cust_erpid+'" data-cust_erpcode="'+resp.data[i].cust_erpcode+'" data-user_login="'+resp.data[i].login_id+'" data-user_pwd="'+resp.data[i].user_pwd+'" >Place Order</button>'+
                              '</div>'+
						                                

							                '<div class="col-md-4 col-lg-4 col-sm-4 col-xs-4  margin-top-0 text-center no-lr-pad ">'+
								                '<div class="col-md-5 col-lg-5 col-sm-5 col-xs-5  margin-top-15 text-center ">'+
								                	'<div id="stars-existing1" class=" starrr small-rating disable-rating pull-left "   data-rating="'+resp.data[i].rating+'">'+
								                	'</div>'+
								                '</div>'+
							                	 '<div class="col-md-2 col-lg-2 col-sm-2 col-xs-2  margin-top-0 text-left padding-left-0">'+
									                 '<button class="btn btn-default btn-rem-img  margin-top-10 remarks_button_cust" data-customer_id="'+resp.data[i].customer_id+'"><img src="<?php echo ROOT_URI; ?>/assets/img/assets/help-support-blue.svg"></button>'+
									            '</div> '+
									            '<div class="col-md-5 col-lg-5 col-sm-5 col-xs-5  margin-top-2 text-right no-lr-pad ">'+
									                 '<button class="btn btn-default brand-btn add-mem-btn1 width-btn-120 margin-top-10 call-btn-task-tiles call_connect" data-last_contacted="'+last_contacted_number+'" customer-id="'+resp.data[i].customer_id+'" data-dist_id="'+resp.data[i].dist_id+'" data-cust_erpid="'+resp.data[i].cust_erpid+'" data-cust_erpcode="'+resp.data[i].cust_erpcode+'" data-user_login="'+resp.data[i].login_id+'" data-user_pwd="'+resp.data[i].user_pwd+'" >Details</button>'+
									           ' </div>'+
							                '</div>'+
						 
								            '<div class="col-md-2 col-lg-2 col-sm-2 col-xs-2 no-lr-pad  margin-top-8 text-right">'+
								                '<div class="dropdown">'+
								                '<button class="btn btn-default brand-btn-white add-mem-btn1 btn-drop-dwn width-90 dropdown-toggle" type="button" data-toggle="dropdown" id="drop-d-action-task">Action'+
								                  '<img class="margin-left-10" src="<?php echo ROOT_URI; ?>/assets/img/assets/dropdown-arrow.svg">'+
								                '<!-- <span class="caret"></span> --></button>'+
								                '<ul class="dropdown-menu pull-right">'+
								                  '<li ><a class="drop-sub-chl re-assign-pop resassign_modal_cust" data-customer_id="'+resp.data[i].customer_id+'" >Re-assign</a></li>'+
								                  '<li ><a class="drop-sub-chl rate_modal_customer" data-customer_id="'+resp.data[i].customer_id+'" >Rate This Customer</a></li>'+
								                  '<li ><a class="drop-sub-chl assign_task_customer" data-customer_name="'+resp.data[i].customer_name+'" data-customer_id="'+resp.data[i].customer_id+'" >Create Task</a></li>'+
								                  '<li ><a class="drop-sub-chl assign_case_customer" data-customer_name="'+resp.data[i].customer_name+'" data-customer_id="'+resp.data[i].customer_id+'" >Create Case</a></li>'+
								                  // '<li ><a class="drop-sub-chl">View Reports</a></li>'+
								                '</ul>'+
								              '</div>'+
								            '</div>'+
									        
							            '</div>'+		           

							        '</div>';


                                }
                                  if(page_no==1){
                                  $("#append_customer").html(""); 
                                  }

                                //alert(htmlText);
                                 $("#append_customer").append(htmlText); 
                                 bind_ratings();
                                 $('[data-toggle="tooltip"]').tooltip();
                         }
                          else
                         {
                          toast_it("NO DATA TO SHOW");
                          $('.comment_loader_message').hide();
                         }

                                      });
 		// main ajax ends

}

  
	$('.js-data-select-ajax').select2({
	       ajax: {
	            url: "<?php echo ROOT_URI; ?>/resources/services/tag_api.php",
	            dataType: 'json',
	            delay: 350,
	            data: function (params) {
	                return {
	                    q: params.term, // search term
	                      page: params.page || 1,
                        token: sessionStorage.getItem("token")
	                };
	            },
	            processResults: function (data, params) {
	                 params.page = params.page || 1;
	                return {
	                    results: data.results,
	                    pagination: {
	                      more: (params.page * 10) < data.total_count
	                    }

	                };
	            },
	            cache: true
	        },
	        
	        tags: true,
	        placeholder: "Select Customer"
	});

   $('#customer_display').select2({
              ajax: {
                   url: "<?php echo ROOT_URI; ?>/resources/services/tag_api.php",
                   dataType: 'json',
                   delay: 350,
                   data: function (params) {
                       return {
                           q: params.term, // search term
                             page: params.page || 1,
                             token: sessionStorage.getItem("token"),
                             dist_id:sessionStorage.getItem('dist_id')
                       };
                   },
                   processResults: function (data, params) {
                        params.page = params.page || 1;
                       return {
                           results: data.results,
                           pagination: {
                             more: (params.page * 10) < data.total_count
                           }

                       };
                   },
                   cache: true
               },
               
               tags: true,
               placeholder: "Search Customer"
       });

  $('#customer_display').on('change', function() {
            var data = $("#customer_display option:selected").val();
             display_customer(1,data);
  });  

	$(document).on('click','.call-btn-task-tiles',function(){
	    $("#main-section-task").hide();
	    $("#task-detail-section").show();
	});
	$(document).on('click','#back-btn-task',function(){
	    $("#task-detail-section").hide();
	    $("#main-section-task").show();
      setInterval(function(){ location.reload();}, 100);
	    
	});

	  $(document).on('click', '.add_remark_call', function () {
        var comment=$('.remark_text').val();
        if(comment=='' || comment==null || comment==' '){
          toast_it("Remark can't be empty");
          return false;
        }
        var id=$('.add_remark_call').attr('data-customer_id');
        // console.log(id);
        var user_id=sessionStorage.getItem('user_id');
        //var profile_pic=sessionStorage.getItem('')
        //var flag=$('#add_comment').attr('flag');
         $.ajax({
              dataType :'json',
                       method : 'POST',
                       async:false,
                  url : "<?php echo ROOT_URI; ?>/resources/services/remark_api.php",
                  data: "{\n\"action\":\"add_remarks\",\n\"user_id\":\""+user_id+"\",\n\"entity_id\":\""+id+"\",\n\"entity_type\":\"2\",\n\"remark\":\""+comment+"\"\n}",}).success(function(resp) {
                    var html='';
                    if(resp.response_code==200)
                    {
                    
                       html+='<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">'+

                    '<div class="col-md-1 col-lg-1 col-sm-1 col-xs-1">'+
                      '<img src="<?php echo S3_BUCKET; ?>'+sessionStorage.getItem("profile_pic")+'" class="pull-right user_assign-img">'+
                '</div>'+
                    '<div class="col-md-11 col-lg-11 col-sm-11 col-xs-11 padding-left-0">'+
                      
                      '<p class="font-size-12">'+comment+'</p>'+
                      '<p class="font-size-12 color-grey">just now</p>'+
                      '<hr>'+
                    '</div>'+
                    
                  '</div>';
                }
                $('#remark_call').append(html);
                $('.remark_text').val('');
              });
        });

 		// remark ends
    var customer_id='';
		$(document).on('click', '#call_again', function () {
			  var text=$(this).html();
			  var call_id=$(this).attr('call_sid');

			 // call_id='da2ea84e4e47558e05014d30236e1cc5';
			  if(text=='Check Status')
			  {
			    $.ajax({
			              dataType :'json',
			                       method : 'POST',
			                       async:false,
			                  url : "<?php echo ROOT_URI; ?>/resources/services/call_history.php",
			                  data: '{\n\"action\":\"check_call",\n\"call_id\":\"'+call_id+'\"\n}',
			                       }).success(function(resp){
			                         if(resp.response_code==200)
			                    {
			                      $('#call_again').html("Call Again");
			                      $('#drop-d-action-task1').show();
			                      $('#connected_msg').html("CALL COMPLETED");
			                      $('#connected_msg').show();
			                    }
			                    else
			                    {
			                      $('#connected_msg').show();
			                    }

			  });
			}
			if(text=='Call Again')
			{
			  // var customer_id=$('#call_again').attr('customer_id');
			  // var customer_id=$('.call_connect').attr('customer_id');
        var calling_number = $('#cust_ph_call_panel').val();

			               $.ajax({
			                  dataType :'json',
			                  method : 'POST',
			                  async:false,
			                  url : "<?php echo ROOT_URI; ?>/resources/services/call_services.php",
			                  data: "{\n\"User_id\":\""+sessionStorage.getItem('user_id')+"\",\n\"token\":\""+sessionStorage.getItem('token')+"\",\n\"task_id\":\"0\",\n\"Customer_User_id\":\""+customer_id+"\",\n\"to\":\""+calling_number+"\"}",}).success(function(resp) {
			                    console.log(resp);
			                          if(resp.response_status==2){
                                   toast_it("Customer contact number is not available"); 
                                }
                                else if(resp.response_status==0){
                                   toast_it("Couldn't place call, please try again"); 
                                }
                                else if(resp.response_status==1){
                                  if(resp.data.Call.Status=="in-progress"){
                                      $('.call-customer').attr('disabled','disabled');
                                      toast_it("Call is being connected to :"+resp.data.Call.To);
                                      $('#call_again').attr("call_sid",resp.data.Call.Sid);
                                       
                                  }
                                }
			             });   
			}

		});

  
 		$(document).on('click', '.call_connect', function () {	
        // var task_id=$(this).attr("data-id-task");
        customer_id=$(this).attr("customer-id");
        // alert(customer_id);
        $('.add_remark_call').attr("data-customer_id",customer_id);
        $('#last_contacted_number').html($(this).attr('data-last_contacted'));
        $('.place_order').attr('data-user_login',$(this).attr('data-user_login'));
        $('.place_order').attr('data-cust_erpid',$(this).attr('data-cust_erpid'));
        $('.place_order').attr('data-cust_erpcode',$(this).attr('data-cust_erpcode'));
        $('.place_order').attr('data-dist_id',$(this).attr('data-dist_id'));
        $('.place_order').attr('data-user_pwd',$(this).attr('data-user_pwd'));
        $('#call_again').attr("customer-id",customer_id);
        $('#drop-d-action-task1').hide();
        $('#call_again').html("Check Status");
        $('#call_again').hide();
        $('#connected_msg').hide();
        $('.call-customer').removeAttr('disabled');
        var token=sessionStorage.getItem("token");
         $.ajax({
              dataType :'json',
                       method : 'POST',
                       async:false,
                  url : "<?php echo ROOT_URI; ?>/resources/services/remark_api.php",
                  data: "{\n\"action\":\"display_remarks_all\",\n\"entity_id\":\""+customer_id+"\",\n\"entity_type\":\"2\"\n}",}).success(function(resp) {
                    var html='';
                    if(resp.response_code==400)
                    {
                      flag=1;
                      html='';
                    }
                    else{
                      for(i=0;i<resp.data.length;i++){
                        var d = new Date(resp.data[i].created_date);
                        var p=(ago(d.getTime()));
                      html+='<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">'+

                    '<div class="col-md-1 col-lg-1 col-sm-1 col-xs-1">'+
                      '<img src="<?php echo S3_BUCKET; ?>'+resp.data[i].user_profile_pic+'" class="pull-right user_assign-img">'+
                    '</div>'+
                    '<div class="col-md-11 col-lg-11 col-sm-11 col-xs-11 padding-left-0">'+
                      
                      '<p class="font-size-12">'+resp.data[i].remark+'</p>'+
                      '<p class="font-size-12 color-grey">'+p+' ago</p>'+
                      '<hr>'+
                    '</div>'+
                    
                  '</div>';

                    }
                  }
                    $('#remark_call').html(html);
                  });


        $.ajax({
              dataType :'json',
                       method : 'POST',
                       async:false,
                  url : "<?php echo ROOT_URI; ?>/resources/services/details_task_page.php",
                  data: "{\n\"action\":\"20_most_ordered_company\",\n\"token\":\""+token+"\",\n\"customer_id\":"+customer_id+"\n}",
                       }).success(function(resp){

                        console.log(resp);
                        if(resp.response_code==200)
                    {
                      html='';
                      if(resp.data.length==0)
                      {
                        html='<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">'+
            '<p class="text-center font-size-12 margin-top-25"><b>No Company to Show</b></p>'+
          '</div>';
                      }
                      else
                      {
                      for(i=0;i<resp.data.length;i++){
                       html+='<div class="font-size-12 tag-box-for-comp float-inline">'+resp.data[i].Company+'</div>'
                      ;
                     }
                   }
                     $('#20_most_ordered').html(html);
                   }
                    
                      });   
                       $.ajax({
              dataType :'json',
                       method : 'POST',
                       async:false,
                  url : "<?php echo ROOT_URI; ?>/resources/services/details_task_page.php",
                  data: "{\n\"action\":\"20_most_ordered_product\",\n\"token\":\""+token+"\",\n\"customer_id\":"+customer_id+"\n}",
                       }).success(function(resp){

                        console.log(resp);
                        if(resp.response_code==200)
                    {
                      html='';
                       if(resp.data.length==0)
                      {
                        html='<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">'+
            '<p class="text-center font-size-12 margin-top-25"><b>No Products Ordered</b></p>'+
          '</div>';
                      }
                      else
                      {
                      for(i=0;i<resp.data.length;i++){
                       html+='<tr>'+
                  '<td><b>'+resp.data[i].ProductName+'</b></td>'+
                  '<td class="font-size-12">'+resp.data[i].Company+'</td>'+
                  '<td class="font-size-12">'+resp.data[i].Quntity+'</td>'+
                '</tr>';
                     }
                   }
                     $('#20_most_ordered_product').html(html);
                   }
                    
                      });

                     $.ajax({
                       dataType :'json',
                       method : 'POST',
                       async:false,
                       url : "<?php echo ROOT_URI; ?>/resources/services/details_task_page.php",
                       data: "{\n\"action\":\"last_5_order_details\",\n\"token\":\""+token+"\",\n\"customer_id\":"+customer_id+"\n}",
                       }).success(function(resp){
                      if(resp.response_code==200)
                      {
                          $('#last_5_order_details_items').html("");
                          var order = '';

                          order_list = resp.data;

                          order_list.forEach(function(entry){

                            order = order + '<li role="presentation margin-top-25"><a role="menuitem" tabindex="-1" class="a-deco order_list_click" data-order_id='+entry._OrderID+' data-order_date="'+entry._Date+'">Order ID : '+entry._OrderID+' - Order Date : '+entry._Date+'</a></li>';
                            $('#append_last_5_orders').html(order);
                          });

                            $('#last_5_order_details_items').html(order);

                      }                   
                            
                    });

                    $(document).on('click','.order_list_click',function(){

                      var order_id = $(this).attr('data-order_id');
                      var order_date = $(this).attr('data-order_date');

                      $.ajax({
                       dataType :'json',
                       method : 'POST',
                       async:false,
                       url : "<?php echo ROOT_URI; ?>/resources/services/details_task_page.php",
                       data: "{\n\"action\":\"last_5_order_items\",\n\"token\":\""+token+"\",\n\"order_id\":"+order_id+"\n,\n\"customer_id\":"+customer_id+"\n}",
                       }).success(function(resp){
                          if(resp.response_code==200)
                          {
                          $('#last_5_order_details_items').show(); 
                          html='<div class="font-size-14 margin-left-10 margin-bottom-10 text-bold">Order ID - '+order_id+' &emsp; Order Date - '+order_date+'</div>'+
                          '<table class="table border-bottom">'+
                              '<thead>'+
                                '<tr>'+
                                  '<th>ITEM</th>'+
                                  '<th>UNIT PRICE</th>'+
                                  '<th>QTY</th>'+
                                  '<th>TOTAL</th>'+
                                '</tr>'+
                              '</thead>'+
                              '<tbody >';
                          var order = '';

                          order_list = resp.data;

                          order_list.forEach(function(entry){
                           html+='<tr>'+
                          '<td><span class="color-grey font-size-12">'+entry.Manufacturer+'</span><br>'+entry.Product_Desc+'</td>'+
                          '<td>Rs.'+entry.PTR+'</td>'+
                          '<td>'+entry.Qty+'</td>'+
                          '<td><b>Rs.'+ entry.Total+'</b><br><span class="color-save">-('+entry.TotalSaving+'%) Rs.'+entry.Total+'</span></td>'+
                          '</tr>';
                           });  // close for each
                          html+='</tbody>'+
                          '</table>';
               
                         $('#last_5_order_details_items').html(html);
                       }  
                       else{
                         $('#last_5_order_details_items').show();
                         $('#last_5_order_details_items').html('<p class="text-center">No Items</p>');

                       }
                            
                       });   // close success


                    });    /// close on click order list


                        $.ajax({
                         dataType :'json',
                         method : 'POST',
                         async:false,
                         url : "<?php echo ROOT_URI; ?>/resources/services/details_task_page.php",
                         data: "{\n\"action\":\"customer_details\",\n\"token\":\""+token+"\",\n\"customer_id\":"+customer_id+"\n}",
                        }).success(function(resp){

                        //console.log(resp);
                        if(resp.response_code==200)
                        {
                           html='';
                           $('#cust_name_call_panel').html(resp.data[0].S_Name);
                          if(!resp.data[0].S_MobContact){
                            resp.data[0].S_MobContact='NA';
                          }
                          if(!resp.data[0].S_AvgPrice){
                            resp.data[0].S_AvgPrice='NA';
                          }
                          if(!resp.data[0].OrderFrq){
                            resp.data[0].OrderFrq='NA';
                          }else{
                            resp.data[0].OrderFrq=resp.data[0].OrderFrq+' times per week';
                          }

                           $('#cust_ph_call_panel').val(resp.data[0].S_Tel_No);
                           html+='<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">'+
                           '<p class="text-center font-size-24 margin-top-25"><b>'+resp.data[0].S_Name+'</b></p>'+
                           '</div>'+
                           '<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6 padding-right-5">'+
                                   
                                    '<p class="float-inline pull-right margin-top--2">'+resp.data[0].S_Email+'</p>'+
                                     '<img class="margin-right-8 float-inline pull-right" src="<?php echo ROOT_URI; ?>/assets/img/assets/email.svg">'+
                                '</div>'+
                                '<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6 padding-left-5">'+
                                    '<img class="margin-right-8 float-inline" src="<?php echo ROOT_URI; ?>/assets/img/assets/phone.svg">'+
                                    '<p class="float-inline margin-top--2">'+resp.data[0].S_Tel_No+'</p>'+
                                '</div>'+
                                '<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 margin-top-25">'+
                                  '<div class="col-md-4 col-lg-4 col-sm-4 col-xs-4 text-right">'+
                                      '<p class="font-size-12 color-grey">Customer Since</p>'+
                                      '<p class=""><b>'+resp.data[0].S_CreateDate+'</b></p>'+
                                  '</div>'+
                                  '<div class="col-md-4 col-lg-4 col-sm-4 col-xs-4 text-center">'+
                                      '<p class="font-size-12 color-grey">Avg. Order Price</p>'+
                                      '<p class=""><b>Rs. '+resp.data[0].S_AvgPrice+'</b></p>'+
                                  '</div>'+
                                  '<div class="col-md-4 col-lg-4 col-sm-4 col-xs-4">'+
                                      '<p class="font-size-12 color-grey">Order Frequency</p>'+
                                      '<p class=""><b>'+resp.data[0].OrderFrq+'</b></p>'+
                                  '</div>'+
                                '</div>';
                     
                            $('#cust_detail').html(html);
                            $('#credit_outstanding_c').html('<a data-customer_id="'+customer_id+'"  data-amount="'+resp.data[0].outstandingBalwithPDC+'" data-target="#invoice_modal" class="open_outstanding_modal"  data-toggle="modal">&#x20a8 '+resp.data[0].outstandingBalwithPDC+'</a>');
                            $('#credit_days_c').html(resp.data[0].creditDays);
                            $('#credit_re_limit_c').html(resp.data[0].remaining_creditLimit);
                            $('#credit_limit_c').html(resp.data[0].creditLimit);
                            
                        }
                    
                        }); 

                       $.ajax({
              dataType :'json',
                       method : 'POST',
                       async:false,
                  url : "<?php echo ROOT_URI; ?>/resources/services/call_history.php",
                  data: '{\n\"action\":\"call_history\",\n\"cust_id\":\"'+customer_id+'\",\n\"page_no\":1\n}',
                       }).success(function(resp){
                         if(resp.response_code==200)
                    {
                      html='';
                      for(i=0;i<resp.data.length;i++){
                       html+='<tr>'+
                  '<td><b>'+resp.data[i].start_date+'</b></td>'+
                  '<td class="font-size-12"><span class="color-grey">Time:</span> <b>'+resp.data[i].time+'</b></td>'+
                  '<td class="font-size-12"><span class="color-grey">Call Duration: </span> <b>'+resp.data[i].call_duration+'</b></td>'+
                '</tr>';
                     }
                     $('#body_call_display').html(html);
                   }
                    
                      });   
    
    $('.open_outstanding_modal').on("click",function(){
      var cust_id_out = $(this).attr("data-customer_id");
      $(".amount_modal").html($(this).attr("data-amount"));
      $.ajax({
              dataType :'json',
              method : 'POST',
              async:false,
              url : "<?php echo ROOT_URI; ?>/resources/services/customer_api.php",
              data: "{\n\"action\":\"get_invoice_detail\",\n\"dist_id\":\""+sessionStorage.getItem('dist_id')+"\",\n\"token\":\""+sessionStorage.getItem('token')+"\",\n\"cust_id\":\""+cust_id_out+"\"}",
              }).success(function(resp) {

                    var html_ele = '';

                    resp.data.forEach(function(entry){
                        // console.log(entry);
                        html_ele +='<tr>'+
                                      '<td class="padding-left-5">'+entry.InvoiceNo+'</td>'+
                                      '<td class="padding-left-5">'+entry.InvoiceDate+'</td>'+
                                      '<td class="padding-left-5">'+entry.TotalAmount+'</td>'+
                                      '<td class="padding-left-5">'+entry.Paid_Amount+'</td>'+
                                      '<td class="padding-left-5">'+entry.BalanceAmount+'</td>'+
                                      '<td class="padding-left-5">'+entry.Days_To_Pay+'</td>'+
                                    '</tr>'; 
                    });
                    $(".invoice_data").html(html_ele);
              }); 
         
             

       


    });
        
    $('.call-customer').on("click",function(){
       // alert(customer_id);return false;

            var calling_number = $('#cust_ph_call_panel').val();

            $.ajax({
              dataType :'json',
                       method : 'POST',
                       async:false,
                  url : "<?php echo ROOT_URI; ?>/resources/services/call_services.php",
                  data: "{\n\"User_id\":\""+sessionStorage.getItem('user_id')+"\",\n\"token\":\""+sessionStorage.getItem('token')+"\",\n\"task_id\":\""+0+"\",\n\"Customer_User_id\":\""+customer_id+"\",\n\"to\":\""+calling_number+"\"}",}).success(function(resp) {
                          
                          if(resp.response_status ==2){
                             toast_it("Customer contact number is not available"); 
                          }
                          else if(resp.response_status==0){
                             toast_it("Couldn't place call, please try again"); 
                          }
                          else if(resp.response_status==1){
                            if(resp.data.Call.Status=="in-progress"){
                                $('.call-customer').attr('disabled','disabled');
                                sessionStorage.setItem("number_to_send_order",calling_number);                                
                                toast_it("Call is being connected to :"+resp.data.Call.To);
                                $('#call_again').attr("call_sid",resp.data.Call.Sid);
                                // $('#call_again').show()
                            }
                          }
                      });   

            
          });
  });

    $(document).on('click','.place_order',function(){

     var user_loginid = $(this).attr('data-user_login');
     var user_pwd = $(this).attr('data-user_pwd');
     var stk_id = $(this).attr('data-dist_id');
     var cust_erpid = $(this).attr('data-cust_erpid');
     var cust_erpcode = $(this).attr('data-cust_erpcode');
     var user_id = sessionStorage.getItem("og_id");
     var number_order = sessionStorage.getItem("number_to_send_order");

     var url = "http://crm.enterodirect.com/login?email="+user_loginid+"&password="+user_pwd+"&distributor="+stk_id+"&customer_erpid="+cust_erpid+"&customer_erpcode="+cust_erpcode+"&crmuserid="+user_id+"&salesman_erpcode="+user_id+"&callerInfo="+number_order+"&customer_parameter="+select_multiple;

     console.log(url);
     window.open(url, '_blank'); 

    });  // close 

                   $('#check_emp_task').change(function() {
                      if ($(this).is(':checked'))
                      {
                          $('#assign_div_task :input').attr('disabled', true);
                      } 
                      else 
                      {
                          $('#assign_div_task :input').removeAttr('disabled');
                      }   
                   });

                   $('#check_emp_case').change(function() {
                      if ($(this).is(':checked'))
                      {
                          $('#assign_div_case :input').attr('disabled', true);
                      } 
                      else 
                      {
                          $('#assign_div_case :input').removeAttr('disabled');
                      }   
                   });


                   $(document).on('click', '#create_task_ajax', function () {
                          var user_id   =$('#customer_name_task').attr('data-customer_id_task');
                          var time      =$('#timepicker').val();
                          var task_name =$('#task_name').val();
                          var emp_id    ='';
                          if ($('#check_emp_task').is(':checked'))
                            {
                               emp_id=sessionStorage.getItem('user_id');
                            } 
                          else {
                              emp_id=$('#assigned_user_task').val();
                            } 
                          if(emp_id==''){
                             emp_id=sessionStorage.getItem('user_id');
                            // return false;
                          } 
                         // alert(emp_id);
                          if(emp_id==null){
                            emp_id=sessionStorage.getItem('user_id');
                            //return false;
                          } 
                          
                          var hours   = Number(time.match(/^(\d+)/)[1]);
                          var minutes = Number(time.match(/:(\d+)/)[1]);
                          var AMPM    = time.match(/\s(.*)$/)[1];
                          if(AMPM == "PM" && hours<12) hours = hours+12;
                          if(AMPM == "AM" && hours==12) hours = hours-12;
                          var sHours   = hours.toString();
                          var sMinutes = minutes.toString();
                          if(hours<10) sHours = "0" + sHours;
                          if(minutes<10) sMinutes = "0" + sMinutes;
                          var time=sHours + ":" + sMinutes+":"+"00";
                          
                          $.ajax({
                                dataType :'json',
                                         method : 'POST',
                                         async:false,
                                    url : "<?php echo ROOT_URI; ?>/resources/services/task_main.php",
                                    data: "{\n\"action\":\"add_task\",\n\"taskname\":\""+task_name+"\",\n\"emp_id\":\""+emp_id+"\",\n\"cust_id\":\""+user_id+"\",\n\"current_cust_id\":\""+sessionStorage.getItem('user_id')+"\",\n\"task_when\":\""+time+"\"}",}).success(function(resp) {
                                      toast_it(resp.response_message);
                                        }); 
                                    $('#create_task_modal').modal('toggle');
                                    setInterval(function(){ location.reload();}, 1000);

                  });

                  $('#timepicker').mdtimepicker();
                  $('#timepicker1').mdtimepicker();
                  $('#timepicker2').mdtimepicker();


                  $(document).on('click', '#create_case_ajax', function () {
                          var user_id   = $('#customer_name_case').attr('data-customer_id_case');
                          var case_name = $('#case_name').val();
                          

                           var emp_id    ='';
                          if ($('#check_emp_case').is(':checked'))
                            {
                               emp_id=sessionStorage.getItem('user_id');
                            } 
                          else {
                              emp_id=$('#assigned_user_case').val();
                            } 
                          if(emp_id==''){
                             emp_id=sessionStorage.getItem('user_id');
                            // return false;
                          } 
                         // alert(emp_id);
                          if(emp_id==null){
                            emp_id=sessionStorage.getItem('user_id');
                            //return false;
                          }   

                          $.ajax({
                                dataType :'json',
                                         method : 'POST',
                                         async:false,
                                    url : "<?php echo ROOT_URI; ?>/resources/services/case_main.php",
                                    data: "{\n\"action\":\"add_case\",\n\"casename\":\""+case_name+"\",\n\"empid\":\""+emp_id+"\",\n\"user\":\""+user_id+"\",\n\"current_empid\":\""+sessionStorage.getItem('user_id')+"\"}",}).success(function(resp) {
                                      toast_it(resp.response_message);
                                        }); 
                                    $('#create_case_modal').modal('toggle');
                                    setInterval(function(){ location.reload();}, 1000);

                  });

                    $('.agent_display').select2({
                          ajax: {
                                url: "<?php echo ROOT_URI; ?>/resources/services/display_all_customer.php",
                                dataType: 'json',
                                delay: 350,
                                data: function (params) {
                                    return {
                                        q: params.term, // search term
                                          page: params.page || 1,
                                          user_id:sessionStorage.getItem('user_id'),
                                          role:sessionStorage.getItem('role')
                                    };
                                },
                                processResults: function (data, params) {
                                     params.page = params.page || 1;
                                    return {
                                        results: data.results,
                                        pagination: {
                                          more: (params.page * 5) < data.total_count
                                        }

                                    };
                                },
                                cache: true
                            },
                            
                            tags: true,
                            placeholder: "Select team member"
                    });

                  $(document).on('click','.assign_task_customer',function(){

                    var customer_id_task   = $(this).attr('data-customer_id');
                    var customer_name_task   = $(this).attr('data-customer_name');

                    $('#customer_name_task').text(customer_name_task);

                    $('#customer_name_task').attr('data-customer_id_task',customer_id_task);
                    //toast_it(user_id_task);
                    $('#create_task_modal').modal('show');

                  });

                  $(document).on('click','.assign_case_customer',function(){

                    var customer_id_case   = $(this).attr('data-customer_id');
                    var customer_name_case   = $(this).attr('data-customer_name');

                    $('#customer_name_case').text(customer_name_case);

                    $('#customer_name_case').attr('data-customer_id_case',customer_id_case);

                    $('#create_case_modal').modal('show');   

                  });

                  $(document).on('click','.resassign_modal_cust',function(){

                    //toast_it("NEW");
                    var customer_id = $(this).attr('data-customer_id');
                    // toast_it(customer_id);
                    $('.confirm_assign_button').attr('data-customer_id',customer_id);
                    $('#reassign-modal').modal('show');
                    $('.confirm_assign_button').attr('data-flag',1);

                  });

                  $(document).on('click','.confirm_assign_button',function(){


                    if($(this).attr('data-flag')==1){
                         
                         var customer_id = $(this).attr('data-customer_id');
                         var user_id = $('#user_id_assign').val();
                         
                         assign_customer(customer_id,user_id,1);

                    }else if($(this).attr('data-flag')==2){

                         var jObject_selectedcustomers={};
                         var customer_id = {};
                         customer_id['customers'] = [];
                         var user_id = $('#user_id_assign').val();
                        

                         $('.assign_checkbox:checked').each(function(){        
                          var values = $(this).val();
                          customer_id['customers'].push(values);
                         });

                          if(!(customer_id['customers'].length)){
                            toast_it('Select Customer');
                            return false;

                           }

                         // customer_id.push(')');
                         jObject_selectedcustomers = JSON.stringify(customer_id);
                         
                         assign_customer(jObject_selectedcustomers,user_id,2);
                    }

                   
                    // toast_it(user_id);

                  }); // close on click assign 


                  function assign_customer(customer_id,user_id,assign_condition){

                    $.ajax({
                       url : "<?php echo ROOT_URI; ?>/resources/services/customer_api.php",
                       type : 'POST',
                       data : "{\n\"user_id\":\""+user_id+"\",\n\"assign_condition\":\""+assign_condition+"\",\n\"customer_id\":"+customer_id+",\n\"action\":\"assign_customer\"\n}",

                       processData: false,  // tell jQuery not to process the data
                       contentType: false,  // tell jQuery not to set contentType 
                       dataType: 'JSON',
                             success: function(response) {

                              if(response.response_code == 200){
                                toast_it("User Assigned");
                                setInterval(function(){ location.reload();}, 1200);
                              }
                              
                             }
                             // error: function() {  
                             //   toast_it("There was an error.");
                             // }
                       }); //ajax close

                  }  // close function 

                  $(document).on('click','.rate_modal_customer',function(){

                    var customer_id = $(this).attr('data-customer_id');
                    $('.save_rating_button').attr('data-customer_id',customer_id);
                    $('#rate_modal').modal('show');

                  });

                  $('#stars-existing').on('starrr:change', function(e, value){
                      $('.save_rating_button').attr('data-rating',value);
                  });

                  $(document).on('click','.save_rating_button',function(){

                     var rating = $(this).attr('data-rating');
                     var customer_id = $(this).attr('data-customer_id');
                    // toast_it(rating);

                    $.ajax({
                       url : "<?php echo ROOT_URI; ?>/resources/services/customer_api.php",
                       type : 'POST',
                       data : "{\n\"user_id\":\""+sessionStorage.getItem('user_id')+"\",\n\"rating\":\""+rating+"\",\n\"customer_id\":\""+customer_id+"\",\n\"action\":\"rate_customer\"\n}",

                       processData: false,  // tell jQuery not to process the data
                       contentType: false,  // tell jQuery not to set contentType 
                       dataType: 'JSON',
                             success: function(response) {

                              if(response.response_code == 200){
                                toast_it("Customer Rated");
                                setInterval(function(){ location.reload();}, 1200);
                              }
                              
                             }
                             // error: function() {  
                             //   toast_it("There was an error.");
                             // }
                       }); //ajax close
                    

                  });

                  $(document).on('click','.remarks_button_cust',function(){

                      var customer_id = $(this).attr('data-customer_id');
                      $('#add_comment_cust').attr('data-customer_id',customer_id);
                      $('#add_remark-modal').modal('show');

                      var entity_type=3;
                      var flag=0;
                       $.ajax({
                              dataType :'json',
                                       method : 'POST',
                                       async:false,
                                  url : "<?php echo ROOT_URI; ?>/resources/services/remark_api.php",
                                  data: "{\n\"action\":\"display_remarks\",\n\"entity_id\":\""+customer_id+"\",\n\"entity_type\":\""+entity_type+"\",\n\"page_no\":\"1\"\n}",}).success(function(resp) {
                                    var html='';
                                    if(resp.response_code==400)
                                    {
                                      flag=1;
                                      html='<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 margin-top-20 text-center margin-bottom-20"><p class="text-center">'+
                                      'No Remarks For This Task</p></div>';
                                    }
                                    else{

                                       for(i=0;i<resp.data.length;i++){
                                        var d = new Date(resp.data[i].created_date);
                                  var p=(ago(d.getTime()));
                                      html+='<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">'+

                                    '<div class="col-md-1 col-lg-1 col-sm-1 col-xs-1">'+
                                      '<img src="<?php echo S3_BUCKET; ?>'+resp.data[i].user_profile_pic+'" class="pull-right user_assign-img">'+
                                    '</div>'+
                                    '<div class="col-md-11 col-lg-11 col-sm-11 col-xs-11 padding-left-0">'+
                                      
                                      '<p class="font-size-12">'+resp.data[i].remark+'</p>'+
                                      '<p class="font-size-12 color-grey">'+p+' ago</p>'+
                                      '<hr>'+
                                    '</div>'+
                                    
                                  '</div>';
                                }
                                    }
                                    //+alert(html);
                                    $("#1234").html(html);
                                    $("#add_comment_cust").attr("flag",flag); 

                                      });

                  });  // on click close

                  $(document).on('click','#add_comment_cust',function(){

                      var comment=$('.remark-comment').val();
                      if(comment=='' || comment==null || comment==' '){
                        toast_it("Remark can't be empty");
                        return false;
                      }

                      var id = $(this).attr('data-customer_id');
                      var flag=$('#add_comment_cust').attr('flag');

                      $.ajax({
                        dataType :'json',
                                 method : 'POST',
                                 async:false,
                            url : "<?php echo ROOT_URI; ?>/resources/services/remark_api.php",
                            data: "{\n\"action\":\"add_remarks\",\n\"user_id\":\""+sessionStorage.getItem('user_id')+"\",\n\"entity_id\":\""+id+"\",\n\"entity_type\":\"3\",\n\"remark\":\""+comment+"\"\n}",}).success(function(resp) {
                              var html='';
                              if(resp.response_code==200)
                              {
                                 html+='<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">'+

                              '<div class="col-md-1 col-lg-1 col-sm-1 col-xs-1">'+
                                '<img src="https://s3.ap-south-1.amazonaws.com/entero-crm/'+sessionStorage.getItem('profile_pic')+'" class="pull-right user_assign-img">'+
                              '</div>'+
                              '<div class="col-md-11 col-lg-11 col-sm-11 col-xs-11 padding-left-0">'+
                                
                                '<p class="font-size-12">'+comment+'</p>'+
                                '<p class="font-size-12 color-grey">just now</p>'+
                                '<hr>'+
                              '</div>'+
                              
                            '</div>';
                            if(flag==1)
                            {
                                $("#1234").html(html); 
                                $('.remark-comment').val('');
                                 $("#add_comment_cust").attr("flag",flag);
                            }
                             else
                             {
                                 $("#1234").append(html); 
                                $('.remark-comment').val('');
                            }
                          }
                          else
                              {
                                $('.remark-comment').val('');
                                toast_it(resp.response_message);
                                $('#add_remark-modal').modal('toggle');


                              }
                      }); 

                  });  // on click close

                  $(document).on('click','#assign_multiple_customer',function(){

                     
                      $('#reassign-modal').modal('show');

                      $('.confirm_assign_button').attr('data-flag',2);

                      //toast_it(final);

                  });


                  $(document).on('click','#open_promotions_modal',function(){

                      $('#promotions_modal').modal('show');

                  });


                  $(document).on('click','#add_promotions_csv',function(){

                      $('#add_file_csv').click();

                  });

                  $(document).on('change','#add_file_csv',function(){
                          
                        var fileInput = document.getElementById('add_file_csv');
                        var filePath = fileInput.value;
                        var allowedExtensions = /(\.csv)$/i;

                        if(!allowedExtensions.exec(filePath)){
                            toast_it("Not a csv file");
                            fileInput.value = '';
                            return false;
                        }

                        var file_data = $("#add_file_csv").prop("files")[0];

                        // var create_assigned_to     = $('#agent_display').val();
                        var lead_type              = sessionStorage.getItem("team_type");

                        // if(create_assigned_to){

                        // }else{
                            var distributor_id     = sessionStorage.getItem("dist_id");
                        // }

                        var create_user_id_creator = sessionStorage.getItem("user_id");


                        var formData = new FormData();
                        formData.append("file", file_data);

                        formData.append('action', 'add_promotions_csv');
                        formData.append('distributor_id', dist_id );
                        // formData.append('create_assigned_to', create_assigned_to);

                        console.log(file_data);

                        $.ajax({
                            url: "<?php echo ROOT_URI; ?>/resources/services/add_promotions_csv.php",
                            type: 'POST',
                            data: formData,
                            success: function (data) {
                                
                                console.log(data);
                            },
                            cache: false,
                            contentType: false,
                            processData: false
                        });  // close ajax

                  }); // close on click

                  $(document).on('click','#delete_promotions_csv',function(){

                      $('.delete_promotions_checkbox').show();
                      $(this).hide();
                      $('#add_promotions_csv').hide();
                      $('#delete_promotions_final').show();
                  });

                  $(document).on('click','#delete_promotions_final',function(){

                      $('.delete_promotions_checkbox').hide();
                      $('#add_promotions_csv').show();
                      $('#delete_promotions_csv').show();
                      $('.checkbox_check').prop('checked',false);
                      $(this).hide();
                  });

                  $('#create_case_modal').on('hidden.bs.modal', function (e) { $(this) .find("input,textarea,select") .val('') .end() .find("input[type=checkbox], input[type=radio]") .prop("checked", "") .end();
                   })
                  $(".modal").on("hidden.bs.modal", function(){
                    $(this).removeData();
                  });

                  $('#create_task_modal').on('hidden.bs.modal', function (e) { $(this) .find("input,textarea,select") .val('') .end() .find("input[type=checkbox], input[type=radio]") .prop("checked", "") .end();
                   })
                  $(".modal").on("hidden.bs.modal", function(){
                    $(this).removeData();
                  });

	 
	          //function to bind ratings ,will be used to display rating
        function bind_ratings(){
            var __slice = [].slice;

                        (function($, window) {
                          var Starrr;

                          Starrr = (function() {
                            Starrr.prototype.defaults = {
                              rating: void 0,
                              numStars: 5,
                              change: function(e, value) {}
                            };

                            function Starrr($el, options) {
                              var i, _, _ref,
                                _this = this;

                              this.options = $.extend({}, this.defaults, options);
                              this.$el = $el;
                              _ref = this.defaults;
                              for (i in _ref) {
                                _ = _ref[i];
                                if (this.$el.data(i) != null) {
                                  this.options[i] = this.$el.data(i);
                                }
                              }
                              this.createStars();
                              this.syncRating();  
                            }

                            Starrr.prototype.createStars = function() {
                              var _i, _ref, _results;

                              _results = [];
                              for (_i = 1, _ref = this.options.numStars; 1 <= _ref ? _i <= _ref : _i >= _ref; 1 <= _ref ? _i++ : _i--) {
                                _results.push(this.$el.append("<span class='glyphicon .glyphicon-star-empty color-grey'></span>"));
                              }
                              return _results;
                            };

                            

                            Starrr.prototype.syncRating = function(rating) {
                              var i, _i, _j, _ref;

                              rating || (rating = this.options.rating);
                              if (rating) {
                                for (i = _i = 0, _ref = rating - 1; 0 <= _ref ? _i <= _ref : _i >= _ref; i = 0 <= _ref ? ++_i : --_i) {
                                  this.$el.find('span').eq(i).removeClass('glyphicon-star-empty').addClass('glyphicon-star').removeClass('color-grey');
                                }
                              }
                              if (rating && rating < 5) {
                                for (i = _j = rating; rating <= 4 ? _j <= 4 : _j >= 4; i = rating <= 4 ? ++_j : --_j) {
                                  this.$el.find('span').eq(i).removeClass('glyphicon-star').addClass('glyphicon-star-empty').addClass('color-grey');
                                }
                              }
                              if (!rating) {
                                return this.$el.find('span').removeClass('glyphicon-star').addClass('glyphicon-star-empty').addClass('color-grey');
                              }
                            };

                            return Starrr;

                          })();
                          return $.fn.extend({
                            starrr: function() {
                              var args, option;

                              option = arguments[0], args = 2 <= arguments.length ? __slice.call(arguments, 1) : [];
                              return this.each(function() {
                                var data;

                                data = $(this).data('star-rating');
                                if (!data) {
                                  $(this).data('star-rating', (data = new Starrr($(this), option)));
                                }
                                if (typeof option === 'string') {
                                  return data[option].apply(data, args);
                                }
                              });
                            }
                          });
                        })(window.jQuery, window);

                        $(function() {
                          return $(".starrr").starrr();
                        });

        }//end of bind ratings function
});
 
</script>
 

 