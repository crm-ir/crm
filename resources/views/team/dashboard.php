<?php 
  $path_init = $_SERVER['DOCUMENT_ROOT']."/crm/resources/init.php";
include_once($path_init);

 ?>
<link href="<?php echo ROOT_URI; ?>/assets/css/team.css" rel="stylesheet" type="text/css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.min.js"></script>

 
<!-- right side main body starts -->
<div class="col-md-10 col-lg-10 col-sm-10 col-xs-10 col-lg-offset-2 col-md-offset-2 col-sm-offset-2 col-xs-offset-2 no-lr-pad back-color-grey">
   <!-- for upper part -->
    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 padding-top-15 border-bottom padding-left-40 back-color-white">
        <p class="font-size-24 float-inline"><b>Dashboard</b></p>
    </div>
   <!-- upper part ends -->
   <!-- lower part/main part starts here -->
   <!-- Dashboard 1st row starts here  -->
   <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 margin-top-10 padding-left-40 padding-right-40 " id="dashboard">
          <!-- Small Tiles order info  -->
          <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 ">
              <div class="back-color-white dashboard-small-tile custom-tile  margin-5"> 
                    <p class="color-grey-1 text-center text-uppercase font-size-12 padding-top-20">Customers Ordered</p>
                    <p class="text-bold text-center font-size-36">56</p>
                    <p class="text-success text-center text-bold font-size-14">+9.8 from yesterday </p>
              </div> 
          </div>
          <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 ">
              <div class="back-color-white dashboard-small-tile custom-tile  margin-5"> 
                    <p class="color-grey-1 text-center text-uppercase font-size-12 padding-top-20">Total Ordered Items</p>
                    <p class="text-bold text-center font-size-36">56</p>
                    <p class="text-danger text-center text-bold font-size-14">+9.8 from yesterday 
                    <a data-toggle="modal" data-target="#ordered-items_modal" class="a-deco">
                      <img src="<?php echo ROOT_URI; ?>/assets/img/assets/orders-modal.svg" width="15" >
                    </a>
                    </p>
              </div> 
          </div>
          <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 ">
              <div class="back-color-white dashboard-small-tile custom-tile  margin-5"> 
                    <p class="color-grey-1 text-center text-uppercase font-size-12 padding-top-20">Average Order Value</p>
                    <p class="text-bold text-center font-size-36">56</p>
                    <p class="text-success text-center text-bold font-size-14">+9.8 from yesterday </p>
              </div> 
          </div>
          <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 ">
              <div class="back-color-white dashboard-small-tile custom-tile  margin-5"> 
                    <p class="color-grey-1 text-center text-uppercase font-size-12 padding-top-20">Total Order Value</p>
                    <p class="text-bold text-center font-size-36">56</p>
                    <p class="text-success text-center text-bold font-size-14">+9.8 from yesterday </p>
              </div> 
          </div>
          <!-- Small Tiles order info ends here  -->        
   </div>
   <!-- Dashboard 1st row ends here  -->
   <!-- Dashboard 2nd row starts here -->
   <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 margin-top-10 pad-left-right-55 margin-bottom-20 ">
          <!-- Call summary starts here  -->
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-top-15 padding-left-15 back-color-white">
              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"> 
                  <p class="text-bold text-uppercase"> Call Summary</p>
              </div>               
              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 pull-right"> 
                  <p class="font-size-14"> <a href="#" class="a-deco pull-right">View Full Report <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a></p>
              </div>  
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-6">
                  <canvas id="callsummary" width="400" height="170"><canvas>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-6 ">
                  <div class="border-around-1 dashboard-small-tile custom-tile  margin-top-30 pad-10">
                    <p class="color-grey-1 text-center text-uppercase font-size-12 padding-top-20">Number of calls</p>
                    <p class="text-bold text-center font-size-36">30</p>
                    <p class="text-success text-center text-bold font-size-14">2 calls from yesterday </p>
                  </div>            
                </div>     
          </div>
          <!-- call summary ends here  -->
    </div>
   <!-- Dashboard 2nd row ends here  -->
   <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 margin-top-10 pad-left-right-55 margin-bottom-20 ">
          <!-- Perform and Task summary starts here  -->
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-left-15 back-color-white">
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 border-right">
                <div class="padding-top-15 margin-bottom-20">
                  <p class="text-bold text-uppercase"> Your Performance</p>
                  <canvas id="performance_chart" width="400" height="400"></canvas>
                </div>
              </div>     
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="padding-top-15 padding-bottom-15">  <p class="text-bold text-uppercase pull-left">Today's Task Summary</p>
                <p class=""> <a href="#" class="a-deco pull-right">See All Tasks <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a></p>

                <canvas id="task-summary_chart" width="400" height="200"></canvas>
              </div>
              </div>     
          </div>
          <!-- Performance and Task summary ends here  -->
    </div>
    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 margin-top-10 pad-left-right-55 margin-bottom-20">
          <!-- Last 5 Task Summary -->
          <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 no-lr-pad back-color-white">
              <div class="" style="">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 ">
                  <div class="padding-top-15 margin-bottom-20">
                    <p class="text-bold text-uppercase"> Last 5 Task Summary</p>
                  </div>
                </div>     
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                  <div class="padding-top-15 margin-bottom-20">
                    <p class=""> <a href="#" class="a-deco pull-right">View Today's Report <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a></p>
                  </div>
                </div>
                <div class="col-lg-12 col-md-12 ">
                  <div class="table-responsive">
                    <table class="table table-hover">
                      <tbody>
                        <tr>
                          <td>Health Industry Pvt Ltd</td>
                          <td>50 Items</td>
                          <td><span class="order-status not-ordered a-deco">Not Ordered</span></td>
                          <td><i class="fa fa-inr" aria-hidden="true"></i> 8000</td>
                        </tr><tr>
                          <td>Health Industry Pvt Ltd</td>
                          <td>50 Items</td>
                          <td><span class="order-status ordered a-deco">Ordered</span></td>
                          <td><i class="fa fa-inr" aria-hidden="true"></i> 9000</td>
                        </tr><tr>
                          <td>Health Industry Pvt Ltd</td>
                          <td>50 Items</td>
                          <td><span class="order-status call-not-picked a-deco">Call Not Picked</span></td>
                          <td><i class="fa fa-inr" aria-hidden="true"></i> 900</td>
                        </tr><tr>
                          <td>Health Industry Pvt Ltd</td>
                          <td>50 Items</td>
                          <td><span class="order-status ordered a-deco">Ordered</span></td>
                          <td><i class="fa fa-inr" aria-hidden="true"></i> 9000</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>      
              </div>
          </div>  
          <!-- Last 5 Task Summart ends here  -->
          <!-- Top 10 brands summary -->
          <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 padding-right-0" >
                 <div class="col-lg-12 back-color-white no-lr-pad" style="">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 ">
                      <div class="padding-top-15 margin-bottom-20 ">
                        <p class="text-bold text-uppercase"> TOP 10 BRANDS</p>
                      </div>
                    </div>     
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                      <div class="padding-top-15 margin-bottom-20 pull-right">
                        <p class="font-size-14"> <a href="#" class="a-deco pull-right">View Brand's Report <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a></p>
                      </div>
                    </div> 
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <?php 
                        for($i=0 ; $i<5 ;$i++){ 
                      ?>
                      <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 no-lr-pad">
                        <p>Himalayas</p>
                      </div>
                      <div class="col-lg-8 col-md-8 col-sm-11 col-xs-11 ">
                         <div class="progress">
                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="50"
                            aria-valuemin="0" aria-valuemax="100" style="width:50%">
                            
                            </div>
                          </div>  
                      </div>
                      <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 no-lr-pad">
                        <p class="margin-right-10">50%</p>
                      </div>
                     <?php } ?>
                    </div>    
                </div>
          </div>  
          <!-- Top 10 brands summary ends here -->
    </div>
   <!-- lower part/main part ends here -->
   <!-- modals -->


  <!-- Modal -->
 <div id="ordered-items_modal" class="modal fade" role="dialog">
    <div class="modal-dialog margin-top-100">

      <!-- Modal content-->
      <div class="modal-content col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
        <div class="modal-header col-md-12 col-lg-12 col-sm-12 col-xs-12 ">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <p class="font-size-20 margin-left-10"><b>TODAY'S ORDERED ITEMS</b></p>
        </div>
        <div class="modal-body col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
            <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12  ">
                  <div class="table-responsive">
                    <table class="table " id="ordered-items">
                      <tbody>
                        <?php for($i=0;$i<10;$i++) { ?>
                        <tr>
                          <td class="text-bold text-uppercase font-size-14">ALFALFA TONIC - GENERAL</td>
                          <td class="text-uppercase font-size-12">ALFALFA TONIC</td>
                          <td class="text-uppercase font-size-12">500 ML</td>
                        </tr>
                      <?php } ?>
                      </tbody>
                    </table>
                  </div>
            </div>
        </div>
        <div class="modal-footer col-md-12 col-lg-12 col-sm-12 col-xs-12">
          <button type="button" class="btn btn-default pull-right brand-btn add-mem-btn1">close</button>
        </div>
      </div>

    </div>
  </div> <
  <!-- modals ends here -->
</div>
 <!-- right side main body ends -->
      
 
<script type="text/javascript">
var $table = $('#ordered-items'),
    $bodyCells = $table.find('tbody tr:first').children(),
    colWidth;

$(window).resize(function() {
    // Get the tbody columns width array
    colWidth = $bodyCells.map(function() {
        return $(this).width();
    }).get();
    
    // Set the width of thead columns
    $table.find('thead tr').children().each(function(i, v) {
        $(v).width(colWidth[i]);
    });    
}).resize();

    
  var ctx = document.getElementById("callsummary");
  ctx.height = 120;
  var myChart = new Chart(ctx, {
      type: 'bar',
      data: {
          labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
          datasets: [{
              label: '# of Calls',
              data: [12, 99, 3, 5, 2, 3],
              backgroundColor: [
                  'rgba(255, 99, 132, 0.2)',
                  'rgba(54, 162, 235, 0.2)',
                  'rgba(255, 206, 86, 0.2)',
                  'rgba(75, 192, 192, 0.2)',
                  'rgba(153, 102, 255, 0.2)',
                  'rgba(255, 159, 64, 0.2)'
              ],
              borderColor: [
                  'rgba(255,99,132,1)',
                  'rgba(54, 162, 235, 1)',
                  'rgba(255, 206, 86, 1)',
                  'rgba(75, 192, 192, 1)',
                  'rgba(153, 102, 255, 1)',
                  'rgba(255, 159, 64, 1)'
              ],
              borderWidth: 1
          }]
      },
      options: {
          scales: {
              yAxes: [{
                  ticks: {
                      beginAtZero:true
                  }
              }]
          }
      }
  });
  var ctx1 = document.getElementById("performance_chart");
  ctx1.height = 200;
  var chart = new Chart(ctx1, {
    // The type of chart we want to create
    type: 'line',

    // The data for our dataset
    data: {
        labels: ["January", "February", "March", "April", "May", "June", "July"],
    datasets: [{
        label: "My First dataset",
        data: [65, 59, 80, 81, 56, 55, 40],
        backgroundColor: [
          'rgba(105, 0, 132, .2)',
        ],
        borderColor: [
          'rgba(200, 99, 132, .7)',
        ],
        borderWidth: 2
      },
        {
        label: "My Second dataset",
        data: [28, 48, 40, 19, 86, 27, 90],
        backgroundColor: [
          'rgba(0, 137, 132, .2)',
        ],
        borderColor: [
          'rgba(0, 10, 130, .7)',
        ],
        borderWidth: 2
      }

        ]
    },

    // Configuration options go here
    options: {}
  });
  window.onload = function() {
      var ctx2 = document.getElementById('task-summary_chart').getContext('2d');
      ctx2.height = 100;
      window.myDoughnut = new Chart(ctx2, config);

  };
  var randomScalingFactor = function() {
      return Math.round(Math.random() * 100);
    };
  var config = {
      type: 'doughnut',
      data: {
        datasets: [{
          data: [
            randomScalingFactor(),
            randomScalingFactor(),
            randomScalingFactor(),
            randomScalingFactor(),
            randomScalingFactor(),
          ],
          backgroundColor: [
            'rgb(255,0,0)',
            'rgb(0,255,0)',
            'rgb(0,0,255)',
            'rgb(255,255,0)',
            'rgb(0,255,255)',
          ],
          label: 'Dataset 1'
        }],
        labels: [
          'Red',
          'Orange',
          'Yellow',
          'Green',
          'Blue'
        ]
      },
      options: {
        responsive: true,
        legend: {
          position: 'top',
        },
        title: {
          display: true,
          text: ''
        },
        animation: {
          animateScale: true,
          animateRotate: true
        }
      }
    };
</script>
 

 