  <?php 
  $page_name="page";    

  $path = $_SERVER['DOCUMENT_ROOT']."/crm/resources/views/header.php";
  include_once($path); 
   
  ?>

  <link href="<?php echo ROOT_URI; ?>/assets/css/temp.css" rel="stylesheet" type="text/css">
  <div class="container-fluid ">
     <section class="padding-top-20">
        <div class="col-lg-12">
          <ul class="nav nav-tabs">
            <li><a data-toggle="tab" href="#all" >All </a></li>
            <li><a data-toggle="tab" href="#articles">Articles</a></li>
            <li><a data-toggle="tab" href="#ebooks">E-books</a></li>
            <li><a data-toggle="tab" href="#videos">Videos</a></li> 
            <li><a data-toggle="tab" href="#courses">Courses</a></li>
            <li><a data-toggle="tab" href="#podcast">Podcast</a></li>
            <li><a data-toggle="tab" href="#infographics">Infographics</a></li>
            <li class="active"><a data-toggle="tab" href="#library-books">Library Books</a></li>
          </ul>

        <div class="tab-content">
          <div id="all" class="tab-pane fade in ">
            <!-- all tab content -->
              <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 margin-top-30">          
                <div class="row">
                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 no-lr-pad ">
                       <p>All</p>               
                    </div> 
                    <div class="right-section col-lg-3 col-md-3 col-sm-3 hidden-xs padding-top-10 box-shadow ">
                      <div class="col-md-12 col-lg-12 col-sm-12 border-bottom ">
                        <h5 class="right-headings"><b><i class="fa fa-filter"></i>Filters</b> </h5>
                      </div>
                      <div class="col-md-12 col-lg-12 col-sm-12 ">
                        <h5 class="right-headings"><b> Search By</b><i class="fa fa-chevron-down pull-right"></i></h5>
                        <div class="search-by-filters border-bottom">
                          <div class="checkbox-container">
                            <input type="checkbox" name="">
                            <label>Title</label>
                          </div>
                          <div class="checkbox-container">
                            <input type="checkbox" name="">
                            <label>Author</label>
                          </div>
                          <div class="checkbox-container">
                            <input type="checkbox" name="">
                            <label>ISBN</label>
                          </div>
                        </div>
                        <div class="checkbox-container border-bottom">
                            <input type="checkbox" name="">
                            <label class="padding-top-5 padding-bottom-5 ">My Company</label>
                        </div>
                        
                      </div>  
                      <div class="col-md-12 col-lg-12 col-sm-12 ">
                        <h5 class="right-headings"><b> Category</b></h5>
                              <div class="category-filters border-bottom">
                                <div class="category-heading padding-t-b">
                                  <label>Title</label>
                                  <i data-toggle="collapse"  class="fa fa-chevron-down pull-right"></i>
                                </div>
                                <div id="category1" class="category-1-collapse collapse border-bottom padding-left-30">
                                  <div class="checkbox-container">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Action</label>
                                  </div><div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Action</label>
                                  </div><div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Action</label>
                                  </div><div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 ">Action</label>
                                  </div>    
                                </div>
                                <div class="category-heading padding-t-b">
                                  <label>Title</label>
                                  <i data-toggle="collapse"  class="fa fa-chevron-down pull-right"></i>
                                </div>
                                <div id="category2" class="category-2-collapse collapse border-bottom padding-left-30">
                                  <div class="checkbox-container">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Health & fitness</label>
                                  </div><div class="checkbox-container">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Health & fitness</label>
                                  </div><div class="checkbox-container">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Health & fitness</label>
                                  </div><div class="checkbox-container">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Health & fitness</label>
                                  </div>    
                                </div>
                                <div class="category-heading padding-t-b">
                                  <label>Title</label>
                                  <i data-toggle="collapse"  class="fa fa-chevron-down pull-right"></i>
                                </div>
                                <div id="category3" class="category-3-collapse collapse padding-left-30">
                                  <div class="checkbox-container">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>       <div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>       <div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>       <div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>       <div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>    
                                </div> 
                                <div class="category-heading padding-t-b">
                                  <label>Title</label>
                                  <i data-toggle="collapse"  class="fa fa-chevron-down pull-right"></i>
                                </div>
                                <div id="category4" class="category-4-collapse collapse padding-left-30">
                                  <div class="checkbox-container">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>       <div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>       <div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>       <div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>       <div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>    
                                </div>
                                <div class="category-heading padding-t-b padding-bottom-20">
                                  <label>Title</label>
                                  <i data-toggle="collapse"  class="fa fa-chevron-down pull-right"></i>
                                </div>
                                <div id="category5" class="category-4-collapse collapse padding-left-30">
                                  <div class="checkbox-container">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>       <div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>       <div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>       <div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>       <div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>    
                                </div>
                              </div>
                      </div>            
                    <hr>
                    </div>
                </div>             
              </div>
            <!-- all tab content ends here  --> 
          </div>
          <div id="articles" class="tab-pane fade in ">
            <!-- articles tab content -->
              <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 margin-top-30">          
                <div class="row">
                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 no-lr-pad ">
                      <p>Articles</p>                                      
                    </div> 
                    <div class="right-section col-lg-3 col-md-3 col-sm-3 hidden-xs padding-top-10 box-shadow ">
                      <div class="col-md-12 col-lg-12 col-sm-12 border-bottom ">
                        <h5 class="right-headings"><b><i class="fa fa-filter"></i>Filters</b> </h5>
                      </div>
                      <div class="col-md-12 col-lg-12 col-sm-12 ">
                        <h5 class="right-headings"><b> Search By</b><i class="fa fa-chevron-down pull-right"></i></h5>
                        <div class="search-by-filters border-bottom">
                          <div class="checkbox-container">
                            <input type="checkbox" name="">
                            <label>Title</label>
                          </div>
                          <div class="checkbox-container">
                            <input type="checkbox" name="">
                            <label>Author</label>
                          </div>
                          <div class="checkbox-container">
                            <input type="checkbox" name="">
                            <label>ISBN</label>
                          </div>
                        </div>
                        <div class="checkbox-container border-bottom">
                            <input type="checkbox" name="">
                            <label class="padding-top-5 padding-bottom-5 ">My Company</label>
                        </div>
                        
                      </div>  
                      <div class="col-md-12 col-lg-12 col-sm-12 ">
                        <h5 class="right-headings"><b> Category</b></h5>
                              <div class="category-filters border-bottom">
                                <div class="category-heading padding-t-b">
                                  <label>Title</label>
                                  <i data-toggle="collapse"  class="fa fa-chevron-down pull-right"></i>
                                </div>
                                <div id="category1" class="category-1-collapse collapse border-bottom padding-left-30">
                                  <div class="checkbox-container">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Action</label>
                                  </div><div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Action</label>
                                  </div><div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Action</label>
                                  </div><div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 ">Action</label>
                                  </div>    
                                </div>
                                <div class="category-heading padding-t-b">
                                  <label>Title</label>
                                  <i data-toggle="collapse"  class="fa fa-chevron-down pull-right"></i>
                                </div>
                                <div id="category2" class="category-2-collapse collapse border-bottom padding-left-30">
                                  <div class="checkbox-container">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Health & fitness</label>
                                  </div><div class="checkbox-container">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Health & fitness</label>
                                  </div><div class="checkbox-container">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Health & fitness</label>
                                  </div><div class="checkbox-container">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Health & fitness</label>
                                  </div>    
                                </div>
                                <div class="category-heading padding-t-b">
                                  <label>Title</label>
                                  <i data-toggle="collapse"  class="fa fa-chevron-down pull-right"></i>
                                </div>
                                <div id="category3" class="category-3-collapse collapse padding-left-30">
                                  <div class="checkbox-container">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>       <div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>       <div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>       <div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>       <div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>    
                                </div> 
                                <div class="category-heading padding-t-b">
                                  <label>Title</label>
                                  <i data-toggle="collapse"  class="fa fa-chevron-down pull-right"></i>
                                </div>
                                <div id="category4" class="category-4-collapse collapse padding-left-30">
                                  <div class="checkbox-container">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>       <div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>       <div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>       <div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>       <div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>    
                                </div>
                                <div class="category-heading padding-t-b padding-bottom-20">
                                  <label>Title</label>
                                  <i data-toggle="collapse"  class="fa fa-chevron-down pull-right"></i>
                                </div>
                                <div id="category5" class="category-4-collapse collapse padding-left-30">
                                  <div class="checkbox-container">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>       <div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>       <div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>       <div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>       <div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>    
                                </div>
                              </div>
                      </div>            
                    <hr>
                    </div>
                </div>             
              </div>
            <!-- articles tab content end here -->
          </div>
          <div id="ebooks" class="tab-pane fade in ">
            <!-- E-books tab content  -->
              <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 margin-top-30">          
                <div class="row">
                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 no-lr-pad ">
                      <p>E-books</p>   
                    </div> 
                    <div class="right-section col-lg-3 col-md-3 col-sm-3 hidden-xs padding-top-10 box-shadow ">
                      <div class="col-md-12 col-lg-12 col-sm-12 border-bottom ">
                        <h5 class="right-headings"><b><i class="fa fa-filter"></i>Filters</b> </h5>
                      </div>
                      <div class="col-md-12 col-lg-12 col-sm-12 ">
                        <h5 class="right-headings"><b> Search By</b><i class="fa fa-chevron-down pull-right"></i></h5>
                        <div class="search-by-filters border-bottom">
                          <div class="checkbox-container">
                            <input type="checkbox" name="">
                            <label>Title</label>
                          </div>
                          <div class="checkbox-container">
                            <input type="checkbox" name="">
                            <label>Author</label>
                          </div>
                          <div class="checkbox-container">
                            <input type="checkbox" name="">
                            <label>ISBN</label>
                          </div>
                        </div>
                        <div class="checkbox-container border-bottom">
                            <input type="checkbox" name="">
                            <label class="padding-top-5 padding-bottom-5 ">My Company</label>
                        </div>
                        
                      </div>  
                      <div class="col-md-12 col-lg-12 col-sm-12 ">
                        <h5 class="right-headings"><b> Category</b></h5>
                              <div class="category-filters border-bottom">
                                <div class="category-heading padding-t-b">
                                  <label>Title</label>
                                  <i data-toggle="collapse"  class="fa fa-chevron-down pull-right"></i>
                                </div>
                                <div id="category1" class="category-1-collapse collapse border-bottom padding-left-30">
                                  <div class="checkbox-container">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Action</label>
                                  </div><div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Action</label>
                                  </div><div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Action</label>
                                  </div><div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 ">Action</label>
                                  </div>    
                                </div>
                                <div class="category-heading padding-t-b">
                                  <label>Title</label>
                                  <i data-toggle="collapse"  class="fa fa-chevron-down pull-right"></i>
                                </div>
                                <div id="category2" class="category-2-collapse collapse border-bottom padding-left-30">
                                  <div class="checkbox-container">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Health & fitness</label>
                                  </div><div class="checkbox-container">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Health & fitness</label>
                                  </div><div class="checkbox-container">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Health & fitness</label>
                                  </div><div class="checkbox-container">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Health & fitness</label>
                                  </div>    
                                </div>
                                <div class="category-heading padding-t-b">
                                  <label>Title</label>
                                  <i data-toggle="collapse"  class="fa fa-chevron-down pull-right"></i>
                                </div>
                                <div id="category3" class="category-3-collapse collapse padding-left-30">
                                  <div class="checkbox-container">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>       <div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>       <div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>       <div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>       <div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>    
                                </div> 
                                <div class="category-heading padding-t-b">
                                  <label>Title</label>
                                  <i data-toggle="collapse"  class="fa fa-chevron-down pull-right"></i>
                                </div>
                                <div id="category4" class="category-4-collapse collapse padding-left-30">
                                  <div class="checkbox-container">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>       <div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>       <div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>       <div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>       <div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>    
                                </div>
                                <div class="category-heading padding-t-b padding-bottom-20">
                                  <label>Title</label>
                                  <i data-toggle="collapse"  class="fa fa-chevron-down pull-right"></i>
                                </div>
                                <div id="category5" class="category-4-collapse collapse padding-left-30">
                                  <div class="checkbox-container">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>       <div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>       <div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>       <div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>       <div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>    
                                </div>
                              </div>
                      </div>            
                    <hr>
                    </div>
                </div>             
              </div>

            <!-- E-books tab content ends here  -->

          </div>
          <div id="videos" class="tab-pane fade in ">
              <!-- Videos Tab Content -->
                <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 margin-top-30">          
                <div class="row">
                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 no-lr-pad ">
                      <p>Videos</p>     
                    </div> 
                    <div class="right-section col-lg-3 col-md-3 col-sm-3 hidden-xs padding-top-10 box-shadow ">
                      <div class="col-md-12 col-lg-12 col-sm-12 border-bottom ">
                        <h5 class="right-headings"><b><i class="fa fa-filter"></i>Filters</b> </h5>
                      </div>
                      <div class="col-md-12 col-lg-12 col-sm-12 ">
                        <h5 class="right-headings"><b> Search By</b><i class="fa fa-chevron-down pull-right"></i></h5>
                        <div class="search-by-filters border-bottom">
                          <div class="checkbox-container">
                            <input type="checkbox" name="">
                            <label>Title</label>
                          </div>
                          <div class="checkbox-container">
                            <input type="checkbox" name="">
                            <label>Author</label>
                          </div>
                          <div class="checkbox-container">
                            <input type="checkbox" name="">
                            <label>ISBN</label>
                          </div>
                        </div>
                        <div class="checkbox-container border-bottom">
                            <input type="checkbox" name="">
                            <label class="padding-top-5 padding-bottom-5 ">My Company</label>
                        </div>
                        
                      </div>  
                      <div class="col-md-12 col-lg-12 col-sm-12 ">
                        <h5 class="right-headings"><b> Category</b></h5>
                              <div class="category-filters border-bottom">
                                <div class="category-heading padding-t-b">
                                  <label>Title</label>
                                  <i data-toggle="collapse"  class="fa fa-chevron-down pull-right"></i>
                                </div>
                                <div id="category1" class="category-1-collapse collapse border-bottom padding-left-30">
                                  <div class="checkbox-container">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Action</label>
                                  </div><div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Action</label>
                                  </div><div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Action</label>
                                  </div><div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 ">Action</label>
                                  </div>    
                                </div>
                                <div class="category-heading padding-t-b">
                                  <label>Title</label>
                                  <i data-toggle="collapse"  class="fa fa-chevron-down pull-right"></i>
                                </div>
                                <div id="category2" class="category-2-collapse collapse border-bottom padding-left-30">
                                  <div class="checkbox-container">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Health & fitness</label>
                                  </div><div class="checkbox-container">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Health & fitness</label>
                                  </div><div class="checkbox-container">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Health & fitness</label>
                                  </div><div class="checkbox-container">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Health & fitness</label>
                                  </div>    
                                </div>
                                <div class="category-heading padding-t-b">
                                  <label>Title</label>
                                  <i data-toggle="collapse"  class="fa fa-chevron-down pull-right"></i>
                                </div>
                                <div id="category3" class="category-3-collapse collapse padding-left-30">
                                  <div class="checkbox-container">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>       <div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>       <div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>       <div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>       <div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>    
                                </div> 
                                <div class="category-heading padding-t-b">
                                  <label>Title</label>
                                  <i data-toggle="collapse"  class="fa fa-chevron-down pull-right"></i>
                                </div>
                                <div id="category4" class="category-4-collapse collapse padding-left-30">
                                  <div class="checkbox-container">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>       <div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>       <div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>       <div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>       <div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>    
                                </div>
                                <div class="category-heading padding-t-b padding-bottom-20">
                                  <label>Title</label>
                                  <i data-toggle="collapse"  class="fa fa-chevron-down pull-right"></i>
                                </div>
                                <div id="category5" class="category-4-collapse collapse padding-left-30">
                                  <div class="checkbox-container">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>       <div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>       <div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>       <div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>       <div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>    
                                </div>
                              </div>
                      </div>            
                    <hr>
                    </div>
                </div>             
              </div>

              <!-- Videos Tab Content ends here  -->
          </div>
          <div id="courses" class="tab-pane fade in "> 
            <!-- Courses Tab Content -->
              <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 margin-top-30">          
                <div class="row">
                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 no-lr-pad ">
                       <p>Courses</p>               
                    </div> 
                    <div class="right-section col-lg-3 col-md-3 col-sm-3 hidden-xs padding-top-10 box-shadow ">
                      <div class="col-md-12 col-lg-12 col-sm-12 border-bottom ">
                        <h5 class="right-headings"><b><i class="fa fa-filter"></i>Filters</b> </h5>
                      </div>
                      <div class="col-md-12 col-lg-12 col-sm-12 ">
                        <h5 class="right-headings"><b> Search By</b><i class="fa fa-chevron-down pull-right"></i></h5>
                        <div class="search-by-filters border-bottom">
                          <div class="checkbox-container">
                            <input type="checkbox" name="">
                            <label>Title</label>
                          </div>
                          <div class="checkbox-container">
                            <input type="checkbox" name="">
                            <label>Author</label>
                          </div>
                          <div class="checkbox-container">
                            <input type="checkbox" name="">
                            <label>ISBN</label>
                          </div>
                        </div>
                        <div class="checkbox-container border-bottom">
                            <input type="checkbox" name="">
                            <label class="padding-top-5 padding-bottom-5 ">My Company</label>
                        </div>
                        
                      </div>  
                      <div class="col-md-12 col-lg-12 col-sm-12 ">
                        <h5 class="right-headings"><b> Category</b></h5>
                              <div class="category-filters border-bottom">
                                <div class="category-heading padding-t-b">
                                  <label>Title</label>
                                  <i data-toggle="collapse"  class="fa fa-chevron-down pull-right"></i>
                                </div>
                                <div id="category1" class="category-1-collapse collapse border-bottom padding-left-30">
                                  <div class="checkbox-container">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Action</label>
                                  </div><div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Action</label>
                                  </div><div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Action</label>
                                  </div><div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 ">Action</label>
                                  </div>    
                                </div>
                                <div class="category-heading padding-t-b">
                                  <label>Title</label>
                                  <i data-toggle="collapse"  class="fa fa-chevron-down pull-right"></i>
                                </div>
                                <div id="category2" class="category-2-collapse collapse border-bottom padding-left-30">
                                  <div class="checkbox-container">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Health & fitness</label>
                                  </div><div class="checkbox-container">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Health & fitness</label>
                                  </div><div class="checkbox-container">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Health & fitness</label>
                                  </div><div class="checkbox-container">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Health & fitness</label>
                                  </div>    
                                </div>
                                <div class="category-heading padding-t-b">
                                  <label>Title</label>
                                  <i data-toggle="collapse"  class="fa fa-chevron-down pull-right"></i>
                                </div>
                                <div id="category3" class="category-3-collapse collapse padding-left-30">
                                  <div class="checkbox-container">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>       <div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>       <div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>       <div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>       <div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>    
                                </div> 
                                <div class="category-heading padding-t-b">
                                  <label>Title</label>
                                  <i data-toggle="collapse"  class="fa fa-chevron-down pull-right"></i>
                                </div>
                                <div id="category4" class="category-4-collapse collapse padding-left-30">
                                  <div class="checkbox-container">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>       <div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>       <div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>       <div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>       <div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>    
                                </div>
                                <div class="category-heading padding-t-b padding-bottom-20">
                                  <label>Title</label>
                                  <i data-toggle="collapse"  class="fa fa-chevron-down pull-right"></i>
                                </div>
                                <div id="category5" class="category-4-collapse collapse padding-left-30">
                                  <div class="checkbox-container">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>       <div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>       <div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>       <div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>       <div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>    
                                </div>
                              </div>
                      </div>            
                    <hr>
                    </div>
                </div>             
              </div>

            <!-- Courses Tab Content ends here  -->

          </div>
          <div id="podcast" class="tab-pane fade in ">
             <!-- Podcasts tab content  -->
              <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 margin-top-30">          
                <div class="row">
                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 no-lr-pad ">
                      <p>Podcasts</p>   
                    </div> 
                    <div class="right-section col-lg-3 col-md-3 col-sm-3 hidden-xs padding-top-10 box-shadow ">
                      <div class="col-md-12 col-lg-12 col-sm-12 border-bottom ">
                        <h5 class="right-headings"><b><i class="fa fa-filter"></i>Filters</b> </h5>
                      </div>
                      <div class="col-md-12 col-lg-12 col-sm-12 ">
                        <h5 class="right-headings"><b> Search By</b><i class="fa fa-chevron-down pull-right"></i></h5>
                        <div class="search-by-filters border-bottom">
                          <div class="checkbox-container">
                            <input type="checkbox" name="">
                            <label>Title</label>
                          </div>
                          <div class="checkbox-container">
                            <input type="checkbox" name="">
                            <label>Author</label>
                          </div>
                          <div class="checkbox-container">
                            <input type="checkbox" name="">
                            <label>ISBN</label>
                          </div>
                        </div>
                        <div class="checkbox-container border-bottom">
                            <input type="checkbox" name="">
                            <label class="padding-top-5 padding-bottom-5 ">My Company</label>
                        </div>
                        
                      </div>  
                      <div class="col-md-12 col-lg-12 col-sm-12 ">
                        <h5 class="right-headings"><b> Category</b></h5>
                              <div class="category-filters border-bottom">
                                <div class="category-heading padding-t-b">
                                  <label>Title</label>
                                  <i data-toggle="collapse"  class="fa fa-chevron-down pull-right"></i>
                                </div>
                                <div id="category1" class="category-1-collapse collapse border-bottom padding-left-30">
                                  <div class="checkbox-container">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Action</label>
                                  </div><div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Action</label>
                                  </div><div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Action</label>
                                  </div><div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 ">Action</label>
                                  </div>    
                                </div>
                                <div class="category-heading padding-t-b">
                                  <label>Title</label>
                                  <i data-toggle="collapse"  class="fa fa-chevron-down pull-right"></i>
                                </div>
                                <div id="category2" class="category-2-collapse collapse border-bottom padding-left-30">
                                  <div class="checkbox-container">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Health & fitness</label>
                                  </div><div class="checkbox-container">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Health & fitness</label>
                                  </div><div class="checkbox-container">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Health & fitness</label>
                                  </div><div class="checkbox-container">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Health & fitness</label>
                                  </div>    
                                </div>
                                <div class="category-heading padding-t-b">
                                  <label>Title</label>
                                  <i data-toggle="collapse"  class="fa fa-chevron-down pull-right"></i>
                                </div>
                                <div id="category3" class="category-3-collapse collapse padding-left-30">
                                  <div class="checkbox-container">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>       <div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>       <div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>       <div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>       <div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>    
                                </div> 
                                <div class="category-heading padding-t-b">
                                  <label>Title</label>
                                  <i data-toggle="collapse"  class="fa fa-chevron-down pull-right"></i>
                                </div>
                                <div id="category4" class="category-4-collapse collapse padding-left-30">
                                  <div class="checkbox-container">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>       <div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>       <div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>       <div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>       <div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>    
                                </div>
                                <div class="category-heading padding-t-b padding-bottom-20">
                                  <label>Title</label>
                                  <i data-toggle="collapse"  class="fa fa-chevron-down pull-right"></i>
                                </div>
                                <div id="category5" class="category-4-collapse collapse padding-left-30">
                                  <div class="checkbox-container">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>       <div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>       <div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>       <div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>       <div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>    
                                </div>
                              </div>
                      </div>            
                    <hr>
                    </div>
                </div>             
              </div>

             <!-- Podcast tab content ends here  -->
          </div>
          <div id="infographics" class="tab-pane fade in ">
            <!-- Infographics tab -->
              <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 margin-top-30">          
                <div class="row">
                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 no-lr-pad ">
                      Infographics                  
                    </div> 
                    <div class="right-section col-lg-3 col-md-3 col-sm-3 hidden-xs padding-top-10 box-shadow ">
                      <div class="col-md-12 col-lg-12 col-sm-12 border-bottom ">
                        <h5 class="right-headings"><b><i class="fa fa-filter"></i>Filters</b> </h5>
                      </div>
                      <div class="col-md-12 col-lg-12 col-sm-12 ">
                        <h5 class="right-headings"><b> Search By</b><i class="fa fa-chevron-down pull-right"></i></h5>
                        <div class="search-by-filters border-bottom">
                          <div class="checkbox-container">
                            <input type="checkbox" name="">
                            <label>Title</label>
                          </div>
                          <div class="checkbox-container">
                            <input type="checkbox" name="">
                            <label>Author</label>
                          </div>
                          <div class="checkbox-container">
                            <input type="checkbox" name="">
                            <label>ISBN</label>
                          </div>
                        </div>
                        <div class="checkbox-container border-bottom">
                            <input type="checkbox" name="">
                            <label class="padding-top-5 padding-bottom-5 ">My Company</label>
                        </div>
                        
                      </div>  
                      <div class="col-md-12 col-lg-12 col-sm-12 ">
                        <h5 class="right-headings"><b> Category</b></h5>
                              <div class="category-filters border-bottom">
                                <div class="category-heading padding-t-b">
                                  <label>Title</label>
                                  <i data-toggle="collapse"  class="fa fa-chevron-down pull-right"></i>
                                </div>
                                <div id="category1" class="category-1-collapse collapse border-bottom padding-left-30">
                                  <div class="checkbox-container">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Action</label>
                                  </div><div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Action</label>
                                  </div><div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Action</label>
                                  </div><div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 ">Action</label>
                                  </div>    
                                </div>
                                <div class="category-heading padding-t-b">
                                  <label>Title</label>
                                  <i data-toggle="collapse"  class="fa fa-chevron-down pull-right"></i>
                                </div>
                                <div id="category2" class="category-2-collapse collapse border-bottom padding-left-30">
                                  <div class="checkbox-container">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Health & fitness</label>
                                  </div><div class="checkbox-container">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Health & fitness</label>
                                  </div><div class="checkbox-container">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Health & fitness</label>
                                  </div><div class="checkbox-container">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Health & fitness</label>
                                  </div>    
                                </div>
                                <div class="category-heading padding-t-b">
                                  <label>Title</label>
                                  <i data-toggle="collapse"  class="fa fa-chevron-down pull-right"></i>
                                </div>
                                <div id="category3" class="category-3-collapse collapse padding-left-30">
                                  <div class="checkbox-container">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>       <div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>       <div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>       <div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>       <div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>    
                                </div> 
                                <div class="category-heading padding-t-b">
                                  <label>Title</label>
                                  <i data-toggle="collapse"  class="fa fa-chevron-down pull-right"></i>
                                </div>
                                <div id="category4" class="category-4-collapse collapse padding-left-30">
                                  <div class="checkbox-container">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>       <div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>       <div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>       <div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>       <div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>    
                                </div>
                                <div class="category-heading padding-t-b padding-bottom-20">
                                  <label>Title</label>
                                  <i data-toggle="collapse"  class="fa fa-chevron-down pull-right"></i>
                                </div>
                                <div id="category5" class="category-4-collapse collapse padding-left-30">
                                  <div class="checkbox-container">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>       <div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>       <div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>       <div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>       <div class="checkbox-container ">
                                    <input type="checkbox" name="">
                                    <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                  </div>    
                                </div>
                              </div>
                      </div>            
                    <hr>
                    </div>
                </div>             
              </div>

            <!-- infographics tab content ends here  -->
          </div>
          <div id="library-books" class="tab-pane fade in active">
           <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 margin-top-30">          
              <div class="row">
                  <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 no-lr-pad ">
                    <a href="" class="list-item-link">
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 box-shadow margin-bottom-20 no-lr-pad">
                      <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4 no-lr-pad list-item-height">
                        <img src="https://s3-ap-southeast-1.amazonaws.com/ireads/images/books/9780143333746.jpg" class="img-responsive img-list-item">
                      </div>
                      <div class="col-md-10 col-sm-10 col-xs-6">
                        <h4 class="text-wrap">Escape From Java & Other Tales Of Danger</h4>
                        <p>Source: IndiaReads</p>
                        <span><i class="fa fa-ellipsis-h" aria-hidden="true"></i></span>
                        <hr class="list-item-hr ">
                        <p class="library-icon-txt">
                          <img src="https://indiareads.indiareads.com/ksp/public/img/icon/icons/9.svg" width="20">
                          Library Book
                        </p>
                      </div> 
                    </div>  
                    </a>
                    <a href="" class="list-item-link">
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 box-shadow margin-bottom-20 no-lr-pad">
                      <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4 no-lr-pad list-item-height">
                        <img src="https://s3-ap-southeast-1.amazonaws.com/ireads/images/books/9780143333746.jpg" class="img-responsive img-list-item">
                      </div>
                      <div class="col-md-10 col-sm-10 col-xs-6">
                        <h4>Escape From Java & Other Tales Of Danger</h4>
                        <p>Source: IndiaReads</p>
                        <span><i class="fa fa-ellipsis-h" aria-hidden="true"></i></span>
                        <hr class="list-item-hr">
                        <p class="library-icon-txt">
                          <img src="https://indiareads.indiareads.com/ksp/public/img/icon/icons/9.svg" width="20">
                          Library Book
                        </p>
                      </div> 
                    </div>  
                    </a> 
                    <a href="" class="list-item-link">
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 box-shadow margin-bottom-20 no-lr-pad">
                      <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4 no-lr-pad list-item-height">
                        <img src="https://s3-ap-southeast-1.amazonaws.com/ireads/images/books/9780143333746.jpg" class="img-responsive img-list-item">
                      </div>
                      <div class="col-md-10 col-sm-10 col-xs-6">
                        <h4>Escape From Java & Other Tales Of Danger</h4>
                        <p>Source: IndiaReads</p>
                        <span><i class="fa fa-ellipsis-h" aria-hidden="true"></i></span>
                        <hr class="list-item-hr">
                        <p class="library-icon-txt">
                          <img src="https://indiareads.indiareads.com/ksp/public/img/icon/icons/9.svg" width="20">
                          Library Book
                        </p>
                      </div> 
                    </div>  
                      </a>
                       <a href="" class="list-item-link">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 box-shadow margin-bottom-20 no-lr-pad">
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4 no-lr-pad list-item-height">
                          <img src="https://s3-ap-southeast-1.amazonaws.com/ireads/images/books/9780143333746.jpg" class="img-responsive img-list-item">
                        </div>
                        <div class="col-md-10 col-sm-10 col-xs-6">
                          <h4>Escape From Java & Other Tales Of Danger</h4>
                          <p>Source: IndiaReads</p>
                          <span><i class="fa fa-ellipsis-h" aria-hidden="true"></i></span>
                          <hr class="list-item-hr">
                          <p class="library-icon-txt">
                            <img src="https://indiareads.indiareads.com/ksp/public/img/icon/icons/9.svg" width="20">
                            Library Book
                          </p>
                        </div> 
                      </div>  
                      </a>                  
                  </div> 
                  <div class="right-section col-lg-3 col-md-3 col-sm-3 hidden-xs padding-top-10 box-shadow ">
                    <div class="col-md-12 col-lg-12 col-sm-12 border-bottom ">
                      <h5 class="right-headings"><b><i class="fa fa-filter"></i>Filters</b> </h5>
                    </div>
                    <div class="col-md-12 col-lg-12 col-sm-12 ">
                      <h5 class="right-headings"><b> Search By</b><i class="fa fa-chevron-down pull-right"></i></h5>
                      <div class="search-by-filters border-bottom">
                        <div class="checkbox-container">
                          <input type="checkbox" name="">
                          <label>Title</label>
                        </div>
                        <div class="checkbox-container">
                          <input type="checkbox" name="">
                          <label>Author</label>
                        </div>
                        <div class="checkbox-container">
                          <input type="checkbox" name="">
                          <label>ISBN</label>
                        </div>
                      </div>
                      <div class="checkbox-container border-bottom">
                          <input type="checkbox" name="">
                          <label class="padding-top-5 padding-bottom-5 ">My Company</label>
                      </div>
                      
                    </div>  
                    <div class="col-md-12 col-lg-12 col-sm-12 ">
                      <h5 class="right-headings"><b> Category</b></h5>
                            <div class="category-filters border-bottom">
                              <div class="category-heading padding-t-b">
                                <label>Title</label>
                                <i data-toggle="collapse"  class="fa fa-chevron-down pull-right"></i>
                              </div>
                              <div id="category1" class="category-1-collapse collapse border-bottom padding-left-30">
                                <div class="checkbox-container">
                                  <input type="checkbox" name="">
                                  <label class="padding-top-5 padding-bottom-5 ">Action</label>
                                </div><div class="checkbox-container ">
                                  <input type="checkbox" name="">
                                  <label class="padding-top-5 padding-bottom-5 ">Action</label>
                                </div><div class="checkbox-container ">
                                  <input type="checkbox" name="">
                                  <label class="padding-top-5 padding-bottom-5 ">Action</label>
                                </div><div class="checkbox-container ">
                                  <input type="checkbox" name="">
                                  <label class="padding-top-5 ">Action</label>
                                </div>    
                              </div>
                              <div class="category-heading padding-t-b">
                                <label>Title</label>
                                <i data-toggle="collapse"  class="fa fa-chevron-down pull-right"></i>
                              </div>
                              <div id="category2" class="category-2-collapse collapse border-bottom padding-left-30">
                                <div class="checkbox-container">
                                  <input type="checkbox" name="">
                                  <label class="padding-top-5 padding-bottom-5 ">Health & fitness</label>
                                </div><div class="checkbox-container">
                                  <input type="checkbox" name="">
                                  <label class="padding-top-5 padding-bottom-5 ">Health & fitness</label>
                                </div><div class="checkbox-container">
                                  <input type="checkbox" name="">
                                  <label class="padding-top-5 padding-bottom-5 ">Health & fitness</label>
                                </div><div class="checkbox-container">
                                  <input type="checkbox" name="">
                                  <label class="padding-top-5 padding-bottom-5 ">Health & fitness</label>
                                </div>    
                              </div>
                              <div class="category-heading padding-t-b">
                                <label>Title</label>
                                <i data-toggle="collapse"  class="fa fa-chevron-down pull-right"></i>
                              </div>
                              <div id="category3" class="category-3-collapse collapse padding-left-30">
                                <div class="checkbox-container">
                                  <input type="checkbox" name="">
                                  <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                </div>       <div class="checkbox-container ">
                                  <input type="checkbox" name="">
                                  <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                </div>       <div class="checkbox-container ">
                                  <input type="checkbox" name="">
                                  <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                </div>       <div class="checkbox-container ">
                                  <input type="checkbox" name="">
                                  <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                </div>       <div class="checkbox-container ">
                                  <input type="checkbox" name="">
                                  <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                </div>    
                              </div> 
                              <div class="category-heading padding-t-b">
                                <label>Title</label>
                                <i data-toggle="collapse"  class="fa fa-chevron-down pull-right"></i>
                              </div>
                              <div id="category4" class="category-4-collapse collapse padding-left-30">
                                <div class="checkbox-container">
                                  <input type="checkbox" name="">
                                  <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                </div>       <div class="checkbox-container ">
                                  <input type="checkbox" name="">
                                  <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                </div>       <div class="checkbox-container ">
                                  <input type="checkbox" name="">
                                  <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                </div>       <div class="checkbox-container ">
                                  <input type="checkbox" name="">
                                  <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                </div>       <div class="checkbox-container ">
                                  <input type="checkbox" name="">
                                  <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                </div>    
                              </div>
                              <div class="category-heading padding-t-b padding-bottom-20">
                                <label>Title</label>
                                <i data-toggle="collapse"  class="fa fa-chevron-down collapser pull-right"></i>
                              </div>
                              <div id="category5" class="category-4-collapse collapse padding-left-30">
                                <div class="checkbox-container">
                                  <input type="checkbox" name="">
                                  <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                </div>       <div class="checkbox-container ">
                                  <input type="checkbox" name="">
                                  <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                </div>       <div class="checkbox-container ">
                                  <input type="checkbox" name="">
                                  <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                </div>       <div class="checkbox-container ">
                                  <input type="checkbox" name="">
                                  <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                </div>       <div class="checkbox-container ">
                                  <input type="checkbox" name="">
                                  <label class="padding-top-5 padding-bottom-5 ">Science & Technology </label>
                                </div>    
                              </div>
                            </div>
                    </div>            
                  <hr>
                  </div>
              </div>             
           </div>
        
          </div>
        </div>

    </section >
  </div>
   
    <script type="text/javascript">
        $(document).ready(function() {
                
            $('.fa-chevron-down').click(function() {
                $(this).parent().next().collapse('toggle');
            });          

        });
      
    </script>
    </body>
</html>