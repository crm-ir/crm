var keycloak = new Keycloak();
  
var serviceUrl = 'http://127.0.0.1:8080/service'

function notAuthenticated() {

    document.getElementById('not-authenticated').style.display = 'block';
    document.getElementById('authenticated').style.display = 'none';
    
}

function authenticated() {
    document.getElementById('not-authenticated').style.display = 'none';
    document.getElementById('authenticated').style.display = 'block';
    document.getElementById('message').innerHTML = 'User: ' + keycloak.tokenParsed['preferred_username'];
}
 

function request(endpoint) {
    var req = function() {
        var req = new XMLHttpRequest();
        var output = document.getElementById('message');
        req.open('GET', serviceUrl + '/' + endpoint, true);

        if (keycloak.authenticated) {
            req.setRequestHeader('Authorization', 'Bearer ' + keycloak.token);
        }

        req.onreadystatechange = function () {
            if (req.readyState == 4) {
                if (req.status == 200) {
                    output.innerHTML = 'Message: ' + JSON.parse(req.responseText).message;
                } else if (req.status == 0) {
                    output.innerHTML = '<span class="error">Request failed</span>';
                } else {
                    output.innerHTML = '<span class="error">' + req.status + ' ' + req.statusText + '</span>';
                }
            }
        };

        req.send();
    };

    if (keycloak.authenticated) {

        keycloak.updateToken(30).success(req);

    } else {
        req();
    }
}

window.onload = function () {
// var DEBUG = false;
// if(!DEBUG){
//     if(!window.console) window.console = {};
//     var methods = [ "debug", "warn", "info"];
//     for(var i=0;i<methods.length;i++){
//         console[methods[i]] = function(){};
//     }
// }
    keycloak.init({ onLoad: 'login-required', checkLoginIframeInterval: 1 }).success(function () {

        if (keycloak.authenticated) {
            
             
             authenticated();
// updateLocalStorage();
            console.log("logged in");
            console.log(keycloak.tokenParsed.email);
        } else {
            notAuthenticated();
             console.log("not logged in");
        }

        document.body.style.display = 'block';
 
         
    });
         
}

keycloak.onAuthLogout = notAuthenticated;
 
 
var i = setInterval(function(){
    // do your thing
 
    if(keycloak.tokenParsed==undefined) {
        // console.log("wait");
    }
    else{
        clearInterval(i);
         console.log("logged in");
         console.log(keycloak.tokenParsed.email);
          document.getElementById('username').innerHTML=keycloak.tokenParsed.preferred_username;

    }
}, 200);
  setTimeout(function(){ console.log(keycloak.tokenParsed); }, 5000);
