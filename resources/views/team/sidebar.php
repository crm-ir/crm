<?php 
$page_name="team";    

$path = $_SERVER['DOCUMENT_ROOT']."/crm/resources/views/header.php";
include_once($path); 
 $panel='';
$panel=@$_GET['panel'];
if(empty($panel)){
  $panel=1;
}

?>
  <script src="https://users.enteroteam.com/auth/js/keycloak.js"></script>

<script src="https://cdn.pubnub.com/sdk/javascript/pubnub.4.21.7.min.js"></script>
  
    <script src="app.js"></script>
  <link href="<?php echo ROOT_URI; ?>/assets/css/temp.css" rel="stylesheet" type="text/css">

    <section class="padding-top-0" id="main-section">
        <!-- main content starts-->
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
            <!-- side bar starts -->
            <div class="col-md-2 col-lg-2 col-sm-3 col-xs-3 hidden-xs border-right no-lr-pad sidebar-static">
              <!-- for name of website -->
              <div class="h1-for-set-h col-md-12 col-lg-12 col-sm-12 col-xs-12 border-bottom no-lr-pad text-center margin-top-5 padding-bottom-5">
              
                <img src="<?php echo ROOT_URI; ?>/assets/img/logo_entero.png" class="img-responsive center-image" width="130">      
              </div>
              <!-- menu panel starts  -->
              <div class="h2-for-set-h col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad menu_box_sc" style="overflow-y:auto;">
                <!-- sub heading of menu -->
                <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12  ">
                    <p class="font-size-12 margin-top-20 color-grey-1">Main Menu</p>
                </div>
                <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12  menu-list-side" id="panel-1">
                    <a  class="a-deco-grey">
                      <img class="img-responsive float-inline" src="<?php echo ROOT_URI; ?>/assets/img/assets/dashboard-grey.svg">
                      <p class="margin-top-bot-2 side-menu-pad-img float-inline"><b>Dashboard</b></p>
                    </a>
                </div>
                <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12  menu-list-side" id="panel-2">
                  <a  class="a-deco-grey">
                    <img class="img-responsive float-inline" src="<?php echo ROOT_URI; ?>/assets/img/assets/my-task-grey.svg">
                    <p class="margin-top-bot-2 side-menu-pad-img float-inline "><b>My Tasks</b></p>
                  </a>
                </div>
                <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12  menu-list-side" id="panel-8">
                  <a  class="a-deco-grey">
                    <img class="img-responsive float-inline" src="<?php echo ROOT_URI; ?>/assets/img/assets/call-grey.svg">
                    <p class="margin-top-bot-2 side-menu-pad-img float-inline "><b>Call Plan</b></p>
                  </a>
                </div>
                  <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12  menu-list-side" id="panel-91">
                  <a  class="a-deco-grey">
                    <img class="img-responsive float-inline margin-top-10" src="<?php echo ROOT_URI; ?>/assets/img/assets/misscall-grey.svg">
                    <p class="margin-top-bot-2 side-menu-pad-img float-inline "><b>Missed Calls</b> <span  class="badge label-danger" id="misscall_status"></span></p>
                  </a>
                </div>
                <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12  menu-list-side" id="panel-92">
                  <a  class="a-deco-grey">
                    <img class="img-responsive float-inline margin-top-0" src="<?php echo ROOT_URI; ?>/assets/img/assets/recording_order-grey.svg">
                    <p class="margin-top-bot-2 side-menu-pad-img float-inline "><b>IVR  Orders</b> <span  class="badge label-danger" id="ivr_order"></span></p>
                  </a>
                </div>
                <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12  menu-list-side" id="panel-3">
                  <a  class="a-deco-grey">
                    <img class="img-responsive float-inline" src="<?php echo ROOT_URI; ?>/assets/img/assets/reports-grey.svg">
                    <p class="margin-top-bot-2 side-menu-pad-img float-inline"><b>My Report</b></p>
                  </a>
                </div>
               
                <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12  menu-list-side" id="panel-9">
                  <a  class="a-deco-grey">
                    <img class="img-responsive float-inline" src="<?php echo ROOT_URI; ?>/assets/img/assets/reports-grey.svg">
                    <p class="margin-top-bot-2 side-menu-pad-img float-inline"><b>Call Logs</b></p>
                  </a>
                </div>
                <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12  menu-list-side" id="panel-10">
                  <a  class="a-deco-grey">
                    <img class="img-responsive float-inline" src="<?php echo ROOT_URI; ?>/assets/img/assets/reports-grey.svg">
                    <p class="margin-top-bot-2 side-menu-pad-img float-inline"><b>Add Distributor</b></p>
                  </a>
                </div>
                <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12  menu-list-side" id="panel-4">
                  <a  class="a-deco-grey">
                    <img class="img-responsive float-inline" src="<?php echo ROOT_URI; ?>/assets/img/assets/cases-grey.svg">
                    <p class="margin-top-bot-2 side-menu-pad-img float-inline"><b>Cases</b></p>
                  </a>
                </div>
                <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12  menu-list-side" id="panel-5">
                  <a  class="a-deco-grey">
                    <img class="img-responsive float-inline" src="<?php echo ROOT_URI; ?>/assets/img/assets/my-customer-grey.svg">
                    <p class="margin-top-bot-2 side-menu-pad-img float-inline"><b>My Customers</b></p>
                  </a>
                </div>
                <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12  menu-list-side" id="panel-6">
                  <a  class="a-deco-grey">
                    <img class="img-responsive float-inline" src="<?php echo ROOT_URI; ?>/assets/img/assets/my-team-grey.svg">
                    <p class="margin-top-bot-2 side-menu-pad-img float-inline"><b>My Team</b></p>
                  </a>
                </div>
                 <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12  menu-list-side" id="panel-61">
                  <a  class="a-deco-grey">
                    <img class="img-responsive float-inline" src="<?php echo ROOT_URI; ?>/assets/img/assets/priority-grey.svg" width="27" style="margin-left:-5px;">
                    <p class="margin-top-bot-2 side-menu-pad-img float-inline" ><b>Call Priority</b></p>
                  </a>
                </div>
               <!--  <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12  menu-list-side" id="panel-7">
                  <a  class="a-deco-grey">
                    <img class="img-responsive float-inline" src="<?php //echo ROOT_URI; ?>/assets/img/assets/my-customer-grey.svg">
                    <p class="margin-top-bot-2 side-menu-pad-img float-inline"><b>Customer Signup</b></p>
                  </a>
                </div> -->
                <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12" >
                  
                </div>
                <!-- <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12  menu-list-side" id="panel-7">
                  <a class="a-deco-grey">
                    <img class="img-responsive float-inline" src="<?php echo ROOT_URI; ?>/assets/img/assets/help-support-grey.svg">
                    <p class="margin-top-bot-2 side-menu-pad-img float-inline"><b>Help and Support</b></p>
                  </a>
                </div> -->

              </div>
              <!-- menu panel ends  -->
              <!-- user panel bottom sidebar starts -->
              <div class="h3-for-set-h col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad border-top">
                <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad padding-top-10 padding-bottom-10 ">
                <img class="margin-left-10 margin-right-10 img-responsive float-inline user-face-pic " id="profile_pic_sidebar" src="<?php echo S3_BUCKET; ?>icons/dummy.png" >
                        
                      <div class="dropup" id="profile">
                        <a href="" class="a-deco-grey dropdown-toggle" data-toggle="dropdown"><i class="fa fa-ellipsis-v pull-right padding-right-20"></i> </a>  
                        <ul class="dropdown-menu pull-right">
                          <li>
                           <a data-toggle="modal" id="profile_update_modal" data-target="#profile-setting" class="a-deco"><i class="fa fa-cog padding-right-10" aria-hidden="true"></i> Profile Settings</a>
                          </li>
 
                          <li><a class="a-deco-grey"  name="logoutBtn" onclick="keycloak.logout()"><i class="fa fa-power-off padding-right-10" aria-hidden="true"></i> Logout</a></li>
 
                        </ul>
                      </div>
                    <p><b id="my_acc_name"><br><span class="font-size-12 color-grey-1"><!-- Distributor Partner --></span></b>
                
                    <br></p>
                </div>
                
                <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad border-top padding-top-10">  
                    <img src="<?php echo ROOT_URI; ?>/assets/img/assets/logo.png" class="  img-responsive center-image" width="150">          
                    <p class="text-center color-grey-1 padding-top-10 font-size-12">© Entero Customer Connect</p>
                </div>
              </div>
              <!-- user panel bottom sidebar ends -->
            </div>
            <!-- side bar ends -->
            <!-- right side main body starts -->
            <?php 
              // $path=ROOT_URI."/resources/views/team/dashboard.php";
               // $path="dashboard.php";
               $path="dashboard_2.php";
              if($panel==6){
                // $path = ROOT_URI."/resources/views/team/team.php";
                $path =  "team.php";
              }
              elseif ($panel==2) {
                 $path =  "task.php";
              }
              elseif ($panel==5) {
                 $path =  "customer.php";
              }
              elseif ($panel==4) {
                 $path =  "cases.php";
              }
              elseif($panel==3){
                 $path = "reports.php";
              }
              elseif($panel==7){
                $path = "add_customer.php";
              }
              elseif($panel==8){
                $path = "call_planer.php";
              }
              elseif($panel==9){
                $path = "call.php";
              }
              elseif($panel==91){
                $path = "misscall.php";
              }
              elseif($panel==92){
                $path = "recording_order.php";
              }
              elseif($panel==10){
                $path = "admin.php";
              }
              elseif($panel==61){
                $path = "priority.php";
              }
              else{}
                
                include_once($path); 
             ?>
             <!-- right side main body ends -->
        </div>
        <!-- main content ends -->
       
          <!-- Modals for sidebar  -->
          <!-- Profile setting modal  -->
          <div id="profile-setting" class="modal fade in" role="dialog">
           <div class="modal-dialog margin-top-70">

                <!-- Modal content-->
                <div class="modal-content col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
                  <div class="modal-header col-md-12 col-lg-12 col-sm-12 col-xs-12 ">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <p class="font-size-20 margin-left-10"><b>Profile Settings</b></p>
                  </div>
                  <div class="modal-body col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                      <div class="center-block profile-pic-container">
                        <img class="img-responsive profile-setting-pic center-block" data-profile_pic="" id="paste_profile_picture" src="<?php echo S3_BUCKET; ?>icons/dummy.png" width="100" height="100">
                        <img src="<?php echo ROOT_URI; ?>/assets/img/assets/edit-uploaded-image.svg" class="profile-edit img-circle back-color-brand a-deco pull-right" width="16" data-toggle="modal"  data-target="#change-profile-pic_modal">  
                      </div>
                      
                    </div>
                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                         <div class="form-group col-md-6 col-lg-6 col-sm-12 col-xs-12">
                            <p class="font-size-12 color-grey-1 margin-bottom--15"  for="">
                            FIRST NAME</p><br>
                            <input type="text" name="" class="form-control margin-top-5" id="first_name_profile" placeholder="Enter First Name" >   
                          </div>
                          <div class="form-group col-md-6 col-lg-6 col-sm-12 col-xs-12">
                            <span class="font-size-12 color-grey-1"  for="">LAST NAME</span>
                            <input type="text" name="" id="last_name_profile" class="form-control margin-top-5" placeholder="Enter Last Name" >
                          </div>
                          <div class="form-group col-md-12 col-lg-12 col-sm-12 col-xs-12">
                            <span class="font-size-12 color-grey-1"  for="">CALLING NO.</span>
                            <input type="text" name="" id="calling_no_profile" class="form-control margin-top-5" placeholder="Enter phone number" >
                          </div>
                          <div class="form-group col-md-12 col-lg-12 col-sm-12 col-xs-12">
                            <span class="font-size-12 color-grey-1"  for="">ENTERO DIRECT ID</span>
                            <input type="text" name="" id="ordergini_id_val" class="form-control margin-top-5" placeholder="Enter ENTERO DIRECT ID" readonly>
                          </div>
                    </div>

                   <!--  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <p class="font-size-12 text-bold text-center">Want to change the password. <a data-toggle="modal" data-target="#change-password_modal" class="a-deco">Click Here?</a></p>
                    </div> -->
                  </div>
                  <div class="modal-footer col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 padding-left-30 padding-right-30">
                        <button type="button" class="btn btn-default pull-left add-mem-btn1" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-default pull-right brand-btn add-mem-btn"  id="update_profile">Save</button>
                    </div>
                  </div>
                </div></div>
          </div>
          <!-- Profile setting modal ends here  -->
          
          <!-- Change Profile Pic modal  -->
          <div id="change-profile-pic_modal" class="modal fade in" role="dialog">
             <div class="modal-dialog margin-top-70">

                  <!-- Modal content-->
                  <div class="modal-content col-md-6 col-lg-6 col-sm-12 col-xs-12 col-md-offset-3 col-lg-offset-3 ">
                    <div class="modal-body col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                        <p class="font-size-20 text-bold text-center padding-bottom-20">Customer Connect</p>
                      </div>
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                          <input type="file" id="imgupload" style="display: none" > 
                          <div class="center-block upload-profile-pic-icon a-deco " id="choose_image">           
                            <img src="" id="profile_pic_changed" class="center-block upload-profile-icon1"> 
                          </div>  

                            <p class="text-center margin-bottom-30 text-bold color-grey-1">Set up your profile picture</p>
                      </div>                     
                      <div class="modal-footer col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
                          <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 padding-left-30 padding-right-30">
                              <button type="button" class="btn btn-default width-100 brand-btn add-mem-btn"  id="continue_profile">Continue</button>
                          </div>
                      </div>
                    </div>
                  </div>
              </div>
           </div>
          <!-- Change Profile Pic modal ends here  -->
 

          <!-- Change Password modal  -->
          <div id="change-password_modal" class="modal fade in" role="dialog">
             <div class="modal-dialog margin-top-70">
                  <!-- Modal content-->
                  <div class="modal-content col-md-12 col-lg-12 col-sm-12 col-xs-12 col-md-offset-3 col-lg-offset-1 ">
                    <div class="modal-body col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
                      <div class="modal-header col-md-12 col-lg-12 col-sm-12 col-xs-12 ">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <p class="font-size-20 margin-left-10"><b>Change Password</b></p>
                      </div>
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                          <div class="form-group col-md-12 col-lg-12 col-sm-12 col-xs-12 margin-top-15">
                            <p class="font-size-12 color-grey-1 margin-bottom--15"  for="">
                            CURRENT PASSWORD</p><br>
                            <input type="text" name="" class="form-control margin-top-5" placeholder="Current Password" id="">   
                          </div>
                          <div class="form-group col-md-12 col-lg-12 col-sm-12 col-xs-12">
                            <span class="font-size-12 color-grey-1"  for="">NEW PASSWORD</span>
                            <input type="text" name="" class="form-control margin-top-5" placeholder="New Password" id="">
                          </div>
                            <div class="form-group col-md-12 col-lg-12 col-sm-12 col-xs-12">
                            <p class="font-size-12 color-grey-1 margin-bottom--15"  for="">
                            REPEAT PASSWORD</p><br>
                            <input type="text" name="" class="form-control margin-top-5" placeholder="Repeat Password" id="">   
                          </div>
                      </div>                     
                      <div class="modal-footer col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad">
                          <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12 padding-left-30 padding-right-30 pull-left">
                              <button type="button" class="btn btn-default width-100 brand-btn-white add-mem-btn pull-left"  id="">Cancel</button>
                          </div> 
                          <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12 padding-left-30 padding-right-30 pull-right">
                              <button type="button" class="btn btn-default width-100 brand-btn add-mem-btn pull-right"  id="">Save</button>
                          </div>
                      </div>
                    </div>
                  </div>
              </div>
           </div>
          <!-- Change Password modal ends here  -->


          <!-- Modals for sidebar ends here  -->
    </section>

    <div id="incoming_call_panel" class="modal fade" role="dialog">
      <div class="modal-dialog modal-sm">
        <!-- Modal content-->
        <div class="modal-content back-call">
          <div class="modal-header" style="border-color:#edebed;">
            <button type="button" class="close" style="color:#000 !important;opacity:1;" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body back-call" >
            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                <h3 class="text-center margin-top-0" id="call_name_pop"></h3>
                <h4 class="text-center margin-top-20 text-bold" id="call_number_pop"></h4>
                <h4 class="text-center margin-top-20">Incoming Call</h4>
            </div>
            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 margin-bottom-20">
              <div class="col-md-3 col-lg-3 col-sm-3 col-xs-3">
              </div>
              <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
                <img src="<?php echo ROOT_URI; ?>/assets/img/call.gif" class="img-responsive">
              </div>
              <div class="col-md-3 col-lg-3 col-sm-3 col-xs-3">
              </div>
            </div>
          </div>
          <div class="modal-footer" style="border-color:#edebed;">
            <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
          </div>
        </div>

      </div>
    </div>

    <script type="text/javascript">
    $(document).ready(function() {
          // var base_window_h_final = $(window).height()-$(".h1-for-set-h").height()-$(".h2-for-set-h").height()-142;
          var base_window_h_final1 = $(window).height()-$(".h1-for-set-h").height()-142;
          // $('.h3-for-set-h').css({'margin-top':(base_window_h_final)+'px'});
           $('.h2-for-set-h').css({'height': base_window_h_final1+'px'});

          var panel = "<?php echo $panel;?>";
          $(".menu-list-side").removeClass("active-side-bar-list");
          $("#panel-"+panel).addClass('active-side-bar-list');    

          var src_panel_img = $("#panel-"+panel).children('a').children("img").attr('src'); 
          if(src_panel_img!=undefined){
            var src_panel_img_temp = src_panel_img.substring(0, src_panel_img.length - 8);
            $("#panel-"+panel).children('a').children("img").attr('src',src_panel_img_temp+'blue.svg');
          }
          $(document).on('click', '.menu-list-side', function(){
            var temp_id_to_reload = $(this).attr("id");
            var temp_id_to_reload_temp = temp_id_to_reload.substring(6);
            
            window.location.href="<?php echo DOMAIN_BASE;?>/resources/views/team/sidebar.php?panel="+temp_id_to_reload_temp;
          });

           $(function() {
            return $(".modal").on("show.bs.modal", function() {
              var curModal;
              curModal = this;
              $(".modal").each(function() {
                if (this !== curModal) {
                  $(this).modal("hide");
                }
              });
            });
          });

           // ajax for session set
           $.ajax({
                url: '<?php echo ROOT_URI; ?>/resources/services/set_session_web.php',
                method: 'POST',
                data: "{\n\"dist\":\""+ sessionStorage.getItem("dist_id")+"\",\n\"action\":\"get_miss\"\n}",
                dataType: 'json', 
            }).success(function(resp) {
 
                   if(resp.response_code==200){
                        $('#misscall_status').html(resp.data);
                        console.log(resp.data);

                   }else{
                      $('#misscall_status').hide();
                   }      
            });//ajax 

           
           var user_role = sessionStorage.getItem("role");
           if(user_role == 4){
              $('#panel-6').hide();
              $('#panel-8').hide();
              // $('#panel-9').hide();
              $('#panel-10').hide();
              $('#panel-61').hide();


           }
           else if(user_role == 3){
              $('#panel-10').hide();
              $('#panel-61').hide();

           }
           else if(user_role == 2){
              $('#panel-10').hide();

           }
           else if(user_role == 1){
            $('#panel-1 ,#panel-2 ,#panel-3 ,#panel-4 ,#panel-5 ,#panel-6 ,#panel-7 ,#panel-8 ,#panel-9,#panel-61').hide();
            
           }

           $(document).on('click','#profile_update_modal',function(){

             var user_name = sessionStorage.getItem("name");
             var phone     = sessionStorage.getItem("calling_no");
             var profile_pic = sessionStorage.getItem("profile_pic");
             var og_id     = sessionStorage.getItem("og_id");
                //toast_it(user_name);
             // var firstName = user_name.split(' ').slice(0, -1).join(' ');
             // var lastName  = user_name.split(' ').slice(-1).join(' ');
             var full_name = user_name.split(' ');
             //var name = full_name.split(' ');
             var firstName = full_name[0];
             var lastName = user_name.substring(full_name[0].length).trim();

             console.log(firstName);
             console.log(lastName);


                $('#first_name_profile').val(firstName);
                $('#last_name_profile').val(lastName);
                $('#calling_no_profile').val(phone);
                $('#ordergini_id_val').val(og_id);
                $('#paste_profile_picture').attr('data-profile_pic',profile_pic);
                $('#paste_profile_picture').attr('src','<?php echo S3_BUCKET; ?>'+profile_pic);

           });

           $(document).on('click','#update_profile',function(){

              var firstName    = $('#first_name_profile').val();
              var lastName     = $('#last_name_profile').val();
              var calling_no   = $('#calling_no_profile').val();
              var ordergini_id = $('#ordergini_id_val').val();
              var profile_pic  = $('#paste_profile_picture').attr('data-profile_pic');
            
             // console.log(profile_pic); return false;
              if(calling_no=='' || calling_no==' '){
              toast_it("phone number is mandatory");
              return false;
              }

              if(firstName == ''){
                toast_it("First name cannot be empty");
                return false;
              }

              if(!ordergini_id){
                toast_it('Ordergini Id cannot be empty');
                return false;
              }


              var fullname  = firstName+" "+lastName;

               $.ajax({
                       url : "<?php echo ROOT_URI; ?>/resources/services/modify_user.php",
                       type : 'POST',
                       data : "{\n\"user_id\":\""+sessionStorage.getItem('user_id')+"\",\n\t\"name\":\""+fullname+"\"\n,\n\t\"firstname\":\""+firstName+"\"\n,\n\t\"lastname\":\""+lastName+"\"\n,\n\t\"calling_no\":\""+calling_no+"\"\n,\n\t\"ordergini_id\":\""+ordergini_id+"\"\n,\n\t\"profile_pic\":\""+profile_pic+"\"\n}",

                       processData: false,  // tell jQuery not to process the data
                       contentType: false,  // tell jQuery not to set contentType 
                       dataType: 'JSON',
                             success: function(response) {

                              if(response.response_code == 200){
                                sessionStorage.setItem("profile_pic",profile_pic);
                                sessionStorage.setItem("name", fullname);
                                sessionStorage.setItem("phone",calling_no);
                                $('#my_acc_name').text(fullname);
                                $('#profile_pic_sidebar').attr('src','<?php echo S3_BUCKET; ?>'+profile_pic);
                                toast_it("User profile updated successfully");
                                setInterval(function(){ location.reload();}, 1200);
                              }                              
                             }
                             // error: function() {  
                             //   toast_it("There was an error.");
                             // }
                }); //ajax close 

           });

           $(document).on('click','#choose_image',function(){
                $('#imgupload').trigger('click');
           });

           $("#imgupload").on("change",function(){
               
               var feat_img = new FormData();
               var file_data = $("#imgupload").prop("files")[0];
               //console.log(file_data); return false;
               var fileType = file_data["type"];
               var ValidImageTypes = ["image/gif", "image/jpeg", "image/png", "image/jpeg"];
               if ($.inArray(fileType, ValidImageTypes) < 0){
                     // invalid file type code goes here.
                     toast_it("Upload a valid feature image");
                     return false;
               }
               feat_img.append("file", file_data);
               feat_img.append("id",sessionStorage.getItem("user_id") );

              $.ajax({
               url : "<?php echo ROOT_URI; ?>/resources/services/upload_image.php",
               type : 'POST',
               data : feat_img,
               processData: false,  // tell jQuery not to process the data
               contentType: false,  // tell jQuery not to set contentType
              beforeSend: function()
                {  
                   $('.overlay1').show();
                
                },

              success : function(resp){
                    //console.log(resp.data);
                 $('#paste_profile_picture').attr('data-profile_pic',resp.data);
                 $('#profile_pic_changed').attr('src','<?php echo S3_BUCKET; ?>'+resp.data);
                 $('#profile_pic_changed').addClass("upload-profile-icon").removeClass("upload-profile-icon1");
                 $('#paste_profile_picture').attr('src','<?php echo S3_BUCKET; ?>'+resp.data);
              }
              }); // close ajax.


          });  // close on change feature image.

          $(document).on('click','#continue_profile',function(){

              $('#profile-setting').modal('show');
          });

    });//document ready
     
    </script>
    <script>
      // $('#incoming_call_panel').modal('show');
      
      const pubnub = new PubNub({
        publishKey: "pub-c-add7802c-5963-49f2-b79e-81999f1d87db",
        subscribeKey: "sub-c-9d06025e-db85-11e9-b2a7-e243f66d3f10"
      });
      var phon_ch =  sessionStorage.getItem("calling_no").substr(sessionStorage.getItem("calling_no").length - 10);
      pubnub.subscribe({
        channels: [phon_ch] 
      });

      pubnub.addListener({
        message: function(event) {
          if(event.message.status=="busy"){
            console.log("live");
             // ajax for customer number
            // $.ajax({
            //     url: '<?php echo ROOT_URI; ?>/resources/services/customer_api.php',
            //     method: 'POST',
            //     data: "{\n\"dist_id\":\""+ sessionStorage.getItem("dist_id")+"\",\n\"token\":\""+sessionStorage.getItem('token')+"\",\n\"phone\":\""+event.message.callnumber+"\",\n\"action\":\"get_incoming_number\"\n}",
            //     dataType: 'json', 
            // }).success(function(resp) {
 
            //        if(resp.response_code==200){
                         
            //             $('#incoming_call_panel').modal('show');
            //             $("#call_number_pop").text(event.message.callnumber); 

            //             $("#call_name_pop").text(resp.data); 

            //        }else{
            //             $('#incoming_call_panel').modal('show');
            //             $("#call_number_pop").text(event.message.callnumber); 

            //             $("#call_name_pop").text("Number not found");
                        
                       
            //        }      
            // });//ajax

            $('#incoming_call_panel').modal('show');
            $("#call_number_pop").text(event.message.callnumber); 
            $('#paste_phone_number_customer_screen').html(event.message.callnumber);   
            $('#paste_phone_icon').attr('src','<?php echo ROOT_URI; ?>/assets/img/assets/call-blue.svg');  
          }
          if(event.message.status=="free"){
             $('#incoming_call_panel').modal('hide');
             $("#call_number_pop").text(''); 
          }
          
           sessionStorage.setItem("number_to_send_order",event.message.callnumber);
        }
      });
     

    </script>
    </body>
</html>