<?php
class task_class
{
	private $_db, $dt,$_user_obj;

	public function __construct($user=null)
	{
		$this->_db = DB::getInstance();
		$this->_user_obj = user::getInstance_of_user();
		date_default_timezone_set('Asia/Calcutta');
        $this->dt = date('Y-m-d H:i:s');
	}
	//Used to add a new task
	 public function add_task($task,$cuid,$auid,$cus_id,$dat){
   
      $sql ="INSERT INTO tasks(task,creator_user_id,assigned_user_id,customer_id,task_when) VALUES(?,?,?,?,?)";
      $result = $this->_db->query($sql,array($task,$cuid,$auid,$cus_id,$dat));
      //print_r($result->_errorInfo);
      
   	  if($result->_error)
   		{
            return 0;
        }
        else
        {
	 		return 1;
	  	}
	}
	public function task_details($agent_id,$condition,$filter,$role,$page_no,$limit,$token,$dist_id)
	{
		$cond="";
			$current_time=date('Y-m-d H:i:s');
			$user_str='';
			$cond4='';
			if($filter==1)
			{
				$new_time = date("Y-m-d H:i:s", strtotime('-3 hours', strtotime($current_time)));
				$cond4="AND ts.created_date >='".$new_time."' AND ts.status=1";
			}
			elseif($filter==2)
			{
				$new_time = date("Y-m-d H:i:s", strtotime('-3 hours', strtotime($current_time)));
				$new_time1 = date("Y-m-d H:i:s", strtotime('-12 hours', strtotime($current_time)));
				$cond4="AND ts.created_date >='".$new_time1."' AND ts.created_date <='".$new_time."' AND ts.status=1";
			}
			elseif($filter==4)
			{
				
				$new_time = date("Y-m-d H:i:s", strtotime('-12 hours', strtotime($current_time)));
				$new_time1 = new DateTime();
				$new_time1->setTime(0,0);
				$new_time1 = $new_time1->format('Y-m-d 00:00:00');
				$cond4="AND ts.created_date >='".$new_time1."' AND ts.created_date <='".$new_time."' AND ts.status=1";
			}
			elseif($filter==3)
			{
				$new_time1 = new DateTime();
				$new_time1->setTime(0,0);
				$new_time1 = $new_time1->format('Y-m-d 00:00:00');
				$cond4="AND ts.created_date >='".$new_time1."' AND ts.status=0";
			}
			else
			{
				$new_time1 = new DateTime();
				$new_time1->setTime(0,0);
				$new_time1 = $new_time1->format('Y-m-d 00:00:00');
				// print_r($new_time1);
				$cond4="AND ts.created_date >='".$new_time1."'";
			}


			//print_r($agent_id);
			$user_id_dorm=$this->_user_obj->get_userid($agent_id,$role);
			//print_r($user_id_dorm);
			foreach ($user_id_dorm as $key => $value) {
				$user_str.=",'".$value."'";
			}
			$user_str=ltrim($user_str,',');
		
			// $user_str=implode(",",$user_id);x
			
			//echo $user_str;
			$cond1=" AND ts.assigned_user_id IN(".$user_str.")";
		
			$cond2=" ORDER BY ts.created_date DESC, ts.time_to ASC";
			$cond3=" LIMIT $page_no,$limit";
			if($condition==2)
			{
				$sql="SELECT cl.customer_id,max(cl.`created_date`) as crt_date,ts.task_id,ts.task,ts.assigned_user_id,ts.task_when,ts.time_to,ts.status,ts.follow_up,ts.created_date FROM `calls` as cl join tasks as ts on cl.task_id=ts.task_id WHERE 1 ".$cond4.$cond1."group by cl.customer_id order by crt_date desc".$cond3;
				$result = $this->_db->query($sql)->results();
			//print_r($result);
			//die();
			return $result;
			}
			else if($condition==3)
			{
				$query="";
				$sql="SELECT cl.customer_id,max(cl.`created_date`) as crt_date,ts.task_id,ts.task,ts.assigned_user_id,ts.task_when,ts.time_to,ts.status,ts.follow_up,ts.created_date FROM `calls` as cl join tasks as ts on cl.task_id=ts.task_id WHERE 1 ".$cond4.$cond1."group by cl.customer_id ";
				$result = $this->_db->query($sql)->results();
				
				$i=0;
				foreach ($result as $key => $value) {
					$temp=array();
					$query.="{\"DistID\":".$dist_id.",\"Chemist_ID\":".$value->customer_id."},";
					//$temp['DistID']=1;
					//$temp['Chemist_ID']=$value->customer_id;
					//$query['LastOrder'][$i++]=$temp;
				}
				$query=rtrim($query,',');
//				echo $query;
				//$query.="]}";

$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => ORDERGINI_API."/api/CRMServices/GetCustomerLastOrder",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_POSTFIELDS => "{\"LastOrder\":[".$query."]}",
  CURLOPT_HTTPHEADER => array(
    "Authorization: Bearer ".$token,
    "Content-Type: application/json",
    "Postman-Token: bee2ffbc-0519-47f8-b59b-3fdadf1135a4",
    "cache-control: no-cache"
  ),
));

$res = curl_exec($curl);
//print_r($res);
//print_r($curl);
$err = curl_error($curl);

curl_close($curl);

			if ($err) {
			  return 0;

			} else {
			  $output=json_decode($res);
			  //print_r($output);
			  //die();
			 $key=0;
			 $re=array();
			 $i=0;
			  foreach($output->GetCustomerLastOrder as $value)
			  {
//			  	print_r($value);
			  	foreach ($result as $k => $v) {

			  		if($v->customer_id==$value->CustomerID)
			  		{
			  			$re[$i++]=$v;
			  		}
			  		# code...
			  	}
			
			}
			//print_r($re);
			//die();			

return $re;
}	
		}
		else if($condition==4)
			{
				$query="";
				$sql="SELECT cl.customer_id,max(cl.`created_date`) as crt_date,ts.task_id,ts.task,ts.assigned_user_id,ts.task_when,ts.time_to,ts.status,ts.follow_up,ts.created_date FROM `calls` as cl join tasks as ts on cl.task_id=ts.task_id WHERE 1 ".$cond4.$cond1."group by cl.customer_id ";
				$result = $this->_db->query($sql)->results();
				
				$i=0;
				foreach ($result as $key => $value) {
					$temp=array();
					$query.="{\"DistID\":".$dist_id.",\"Chemist_ID\":".$value->customer_id."},";
					//$temp['DistID']=1;
					//$temp['Chemist_ID']=$value->customer_id;
					//$query['LastOrder'][$i++]=$temp;
				}
				$query=rtrim($query,',');
//				echo $query;
				//$query.="]}";

$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => ORDERGINI_API."/api/CRMServices/GetCustomerOrderFrq",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_POSTFIELDS => "{\"Orderfrq\":[".$query."]}",
  CURLOPT_HTTPHEADER => array(
    "Authorization: Bearer ".$token,
    "Content-Type: application/json",
    "Postman-Token: bee2ffbc-0519-47f8-b59b-3fdadf1135a4",
    "cache-control: no-cache"
  ),
));

$res = curl_exec($curl);
//print_r($res);
//print_r($curl);
$err = curl_error($curl);

curl_close($curl);

			if ($err) {
			  return 0;

			} else {
			  $output=json_decode($res);
			//print_r($res);
			  //die();
			 $key=0;
			 $re=array();
			 $i=0;
			  foreach($output->GetCustomerOrderFrq as $value)
			  {
//			  	print_r($value);
			  	foreach ($result as $k => $v) {

			  		if($v->customer_id==$value->ChemistID)
			  		{
			  			$re[$i++]=$v;
			  		}
			  		# code...
			  	}
			
			}
			//print_r($re);
			//die();			

return $re;
}	
		}
			
			else{
			$sql="SELECT ts.task_id,ts.task,ts.created_date,ts.assigned_user_id,ts.status,ts.task_when,ts.time_to,ts.customer_id,ts.follow_up from tasks as ts WHERE 1 ".$cond4.$cond1.$cond2.$cond3;
			$result = $this->_db->query($sql)->results();
			//print_r($result);
			//die();
			return $result;
			}
	}
	 public function count_remark_all($entity_id,$entity_type){
	   $sql="SELECT count(*) as total_remark from remarks WHERE entity_id=? AND entity_type=? ";
	   //echo $sql;
	   $result = $this->_db->query($sql,array($entity_id,$entity_type))->results();
	   return $result[0]->total_remark;
	}
	public function close_task($task_id)
	{
		$sql = "update tasks set status = 0 where task_id = ?";
        $result = $this->_db->query($sql,array($task_id));
        if($result){
            return 1;
        }else{
            return 0;
        }
	}
	public function reassign_task($task_id,$assign_cust_id,$date_time)
	{
		$sql = "update tasks set assigned_user_id= ?,task_when=? where task_id = ?";
        $result = $this->_db->query($sql,array($assign_cust_id,$date_time,$task_id));
        //print_r($result);
        if($result){
            return 1;
        }else{
            return 0;
        }
	}
	public function follow_up($task_id,$follow_up)
	{

		date_default_timezone_set('Asia/Calcutta');
        $result_date = date('Y-m-d H:i:s');


		$sql = "update tasks set follow_up= ?, status = 1,created_date='$result_date' where task_id = ? ";
        $result = $this->_db->query($sql,array($follow_up,$task_id));
        if($result){
            return 1;
        }else{
            return 0;
        }
	}

	public function get_user_task($user_id){

		$new_time1 = new DateTime();
		$new_time1->setTime(0,0);
		$new_time1 = $new_time1->format('Y-m-d 00:00:00');
		// print_r($new_time1);
		$cond4  = "AND created_date >='".$new_time1."'";

		$sql 	= "SELECT count(task_id) as task FROM tasks WHERE assigned_user_id = '$user_id' $cond4 AND status = 1";
	    $result = $this->_db->query($sql)->results();

	    return $result[0]->task;

	}

	public function reassign_all_tasks($currently_assigned_to,$new_assign_tasks_to){

		$new_time1 = new DateTime();
		$new_time1->setTime(0,0);
		$new_time1 = $new_time1->format('Y-m-d 00:00:00');
		// print_r($new_time1);
		$cond4  = "AND created_date >='".$new_time1."'";

		$sql = "UPDATE tasks SET assigned_user_id = '$new_assign_tasks_to' WHERE assigned_user_id = '$currently_assigned_to' $cond4 ";
		$result = $this->_db->query($sql);
        if($result){
            return 1;
        }else{
            return 0;
        }

	}  // close function reassign all tasks

	}