<?php
$path_init = $_SERVER['DOCUMENT_ROOT']."/crm/resources/init.php";
include_once($path_init); 
header('Content-type: application/json');
$data = json_decode(file_get_contents('php://input'));
$token=$data->token;
$action=$data->action;
switch ($action) {
	case '20_most_ordered_company':
		
		$customer_id = $data->customer_id;
		$curl 		 = curl_init();

		$curl = curl_init();

		curl_setopt_array($curl, array(
		CURLOPT_URL => ORDERGINI_API."/api/CRMServices/GetChemistTop20OrderCompanies",
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 30,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "POST",
		CURLOPT_POSTFIELDS => "{\"ChemistID\":[".$customer_id."]}\r\n",
		CURLOPT_HTTPHEADER => array(
		"Authorization: Bearer ".$token ,
		"Cache-Control: no-cache",
		"Content-Type: application/json",
		"Postman-Token: 8fd9b8fc-523d-18ef-4de3-7fd56adb8f18"
		),
		));


		$response_company = curl_exec($curl);
		$response_company=json_decode($response_company);

		// print_r($response_company); die();
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		 $response['response_code']=400;
		 $response['response_message']="Something Went wrong";
	
		} 
		else {
		  $response['response_code']=200;
		  $response['response_message']="Succes";
		  $response['data']=$response_company->result;
		}
		break;
	
	case '20_most_ordered_product':
	
		$customer_id = $data->customer_id;
		$curl 		 = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => ORDERGINI_API."/api/CRMServices/GetChemistTop20OrderProduct",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => "{\"ChemistID\":[".$customer_id."]}",
		  CURLOPT_HTTPHEADER => array(
		    "Authorization: Bearer ".$token,
		    "Content-Type: application/json",
		    "Postman-Token: eff42c16-c7b5-4c39-aa09-bf1d039ff769",
		    "cache-control: no-cache"
		  ),
		));

		$response_company = curl_exec($curl);
		$response_company=json_decode($response_company);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		 $response['response_code']=400;
		 $response['response_message']="Something Went wrong";
	
		} 
		else {
		  $response['response_code']=200;
		  $response['response_message']="Succes";
		  $response['data']=$response_company->result;
		}
		break;
		
		case 'customer_details' :
		
		$customer_id = $data->customer_id;
		$curl 		 = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => ORDERGINI_API."/api/CRMServices/GetChemistDetails",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => "{\"ChemistID\":[".$customer_id."]}",
		  CURLOPT_HTTPHEADER => array(
		    "Authorization: Bearer ".$token,
		    "Content-Type: application/json",
		    "Postman-Token: eff42c16-c7b5-4c39-aa09-bf1d039ff769",
		    "cache-control: no-cache"
		  ),
		));

		$response_company = curl_exec($curl);
		$response_company=json_decode($response_company);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		 $response['response_code']=400;
		 $response['response_message']="Something Went wrong";
	
		} 
		else {
		  $response['response_code']=200;
		  $response['response_message']="Succes";
		  $response['data']=$response_company->result;
		}
		break;
	
	case 'last_5_order_details_items' :
		
		$customer_id = $data->customer_id;
		$curl 		 = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => ORDERGINI_API."/api/CRMServices/GetChemist5Orders",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => "{\"ChemistID\":[".$customer_id."]}",
		  CURLOPT_HTTPHEADER => array(
		    "Authorization: Bearer ".$token,
		    "Content-Type: application/json",
		    "Postman-Token: eff42c16-c7b5-4c39-aa09-bf1d039ff769",
		    "cache-control: no-cache"
		  ),
		));

		$response_company = curl_exec($curl);
		$response_company =json_decode($response_company);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		 $response['response_code']=400;
		 $response['response_message']="Something Went wrong";
	
		} 
		else {
		 $response['response_code']=200;
		 $response['response_message']="Succes";
		 $response['data']=$response_company->result;
		}
		break;

	case 'last_5_order_details' :
		
		$customer_id = $data->customer_id;
		$curl 		 = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => ORDERGINI_API."/api/CRMServices/GetChemistLast5Orders",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => "{\"ChemistID\":[".$customer_id."]}",
		  CURLOPT_HTTPHEADER => array(
		    "Authorization: Bearer ".$token,
		    "Content-Type: application/json",
		    "Postman-Token: eff42c16-c7b5-4c39-aa09-bf1d039ff769",
		    "cache-control: no-cache"
		  ),
		));

		$response_company = curl_exec($curl);
		$response_company=json_decode($response_company);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		 $response['response_code']=400;
		 $response['response_message']="Something Went wrong";
	
		} else {
		 $response['response_code']=200;
		 $response['response_message']="Succes";
		 $response['data']=$response_company->result;
		}
		break;

	case 'last_5_order_items' :
		
		$customer_id = $data->customer_id;
		$order_id 	 = $data->order_id;

		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => ORDERGINI_API."/api/CRMServices/ChemistOrderDetails",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => "{\"ChemistID\":[{\"ChemID\":".$customer_id.",\"OID\":".$order_id."}]}",
		  CURLOPT_HTTPHEADER => array(
		    "Authorization: Bearer ".$token,
		    "Content-Type: application/json",
		    "Postman-Token: eff42c16-c7b5-4c39-aa09-bf1d039ff769",
		    "cache-control: no-cache"
		  ),
		));

		$response_company = curl_exec($curl);
		$response_company=json_decode($response_company);



		// print_r($response_company);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		 $response['response_code']=400;
		 $response['response_message']="Something Went wrong";
	
		} else {
		 $response['response_code']=200;
		 $response['response_message']="Succes";
		 $response['data']=$response_company->result;
		}
		break;


	case 'orders_list_customer':

		$customer_id = $data->customer_id;
		$page_no   	 = $data->page_no;
		$token   	 = $data->token;

		$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => ORDERGINI_API."/api/CRMServices/ChemistOrderList",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => "{\"ChemistOrder\":[{\"ChemistID\":".$customer_id.",\"PageID\":".$page_no."}]}",
		  CURLOPT_HTTPHEADER => array(
		    "Authorization: Bearer ".$token,
		    "Content-Type: application/json",
		    "Postman-Token: eff42c16-c7b5-4c39-aa09-bf1d039ff769",
		    "cache-control: no-cache"
		  ),
		));

		$response_company = curl_exec($curl);
		// print_r($response_company); die();
		$response_company=json_decode($response_company);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		 $response['response_code']=400;
		 $response['response_message']="Something Went wrong";
	
		} else {
		 $response['response_code']=200;
		 $response['response_message']="Success";
		 $response['data']=$response_company->result;
		}

		break;
	
	default:
		# code...
		break;
}
echo json_encode($response);
?>