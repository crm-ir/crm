<?php
$path_init = $_SERVER['DOCUMENT_ROOT']."/crm/resources/init.php";
include_once($path_init); 
header('Content-type: application/json');
$data = json_decode(file_get_contents('php://input'));
$obj_case=new case_class();//creates object of task_class
$obj_call=new call();
$obj_user=new user();
$obj_remark=new remark();
$action=$data->action;
switch ($action) {
	case 'add_case':
		$curr_emmp_id=$data->current_empid;
		$cusid=$data->user;//cutomer id 
		$empid=$data->empid;//employe name
		$case=$data->casename;//casename
		$result=$obj_case->add_case($case,$curr_emmp_id,$empid,$cusid);//add case function called which add task 
		if($result==1){
			$response['response_message']= "CASE ADDED";
			$response['response_code'] = 200;
			}
		else{
			$response['response_message']= "UNSECUSSFUL";
			$response['response_code'] = 400;
			}
			break;

	case 'display_case':

		$token=$data->token;
		$user_id=$data->user_id;//user_id of user
		$page_no=$data->page_no;//page no 
		$role=$data->role;//role of user
		$limit=5;
		$condition = $data->condition;
		$filter = $data->filter;

		$response=array();
		$user_details_current=$obj_user->get_user_information($user_id);
$result=$obj_case->case_details($user_id,$condition,$filter,$role,($page_no-1)*$limit,$limit);
$res=array();
if(empty($result))
{
	$response['response_code']=400;
	$response['response_message']="Something Went Wrong";
}
else
{
	$customer_ids='';
	foreach($result as $keys=>$pair)
	{
		$customer_ids.=','.$pair->customer_id;
	}
	$customer_ids=ltrim($customer_ids,',');

	$curl = curl_init();
	curl_setopt_array($curl, array(
	  CURLOPT_URL => ORDERGINI_API."/api/CRMServices/GetAllChemistDetails",
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => "",
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 30,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_CUSTOMREQUEST => "POST",
	  CURLOPT_POSTFIELDS => "{\"ChemistDArry\":[".$customer_ids."]}",
	  CURLOPT_HTTPHEADER => array(
	    "Authorization: Bearer ".$token,
	    "Content-Type: application/json",
	    "Postman-Token: ee2b2d47-87ad-48e8-a522-8c96fb6c31a1",
	    "cache-control: no-cache"
	  ),
	));

	$response_cus_id= curl_exec($curl);

	$err = curl_error($curl);

	curl_close($curl);



if (!$err) {
$response_cus_id=json_decode($response_cus_id);
$i=0;
foreach ($result as $key => $row) 
	{
		$remark=$obj_remark->last_remark($row->case_id,2);
		if($remark)
		{
			$last_remark=$remark->remark;
			$user_details_remark=$obj_user->get_user_information($remark->user_id);
			// print_r($user_details_remark);die;
			$remark_user_profile_pic=$user_details_remark->profile_picture;
			$remark_user_name=$user_details_remark->name;
			$remark_user_role=$user_details_remark->role;
			$remark_created_date=$remark->created_date;
		}
		else{
			$last_remark=null;
			$remark_user_profile_pic=null;
			$remark_user_name=null;
			$remark_user_role=null;
			$remark_created_date=null;
		}

		//print_r($response_cus_id[$key]->ESCID);
		
		$user_details_assigned=$obj_user->get_user_information($row->assigned_user_id);
		$user_details_created=$obj_user->get_user_information($row->creator_user_id);
		$user_details_closing=$obj_user->get_user_information($row->closing_user_id);
		$user_details_cancelling=$obj_user->get_user_information($row->cancelled_user_id);

		$state=$row->status;
		$case_state = $row->case_state;
		if($state==1)
		{
		
		$diff = time()-strtotime($row->created_date);

		//$hr_diff=$interval->format('%H');
		if($diff<3*3600)
		{
			$status_label="NEW";
		}
		else
		{
			$status_label="OPEN";
		}
		}
		else
		{
			$status_label="CLOSED";
		}

		if($case_state == 0){

			$status_label="CANCELLED";
		}

		$tempArray = array(
	                             "case_name"=>$row->case_name,
	                             "case_id"=>$row->case_id,
	                             "case_created_date"=>$row->created_date,
	                             "case_assigned_date"=>$row->assigned_date,
	                             "case_close_date"=>$row->close_date,
	                             "case_cancel_date"=>$row->cancel_date,
	                             "assigned_user_id"=>$row->assigned_user_id,
	                             "closing_user_id"=>$row->closing_user_id,
	                             "cancelling_user_id"=>$row->cancelled_user_id,
	                             "assigned_user_name"=>$user_details_assigned->name,
	                             "assigned_user_profile_pic"=>$user_details_assigned->profile_picture,
	                             "closing_user_profile_pic"=>@$user_details_closing->profile_picture,
	                             "cancelling_user_profile_pic"=>@$user_details_cancelling->profile_picture,
	                             "closing_user_name"=>@$user_details_closing->name,
	                             "cancelling_user_name"=>@$user_details_cancelling->name,
	                             "user_name_current"=>$user_details_current->name,
	                             "user_id_current"=>$user_id,
	                             "user_current_profile_pic"=>$user_details_current->profile_picture,
	                             "creator_user_id"=>$row->creator_user_id,
	                             "creator_user_name"=>$user_details_created->name,
	                             "creator_profile_pic"=>$user_details_created->profile_picture,
	                             "customer_id"=>@$response_cus_id->result[$key]->ESCID,
	                             "customer_name"=>@$response_cus_id->result[$key]->S_Name,
	                             "customer_address"=>@$response_cus_id->result[$key]->Address,
	                             "last_remark"=>$last_remark,
	                             "remark_user_name"=>$remark_user_name,
	                             "remark_user_profile_pic"=>$remark_user_profile_pic,
	                             "remark_created_date"=>$remark_created_date,
	                             "remark_user_role"=>$remark_user_role,
	                             "case_state"=>$row->case_state,
	                             "status"=>$row->status,
	                             "status_label"=>$status_label

	                        );
		$result_array[$i++]=$tempArray;
	}
	$response['response_code']=200;
	$response['response_message']="Success";
	$response['data']=$result_array;
}
else
{
	$response['response_code']=400;
	$response['response_message']="Somethng Went Wrong";
}
}
		break;

		case 'display_single_case':

		$case_id=$data->case_id;
		$token = $data->token;
		
		$response=array();
		//$user_details_current=$obj_user->get_user_information($user_id);
		$result=$obj_case->case_details_single($case_id);

		  $curl_case_details = curl_init();
		  curl_setopt_array($curl_case_details, array(
		  CURLOPT_URL => ORDERGINI_API."/api/CRMServices/Caselist",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => "{\"ChemistID\":[".$result->customer_id."]}",
		  CURLOPT_HTTPHEADER => array(
		    "Authorization: Bearer ".$token,
		    "Content-Type: application/json",
		    "Postman-Token: ee2b2d47-87ad-48e8-a522-8c96fb6c31a1",
		    "cache-control: no-cache"
		  ),
		));

		$response_cust_details = curl_exec($curl_case_details);
		$details = json_decode($response_cust_details);

		// print_r($details);  die();

		$err2 = curl_error($curl_case_details);

		curl_close($curl_case_details);

		if(!$result)
		{
			$response['response_code']=400;
			$response['response_message']="Something Went Wrong";
		}
		else
		{	
		$user_details_assigned=$obj_user->get_user_information($result->assigned_user_id);
		$user_details_created=$obj_user->get_user_information($result->creator_user_id);
		$user_details_closing=$obj_user->get_user_information($result->closing_user_id);
		$user_details_cancelling=$obj_user->get_user_information($result->cancelled_user_id);

		$tempArray = array(
	                             "case_name"=>$result->case_name,
	                             "case_id"=>$result->case_id,
	                             "case_created_date"=>$result->created_date,
	                             "case_assigned_date"=>$result->assigned_date,
	                             "case_cancel_date"=>$result->assigned_date,
	                             "case_close_date"=>$result->close_date,
	                             "assigned_user_id"=>$result->assigned_user_id,
	                             "closing_user_id"=>@$result->closing_user_id,
	                             "cancelling_user_id"=>@$result->cancelled_user_id,
	                             "assigned_user_name"=>@$user_details_assigned->name,
	                             "closing_user_name"=>@$user_details_closing->name,
	                             "cancelling_user_name"=>@$user_details_cancelling->name,
	                             "assigned_user_profile_pic"=>$user_details_assigned->profile_picture,
	                             "closing_user_profile_pic"=>@$user_details_closing->profile_picture,
	                             "cancelling_user_profile_pic"=>@$user_details_cancelling->profile_picture,
	                             "creator_user_id"=>$result->creator_user_id,
	                             "creator_user_name"=>$user_details_created->name,
	                             "creator_profile_pic"=>$user_details_created->profile_picture,
	                             "customer_name"=>@$details->result[0]->Chemist_Legal_Name,
	                             "customer_address"=>@$details->result[0]->Address,
	                             "case_state"=>$result->case_state,
	                             "status"=>$result->status,

	                        );
		
	
			$response['response_code']=200;
			$response['response_message']="Success";
			$response['data']=$tempArray;

		}
		break;

		case 'close_case':

			$case_id=$data->case_id;
			$closing_user_id = $data->closing_user_id;
			$result=$obj_case->close_case($case_id,$closing_user_id);
			if($result==1)//if task closed sucessfully 
			{
			$response['response_message']= "CASE CLOSED";
			$response['response_code'] = 200;
			}
			if($result==0)//if error occurs
			{
			$response['response_message']= "UNSECUSSFUL";
			$response['response_code'] = 400;
			}
			break;		


		case 'cancel_case':

			$case_id=$data->case_id;
			$cancelled_user_id = $data->cancelled_user_id;
			$result=$obj_case->cancel_case($case_id,$cancelled_user_id);
			if($result==1)//if task closed sucessfully 
			{
			$response['response_message']= "CASE CANCLLED";
			$response['response_code'] = 200;
			}
			if($result==0)//if error occurs
			{
			$response['response_message']= "UNSUCCESFUL";
			$response['response_code'] = 400;
			}
				
		break;
// 			case 'close_case':

// $case_id=$data->case_id;
// $result=$obj_case->close_case($case_id);
// if($result==1)//if task closed sucessfully 
// 	{
// 	$response['response_message']= "CASE CLOSED";
// 	$response['response_code'] = 200;
// 	}
// if($result==0)//if error occurs
// 	{
// 	$response['response_message']= "UNSECUSSFUL";
// 	$response['response_code'] = 400;
// 	}
// 	break;
	case 'reassign_case': 
		$response=array();
		$task_id=$data->case_id;
		$assign_cust_id=$data->assigned_user_id;
		$result=$obj_case->reassign_case($task_id,$assign_cust_id);
		if($result==1)//if task closed sucessfully 
			{
			$response['response_message']= "CASE REASSIGNED";
			$response['response_code'] = 200;
			}
		if($result==0)//if error occurs
			{
			$response['response_message']= "UNSECUSSFUL";
			$response['response_code'] = 400;
			}
			break;


			case 'comments_save':

				$entity_id = $data->case_id;
				$remark = $data->comment;
				$user_id= $data->user_id;

				$result = $obj_case->comments_save($entity_id,$remark,2,$user_id);
				if($result==1){
				$response['response_message']= "Successfull";
				$response['response_code'] = 200;
				}
				if($result==0){
				$response['response_message']= "UNSECUSSFUL";
				$response['response_code'] = 400;
				}

			break;

	}
		echo json_encode($response)
?>