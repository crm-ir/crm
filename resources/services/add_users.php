<?php
	$path_init = $_SERVER['DOCUMENT_ROOT']."/crm/resources/init.php";
	include_once($path_init); 
    header('Content-type: application/json');
	$data = json_decode(file_get_contents('php://input'));
	$user_obj=new user();//creats object of user class
	$fname=$data->fname;//full name of user
	$mobile=$data->mobile;//mobile no of user
	$email=$data->email;//email address of user
	$role=$data->role;//role of user either l1 or l2
	$idl1=$data->role2;//if role is l1 then respective id of l2 user otherwise null
	
if($role=='l2')//if l2 user is added
{
	$insert=$user_obj->insert_details_l2($_SESSION['id'],$fname,$email,$mobile);
	if($insert)
	{
		$response['response_message']= "User Added Successfuly";
		$response['response_code'] = 200;

	}
	else
	{
		$response['response_message']= "UNSUCCESFUL";
		$response['response_code'] = 400;
	}
}
if($role=='l1')//if l1 user is added
{
	$insert=$user_obj->insert_details_l1($_SESSION['id'],$fname,$email,$mobile,$idl1);
	if($insert)
	{
		$response['response_message']= "User Added Successfuly";
		$response['response_code'] = 200;
	}
	else
	{
		$response['response_message']= "UNSECUSSFUL";
		$response['response_code'] = 400;
	}
}
echo json_encode($response);

	?>
