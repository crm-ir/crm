<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Keycloak App 2</title>

    <link rel="stylesheet" type="text/css" href="styles.css"/>

    <script src="http://users.enteroteam.com/auth/js/keycloak.js"></script>
    <script src="app.js"></script>
</head>

<body>
    <div class="wrapper">
        <div id="not-authenticated" class="">
            <!-- <button name="loginBtn" onclick="keycloak.login()">Login</button> -->
        </div>

        <div id="authenticated" class="" style="float:right;">
            <button name="logoutBtn" onclick="keycloak.logout()">Logout</button>
            <!-- <button name="accountBtn" onclick="keycloak.accountManagement()">Account</button> -->
        </div>

        <!-- <div class="content">
            <button name="publicBtn" onclick="request('public')">Invoke Public</button>
            <button name="securedBtn" onclick="request('secured')">Invoke Secured</button>
            <button name="adminBtn" onclick="request('admin')">Invoke Admin</button>

            <div class="message" id="message"></div>
        </div> -->
        <br><br><Br><B>
        <h1 style="color:#fff;text-align:center;">Welcome to App 2<br><small id="username"></small></h1>
    </div>
</body>

</html>