	<?php
	error_reporting(E_ALL);
	ini_set('display_errors', 'On');
	ini_set("memory_limit","-1");

	ini_set('session.cookie_domain', '.crm.com');
	header('Access-Control-Allow-Origin: *');
		
	session_start();
 
	//print_r($_SESSION);
	if(!isset($_SESSION))
	{
		$session_name = session_name("CRM");
		session_set_cookie_params(0, '/', '.CRM.com');
	}

    include("lib/php-master/vendor/autoload.php");
	
    // include("lib/vendor/autoload.php");
	define("APP_ROOT", dirname(__FILE__));
	// define("ROOT_URI", "http://enteroteam.com/crm");
	// define("DOMAIN_BASE", "http://enteroteam.com/crm");
	define("ORDERGINI_API", "http://uat.enterodirect.com");
	
	define("ROOT_URI", "http://localhost/crm");
	define("DOMAIN_BASE", "http://localhost/crm");

	define("S3_BUCKET", "https://s3.ap-south-1.amazonaws.com/entero-crm/");
	  
	 define("S3_BUCKET_IR", "https://s3.ap-south-1.amazonaws.com/ireadksp/ksp/");


	$GLOBALS['config']=array(										//Globals array to store standard constant values
							'mysql' 	=> 	array(		
													'host' =>'localhost',
													'user'=>'root',
													'pass'=>'',
													'db'=>'crm'
											)
							);
		
	spl_autoload_register(function($class){						
		require_once(APP_ROOT.'/classes/'.$class.'.php');		
	});
	// //Used to autoload classes without the need for including on every page
	
	 