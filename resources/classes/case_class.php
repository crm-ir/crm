<?php
class case_class
{
	private $_db, $dt,$_user_obj;

	public function __construct($user=null)
	{
		$this->_db = DB::getInstance();
		$this->_user_obj = user::getInstance_of_user();
		date_default_timezone_set('Asia/Calcutta');
        $this->dt = date('Y-m-d H:i:s');
	}
	 public function add_case($case,$cuid,$auid,$cus_id)//Used to add a new case
  {
   
      $sql ="INSERT INTO cases(case_name,creator_user_id,assigned_user_id,customer_id) VALUES(?,?,?,?)";
      $result = $this->_db->query($sql,array($case,$cuid,$auid,$cus_id));
   	  if($result->_error)
   		{
            return 0;
        }
        else
        {
	 		return 1;
	  	}
	}
	public function case_details($agent_id,$condition,$filter,$role,$page_no,$limit)
	{
		$cond4="";

			$user_str='';
			//print_r($agent_id);
			$user_id_dorm=$this->_user_obj->get_userid($agent_id,$role);
			//print_r($user_id);
			foreach ($user_id_dorm as $key => $value) {
				$user_str.=",'".$value."'";
			}
			$user_str=ltrim($user_str,',');
			// $user_str=implode(",",$user_id);
			//echo $user_str;
			$cond1="AND assigned_user_id IN(".$user_str.")";

			if($filter == 5){
				$cond1 = " AND assigned_user_id ='".$agent_id."' ";
			}elseif($filter == 6){
				$cond1 = "AND assigned_user_id IN(".$user_str.") AND assigned_user_id !='".$agent_id."' ";
			}
		
			$cond2=" ORDER BY created_date DESC";
			$cond3=" LIMIT $page_no,$limit";

			if($condition == 1){
				$cond4 = "";
			}elseif($condition == 2){
				$cond4 = " AND status = 1 AND case_state = 1 ";  // OPEN CASE
			}elseif($condition == 3){
				$cond4 = " AND case_state = 0 ";  // CANCELLED CASE
			}elseif($condition == 4){
				$cond4 = " AND status = 0 AND case_state = 1 ";  // CLOSED CASE
			}

			$sql="SELECT case_id,case_name,creator_user_id,assigned_user_id,cancelled_user_id,closing_user_id,case_state,customer_id,created_date,assigned_date,close_date,cancel_date,status from cases WHERE 1 ".$cond4.$cond1.$cond2.$cond3;
			$result = $this->_db->query($sql)->results();
			//  print_r($result); die();
			return $result;


	}
	public function case_details_single($case_id)
	{
		
			$sql="SELECT case_id,case_name,creator_user_id,assigned_user_id,cancelled_user_id,closing_user_id,case_state,customer_id,created_date,assigned_date,close_date,cancel_date,status from cases WHERE  case_id=?";
			$result = $this->_db->query($sql,array($case_id))->results();
			 //print_r($result);
			 if(empty($result))
      {
         return 0;
      }
      else
      {
      foreach($result as $last_user=>$row_user)
        {
            return $row_user;
        }

   }


	}
	public function case_details_othercases($agent_id,$role,$page_no,$limit)
	{
		
			$user_str='';
			//print_r($agent_id);
			$user_id_dorm=$this->_user_obj->get_userid($agent_id,$role);
			//print_r($user_id);
			foreach ($user_id_dorm as $key => $value) {
				$user_str.=",'".$value."'";
			}
			$user_str=ltrim($user_str,',');
			// $user_str=implode(",",$user_id);
			//echo $user_str;
			$cond1="AND assigned_user_id IN(".$user_str.")";
		
			$cond2=" ORDER BY created_date DESC";
			$cond3=" LIMIT $page_no,$limit";
			$sql="SELECT case_id,case_name,creator_user_id,assigned_user_id,case_state,customer_id,created_date,status from cases WHERE 1 AND assigned_user_id!=? AND status =1 AND case_state = 1 ".$cond1.$cond2.$cond3;
			$result = $this->_db->query($sql,array($agent_id))->results();
			// print_r($result);
			return $result;


	}
	public function case_details_closedcases($agent_id,$role,$page_no,$limit)
	{
		
			$user_str='';
			//print_r($agent_id);
			$user_id_dorm=$this->_user_obj->get_userid($agent_id,$role);
			//print_r($user_id);
			foreach ($user_id_dorm as $key => $value) {
				$user_str.=",'".$value."'";
			}
			$user_str=ltrim($user_str,',');
			// $user_str=implode(",",$user_id);
			//echo $user_str;
			$cond1="AND assigned_user_id IN(".$user_str.")";
		
			$cond2=" ORDER BY created_date DESC";
			$cond3=" LIMIT $page_no,$limit";
			$sql="SELECT case_id,case_name,creator_user_id,assigned_user_id,case_state,customer_id,created_date,status from cases WHERE 1 AND status =0 AND case_state = 1 ".$cond1.$cond2.$cond3;
			$result = $this->_db->query($sql,array($agent_id))->results();
			// print_r($result);
			return $result;


	}
	public function case_details_cancelledcases($agent_id,$role,$page_no,$limit)
	{
		
			$user_str='';
			//print_r($agent_id);
			$user_id_dorm=$this->_user_obj->get_userid($agent_id,$role);
			//print_r($user_id_dorm);
			foreach ($user_id_dorm as $key => $value) {
				$user_str.=",'".$value."'";
			}
			$user_str=ltrim($user_str,',');
			// $user_str=implode(",",$user_id);
			//echo $user_str;
			$cond1="AND assigned_user_id IN(".$user_str.")";
		
			$cond2=" ORDER BY created_date DESC";
			$cond3=" LIMIT $page_no,$limit";
			$sql="SELECT case_id,case_name,creator_user_id,assigned_user_id,case_state,customer_id,created_date,status from cases WHERE 1 AND status =1 AND case_state = 0 ".$cond1.$cond2.$cond3;
			$result = $this->_db->query($sql,array($agent_id))->results();
			// print_r($result);
			return $result;


	}
	public function case_details_mycases($agent_id,$page_no,$limit)
	{
		
			$cond2=" ORDER BY created_date DESC";
			$cond3=" LIMIT $page_no,$limit";
			$sql="SELECT case_id,case_name,creator_user_id,assigned_user_id,case_state,customer_id,created_date,status from cases WHERE assigned_user_id=? AND status =1 AND case_state = 1".$cond2.$cond3;
			$result = $this->_db->query($sql,array($agent_id))->results();
			// print_r($result);
			return $result;


	}

	public function close_case($case_id,$closing_user_id)
	{
		$sql = "update cases set status = 0, closing_user_id = '$closing_user_id', close_date = now()  where case_id = ?";
        $result = $this->_db->query($sql,array($case_id));
        if($result->_error){
            return 0;
        }else{
            return 1;
        }
	}
	public function cancel_case($case_id,$cancel_user_id)
	{
		$sql = "update cases set case_state = 0, cancelled_user_id = '$cancel_user_id', cancel_date = now() where case_id = ?";
        $result = $this->_db->query($sql,array($case_id));
        if($result->_error){
            return 0;
        }else{
            return 1;
        }
	}
	public function reassign_case($case_id,$assign_cust_id)
	{
		$sql = "update cases set assigned_user_id= ?, assigned_date = now(), case_state = 1, status = 1 where case_id = ? ";
        $result = $this->_db->query($sql,array($assign_cust_id,$case_id));
        //print_r($result);
        if($result->_error){
            return 0;
        }else{
            return 1;
        }
	}

	public function comments_save($entity_id,$remark,$entity_type,$user_id){

		$sql = "INSERT INTO remarks(entity_id,entity_type,remark,user_id) VALUES(?,?,?,?)";
		$result = $this->_db->query($sql,array($entity_id,$entity_type,$remark,$user_id));

		if($result->_error){
            return 0;
        }else{
            return 1;
        }
	}
}
