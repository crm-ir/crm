 <?php 
  $path_init = $_SERVER['DOCUMENT_ROOT']."/crm/resources/init.php";
include_once($path_init);

 ?>

  <link href="<?php echo ROOT_URI; ?>/assets/css/temp.css" rel="stylesheet" type="text/css">
  <link href="<?php echo ROOT_URI; ?>/assets/css/team.css" rel="stylesheet" type="text/css">

<div class="col-md-10 col-lg-10 col-sm-10 col-xs-10 col-lg-offset-2 col-md-offset-2 col-sm-offset-2 col-xs-offset-2 no-lr-pad back-color-grey">
    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad " id="main-section-task">
   
           <!-- for upper part -->
  
            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 back-color-white padding-top-13 border-bottom padding-left-40">
                <p class="font-size-24 float-inline"><b>Add Distributor</b></p>
                 
            </div>
           <!-- upper part ends -->
    </div>
    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 no-lr-pad ">
      <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6 ">
          <div class="form-group col-md-6 col-lg-6 col-sm-6 col-xs-6">
                  <span class="font-size-12 color-grey-1"  for="firstName">First Name</span>
                  <input type="text" name="firstName" class="form-control margin-top-5" placeholder="Enter first name of member" id="firstName">
                </div>
                <div class="form-group col-md-6 col-lg-6 col-sm-6 col-xs-6">
                  <span class="font-size-12 color-grey-1"  for="lastName">Last Name</span>
                  <input type="text" name="lastName" class="form-control margin-top-5" placeholder="Enter last name of member" id="lastName">
                </div>
               
                <div class="form-group col-md-6 col-lg-6 col-sm-6 col-xs-6">
                  <span class="font-size-12 color-grey-1"  for="phone">Phone</span>
                  <input type="text" name="phone" class="form-control margin-top-5" id="phone" placeholder="Enter phone number of member">
                </div>
                <div class="form-group col-md-6 col-lg-6 col-sm-6 col-xs-6">
                  <span class="font-size-12 color-grey-1"  for="email">Email</span>
                  <input type="text" name="email" class="form-control margin-top-5" placeholder="Enter email of member" id="email">
                </div>
                <div class="form-group col-md-6 col-lg-6 col-sm-6 col-xs-6">
                  <span class="font-size-12 color-grey-1"  for="password_new">Password</span>
                  <input type="text" name="password_new" class="form-control margin-top-5" id="password_new" placeholder="Enter password">
                </div>
                <div class="form-group col-md-6 col-lg-6 col-sm-6 col-xs-6">
                  <span class="font-size-12 color-grey-1"  for="Distributor">Entero Direct Distributor ID</span>
                  <input type="text" name="ordergini_id_val" class="form-control margin-top-5" id="ordergini_id_val" placeholder="Enter Entero Direct Distributor ID">
                </div>
                <div class="form-group col-md-6 col-lg-6 col-sm-6 col-xs-6">
                  <span class="font-size-12 color-grey-1"  for="exo">Exo Phone</span>
                  <input type="text" name="exo" class="form-control margin-top-5" id="exo" placeholder="Enter Exo phone number">
                </div>
              <button type="button" class="btn btn-default pull-right brand-btn add-mem-btn margin-top-30 margin-right-10"  id="add_mem_ajax">Add</button>

      </div>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
    $(document).on('click','#add_mem_ajax',function(){
          var fname        =$("#firstName").val();
          var lname        =$("#lastName").val();
          var phone        =$("#phone").val();
          var email        =$("#email").val();
          var exo        =$("#exo").val();
          var ordergini_id =$("#ordergini_id_val").val();
          var role_new     =2;
          var password_new =$("#password_new").val();

         
          if(fname=='' || fname==' '){
            toast_it("First name is mandatory");
              return false;
          }
         
          
          if(exo=='' || exo==' '){
             toast_it("Exo phone number is mandatory");
              return false;
          }
          if(phone=='' || phone==' '){
             toast_it("phone number is mandatory");
              return false;
          }
          if(password_new=='' || password_new==' '){
             toast_it("Password is mandatory");
              return false;
          }
          function isEmail(email1) {
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            return regex.test(email1);
          }
          if(email=='' || email==' '){
             
          }
          else{
              if(!isEmail(email)){
                toast_it("Enter a valid email");
                return false;
              } 
          }

          $.ajax({
                  url: '<?php echo ROOT_URI; ?>/resources/services/add_user_crm.php',
                  method: 'POST',
                  data: "{\n\"firstName\":\""+fname+"\",\n\"lastName\":\""+lname+"\",\n\"email\":\""+email+"\",\n\"password_new\":\""+password_new+"\",\n\"phone\":\""+phone+"\",\n\"role\":\""+role_new+"\",\n\"mapping_user_id\":\""+exo+"\",\n\"ordergini_id\":\""+ordergini_id+"\", \n\"action\":\"start_user_add\"\n}",
                  dataType: 'json', 
                }).success(function(resp) {
                       // console.lo
                      if(resp.response_code==400){
                        toast_it("Something went wrong, Try again");
                      }
                      else{
                        if(resp.response_status==1){
                          // $('#add_member_modal').modal('hide');
                          toast_it("User added successfully");

                          $("#firstName").val("");
                          $("#lastName").val("");
                          $("#email").val("");
                          $("#password_new").val("");
                          $("#exo").val("");
                          $("#ordergini_id_val").val("");
                          $("#phone").val("");

                          $("#user_mem_msg").html('<p class="success-col">User '+phone+' added successfully with id:  <b>'+resp.user_id+'</b></p>');
                        }
                        else if(resp.response_status==4){
                          toast_it("Failure,User added successfully in keycloak but failed to add in crm database,contact admin.");

                          $("#firstName").val("");
                          $("#lastName").val("");
                          $("#email").val("");
                          $("#user_mem_msg").html('<p class="success-col">User '+phone+' added successfully with id:  <b>'+resp.user_id+'</b> in keycloak but failed to add in crm</p>');
                        }
                        else if(resp.response_status==2){
                            toast_it("User already exists with same phone/email");
                             $("#user_mem_msg").html('<p class="error-col">Phone number '+phone+' already exists with id:  <b>'+resp.user_id+'</b></p>');
                        }
                        else if(resp.response_status==5){
                            toast_it("User already exists in keycloak and  added in crm");
                             $("#user_mem_msg").html('<p class="error-col">Phone number '+phone+' already exists in keycloak and  added in crm with id:  <b>'+resp.user_id+'</b></p>');
                        }
                        else if(resp.response_status==6){
                            toast_it("User already exists in keycloak and failed to add in crm");
                             $("#user_mem_msg").html('<p class="error-col">Phone number '+phone+' already in keycloak and failed to add in crm with id:  <b>'+resp.user_id+'</b></p>');
                        }
                        else if(resp.response_status==3){
                            toast_it("please enter 10 digit phone number");
                        } 
                        
                                        
                      }
            });//ajax 


      });
        

});
 
</script>
    </body>
</html>