<?php
$path_init = $_SERVER['DOCUMENT_ROOT']."/crm/resources/init.php";
include_once($path_init); 
header('Content-type: application/json');
$data = json_decode(file_get_contents('php://input')); 
$obj_custm=new customer();
$obj_call=new call();
$obj_user=new user();


$action=$data->action;

switch ($action) {
	

	case 'display_custm':

				$token=$data->token;
				$user_id=$data->user_id;//user_id of user
				$page_no=$data->page_no;//page no 
				$role=$data->role;//role of user
				$customer_id = $data->customer_id_search;
				$limit=30;
				$i=0;

				$og_id = $obj_user->get_ogid($user_id); 
				$customer_ids='';

				$response=array();
				$result=$obj_custm->customer_page_details($user_id,$role,($page_no-1)*$limit,$limit,$customer_id);
				// print_r($result);die;
				$res=array();
				$result_array = array();
				
				if(empty($result))
				{
					// $response['response_code']=400;
					// $response['response_message']="Something Went Wrong";
				}
				else
				{
					$customer_ids='';
					foreach($result as $keys=>$pair)
					{
						$customer_ids.=','.$pair->customer_id;
					}
					$customer_ids=ltrim($customer_ids,',');
					//echo $customer_ids;
					//echo strlen($customer_ids);
					//die;
				$curl = curl_init();

				curl_setopt_array($curl, array(
				  CURLOPT_URL => ORDERGINI_API."/api/CRMServices/GetChemistDetails",
				  CURLOPT_RETURNTRANSFER => true,
				  CURLOPT_ENCODING => "",
				  CURLOPT_MAXREDIRS => 10,
				  CURLOPT_TIMEOUT => 30,
				  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				  CURLOPT_CUSTOMREQUEST => "POST",
				  CURLOPT_POSTFIELDS => "{\"ChemistID\":[".$customer_ids."]}",
				  CURLOPT_HTTPHEADER => array(
				    "Authorization: Bearer ".$token,
				    "Content-Type: application/json",
				    "Postman-Token: ee2b2d47-87ad-48e8-a522-8c96fb6c31a1",
				    "cache-control: no-cache"
				  ),
				));

				$response_cus_id= curl_exec($curl);

				$err = curl_error($curl);

				curl_close($curl);

				if (!$err) {
				$response_cus_id=json_decode($response_cus_id);
				     // print_r($response_cus_id);die;
				// $i=0;
				foreach ($result as $key => $row) 
					{

					 	$call_detail=$obj_call->last_contracted_custm($row->customer_id);
						$user_details=$obj_user->get_user_information($row->user_id);
						$case_count =$obj_user->get_open_case_for_cust($row->customer_id);
						$task_count =$obj_user->get_open_task_for_cust($row->customer_id);
						$rating = $obj_custm->get_rating_customer($row->customer_id);
						  // print_r($user_details);die;
						$is_task=0;
						$status_label='';
						$state=0;
						if($task_count){
							$is_task=1;
						 
							$state=$task_count->status;
							if($state==1)
							{
							
							$diff = time()-strtotime($task_count->created_date);

							//$hr_diff=$interval->format('%H');
							if($diff<3*3600)
							{
								$status_label="NEW";
							}
							elseif($diff>12*3600) 
							{
								$status_label="EXPIRED";
							}
							else
							{
								$status_label="OPEN";
							}
							}
							else
							{
								$status_label="CLOSED";
							}
						}
						$c_detail='';
						if(isset($call_detail->created_date))
						{
							$c_detail=$call_detail->created_date;
						}

						$tempArray = array(
					                            
					                             "assigned_user_id"=>@$user_details->user_id,
					                             "last_contacted"=>@$c_detail,
					                              
					                             "assigned_user_name"=>@$user_details->name,
					                             "assigned_user_profile_pic"=>@$user_details->profile_picture,
					                             "customer_id"=>@$response_cus_id->result[$key]->ESCID,
					                             "customer_name"=>@$response_cus_id->result[$key]->S_Name,
					                             "customer_address"=>@$response_cus_id->result[$key]->Address,
					                             "customer_since"=>@$response_cus_id->result[$key]->S_CreateDate,
					                             "order_frequency"=>@$response_cus_id->result[$key]->OrderFrq,
					                             "order_avg_price"=>@$response_cus_id->result[$key]->S_AvgPrice,
					                             "is_task"=>$is_task,
					                             "status_task"=>$state,
					                             "status_label"=>$status_label,
					                             "case_count"=>$case_count,
					                             "rating"=>$rating,
					                             "last_order_placed"=>@$response_cus_id->result[$key]->LastOrderDate,
					                             "last_contacted_number"=>@$call_detail->to_number,
					                             "dist_id"=>@$response_cus_id->result[$key]->DistID,
					                             "cust_erpid"=>@$response_cus_id->result[$key]->ESCID,
					                             "cust_erpcode"=>@$response_cus_id->result[$key]->cust_erpcode,
					                             "login_id"=>@$response_cus_id->result[$key]->asuser_loginid,
					                             "user_pwd"=>@$response_cus_id->result[$key]->user_pwd,
					                             "isBlocked"=>@$response_cus_id->result[$key]->isBlocked,
					                             "Block_Reason"=>@$response_cus_id->result[$key]->Block_Reason
					                             // "task_id"=>$row->task_id,
					                             // "follow_up"=>$row->follow_up,
					                             // "task_created_date"=>$row->created_date,

					                        );
						$result_array[$i++]=$tempArray;
					}  // close for each

					}

				}   /// close if empty $result

				if(empty($result) && $customer_id){


					$curl = curl_init();

					curl_setopt_array($curl, array(
					  CURLOPT_URL => ORDERGINI_API."/api/CRMServices/GetChemistDetails",
					  CURLOPT_RETURNTRANSFER => true,
					  CURLOPT_ENCODING => "",
					  CURLOPT_MAXREDIRS => 10,
					  CURLOPT_TIMEOUT => 30,
					  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					  CURLOPT_CUSTOMREQUEST => "POST",
					  CURLOPT_POSTFIELDS => "{\"ChemistID\":[".$customer_id."]}",
					  CURLOPT_HTTPHEADER => array(
					    "Authorization: Bearer ".$token,
					    "Content-Type: application/json",
					    "Postman-Token: ee2b2d47-87ad-48e8-a522-8c96fb6c31a1",
					    "cache-control: no-cache"
					  ),
					));

					$response_cus_id= curl_exec($curl);

					$err = curl_error($curl);

					curl_close($curl);

					$response_cus_id=json_decode($response_cus_id);

					$call_detail=$obj_call->last_contracted_custm($customer_id);
					$case_count =$obj_user->get_open_case_for_cust($customer_id);
					$task_count =$obj_user->get_open_task_for_cust($customer_id);

					$is_task=0;
						$status_label='';
						$state=0;
						if($task_count){
							$is_task=1;
						 
							$state=$task_count->status;
							if($state==1)
							{
							
							$diff = time()-strtotime($task_count->created_date);

							//$hr_diff=$interval->format('%H');
							if($diff<3*3600)
							{
								$status_label="NEW";
							}
							elseif($diff>12*3600) 
							{
								$status_label="EXPIRED";
							}
							else
							{
								$status_label="OPEN";
							}
							}
							else
							{
								$status_label="CLOSED";
							}
						}
						$c_detail='';
						if(isset($call_detail->created_date))
						{
							$c_detail=$call_detail->created_date;
						}

					// print_r($response_cus_id); 

						$tempArray = array(
	                            
	                             				 "assigned_user_id"=>'',
					                             "last_contacted"=>@$c_detail,					                          					                             
					                             "assigned_user_name"=>'',
					                             "assigned_user_profile_pic"=>'',
					                             "customer_id"=>@$response_cus_id->result[0]->ESCID,
					                             "customer_name"=>@$response_cus_id->result[0]->S_Name,
					                             "customer_address"=>@$response_cus_id->result[0]->Address,
					                             "customer_since"=>@$response_cus_id->result[0]->S_CreateDate,
					                             "order_frequency"=>@$response_cus_id->result[0]->OrderFrq,
					                             "order_avg_price"=>@$response_cus_id->result[0]->S_AvgPrice,
					                             "is_task"=>$is_task,
					                             "status_task"=>$state,
					                             "status_label"=>$status_label,
					                             "case_count"=>$case_count,
					                             "rating"=>'',
					                             "last_order_placed"=>@$response_cus_id->result[0]->LastOrderDate,
					                             "last_contacted_number"=>@$call_detail->to_number,
					                             "dist_id"=>@$response_cus_id->result[0]->DistID,
					                             "cust_erpid"=>@$response_cus_id->result[0]->ESCID,
					                             "cust_erpcode"=>@$response_cus_id->result[0]->cust_erpcode,
					                             "login_id"=>@$response_cus_id->result[0]->asuser_loginid,
					                             "user_pwd"=>@$response_cus_id->result[0]->user_pwd,
					                             "isBlocked"=>@$response_cus_id->result[0]->isBlocked,
					                             "Block_Reason"=>@$response_cus_id->result[0]->Block_Reason
					                             // "last_contacted_number"=>''
					                             // "task_id"=>$row->task_id,
					                             // "follow_up"=>$row->follow_up,
					                             // "task_created_date"=>$row->created_date,
	                        );
						$result_array[$i++]=$tempArray;



				}else{


						if($role == 2 || $role == 3 || $role == 4){

							$distributor_id = $obj_user->get_distributor_id($data->dist_id);

							$curl = curl_init();

							curl_setopt_array($curl, array(
							  CURLOPT_URL => ORDERGINI_API."/api/CRMServices/DistributorChemistList",
							  CURLOPT_RETURNTRANSFER => true,
							  CURLOPT_ENCODING => "",
							  CURLOPT_MAXREDIRS => 10,
							  CURLOPT_TIMEOUT => 30,
							  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
							  CURLOPT_CUSTOMREQUEST => "POST",
							  CURLOPT_POSTFIELDS => "{\"ChemistList\":[{\"DistID\":".$distributor_id.",\"PageID\": ".$page_no."}]}\r\n",
							  CURLOPT_HTTPHEADER => array(
							    "Authorization: Bearer ".$token,
							    "Content-Type: application/json",
							    "Postman-Token: ee2b2d47-87ad-48e8-a522-8c96fb6c31a1",
							    "cache-control: no-cache"
							  ),
							));

							$response_cus_id= curl_exec($curl);

							// print_r($response_cus_id);

							$err = curl_error($curl);

							curl_close($curl);

							$response_cus_id=json_decode($response_cus_id);

							 // print_r($response_cus_id); die();

							$customer_ids = '';
							$customer_names = '';
							$customer_addss = '';
							$customer_since = '';
							$order_frequency = '';
							$order_avg_price = '';
							$last_order_placed = '';
							$isBlocked = '';
							$Block_Reason = '';
							$dist_id = '';
							$cust_erpid = '';
							$cust_erpcode = '';
							$login_id = '';
							$user_pwd = '';

							if(@$response_cus_id->status != 'failure'){

								foreach (@$response_cus_id->result as $key => $cust) {
								
								$customer_ids.=','.$cust->ChemistID;	
								$customer_names.=','.$cust->Chemist_Legal_Name;	
								$customer_addss.=']'.$cust->Address;	
								$customer_since.=']'.$cust->CreatedDate;	
								$order_frequency.=']'.$cust->OrderFrq;	
								$order_avg_price.=']'.$cust->AvgOrderSize;	
								$last_order_placed.=']'.$cust->LastOrderDate;	
								$isBlocked .= ']'.$cust->isBlocked;
								$Block_Reason .= ']'.$cust->Block_Reason;
								$dist_id .= ']'.$cust->StockistID;
								$cust_erpid .= ']'.$cust->ESCID;
								$cust_erpcode .= ']'.$cust->cust_erpcode;
								$login_id .= ']'.$cust->asuser_loginid;
								$user_pwd .= ']'.$cust->user_pwd;
								}

								// $customer_ids=ltrim($customer_ids,',');
								$customer_ids = substr($customer_ids, 1);

								// $customer_names=ltrim($customer_names,',');
								$customer_names = substr($customer_names, 1);

								// $customer_addss=ltrim($customer_addss,']');
								$customer_addss = substr($customer_addss, 1);

								// $customer_since=ltrim($customer_since,']');
								$customer_since = substr($customer_since, 1);

								// $order_frequency=ltrim($order_frequency,']');
								$order_frequency = substr($order_frequency, 1);

								// $order_avg_price=ltrim($order_avg_price,']');
								$order_avg_price = substr($order_avg_price, 1);

								// $last_order_placed=ltrim($last_order_placed,']');
								$last_order_placed = substr($last_order_placed, 1);
								$isBlocked = substr($isBlocked, 1);
								$Block_Reason = substr($Block_Reason, 1);

								$dist_id = substr($dist_id, 1);
								$cust_erpid = substr($cust_erpid, 1);
								$cust_erpcode = substr($cust_erpcode, 1);
								$login_id = substr($login_id, 1);
								$user_pwd = substr($user_pwd, 1);

								  // print_r($order_frequency); die();

								$cust_array = explode(',', $customer_ids);
								$cust_array_names = explode(',', $customer_names);
								$cust_array_addss = explode(']', $customer_addss);
								$cust_array_since = explode(']', $customer_since);
								$cust_array_freq = explode(']', $order_frequency);
								$cust_array_avg = explode(']', $order_avg_price);
								$cust_array_last = explode(']', $last_order_placed);
								$cust_array_isBlocked = explode(']', $isBlocked);
								$cust_array_Block_Reason = explode(']', $Block_Reason);

								$cust_array_dist_id = explode(']', $dist_id);
								$cust_array_cust_erpid = explode(']', $cust_erpid);
								$cust_array_cust_erpcode = explode(']', $cust_erpcode);
								$cust_array_login_id = explode(']', $login_id);
								$cust_array_user_pwd = explode(']', $user_pwd);

								   // print_r($cust_array_avg);  die();

								foreach ($cust_array as $key2 => $value1) {

									$res_cust = $obj_user->check_customer($value1); 
									$call_detail=$obj_call->last_contracted_custm($value1);
									$case_count =$obj_user->get_open_case_for_cust($value1);
									$task_count =$obj_user->get_open_task_for_cust($value1);

									$is_task=0;
										$status_label='';
										$state=0;
										if($task_count){
											$is_task=1;
										 
											$state=$task_count->status;
											if($state==1)
											{
											
											$diff = time()-strtotime($task_count->created_date);

											//$hr_diff=$interval->format('%H');
											if($diff<3*3600)
											{
												$status_label="NEW";
											}
											elseif($diff>12*3600) 
											{
												$status_label="EXPIRED";
											}
											else
											{
												$status_label="OPEN";
											}
											}
											else
											{
												$status_label="CLOSED";
											}
										}
										$c_detail='';
										if(isset($call_detail->created_date))
										{
											$c_detail=$call_detail->created_date;
										}


									// print_r($res_cust);
									if($res_cust){

									}else{

										$tempArray = array(
					                            
					                             
					                             "assigned_user_id"=>'',
					                             "last_contacted"=>@$c_detail,					                          					                             
					                             "assigned_user_name"=>'',
					                             "assigned_user_profile_pic"=>'',
					                             "customer_id"=>@$cust_array[$key2],
					                             "customer_name"=>@$cust_array_names[$key2],
					                             "customer_address"=>@$cust_array_addss[$key2],
					                             "customer_since"=>@$cust_array_since[$key2],
					                             "order_frequency"=>@$cust_array_freq[$key2],
					                             "order_avg_price"=>@$cust_array_avg[$key2],
					                             "is_task"=>$is_task,
					                             "status_task"=>$state,
					                             "status_label"=>$status_label,
					                             "case_count"=>$case_count,
					                             "rating"=>'',
					                             "last_order_placed"=>@$cust_array_last[$key2],
					                             "last_contacted_number"=>@$call_detail->to_number,
					                             "isBlocked"=>@$cust_array_isBlocked[$key2],
					                             "Block_Reason"=>@$cust_array_Block_Reason[$key2],

					                             "dist_id"=>@$cust_array_dist_id[$key2],
					                             "cust_erpid"=>@$cust_array_cust_erpid[$key2],
					                             "cust_erpcode"=>@$cust_array_cust_erpcode[$key2],
					                             "login_id"=>@$cust_array_login_id[$key2],
					                             "user_pwd"=>@$cust_array_user_pwd[$key2]
					                             // "task_id"=>$row->task_id,
					                             // "follow_up"=>$row->follow_up,
					                             // "task_created_date"=>$row->created_date,

					                        );
										$result_array[$i++]=$tempArray;


									}

								}   // close for each   


							}else{

							}  // if response not failure

							
							
							}else{

							}  // if role == 2


				}

				if($result_array)
				{
					$response['response_code']=200;
					$response['response_message']="Success";
					$response['data']=$result_array;
				}
				else
				{
					$response['response_code']=400;
					$response['response_message']="Something Went Wrong";
				}
				
		break;


		case 'display_call_planner':
		
				$token=$data->token;
				$user_id=$data->user_id;//user_id of user
				$page_no=$data->page_no;//page no 
				$role=$data->role;//role of user
				$customer_id=$data->customer_id;
				$filter_assigned=$data->filter_assigned;
				$user_ids_filter=$data->user_ids_filter;
				$limit=20;

				$og_id = $obj_user->get_ogid($user_id); 
				$customer_ids='';
				$i=0;
				$response=array();
				$result=$obj_custm->caller_page_details($user_id,$role,($page_no-1)*$limit,$limit,$customer_id,$filter_assigned,$user_ids_filter);
				 // print_r($result);die;
				$res=array();
				$result_array = array();
				if(empty($result))
				{	
					// if($customer_id!=0){
					// 	$response['response_code']=400;
					// 	$response['response_message']="Customer does not exist in customer connect database";
					// }else{
					// 	$response['response_code']=400;
					// 	$response['response_message']="Something Went Wrong";
					// }
				}
				else
				{
					// $customer_ids='';
					foreach($result as $keys=>$pair)
					{
						$customer_ids.=','.$pair->customer_id;
					}
					$customer_ids=ltrim($customer_ids,',');
					//echo $customer_ids;
					//echo strlen($customer_ids);
					//die;
				$curl = curl_init();

				curl_setopt_array($curl, array(
				  CURLOPT_URL => ORDERGINI_API."/api/CRMServices/GetChemistDetails",
				  CURLOPT_RETURNTRANSFER => true,
				  CURLOPT_ENCODING => "",
				  CURLOPT_MAXREDIRS => 10,
				  CURLOPT_TIMEOUT => 30,
				  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				  CURLOPT_CUSTOMREQUEST => "POST",
				  CURLOPT_POSTFIELDS => "{\"ChemistID\":[".$customer_ids."]}",
				  CURLOPT_HTTPHEADER => array(
				    "Authorization: Bearer ".$token,
				    "Content-Type: application/json",
				    "Postman-Token: ee2b2d47-87ad-48e8-a522-8c96fb6c31a1",
				    "cache-control: no-cache"
				  ),
				));

				$response_cus_id= curl_exec($curl);

				$err = curl_error($curl);

				curl_close($curl);

				$response_cus_id=json_decode($response_cus_id);
				    // print_r($response_cus_id);die;
				// $i=0;
				foreach ($result as $key => $row) 
					{

						$user_details=$obj_user->get_user_information($row->user_id);
						 

						$tempArray = array(
					                            
					                             "assigned_user_id"=>$row->user_id,
					                             "assigned_user_name"=>@$user_details->name,
					                             "assigned_user_profile_pic"=>@$user_details->profile_picture,
					                             "customer_id"=>$row->customer_id,
					                             "customer_name"=>@$response_cus_id->result[$key]->S_Name,
					                             "customer_address"=>@$response_cus_id->result[$key]->Address,
					                             "time_from"=>$row->time_from,
					                             "time_to"=>$row->time_to,
					                             "mo"=>$row->mo,
					                             "tu"=>$row->tu,
					                             "we"=>$row->we,
					                             "th"=>$row->th,
					                             "fr"=>$row->fr,
					                             "sa"=>$row->sa,
					                             "su"=>$row->su

					                        );
						$result_array[$i++]=$tempArray;
					}

				}

				if($filter_assigned == 1 || $filter_assigned == 2){

					$response['response_code']=200;
					$response['response_message']="Success";
					$response['data']=$result_array;

					echo json_encode($response); die();
				}else{

				}

				if(empty($result) && $customer_id && $role == 2){


					$curl = curl_init();

					curl_setopt_array($curl, array(
					  CURLOPT_URL => ORDERGINI_API."/api/CRMServices/GetChemistDetails",
					  CURLOPT_RETURNTRANSFER => true,
					  CURLOPT_ENCODING => "",
					  CURLOPT_MAXREDIRS => 10,
					  CURLOPT_TIMEOUT => 30,
					  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					  CURLOPT_CUSTOMREQUEST => "POST",
					  CURLOPT_POSTFIELDS => "{\"ChemistID\":[".$customer_id."]}",
					  CURLOPT_HTTPHEADER => array(
					    "Authorization: Bearer ".$token,
					    "Content-Type: application/json",
					    "Postman-Token: ee2b2d47-87ad-48e8-a522-8c96fb6c31a1",
					    "cache-control: no-cache"
					  ),
					));

					$response_cus_id= curl_exec($curl);

					$err = curl_error($curl);

					curl_close($curl);

					$response_cus_id=json_decode($response_cus_id);

					// print_r($response_cus_id); 

						$tempArray = array(
	                            
	                             "assigned_user_id"=>'',
	                             "assigned_user_name"=>'',
	                             "assigned_user_profile_pic"=>'',
	                             "customer_id"=>@$response_cus_id->result[0]->ESCID,
	                             "customer_name"=>@$response_cus_id->result[0]->S_Name,
	                             "customer_address"=>@$response_cus_id->result[0]->Address,
	                             "time_from"=>'',
	                             "time_to"=>'',
	                             "mo"=>'',
	                             "tu"=>'',
	                             "we"=>'',
	                             "th"=>'',
	                             "fr"=>'',
	                             "sa"=>'',
	                             "su"=>''

	                        );
						$result_array[$i++]=$tempArray;



				}else{


						if($role == 2){

							$distributor_id = $obj_user->get_distributor_id($user_id);							

							$curl = curl_init();

							curl_setopt_array($curl, array(
							  CURLOPT_URL => ORDERGINI_API."/api/CRMServices/DistributorChemistList",
							  CURLOPT_RETURNTRANSFER => true,
							  CURLOPT_ENCODING => "",
							  CURLOPT_MAXREDIRS => 10,
							  CURLOPT_TIMEOUT => 30,
							  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
							  CURLOPT_CUSTOMREQUEST => "POST",
							  CURLOPT_POSTFIELDS => "{\"ChemistList\":[{\"DistID\":".$distributor_id.",\"PageID\": ".$page_no."}]}\r\n",
							  CURLOPT_HTTPHEADER => array(
							    "Authorization: Bearer ".$token,
							    "Content-Type: application/json",
							    "Postman-Token: ee2b2d47-87ad-48e8-a522-8c96fb6c31a1",
							    "cache-control: no-cache"
							  ),
							));

							$response_cus_id= curl_exec($curl);

							// print_r($response_cus_id);

							$err = curl_error($curl);

							curl_close($curl);

							$response_cus_id=json_decode($response_cus_id);

							 // print_r($response_cus_id); die();

							$customer_ids = '';
							$customer_names = '';
							$customer_addss = '';

							if($response_cus_id->status != 'failure'){

								foreach ($response_cus_id->result as $key => $cust) {
								
								$customer_ids.=','.$cust->ChemistID;	
								$customer_names.=','.$cust->Chemist_Legal_Name;	
								$customer_addss.=']'.$cust->Address;	
								}

								$customer_ids=ltrim($customer_ids,',');
								$customer_names=ltrim($customer_names,',');
								$customer_addss=ltrim($customer_addss,']');

								 // print_r($customer_addss); die();


							}else{

							}  // if response not failure

								$cust_array = explode(',', $customer_ids);
								$cust_array_names = explode(',', $customer_names);
								$cust_array_addss = explode(']', $customer_addss);

								   // print_r($cust_array_addss);  die();

								foreach ($cust_array as $key2 => $value1) {

									$res_cust = $obj_user->check_customer($value1); 

									// print_r($res_cust);
									if($res_cust){

									}else{

										$tempArray = array(
					                            
					                             "assigned_user_id"=>'',
					                             "assigned_user_name"=>'',
					                             "assigned_user_profile_pic"=>'',
					                             "customer_id"=>@$cust_array[$key2],
					                             "customer_name"=>@$cust_array_names[$key2],
					                             "customer_address"=>@$cust_array_addss[$key2],
					                             "time_from"=>'',
					                             "time_to"=>'',
					                             "mo"=>'',
					                             "tu"=>'',
					                             "we"=>'',
					                             "th"=>'',
					                             "fr"=>'',
					                             "sa"=>'',
					                             "su"=>''

					                        );
										$result_array[$i++]=$tempArray;


									}

									

								}
							
								//die();
							 // print_r($cust_array); die();

							// foreach ($cust_array as $key => $value) {
							// 	print_r($value)
							// }
							// die();
							
							}else{

							}  // if role == 2


				}

					// print_r($result_array); die();

				if(!empty($result_array)){
					$response['response_code']=200;
					$response['response_message']="Success";
					$response['data']=$result_array;
				}
				else
				{
					$response['response_code']=400;
					$response['response_message']="Something Went Wrong";
				}
				
		break;
		
		case 'update_planner':

			$update_column 	 = $data->update_column;
			$customer_id = $data->customer_id;
			$updated_value 	 = $data->updated_value;

			$result = $obj_custm->update_planner($customer_id,$update_column,$updated_value);

			if($result){
				$response['response_code']=200;
				$response['response_message']="Success";
				$response['data']=$result;
			}
			else
			{
				$response['response_code']=400;
				$response['response_message']="Something Went Wrong";
			}

		break;
		case 'upload_task_bulk':

			// $update_column 	 = $data->update_column;
			// $customer_id = $data->customer_id;
			// $updated_value 	 = $data->updated_value;

			// $result = $obj_custm->upload_task_bulk($customer_id,$update_column,$updated_value);

			// if($result){
			// 	$response['response_code']=200;
			// 	$response['response_message']="Success";
			// 	$response['data']=$result;
			// }
			// else
			// {
			// 	$response['response_code']=400;
			// 	$response['response_message']="Something Went Wrong";
			// }

				echo json_encode("sunjn");
				die;
		break;



		case 'get_customer_number':

			$user_id 	 = $data->user_id;
			

			$result = $obj_custm->get_customer_number($user_id);

			if($result){
				$response['response_code']=200;
				$response['response_message']="Success";
				$response['data']=$result;
			}
			else
			{
				$response['response_code']=400;
				$response['response_message']="Something Went Wrong";
			}

		break;

		case 'assign_customer':

			$user_id 	 = $data->user_id;
			$customer_id = $data->customer_id; 
			$assign_condition = $data->assign_condition;

			$result 	 = $obj_custm->customer_assign($user_id,$customer_id,$assign_condition);

			if($result){
				$response['response_code']=200;
				$response['response_message']="Success";
				$response['data']=$result;
			}
			else
			{
				$response['response_code']=400;
				$response['response_message']="Something Went Wrong";
			}

		break;

		case 'rate_customer':

			$user_id 	 = $data->user_id;
			$customer_id = $data->customer_id;
			$rating 	 = $data->rating;

			$result 	 = $obj_custm->rate_customer($user_id,$customer_id,$rating);

			if($result){
				$response['response_code']=200;
				$response['response_message']="Success";
				$response['data']=$result;
			}
			else
			{
				$response['response_code']=400;
				$response['response_message']="Something Went Wrong";
			}

		break;

		case 'cust_signup':

			  $json_array = array();
			  $json_array1 = array();
			  $json_array2 = array();
			  $json_array_final = array();
			 
			  $json_array = new \stdClass();
			  $json_array1 = new \stdClass();
			  $json_array2 = new \stdClass();

			  $date = date('Y-m-d H:i:s');
			  $token = $data->token;

			  $json_array->Active=1;
			  $json_array->Client_LegalName=$data->billing_entity;
			  $json_array->Client_Name=$data->contact_person;
			  $json_array->Client_Email=$data->company_email;
			  $json_array->Client_Address=$data->address;
			  $json_array->CityName=$data->city;
			  $json_array->StateName=$data->state;
			  $json_array->PinCode=$data->search_pincode;
			  $json_array->CityID=$data->city_id;
			  $json_array->StateID=$data->state_id;
			  $json_array->MobileNo=$data->mobile_no;
			  $json_array->ClientTypeID=3;
			  $json_array->Druglicence=$data->drug_licence;
			  $json_array->Client_Contact=$data->landline_no;
			  $json_array->Createdon=$date;    ////
			  $json_array->dl_bill="";
			  $json_array->tin_vat_cst=$data->gst_number;

			  $json_array1->UserName=$data->mobile_no;
			  $json_array1->password="password";
			  $json_array1->Login_Name=$data->contact_person;
			  $json_array1->Full_Name=$data->contact_person;
			  $json_array1->email=$data->company_email;
			  $json_array1->Mobile_No=$data->mobile_no;
			  $json_array1->Active=true;
			  $json_array1->Createdon=$date;   ////
			  $json_array1->ProfileImage='public/upload/dummyImages/dummy_profile_img.jpg';
			  $json_array1->role=$data->role;
			  $json_array1->isDummyPwd=false;

			  $json_array2->ReqBy_Name=$data->requested_by_name;
			  $json_array2->ReqBy_Division=$data->division;
			  $json_array2->ReqBy_Designation=$data->designation;
			  $json_array2->ReqBy_PhoneNo=$data->phone_number;
			  $json_array2->ReqBy_Email=$data->email;



			  $json_array_final[] =  $json_array;
			  $json_array_final[] =  $json_array1;
			  $json_array_final[] =  $json_array2;
			  $json_data   =  json_encode($json_array_final);

			  $curl = curl_init();

			curl_setopt_array($curl, array(
			  CURLOPT_URL => ORDERGINI_API."/api/clientmasters/signup",
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "POST",
			  CURLOPT_POSTFIELDS => $json_data,
			  CURLOPT_HTTPHEADER => array(
			    "cache-control: no-cache",
			    "content-type: application/json",
			    "postman-token: d89035b1-98ae-7f32-ed7a-d21127dbd7be"
			  ),
			));

			$response_cus_id = curl_exec($curl);

			// print_r($response_cus_id);

			$err = curl_error($curl);

			curl_close($curl);

			$response_cus_id=json_decode($response_cus_id);

			// print_r($response_cus_id); die();
			if(@$response_cus_id->error_msg){

				$response['response_code'] = 400;
				$response['response_message'] = $response_cus_id->error_msg;

			}elseif(@$response_cus_id->status == 'success'){

				$response['response_code']=200;
				$response['response_message']="Success";
				$response['data']=$response_cus_id->message;

			}else{

				$response['response_code'] = 400;
				$response['response_message'] = "Something Went Wrong";
			}

		break;
 		case 'get_invoice_detail':
 					$distributor_id = $obj_user->get_distributor_id($data->dist_id);

					$customer_id = $data->cust_id;


					$curl = curl_init();

					curl_setopt_array($curl, array(
					  CURLOPT_URL => ORDERGINI_API."/api/CRMServices/CRM_GetChemistoustandingdetails",
					  CURLOPT_RETURNTRANSFER => true,
					  CURLOPT_ENCODING => "",
					  CURLOPT_MAXREDIRS => 10,
					  CURLOPT_TIMEOUT => 30,
					  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					  CURLOPT_CUSTOMREQUEST => "POST",
					  CURLOPT_POSTFIELDS => "{\"StockistID\":\"".$distributor_id."\",\"ChemistId\":\"".$customer_id."\"}",
					  CURLOPT_HTTPHEADER => array(
					    "Authorization: Bearer ".$data->token,
					    "Content-Type: application/json",
					    "Postman-Token: ee2b2d47-87ad-48e8-a522-8c96fb6c31a1",
					    "cache-control: no-cache"
					  ),
					));

					$response_cus_id= curl_exec($curl);

					
					$err = curl_error($curl);

					curl_close($curl);
					$response = array();
					$response_cus_id=json_decode($response_cus_id);
					
					if($response_cus_id->status=="success"){
						 
						$response['response_code']=200;
						$response['response_message']="Success";
						$response['data']=$response_cus_id->outstandingdetails;
					}
					else{
						$response['response_code']=400;
						$response['response_message']="Something Went Wrong";
					}
					 

 		break;
 		case 'get_incoming_number':
 					$distributor_id = $obj_user->get_distributor_id($data->dist_id);
 					// $distributor_id =89;
					$phone = $data->phone;


					$curl = curl_init();

					curl_setopt_array($curl, array(
					  CURLOPT_URL => ORDERGINI_API."/api/CRMServices/CRM_GetChemistdetailsbyphonenumber",
					  CURLOPT_RETURNTRANSFER => true,
					  CURLOPT_ENCODING => "",
					  CURLOPT_MAXREDIRS => 10,
					  CURLOPT_TIMEOUT => 30,
					  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					  CURLOPT_CUSTOMREQUEST => "POST",
					  CURLOPT_POSTFIELDS => "{\"StockistID\":\"".$distributor_id."\",\"MobileNO\":\"".$phone."\"}",
					  CURLOPT_HTTPHEADER => array(
					    "Authorization: Bearer ".$data->token,
					    "Content-Type: application/json",
					    "Postman-Token: ee2b2d47-87ad-48e8-a522-8c96fb6c31a1",
					    "cache-control: no-cache"
					  ),
					));

					$response_cus_id= curl_exec($curl);

					
					$err = curl_error($curl);

					curl_close($curl);
					$response = array();
					$response_cus_id=json_decode($response_cus_id);
					
					if($response_cus_id->status=="success"){
						 
						$response['response_code']=200;
						$response['response_message']="Success";
						$response['data']=$response_cus_id->chemistdetails[0]->Chemist_Legal_Name;
					}
					else{
						$response['response_code']=400;
						$response['response_message']="Something Went Wrong";
					}
					 

 		break;
 		
 		case 'get_multiple_customer':
 
			$result 	 = $obj_custm->get_multiple_customer();

			if($result){
				$response['response_code']=200;
				$response['response_message']="Success";
				$response['data']=$result;
			}
			else
			{
				$response['response_code']=400;
				$response['response_message']="Something Went Wrong";
			}

		break;
		default:

			$response['response_message']= "Wrong Action ";
			$response['response_code'] = 400;
				# code...
		break;

}

echo json_encode($response);
?>