<?php
class Dashboard
{
	private $_db, $dt,$_user_obj;
      private static $_instance = null;
   public function __construct($user=null)
   {
      $this->_db = DB::getInstance();
     // $this->_user_obj = User::getInstance_of_user();
      date_default_timezone_set('Asia/Calcutta');
        $this->dt = date('Y-m-d H:i:s');
   }
    public static function getInstance_of_user()          //Used to instantiate an object
      {                         //Checks if instance already created else creates a new Instance                                  
      if(!isset(self::$_instance))
      {
        self::$_instance=new Dashboard();
      }
      return self::$_instance;
    }

	public function get_call_summary($user_id){

		$sql = "SELECT (DATE(NOW()) - INTERVAL `day` DAY) AS `DayDate`, COUNT(`id`) AS `calls` 
				FROM ( SELECT 0 AS `day` UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 )
				 AS `week` LEFT JOIN `call_logs` ON DATE(`created_date`) = (DATE(NOW()) - INTERVAL `day` DAY) AND user_id in ($user_id) GROUP BY `DayDate` ORDER BY `DayDate` ASC";
    $result = $this->_db->query($sql)->results();

    // print_r($result); die();

     if(empty($result)){
            return 0;
    }else{
          return $result;
    }
	}

  public function todays_task_summary($user_id,$status,$time_span){

    $condition1 = '';
    if($time_span == 1){

      $condition1 = 'DATE(created_date) = CURDATE()';
    }
    elseif($time_span == 2){

      $condition1 = 'created_date > DATE_SUB(NOW(), INTERVAL 1 WEEK)';
    }

    $condition2 = '';
    if($status == 0 || $status == 1){
    $condition2 = 'AND status = '.$status.'';
    }
    elseif($status == 2){
    $condition2 = 'AND status = 1 AND TIME(follow_up) != 0';
    }
    elseif($status == 3){
      $condition2 = '';
    }

    $sql = "SELECT COUNT(task_id) as count from tasks where assigned_user_id in ($user_id) AND $condition1 $condition2";
    $result = $this->_db->query($sql)->results();

    return $result;

  }

	public function last_5_task_summary($user_id){

    $sql = "SELECT cs.task_id, cs.created_date as call_time, cs.customer_id FROM tasks as ts join calls as cs on ts.task_id = cs.task_id WHERE ts.assigned_user_id in ($user_id)  GROUP BY(cs.task_id) ORDER BY cs.created_date DESC LIMIT 5";
    $result = $this->_db->query($sql)->results();

    // print_r($result); die();

    if(empty($result)){
            return 0;
    }else{
          return $result;
    }

  } 

  public function average_task($user_id){

    $sql = "SELECT (DATE(NOW()) - INTERVAL `day` DAY) AS `DayDate`, COUNT(`task_id`) AS `tasks` 
        FROM ( SELECT  0 AS `day` UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 )
         AS `week` LEFT JOIN `tasks` ON DATE(`created_date`) = (DATE(NOW()) - INTERVAL `day` DAY) AND `assigned_user_id` in ($user_id) AND status = 0 GROUP BY `DayDate` ORDER BY `DayDate` ASC";

    $result = $this->_db->query($sql)->results();

    return $result;

  }

  public function get_distributors(){

      $sql = "SELECT us.user_id,us.name,us.profile_picture FROM users as us join user_role as ur on us.user_id = ur.user_id where ur.role = 2";
      $result = $this->_db->query($sql)->results();

      return $result;
  }

  public function get_team_leads($user_id){

    $sql = "SELECT us.user_id,us.name,us.profile_picture from users as us join user_distributor_mapping as udm on us.user_id = udm.user_id_l2 WHERE user_id_distributor = $user_id AND us.status =1 ";
    $result = $this->_db->query($sql)->results();

    return $result;
  }

  public function get_agents($user_id,$user_role){

    if($user_role == 2){
      $cond = "udm.user_id_distributor =".$user_id;
    }else{
      $cond = "map.user_id_l2 =".$user_id;
    }

    $sql = "SELECT us.user_id,us.name,us.profile_picture, 4 as user_role from l1_l2_mapping as map JOIN user_distributor_mapping as udm on udm.user_id_l2 = map.user_id_l2 JOIN users as us on map.user_id_l1 = us.user_id WHERE $cond AND us.status = 1";
    $result = $this->_db->query($sql)->results();
    // print_r($result); die();
    
    return $result;
  }

  public function get_call_summary_team($user_id){

    $sql = "SELECT (DATE(NOW()) - INTERVAL `day` DAY) AS `DayDate`, COUNT(`id`) AS `calls` 
        FROM ( SELECT 0 AS `day` UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 )
         AS `week` LEFT JOIN `calls` ON DATE(`created_date`) = (DATE(NOW()) - INTERVAL `day` DAY) AND user_id in ($user_id) GROUP BY `DayDate` ORDER BY `DayDate` ASC";
    $result = $this->_db->query($sql,array($user_id))->results();

        return $result;
  }

  public function average_task_team($user_id){

    $sql = "SELECT (DATE(NOW()) - INTERVAL `day` DAY) AS `DayDate`, COUNT(`task_id`) AS `tasks` 
        FROM ( SELECT 0 AS `day` UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 )
         AS `week` LEFT JOIN `tasks` ON DATE(`created_date`) = (DATE(NOW()) - INTERVAL `day` DAY) AND `assigned_user_id` in ($user_id) GROUP BY `DayDate` ORDER BY `DayDate` ASC";

    $result = $this->_db->query($sql,array($user_id))->results();

    return $result;

  }

  public function last_5_task_summary_team($user_id){

    $sql = "SELECT (ts.task_id),ts.task, cs.created_date as time FROM tasks as ts join calls as cs on ts.task_id = cs.task_id WHERE ts.assigned_user_id in ($user_id) AND cs.created_date = (SELECT MAX(created_date) from calls where user_id in ($user_id)) ORDER BY ts.created_date, max(cs.created_date) LIMIT 5";

    $result = $this->_db->query($sql)->results();

    if(empty($result)){
          return 0;
    }else{
          return $result;
    }
  } 

  public function cases_resolved_team($user_id,$time_span){

    $condition1 = '';
    if($time_span == 1){

      $condition1 = 'AND DATE(created_date) = CURDATE()';
    }
    elseif($time_span == 2){

      $condition1 = 'AND created_date > DATE_SUB(NOW(), INTERVAL 1 WEEK)';
    }

    $sql = "SELECT COUNT(case_id) as count FROM `cases` WHERE status = 0 AND assigned_user_id in ($user_id) $condition1";
    $result = $this->_db->query($sql)->results();

    if(empty($result)){
            return 0;
    }else{
          return $result;
    }

  }

  public function cases_unresolved_team($user_id,$time_span){

    $condition1 = '';
    if($time_span == 1){

      $condition1 = 'AND DATE(created_date) = CURDATE()';
    }
    elseif($time_span == 2){

      $condition1 = 'AND created_date > DATE_SUB(NOW(), INTERVAL 1 WEEK)';
    }

    $sql = "SELECT COUNT(case_id) as count FROM `cases` WHERE status = 1 AND assigned_user_id in ($user_id) $condition1";
    $result = $this->_db->query($sql)->results();

    if(empty($result)){
            return 0;
    }else{
          return $result;
    }

  }  // close function case unresolved team

  public function get_og_id($user_id){

    $sql = "SELECT og_id FROM users WHERE user_id in ($user_id)";
    $result = $this->_db->query($sql)->results();

    // print_r($result); 

    $user_str='';

    foreach (@$result as $key => $value) {
        $user_str.=",".$value->og_id."";
    }

    // print_r($user_str); die();
    $user_str=ltrim($user_str,',');

    // print_r($user_str); die();

    if(empty($result)){
            return 0;
    }else{
          return $user_str;
    }

  }  // close function get og id 



}