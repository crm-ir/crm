<?php
$path_init = $_SERVER['DOCUMENT_ROOT']."/crm/resources/init.php";
include_once($path_init); 
header('Content-type: application/json');
$data = json_decode(file_get_contents('php://input')); 
$obj_task=new task_class();
$obj_call=new call();
$obj_user=new user();

$action =$data->action;
	
	switch ($action) {
	
		case 'display_users_team':

			$user_id = $data->user_id_team;
			$user_role = $data->user_role_team;

			$result = $obj_user->get_users_team($user_id,$user_role);
			//print_r($result); die();

			if($result){
			foreach ($result as $key => $value) {

				$user_id_task = $value->user_id;
				// print_r($user_id_task); die();

			if($user_role == 2 || $user_role == 3){
				$result[$key]->member_count = $obj_user->get_members_count($user_id_task,$value->role);
			}else{
				$result[$key]->member_count = 0;
			}
			$customers = $obj_user->get_customers_user($user_id_task);
			$tasks = $obj_user->get_tasks_user($user_id_task);
			$cases = $obj_user->get_cases_user($user_id_task);

			$result[$key]->customer= $customers;
			$result[$key]->tasks= $tasks;
			$result[$key]->cases= $cases;

			//print_r($user_details); die();
			}
			}

			if($result){
			$response['response_code']=200;
			$response['response_message']="Success";
			$response['data']=$result;
			}
			else{
			$response['response_code']=400;
			$response['response_message']="Failure";	
			}

			echo json_encode($response);

		break;
		case 'display_users_for_call_planner':

			$user_id = $data->user_id;
			$user_role = $data->role;

			$result = $obj_user->get_all_the_agents($user_id,$user_role);
			//print_r($result); die();

			if($result){
			$response['response_code']=200;
			$response['response_message']="Success";
			$response['data']=$result;
			}
			else{
			$response['response_code']=400;
			$response['response_message']="Failure";	
			}

			echo json_encode($response);

		break;

		case 'change_role_team': 
			
			$user_id   		= $data->user_id;
			$user_role 		= $data->user_role;
			$user_id_dist   = $data->user_id_dist;

			$result = $obj_user->change_role($user_id,$user_role,$user_id_dist);

			if($result){
			$response['response_code']=200;
			$response['response_message']="Success";
			$response['data']=$result;
			}
			else{
			$response['response_code']=400;
			$response['response_message']="Failure";	
			}

			echo json_encode($response);

		break;

		case 'help_and_support': 
			
			$user_id   = $data->user_id;
			$query_org = $data->query;
			$query 	   = nl2br($query_org);

			$result = $obj_user->help_and_support($user_id,$query);

			if($result){
			$response['response_code']=200;
			$response['response_message']="Success";
			$response['data']=$result;
			}
			else{
			$response['response_code']=400;
			$response['response_message']="Failure";	
			}

			echo json_encode($response);

		break;

		case 'display_users_info_team':

			$user_id   = $data->user_id;
			$user_role = $data->user_role;


			$result            = $obj_user->get_user_information($user_id);

			if($user_role == 2 || $user_role == 3){
				$result->member_count = $obj_user->get_members_count($user_id,$user_role);
			}else{
				$result->member_count = 0;
			}

			$customers 		   = $obj_user->get_customers_user($user_id);
			$tasks 			   = $obj_user->get_tasks_user($user_id);
			$cases 			   = $obj_user->get_cases_user($user_id);

			$result->customers = $customers;
			$result->tasks 	   = $tasks;
			$result->cases 	   = $cases;

			//print_r($user_details); die();
			// }
			// }

			if($result){
			$response['response_code']=200;
			$response['response_message']="Success";
			$response['data']=$result;
			}
			else{
			$response['response_code']=400;
			$response['response_message']="Failure";	
			}

			echo json_encode($response);

		break;

		case 'reassign_multiple_agent':

			$user_id_agents    = $data->user_id_agents;
			$user_id_team_lead = $data->user_id_team_lead;


			$result = $obj_user->reassign_multiple_agent($user_id_team_lead,$user_id_agents);

			if($result){
			$response['response_code']=200;
			$response['response_message']="Success";
			$response['data']=$result;
			}
			else{
			$response['response_code']=400;
			$response['response_message']="Failure";	
			}

			echo json_encode($response);

		break;

		case 'edit_ed_id':

			$user_id = $data->user_id;
			$ed_id = $data->ed_id;

			$result = $obj_user->change_ed_id($user_id,$ed_id);

			if($result){
			$response['response_code']=200;
			$response['response_message']="Success";
			$response['data']=$result;
			}
			else{
			$response['response_code']=400;
			$response['response_message']="Failure";	
			}

			echo json_encode($response);

		break;
		case 'get_user_priority':
				$dist_id = $data->dist_id;
				 

				$result = $obj_user->get_priority_users($dist_id);

				if($result){
				$response['response_code']=200;
				$response['response_message']="Success";
				$response['data']=$result;
				}
				else{
				$response['response_code']=400;
				$response['response_message']="Failure";	
				}

				echo json_encode($response);
		break;
		case 'set_priority':
				$user_id = $data->user_id;
				$new_value = $data->new_value;
				 

				$result = $obj_user->set_priority_users($user_id,$new_value);

				if($result){
				$response['response_code']=200;
				$response['response_message']="Success";
				$response['data']=$result;
				}
				else{
				$response['response_code']=400;
				$response['response_message']="Failure";	
				}

				echo json_encode($response);
		break;

		case 'set_token_firebase':
				$user_id = $data->user_id;
				$token = $data->token;
				 

				$result = $obj_user->set_token_firebase($user_id,$token);

				if($result){
				$response['response_code']=200;
				$response['response_message']="Success";
				
				}
				else{
				$response['response_code']=400;
				$response['response_message']="Failure";	
				}

				echo json_encode($response);
		break;

		

	} // clsoe switch

?>